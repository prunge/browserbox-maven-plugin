# browserbox-maven-plugin

The Browser Box Maven Plugin launches different types and versions of 
browsers in containers, allowing manual and automated tests to test
against different browser versions more easily.  Browers are installed
into containers (either Docker, VirtualBox VMs or using Android Emulator) 
so local installations are not affected.

The following browsers and versions are supported:

| Browser                     | Versions           | Requirements
|-----------------------------|--------------------|----
| Mozilla Firefox             | 3.0 to latest      | Docker
| Google Chrome               | 43.0 to latest     | Docker
| Microsoft Internet Explorer | 6 to 11            | VirtualBox, Vagrant
| Microsoft Edge              | 13.10586 to latest | VirtualBox, Vagrant
| Google Chrome on Android    | 40.0 to latest     | Android SDK with emulator installed 

One common use case for this plugin is to spin up a browser in a container,
then run Selenium integration tests of your project, then tear the browser
down again.

## Setup and Requirements

#### Plugin setup

This plugin exists in Maven Central so Maven will download it when first 
invoked.  No separate download required.

#### Docker

For browsers that run in Docker containers, you will need to install
Docker for your platform.

#### Vagrant container based browsers

For Microsoft browsers you will need to install VirtualBox and 
Vagrant for your platform.  The browser VMs are downloaded by the plugin
as needed.  The Vagrant executable needs to be on the `PATH`.

#### Android browsers

For these you will need to have installed the Android SDK with the 
emulator.  Either the `ANDROID_HOME` environment variable or 
the `android.sdk.path` property (which may be set up as configuration
in Maven's `settings.xml`) need to be configured to point to the
installation location of the Android SDK.

## Usage

#### Command line

------

Start the latest version of Firefox in a container and show a UI:

    mvn au.net.causal.maven.plugins:browserbox-maven-plugin:show -Dbrowser.type=firefox
    
If running for the first time, dependencies will be downloaded - in this 
case the Firefox Selenium container will be downloaded.

When terminated with control+c or the viewer is closed, the browser is 
stopped and then its container is deleted.
To not delete the container on stop, use `-Dbrowserbox.deleteOnStop=false`.
   
------

Launch a specific version of Edge but don't show a UI:

    mvn au.net.causal.maven.plugins:browserbox-maven-plugin:startwait -Dbrowser.type=edge -Dbrowser.version=17.17134

This can be used to test with older versions of browsers for compatibility.

The `startwait` goal runs the browser but does not show a UI, which can
be useful if you need to run Selenium tests manually but don't need to
see the browser itself.  In this case, given Edge will be started in a
VirtualBox VM, it is still possible afterwards to launch VirtualBox
and display the VM UI manually.

------

Display a list of all available versions for Chrome:

    mvn au.net.causal.maven.plugins:browserbox-maven-plugin:versions -Dbrowser.type=chrome
    

#### As part of a Maven project

See the 
integration tests 
for examples of usage within Maven.

Generally use the following configuration in your project:

    <plugin>
        <groupId>au.net.causal.maven.plugins</groupId>
        <artifactId>browserbox-maven-plugin</artifactId>
        <version>1.0</version>
        <configuration>
            <box>
                <browserType>firefox</browserType>
                <browserVersion>57.0</browserVersion>
                <tunnelPorts>
                    <tunnelPort>8080</tunnelPort>
                </tunnelPorts>
            </box>
        </configuration>
        <executions>
            <execution>
                <id>browserbox-start</id>
                <goals>
                    <goal>start</goal>
                </goals>
            </execution>
            <execution>
                <id>browserbox-stop</id>
                <goals>
                    <goal>stop</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    
    
This will start the browser in a container before integration tests and 
then stop and delete it afterwards.  Port 8080 of the host will made
available in the browser through a tunnel.  

Use this plugin in combination with the failsafe plugin for running 
your tests.

#### Creating videos from your tests

Sometimes it can be difficult to debug when something goes wrong in
browser tests.  The plugin can be configured to record videos for every 
test, and optionally only keep videos for tests that fail.

To record video: 

- enable the tool server by setting the  `toolServerUsed` configuration 
option to true.
- add the following dependency to your project at `test` scope:
```
<dependency>
    <groupId>au.net.causal.maven.plugins</groupId>
    <artifactId>browserbox-test-tools</artifactId>
    <version>1.0</version>
    <scope>test</scope>
</dependency>
```  
- configure the failsafe plugin with one of the following listeners:
```
<properties>
    <listener>au.net.causal.maven.plugins.browserbox.testtools.junit4.EveryTestVideoRecorder</listener>
</properties>
```
or
```
<properties>
    <listener>au.net.causal.maven.plugins.browserbox.testtools.junit4.FailingTestVideoRecorder</listener>
</properties>
```
- run your tests like normal using failsafe

Videos are saved to the  `target/testvideo` directory.  This can be 
changed with the `videoDirectory` configuration option of the plugin.
