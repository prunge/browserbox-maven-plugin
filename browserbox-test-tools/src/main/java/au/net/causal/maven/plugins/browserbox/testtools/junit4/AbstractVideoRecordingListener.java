package au.net.causal.maven.plugins.browserbox.testtools.junit4;

import au.net.causal.maven.plugins.browserbox.testtools.VideoRecorder;
import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

public abstract class AbstractVideoRecordingListener extends RunListener
{
    private final VideoRecorder videoRecorder = new VideoRecorder();

    protected String nameForTestDescription(Description testDescription)
    {
        return testDescription.getClassName() + "-" + testDescription.getMethodName();
    }

    protected void startVideoRecording(Description testDescription)
    {
        videoRecorder.startVideoRecording(nameForTestDescription(testDescription));
    }
    
    protected void stopAndSaveVideoRecording(Description testDescription)
    {
        videoRecorder.stopAndSaveVideoRecording(nameForTestDescription(testDescription));
    }

    protected void stopAndCancelVideoRecording(Description testDescription)
    {
        videoRecorder.stopAndCancelVideoRecording(nameForTestDescription(testDescription));
    }
}
