package au.net.causal.maven.plugins.browserbox.testtools.jupiter;

import au.net.causal.maven.plugins.browserbox.testtools.VideoRecorder;
import com.google.auto.service.AutoService;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Locale;

@AutoService(Extension.class)
public class JupiterVideoRecorderExtension
implements BeforeEachCallback, AfterEachCallback
{
    private final VideoRecorder videoRecorder = new VideoRecorder();

    @Override
    public void beforeEach(ExtensionContext context)
    throws Exception
    {
        if (getMode(context) == SaveMode.NEVER)
            return;

        videoRecorder.startVideoRecording(nameForCurrentTest(context));
    }

    @Override
    public void afterEach(ExtensionContext context)
    throws Exception
    {
        if (getMode(context) == SaveMode.NEVER)
            return;

        boolean testFailed = context.getExecutionException().isPresent();
        if (testFailed || getMode(context) == SaveMode.ALWAYS)
            videoRecorder.stopAndSaveVideoRecording(nameForCurrentTest(context));
        else
            videoRecorder.stopAndCancelVideoRecording(nameForCurrentTest(context));
    }

    protected String nameForCurrentTest(ExtensionContext context)
    {
        return context.getRequiredTestClass().getName() + "-" + context.getRequiredTestMethod().getName();
    }

    protected SaveMode getMode(ExtensionContext context)
    {
        return context.getConfigurationParameter("au.net.causal.maven.plugins.browserbox.video.savemode")
                      .map(value -> value.trim().toUpperCase(Locale.ENGLISH))
                      .map(SaveMode::valueOf)
                      .orElse(SaveMode.NEVER);
    }

    /**
     * Control when to save video.
     */
    public static enum SaveMode
    {
        /**
         * Never record or save any video.
         */
        NEVER,

        /**
         * Save video only for tests that fail or have errors.
         */
        FAILURE,

        /**
         * Save video for every test regardless of the result.
         */
        ALWAYS
    }
}
