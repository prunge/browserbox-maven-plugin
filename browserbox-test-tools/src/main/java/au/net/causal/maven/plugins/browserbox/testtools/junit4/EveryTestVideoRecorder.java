package au.net.causal.maven.plugins.browserbox.testtools.junit4;

import org.junit.runner.Description;

public class EveryTestVideoRecorder extends AbstractVideoRecordingListener
{
    @Override
    public void testStarted(Description description) throws Exception
    {
        startVideoRecording(description);
        super.testStarted(description);
    }

    @Override
    public void testFinished(Description description) throws Exception
    {
        super.testFinished(description);
        stopAndSaveVideoRecording(description);
    }
}
