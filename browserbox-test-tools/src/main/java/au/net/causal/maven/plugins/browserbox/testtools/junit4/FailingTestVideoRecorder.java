package au.net.causal.maven.plugins.browserbox.testtools.junit4;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;

public class FailingTestVideoRecorder extends AbstractVideoRecordingListener
{
    @Override
    public void testStarted(Description description) throws Exception
    {
        startVideoRecording(description);
        super.testStarted(description);
    }

    @Override
    public void testFinished(Description description) throws Exception
    {
        super.testFinished(description);
        stopAndCancelVideoRecording(description);
    }

    @Override
    public void testFailure(Failure failure) throws Exception
    {
        super.testFailure(failure);
        stopAndSaveVideoRecording(failure.getDescription());
    }
}
