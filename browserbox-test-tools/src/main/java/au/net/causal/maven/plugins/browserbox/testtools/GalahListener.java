package au.net.causal.maven.plugins.browserbox.testtools;

import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

public class GalahListener extends RunListener
{
    @Override
    public void testRunStarted(Description description) throws Exception 
    {
        System.out.println("Before running #" + description.getClassName() + " " + description.getChildren() + " " + description.getClass().getName());
        Thread.dumpStack();
        super.testRunStarted(description);
    }

    @Override
    public void testStarted(Description description) throws Exception 
    {
        System.out.println("Galah test start: " + description.getClassName() + "." + description.getMethodName());
        super.testStarted(description);
    }

    @Override
    public void testFinished(Description description) throws Exception 
    {
        super.testFinished(description);
        System.out.println("Galah test finish: " + description.getClassName() + "." + description.getMethodName());
    }
}
