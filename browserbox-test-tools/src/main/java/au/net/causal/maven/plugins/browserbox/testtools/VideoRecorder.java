package au.net.causal.maven.plugins.browserbox.testtools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.time.Duration;

public class VideoRecorder
{
    private final URL browserBoxCommandEndpoint;
    private final Duration endRecordingExtraTime = Duration.ofMillis(200L); //TODO make this configurable

    private volatile boolean recordingVideo;

    public VideoRecorder()
    {
        try
        {
            String endpointStr = System.getProperty("browserbox.test.endpoint", "http://localhost:4454/browserbox/");
            browserBoxCommandEndpoint = new URL(endpointStr);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    public void startVideoRecording(String name)
    {
        invokeEndpoint("start", "name", name);
        recordingVideo = true;
    }
    
    private void invokeEndpoint(String command, String parameterName, String rawParameterValue)
    {
        try 
        {
            String parameterValue = URLEncoder.encode(rawParameterValue, "UTF-8");
            URL endpoint = new URL(browserBoxCommandEndpoint, command + "?" + parameterName + "=" + parameterValue);
            System.out.println("Invoke endpoint: " + endpoint.toExternalForm());
            try (InputStream is = endpoint.openStream()) 
            {
                String result = readStreamContent(is);
                if (!"success".equals(result))
                    throw new RuntimeException("Error from browserbox: " + result);
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private String readStreamContent(InputStream is)
    throws IOException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[4096];
        int n;
        do 
        {
            n = is.read(buf);
            if (n > 0)
                out.write(buf, 0, n);
        }
        while (n >= 0);
        
        return out.toString("UTF-8");
    }
    
    public void stopAndSaveVideoRecording(String name)
    {
        if (!recordingVideo)
            return;

        try
        {
            try
            {
                Thread.sleep(endRecordingExtraTime.toMillis());
            }
            catch (InterruptedException e)
            {
            }
            invokeEndpoint("save", "name", name);
        }
        finally
        {
            recordingVideo = false;
        }
    }
    
    public void stopAndCancelVideoRecording(String name)
    {
        if (!recordingVideo)
            return;

        try
        {
            invokeEndpoint("cancel", "name", name);
        }
        finally
        {
            recordingVideo = false;
        }
    }
}
