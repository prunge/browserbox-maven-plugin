# Change Log

### 1.1

Not yet released

- Add `browserbox.seleniumWaiting` option to use Selenium to verify browser box has
started to work around situation where it hasn't fully started up yet
- Do not select preview versions of Android OS when picking an Android image to use
- Allow Android OS version to be selected by a property instead of always picking
the highest available one
- Allow additional browser-box-specific properties to be configured via 
system properties / command line, e.g. `-Dbrowser.properties=androidOsApiLevel=23`
- Add Internet Explorer 7 support for any poor soul that still needs to test their
stuff with that version (or for anyone that feels nostalgic) - use 
`-Dbrowser.type=ie -Dbrowser.version=7` to use it (or configure with 
Maven configuration)
- Added SSL certificate support for all browser types.  PKCS certificates
can be configured for any browser through configuration - can be loaded
as separate files in the filesystem or from a keystore.  Use this to 
ensure the browser trusts your server when running your tests.
- Fix video recording issues on old Firefox/Chrome docker images due to
avconv being used instead of FFMPEG.  Video recording should work for 
much older versions of these browsers now.
- Fix Edge 13 box not working due to errors installing old Edge webdriver.
- Upgrade Docker Maven plugin to 0.31.0.
- Detect when and old Chrome version cannot be installed into Android
due to the system version being higher - now will show a helpful error message
telling you to use a lower Android OS version to test that version of Chrome 

### 1.0

2019-08-18

- Initial version
