package au.net.causal.browserbox.selenium.edge;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeDriver extends org.openqa.selenium.edge.EdgeDriver
{
    public EdgeDriver()
    {
        this(FixedEdgeDriverService.createDefaultService(), new EdgeOptions());
        System.out.println("Making an EdgeDriver!");
    }

    public EdgeDriver(EdgeDriverService service)
    {
        this(service, new EdgeOptions());
        System.out.println("Making an EdgeDriver!");
    }

    public EdgeDriver(Capabilities capabilities)
    {
        this(FixedEdgeDriverService.createDefaultService(), capabilities);
        System.out.println("Making an EdgeDriver!");
    }

    public EdgeDriver(EdgeOptions options)
    {
        this(FixedEdgeDriverService.createDefaultService(), options);
        System.out.println("Making an EdgeDriver!");
    }

    public EdgeDriver(EdgeDriverService service, EdgeOptions options)
    {
        super(service, options);
        System.out.println("Making an EdgeDriver!");
    }

    @Deprecated
    public EdgeDriver(EdgeDriverService service, Capabilities capabilities)
    {
        super(service, capabilities);
        System.out.println("Making an EdgeDriver!");
    }
}
