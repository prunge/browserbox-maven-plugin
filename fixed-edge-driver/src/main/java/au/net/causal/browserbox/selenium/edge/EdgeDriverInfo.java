package au.net.causal.browserbox.selenium.edge;

import com.google.auto.service.AutoService;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverInfo;

import java.util.Optional;

@AutoService(WebDriverInfo.class)
public class EdgeDriverInfo extends org.openqa.selenium.edge.EdgeDriverInfo
{
    @Override
    public Optional<WebDriver> createDriver(Capabilities capabilities)
    throws SessionNotCreatedException
    {
        System.out.println("Making an driver!");

        if (!isAvailable() || !isSupporting(capabilities))
            return Optional.empty();

        return Optional.of(new EdgeDriver(capabilities));
    }
}
