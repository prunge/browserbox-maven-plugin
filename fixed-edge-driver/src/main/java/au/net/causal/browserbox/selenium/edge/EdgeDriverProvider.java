package au.net.causal.browserbox.selenium.edge;

import com.google.auto.service.AutoService;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.server.DefaultDriverProvider;
import org.openqa.selenium.remote.server.DriverProvider;

@AutoService(DriverProvider.class)
public class EdgeDriverProvider extends DefaultDriverProvider
{
    public EdgeDriverProvider()
    {
        super(new EdgeOptions(), EdgeDriver.class);
    }
}
