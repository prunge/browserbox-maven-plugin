package au.net.causal.browserbox.selenium.edge;

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.remote.service.DriverService;

public class FixedEdgeDriverService
{
    public static org.openqa.selenium.edge.EdgeDriverService createDefaultService()
    {
        return new Builder().build();
    }

    @AutoService(DriverService.Builder.class)
    public static class Builder extends org.openqa.selenium.edge.EdgeDriverService.Builder
    {
        @Override
        protected ImmutableList<String> createArgs()
        {
            System.out.println("Using JWP mode!");
            return ImmutableList.<String>builder()
                    .addAll(super.createArgs())
                    .add("--jwp")
                    .build();
        }
    }
}
