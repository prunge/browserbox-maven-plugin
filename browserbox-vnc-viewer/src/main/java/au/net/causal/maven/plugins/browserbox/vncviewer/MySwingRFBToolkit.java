package au.net.causal.maven.plugins.browserbox.vncviewer;

import com.sshtools.rfb.swing.RFBGraphics2D;
import com.sshtools.rfb.swing.SwingRFBToolkit;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.io.IOException;

/**
 * Customized SwingRFBToolkit implementation that fixes resource loading in loadImage() from superclass with
 * Java 9+.
 */
public class MySwingRFBToolkit extends SwingRFBToolkit
{
  @Override
  public RFBImage loadImage(String string)
  {
      try
      {
          //Was fixed because this wouldn't work with JDK 9+ due to modules and resource loading differences
          String resourceNameWithoutSlashPrefix = string.substring(1);
          return new RFBBufferedImage(ImageIO.read(RFBBufferedImage.class.getClassLoader().getResource(resourceNameWithoutSlashPrefix)));
      }
      catch (IOException ioe)
      {
          throw new IllegalArgumentException("Invalid image resource.", ioe);
      }
  }

  //Copy + paste from superclass because they decided to make this package protected, sigh
  static class RFBBufferedImage implements RFBImage {
    BufferedImage backing;
    RFBGraphicsContext ctx;

    RFBBufferedImage(BufferedImage backing) {
      this.backing = backing;
      ctx = new RFBGraphics2D((Graphics2D) backing.getGraphics());
    }

    @Override
    public int getRGB(int x, int y) {
      return backing.getRGB(x, y);
    }

    @Override
    public void setRGB(int x, int y, int rgb) {
      backing.setRGB(x, y, rgb);
    }

    @Override
    public RFBGraphicsContext getGraphicsContext() {
      return ctx;
    }

    @Override
    public int getWidth() {
      return backing.getWidth();
    }

    @Override
    public int getHeight() {
      return backing.getHeight();
    }

    @Override
    public Object getData() {
      switch (backing.getRaster().getDataBuffer().getDataType()) {
        case DataBuffer.TYPE_INT:
          return ((DataBufferInt) backing.getRaster().getDataBuffer()).getData();
        case DataBuffer.TYPE_SHORT:
          return ((DataBufferUShort) backing.getRaster().getDataBuffer()).getData();
        case DataBuffer.TYPE_BYTE:
          return ((DataBufferByte) backing.getRaster().getDataBuffer()).getData();
        default:
          throw new IllegalStateException("Unknown data type");
      }

    }

    @Override
    public Type getType() {
      switch (backing.getType()) {
        case BufferedImage.TYPE_INT_ARGB:
          return Type.ARGB;
        default:
          return Type.UNKNOWN;
      }
    }

  }
}
