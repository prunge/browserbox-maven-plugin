package au.net.causal.maven.plugins.browserbox.vncviewer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

public class FrameTest
{
    public static void main(String... args)
    {
        JFrame f = new JFrame("Yes");
        f.setLayout(new AspectRatioLayout());
        JLabel label = new JLabel("Good Morning", SwingConstants.CENTER);
        label.setFont(new Font("Dialog", Font.PLAIN, 100));
        label.setBackground(Color.RED);
        JPanel pan = new JPanel(new BorderLayout());
        pan.setBackground(Color.RED);
        pan.add(label, BorderLayout.CENTER);
        f.add(pan);
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

}
