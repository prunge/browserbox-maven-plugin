package au.net.causal.maven.plugins.browserbox.vncviewer;

import com.sshtools.rfb.swing.SwingRFBDisplay;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * Subclass to perform interpolation for drawing graphics to make scaling look better.
 */
public class MySwingRFBDisplay extends SwingRFBDisplay
{
    @Override
    public void paintComponent(Graphics gg)
    {
        Graphics2D g = (Graphics2D)gg;
        //g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC); //Bicubic very slow!
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        super.paintComponent(g);
    }
}
