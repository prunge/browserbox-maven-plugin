package au.net.causal.maven.plugins.browserbox.vncviewer;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

public class AspectRatioLayout implements LayoutManager
{
    private Component componentFromContainer(Container parent)
    {
        if (parent.getComponentCount() < 1)
            throw new IllegalStateException("AspectRatioLayout requires a single child component in a container");

        return parent.getComponent(0);
    }

    @Override
	  public void layoutContainer(Container parent)
    {
		    Component component = componentFromContainer(parent);

		    Insets insets = parent.getInsets();
		    Dimension availableSpace = new Dimension(parent.getWidth() - insets.left - insets.right,
		                                             parent.getHeight() - insets.top - insets.bottom);
		    Dimension preferredSize = component.getPreferredSize();
		    Rectangle targetSize = scaleToAvailableSpace(preferredSize, availableSpace);

		    component.setBounds(targetSize);
	  }

	  private Rectangle2D scaleToAvailableSpace2D(Dimension componentSize, Dimension availableSpace)
    {
		    double widthScale = availableSpace.getWidth() / componentSize.getWidth();
		    double heightScale = availableSpace.getHeight() / componentSize.getHeight();
        double scale = Math.min(widthScale, heightScale);
        double targetWidth = scale * componentSize.getWidth();
        double targetHeight = scale * componentSize.getHeight();
        double remainingSpaceX = availableSpace.getWidth() - targetWidth;
        double remainingSpaceY = availableSpace.getHeight() - targetHeight;

        //Center by distributing space evenly top + bottom, left + right
        double xOffset = remainingSpaceX / 2.0;
        double yOffset = remainingSpaceY / 2.0;

        return new Rectangle2D.Double(xOffset, yOffset, targetWidth, targetHeight);
    }

    private Rectangle scaleToAvailableSpace(Dimension componentSize, Dimension availableSpace)
    {
        return scaleToAvailableSpace2D(componentSize, availableSpace).getBounds();
    }

    @Override
    public Dimension preferredLayoutSize(Container parent)
    {
        return componentFromContainer(parent).getPreferredSize();
    }

    @Override
    public Dimension minimumLayoutSize(Container parent)
    {
        return preferredLayoutSize(parent);
    }

    @Override
    public void addLayoutComponent(String name, Component comp)
    {
    }

    @Override
    public void removeLayoutComponent(Component parent)
    {
    }
}
