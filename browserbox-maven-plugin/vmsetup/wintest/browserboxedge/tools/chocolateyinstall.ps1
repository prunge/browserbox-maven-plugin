﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$tmpDir = "$toolsDir\temp"
$edgeExtractDir = "$tmpDir\edgedriverextract"
 
$packageArgs = @{
  packageName  = 'selenium-edge-driver'
  url          = 'https://download.microsoft.com/download/8/D/0/8D0D08CF-790D-4586-B726-C6469A9ED49C/MicrosoftWebDriver.msi'
  checksum     = 'B6E8AF5CBC490F7C24890361DE37D06E'
  checksumType = 'md5'
  fileFullPath = "$tmpDir/MicrosoftWebDriver.msi"
}
 
Get-ChocolateyWebFile @packageArgs
 
$toolsLocation = Get-ToolsLocation
$seleniumDir = "$toolsLocation\selenium"
$driverPath = "$seleniumDir\MicrosoftWebDriver.exe"
 
If (!(Test-Path $seleniumDir)) {
  New-Item $seleniumDir -ItemType directory
}
If (!(Test-Path $edgeExtractDir)) {
  New-Item $edgeExtractDir -ItemType directory
}

$msiexec = "C:\Windows\System32\msiexec.exe"
$msiargs = "/a $tmpDir\MicrosoftWebDriver.msi /qb /quiet /norestart TARGETDIR=$edgeExtractDir"
Start-Process $msiexec -Wait -ArgumentList $msiargs 

Move-Item "$edgeExtractDir\Microsoft Web Driver\MicrosoftWebDriver.exe" $driverPath -Force
Write-Host -ForegroundColor Green Moved driver to $seleniumDir
Remove-Item $tmpDir -Recurse -Force
 
$menuPrograms = [environment]::GetFolderPath([environment+specialfolder]::Programs)
$shortcutArgs = @{
  shortcutFilePath = "$menuPrograms\Selenium\Selenium Edge Driver.lnk"
  targetPath       = $driverPath
  iconLocation     = "$toolsDir\icon.ico"
}
 
Install-ChocolateyShortcut @shortcutArgs