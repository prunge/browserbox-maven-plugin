- Mount guest additions into VM:

VBoxManage storageattach WinIEPrototype --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium additions

- Install certs

e:
cd cert
VBoxCertUtil.exe add-trusted-publisher vbox-sha1.cer
VBoxCertUtil.exe add-trusted-publisher vbox-sha256.cer
VBoxCertUtil.exe add-trusted-publisher vbox-sha256-r3.cer

- Install guest additions

e:
cd \
VBoxWindowsAdditions.exe /S /I

Wait until 
c:\Program Files\Oracle\VirtualBox Guest Additions\install.log
contains text 
Oracle VM VirtualBox Guest Additions successfully installed

find /c "Oracle VM VirtualBox Guest Additions successfully installed" "c:\Program Files\Oracle\VirtualBox Guest Additions\install.log"

Expect:
---------- C:\PROGRAM FILES\ORACLE\VIRTUALBOX GUEST ADDITIONS\INSTALL.LOG: 1

- Reboot

shutdown /r /t 1

