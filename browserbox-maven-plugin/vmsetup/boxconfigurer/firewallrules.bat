netsh advfirewall firewall add rule name="Java" program="%JAVA_HOME%\bin\java.exe" protocol=tcp dir=in enable=yes action=allow profile=any
netsh advfirewall firewall add rule name="Selenium IE" program="c:\tools\Selenium\IEDriverServer.exe" protocol=tcp dir=in enable=yes action=block profile=public,private
