@echo off

e:
cd cert
VBoxCertUtil.exe add-trusted-publisher vbox-sha1.cer
VBoxCertUtil.exe add-trusted-publisher vbox-sha256.cer
VBoxCertUtil.exe add-trusted-publisher vbox-sha256-r3.cer

e:
cd \
VBoxWindowsAdditions.exe /S /I

:repeat
find /c "Oracle VM VirtualBox Guest Additions successfully installed" "c:\Program Files\Oracle\VirtualBox Guest Additions\install.log" > NUL || goto :repeat

echo VirtualBox guest additions installed
