package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.vagrant.VagrantTempCleaner;
import au.net.causal.maven.plugins.browserbox.virtualbox.LocalVirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager.StorageAttachOptions;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.codehaus.plexus.util.cli.Commandline;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Map;

@Disabled("test is run manually only")
class TestVBoxManage
{
    @Test
    void test()
    throws Exception
    {
        Log log = new SystemStreamLog();

        Commandline base = new Commandline("VBoxManage");
        VirtualBoxManager vb = new LocalVirtualBoxManager(base, log);
        
        VirtualBoxManager.GuestControlRunOptions options = new VirtualBoxManager.GuestControlRunOptions("WinIEPrototype", "ipconfig.exe");
        //options.setProgramArgs("/?");
        options.setUserName("Administrator");
        String result = vb.guestControlRun(options);
        
        System.out.println(result);
    }
    
    @Test
    void testVmInfo()
    throws Exception
    {
        Log log = new SystemStreamLog();

        Commandline base = new Commandline("VBoxManage");
        VirtualBoxManager vb = new LocalVirtualBoxManager(base, log);

        VirtualBoxManager.ShowVmInfoOptions options = new VirtualBoxManager.ShowVmInfoOptions("WinIEPrototype");
        Map<String, String> result = vb.showVmInfo(options);

        result.forEach((k, v) -> System.out.println(k + "=" + v));
    }
    
    @Test
    void testStorageAttach()
    throws Exception
    {
        Log log = new SystemStreamLog();

        Commandline base = new Commandline("VBoxManage");
        VirtualBoxManager vb = new LocalVirtualBoxManager(base, log);

        VirtualBoxManager.StorageAttachOptions options = new StorageAttachOptions("WinIEPrototype", "IDE Controller", 0, 1, "dvddrive", "additions");
        vb.storageAttach(options);
    }
    
    @Test
    void testTempCleaner()
    throws Exception
    {
        VagrantTempCleaner cleaner = new VagrantTempCleaner(new SystemStreamLog());
        cleaner.postPackage("WinIEPrototype");
    }
}
