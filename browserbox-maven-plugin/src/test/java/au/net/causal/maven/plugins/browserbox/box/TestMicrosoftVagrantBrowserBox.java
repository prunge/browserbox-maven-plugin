package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ImageUpdateMode;
import au.net.causal.maven.plugins.browserbox.android.Platform;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.config.RegistryAuthConfiguration;
import io.fabric8.maven.docker.log.LogOutputSpecFactory;
import io.fabric8.maven.docker.service.ImagePullManager;
import io.fabric8.maven.docker.util.AuthConfigFactory;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.DefaultMavenExecutionResult;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.filtering.DefaultMavenReaderFilter;
import org.apache.maven.shared.filtering.DefaultMavenResourcesFiltering;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.codehaus.plexus.archiver.manager.DefaultArchiverManager;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.internal.impl.DefaultRepositorySystem;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import static org.assertj.core.api.Assertions.*;

class TestMicrosoftVagrantBrowserBox
{
    private static MicrosoftVagrantBrowserBox box()
    throws BrowserBoxException
    {
        BoxConfiguration boxConfiguration = new BoxConfiguration();
        boxConfiguration.setBrowserType("ie");
        boxConfiguration.setBrowserVersion("11");
        MavenProject project = new MavenProject();
        Log log = new SystemStreamLog();
        Path dir = Paths.get(".");
        DefaultArtifact artifact = new DefaultArtifact("", "", "", "");
        MavenSession session = new MavenSession(null, new DefaultMavenExecutionRequest(), new DefaultMavenExecutionResult(), project);
        BoxContext context = new BoxContext(() -> null, new AuthConfigFactory(null), dir, dir, ImageUpdateMode.ALWAYS, log,
                                            new LogOutputSpecFactory(false, false, ""), session,
                                            new DefaultMavenResourcesFiltering(), new DefaultMavenReaderFilter(), new DefaultRepositorySystem(),
                                            new DefaultRepositorySystemSession(), Collections.emptyList(), new DefaultArchiverManager(),
                                            new ImagePullManager(null, null, null), new RegistryAuthConfiguration(), false,
                                            new DefaultInvoker(), dir, Platform.LINUX_64, "", artifact, artifact, null);
        return new MicrosoftVagrantBrowserBox(boxConfiguration, null, context, null, null, null, null, null, null);
    }

    @Nested
    class CleanUpVirtualBox
    {
        @Test
        void vmDirCleanedUpForEmptyLogsDirectory(@TempDir Path tempDir)
        throws BrowserBoxException, IOException
        {
            Path vmBaseDir = tempDir.resolve("vmbase");
            Path logDir = vmBaseDir.resolve("logs");
            Files.createDirectories(logDir);

            box().cleanUpVirtualboxVmFromLogDirectory(logDir);

            assertThat(vmBaseDir).doesNotExist();
            assertThat(tempDir).isEmptyDirectory();
        }

        @Test
        void vmDirCleanedUpForLogsDirectoryWithOnlyLogs(@TempDir Path tempDir)
        throws BrowserBoxException, IOException
        {
            Path vmBaseDir = tempDir.resolve("vmbase");
            Path logDir = vmBaseDir.resolve("logs");
            Files.createDirectories(logDir);

            Files.write(logDir.resolve("file1.log"), ImmutableList.of("This is a log file"));
            Files.write(logDir.resolve("file2.log"), ImmutableList.of("This is another log file"));

            box().cleanUpVirtualboxVmFromLogDirectory(logDir);

            assertThat(vmBaseDir).doesNotExist();
            assertThat(tempDir).isEmptyDirectory();
        }

        @Test
        void vmDirNotCleanedUpWhenNonLogFilesExist(@TempDir Path tempDir)
        throws BrowserBoxException, IOException
        {
            Path vmBaseDir = tempDir.resolve("vmbase");
            Path logDir = vmBaseDir.resolve("logs");
            Files.createDirectories(logDir);

            Files.write(logDir.resolve("file1.log"), ImmutableList.of("This is a log file"));
            Files.write(logDir.resolve("file2.other"), ImmutableList.of("This is another log file"));

            box().cleanUpVirtualboxVmFromLogDirectory(logDir);

            assertThat(vmBaseDir).isDirectory();
        }

        @Test
        void vmDirNotCleanedUpWhenOtherFilesExistInBase(@TempDir Path tempDir)
        throws BrowserBoxException, IOException
        {
            Path vmBaseDir = tempDir.resolve("vmbase");
            Path logDir = vmBaseDir.resolve("logs");
            Files.createDirectories(logDir);
            Path otherDir = vmBaseDir.resolve("another");
            Files.createDirectories(otherDir);

            box().cleanUpVirtualboxVmFromLogDirectory(logDir);

            assertThat(vmBaseDir).isDirectory();
        }
    }
}
