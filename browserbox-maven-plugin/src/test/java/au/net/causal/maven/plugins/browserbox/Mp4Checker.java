package au.net.causal.maven.plugins.browserbox;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.MediaBox;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import com.googlecode.mp4parser.FileDataSourceImpl;

import java.io.File;
import java.io.IOException;
import java.util.stream.LongStream;

/**
 * Used to read video metadata from integration tests' groovy scripts.
 */
public class Mp4Checker
{
    public static VideoDetails check(File mp4File)
    throws IOException
    {
        try (IsoFile file = new IsoFile(new FileDataSourceImpl(mp4File)))
        {
            TrackHeaderBox trackHeader = readBox(file, TrackHeaderBox.class);
            int width = (int)trackHeader.getWidth();
            int height = (int)trackHeader.getHeight();

            MediaBox media = readBox(file, MediaBox.class);
            long timeScale = media.getMediaHeaderBox().getTimescale();
            TimeToSampleBox timeToSample = media.getMediaInformationBox().getSampleTableBox().getTimeToSampleBox();
            if (timeToSample.getEntries().isEmpty())
                throw new RuntimeException("No time-to-sample entries");

            double deltaAvg = timeToSample.getEntries()
                                          .stream()
                                          //Entry.delta, Entry.count times
                                          .flatMapToLong(e -> LongStream.generate(e::getDelta).limit(e.getCount()))
                                          .summaryStatistics().getAverage();

            double framesPerSecond = timeScale / deltaAvg;

            return new VideoDetails(width, height, framesPerSecond);
        }
    }

    private static <B extends Box> B readBox(IsoFile file, Class<B> boxType)
    {
        return file.getBoxes(boxType, true).stream()
                                           .findAny()
                                           .orElseThrow(() -> new RuntimeException("No box of type " + boxType.getName() + " found in video."));
    }

    public static class VideoDetails
    {
        private final int width;
        private final int height;
        private final double framesPerSecond;

        public VideoDetails(int width, int height, double framesPerSecond)
        {
            this.width = width;
            this.height = height;
            this.framesPerSecond = framesPerSecond;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public double getFramesPerSecond()
        {
            return framesPerSecond;
        }
    }
}
