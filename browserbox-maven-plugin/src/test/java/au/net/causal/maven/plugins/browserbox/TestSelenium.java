package au.net.causal.maven.plugins.browserbox;

import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

@Disabled("test is run manually only")
class TestSelenium
{
    @Test
    void testChrome()
    {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        WebDriver driver = new ChromeDriver(capabilities);
        
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();
        
        System.out.println(driver.getPageSource());
        
        driver.quit();
    }

    @Test
    void testAndroidRemote() throws Exception
    {
        //Need to start ChromeDriver manually first before this one
        String deviceId = "emulator-5554";

        /*
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("androidPackage", "com.android.chrome");
        chromeOptions.setExperimentalOption("androidDeviceSerial", deviceId);
        ChromeDriver driver = new ChromeDriver(chromeOptions);
        */

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "com.android.chrome");
        chromeOptions.put("androidDeviceSerial", deviceId);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        //String seleniumUrl = System.getProperty("selenium.url");
        //DesiredCapabilities capabilities = new Json().toType(DesiredCapabilities.class, System.getProperty("selenium.caps"));

        URL server = new URL("http://localhost:9515");

        System.out.println(new Json().toJson(capabilities));
        
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);


        System.out.println(new Json().toJson(capabilities));
        //System.out.println(new Json().toType(DesiredCap))
        
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();

        System.out.println(driver.getPageSource());

        Files.write(Paths.get("full-screenshot-android.png"), driver.getScreenshotAs(OutputType.BYTES));

        driver.quit();
    }

    @Test
    void testAndroid() throws Exception
    {
        String deviceId = "emulator-5554";
        
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("androidPackage", "com.android.chrome");
        chromeOptions.setExperimentalOption("androidDeviceSerial", deviceId);
        ChromeDriver driver = new ChromeDriver(chromeOptions);
        
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();

        System.out.println(driver.getPageSource());

        Files.write(Paths.get("full-screenshot-android.png"), driver.getScreenshotAs(OutputType.BYTES));

        driver.quit();
    }

    @Test
    void testAndroidFirefox() throws Exception
    {
        String deviceId = "emulator-5554";
        DesiredCapabilities capabilities = new DesiredCapabilities(null, null, null);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "org.mozilla.firefox");
        chromeOptions.put("androidDeviceSerial", deviceId);
        chromeOptions.put("androidIntentArguments", ImmutableList.of("-d", "https://www.google.com"));
        capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, chromeOptions);



        FirefoxProfile profile = new FirefoxProfile();

        //A few profile settings that will allow the debugger to work
        profile.setPreference("remote.enabled", true);
        profile.setPreference("devtools.debugger.remote-enabled", true);
        profile.setPreference("devtools.debugger.prompt-connection", false);
        profile.setPreference("devtools.chrome.enabled", true);
        profile.setPreference("marionette.port", 2829);
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);

        //String seleniumUrl = System.getProperty("selenium.url");
        //DesiredCapabilities capabilities = new Json().toType(DesiredCapabilities.class, System.getProperty("selenium.caps"));

        URL server = new URL("http://localhost:4444");

        System.out.println(new Json().toJson(capabilities));

        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);


        System.out.println(new Json().toJson(capabilities));
        //System.out.println(new Json().toType(DesiredCap))

        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();

        System.out.println(driver.getPageSource());

        Files.write(Paths.get("full-screenshot-android-firefox.png"), driver.getScreenshotAs(OutputType.BYTES));

        driver.quit();
    }

    private <D extends WebDriver & HasCapabilities & JavascriptExecutor> void verifyBrowser(D driver)
    {
        Capabilities caps = driver.getCapabilities();
        System.out.println("Browser name: " + caps.getBrowserName());
        System.out.println("Browser version: " + caps.getCapability("browserVersion"));

        String userAgent = (String)driver.executeScript("return navigator.userAgent");
        System.out.println("User agent: " + userAgent);
    }
    
    @Test
    void testFirefox()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        //WebDriver driver = new FirefoxDriver(capabilities);
        URL server = new URL("http://192.168.99.100:4444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        verifyBrowser(driver);
        
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();

        long timeoutInSeconds = 20L;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

        List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));
        for (WebElement webElement : findElements)
        {
            System.out.println(webElement.getAttribute("href"));
        }
        
        Files.write(Paths.get("full-screenshot.png"), driver.getScreenshotAs(OutputType.BYTES));
        //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));
        
        //Thread.sleep(100_000);
        
        driver.quit();
    }

    @Test
    void testEdge()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.edge();
        //WebDriver driver = new FirefoxDriver(capabilities);
        URL server = new URL("http://localhost:24444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        verifyBrowser(driver);

        /*
        driver.get("http://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("galah");
        searchBox.submit();

        long timeoutInSeconds = 20L;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

        List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));
        for (WebElement webElement : findElements)
        {
            System.out.println(webElement.getAttribute("href"));
        }

         */

        Files.write(Paths.get("full-screenshot.png"), driver.getScreenshotAs(OutputType.BYTES));
        //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));

        //Thread.sleep(100_000);


        //System.out.println(driver.manage().logs().getAvailableLogTypes());
        //System.out.println(driver.manage().logs().get("server").getAll());
        driver.manage().timeouts().setScriptTimeout(10L, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10L, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
        //System.out.println("Before close");
        //driver.close();

        //Thread.sleep(2000L);

        Timer timer = new Timer(true);
        Thread requestThread = Thread.currentThread();
        timer.schedule(new TimerTask()
                       {
                           @Override
                           public void run()
                           {
                                System.out.println("Wow this it taking a while!");
                                requestThread.interrupt();
                           }
                       }, 5000L);


        System.out.println("Before quit!");



        //Response response = driver.getCommandExecutor().execute(new Command(driver.getSessionId(), DriverCommand.QUIT));
        //System.out.println("Response: " + response);

        driver.quit();

        System.out.println("After quit!");

        timer.cancel();
    }

    @Test
    void testIe()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        //capabilities.setCapability("requireWindowFocus", true);
        //capabilities.setCapability(CapabilityType.HAS_NATIVE_EVENTS, false);
        //WebDriver driver = new FirefoxDriver(capabilities);
        URL server = new URL("http://localhost:24444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);
        
        try 
        {
            verifyBrowser(driver);
            long timeoutInSeconds = 20L;
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

            driver.get("http://www.google.com");


            //System.out.println("Page source: " + driver.getPageSource());
            
            
            
            
            wait.until(ExpectedConditions.presenceOfElementLocated(By.name("q")));
            WebElement searchBox = driver.findElement(By.name("q"));
            searchBox.sendKeys("galah");
            searchBox.submit();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main")));

            List<WebElement> findElements = driver.findElements(By.xpath("//div[@id='main']//div/a[starts-with(@href, '/url')]"));
            for (WebElement webElement : findElements) 
            {
                System.out.println(webElement.getAttribute("href"));
            }

            Files.write(Paths.get("full-screenshot-ie.png"), driver.getScreenshotAs(OutputType.BYTES));
            //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));

            //Thread.sleep(100_000);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            throw t;
        }
        finally 
        {
            driver.quit();
        }
    }

    @Test
    void testIeOld()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        //capabilities.setCapability("requireWindowFocus", true);
        //capabilities.setCapability(CapabilityType.HAS_NATIVE_EVENTS, false);
        //WebDriver driver = new FirefoxDriver(capabilities);
        URL server = new URL("http://localhost:24444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        try
        {
            verifyBrowser(driver);
            long timeoutInSeconds = 20L;
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

            driver.get("http://www.google.com");


            //System.out.println("Page source: " + driver.getPageSource());

            wait.until(ExpectedConditions.presenceOfElementLocated(By.name("q")));
            WebElement searchBox = driver.findElement(By.name("q"));
            searchBox.sendKeys("galah\n"); //Use this for IE8 since submit() does not work
            //searchBox.sendKeys("galah");
            //searchBox.submit();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

            //System.out.println("Page source: " + driver.getPageSource());
            //Thread.sleep(1_000_000);

            List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='search']//ol/div//h3/a"));
            for (WebElement webElement : findElements)
            {
                System.out.println(webElement.getAttribute("href"));
            }

            Files.write(Paths.get("full-screenshot-ie.png"), driver.getScreenshotAs(OutputType.BYTES));
            //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));

            //Thread.sleep(100_000);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            throw t;
        }
        finally
        {
            driver.quit();
        }
    }
    
    @Test
    void testLocalEdge()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.edge();
        EdgeDriver driver = new EdgeDriver(capabilities);
        //URL server = new URL("http://localhost:24444/wd/hub");
        //RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        try
        {
            verifyBrowser(driver);
            long timeoutInSeconds = 20L;
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

            driver.get("http://www.google.com");


            //System.out.println("Page source: " + driver.getPageSource());




            wait.until(ExpectedConditions.presenceOfElementLocated(By.name("q")));
            WebElement searchBox = driver.findElement(By.name("q"));
            searchBox.sendKeys("galah");
            searchBox.submit();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("result-stats")));

            List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));
            for (WebElement webElement : findElements)
            {
                System.out.println(webElement.getAttribute("href"));
            }

            Files.write(Paths.get("full-screenshot-edge.png"), driver.getScreenshotAs(OutputType.BYTES));
            //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));

            //Thread.sleep(100_000);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            throw t;
        }
        finally
        {
            driver.quit();
        }
    }

    @Test
    void testIeOnLocalSite()
    throws Exception
    {
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        //capabilities.setCapability("requireWindowFocus", true);
        //capabilities.setCapability(CapabilityType.HAS_NATIVE_EVENTS, false);
        //WebDriver driver = new FirefoxDriver(capabilities);
        URL server = new URL("http://localhost:24444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        try
        {
            verifyBrowser(driver);
            long timeoutInSeconds = 20L;
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

            driver.get("http://192.168.99.1:8080/galah");


            System.out.println("Page source: " + driver.getPageSource());


            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("galah")));
            WebElement item = driver.findElement(By.id("galah"));
            System.out.println("Item: " + item);

            /*
            wait.until(ExpectedConditions.presenceOfElementLocated(By.name("q")));
            WebElement searchBox = driver.findElement(By.name("q"));
            searchBox.sendKeys("galah");
            searchBox.submit();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));

            List<WebElement> findElements = driver.findElements(By.xpath("//*[@id='rso']//h3/a"));
            for (WebElement webElement : findElements)
            {
                System.out.println(webElement.getAttribute("href"));
            }

            Files.write(Paths.get("full-screenshot-ie.png"), driver.getScreenshotAs(OutputType.BYTES));
            //Files.write(Paths.get("results-screenshot.png"), driver.findElement(By.id("resultStats")).getScreenshotAs(OutputType.BYTES));

            //Thread.sleep(100_000);
            */
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            throw t;
        }
        finally
        {
            driver.quit();
        }
    }

    @Test
    void testFirefoxExtension()
    throws Exception
    {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("remote.enabled", true);
        profile.setPreference("devtools.debugger.remote-enabled", true);
        profile.setPreference("devtools.debugger.prompt-connection", false);
        profile.setPreference("devtools.chrome.enabled", true);
        options.setProfile(profile);

        options.addArguments("-start-debugger-server", "44783");

        FirefoxDriver driver = new FirefoxDriver(options);

        driver.get("https://www.google.com");

        driver.quit();
    }
}
