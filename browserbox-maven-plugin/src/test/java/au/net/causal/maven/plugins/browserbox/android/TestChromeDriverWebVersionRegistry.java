package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import com.google.common.collect.Iterators;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

class TestChromeDriverWebVersionRegistry
{
    @Test
    void test()
    throws BrowserBoxException
    {
        URL siteUrl = TestChromeDriverWebVersionRegistry.class.getResource("chromedriverpartialfeed.xml");
        ChromeDriverWebVersionRegistry registry = new ChromeDriverWebVersionRegistry(siteUrl, Platform.WINDOWS, new SystemStreamLog());
        Collection<? extends Item> items = registry.readAllItems(new Query());

        for (Item item : items)
        {
            WebDriverItem c = (WebDriverItem)item;
            System.out.println(c.toString() + " " + c.getCompatibilityInfo());
        }

        //Verify we have a couple of versions
        assertThat(items).hasSize(2);
        WebDriverItem a = (WebDriverItem)Iterators.get(items.iterator(), 0);
        WebDriverItem b = (WebDriverItem)Iterators.get(items.iterator(), 1);
        assertThat(a.getVersion()).isEqualTo("2.35");
        assertThat(b.getVersion()).isEqualTo("2.34");
        assertThat(a.getCompatibilityInfo().getMinMajorVersion()).isEqualTo(62);
        assertThat(a.getCompatibilityInfo().getMaxMajorVersion()).isEqualTo(64);
        assertThat(b.getCompatibilityInfo().getMinMajorVersion()).isEqualTo(61);
        assertThat(b.getCompatibilityInfo().getMaxMajorVersion()).isEqualTo(63);
        assertThat(a.getPlatformSpecificDownloads().entrySet()).hasSize(3);
        assertThat(b.getPlatformSpecificDownloads().entrySet()).hasSize(3);
    }
}
