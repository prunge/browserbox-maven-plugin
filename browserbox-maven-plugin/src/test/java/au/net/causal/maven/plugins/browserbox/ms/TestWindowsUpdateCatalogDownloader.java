package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ms.WindowsUpdateCatalogDownloader.UpdateNotFoundException;
import com.gargoylesoftware.htmlunit.FormEncodingType;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.google.common.collect.ImmutableList;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.*;

class TestWindowsUpdateCatalogDownloader
{
    private static final Logger log = LoggerFactory.getLogger(TestWindowsUpdateCatalogDownloader.class);

    private WindowsUpdateCatalogDownloader downloader;

    @BeforeEach
    void setUpDownloader()
    {
        downloader = new WindowsUpdateCatalogDownloader(new SystemStreamLog());
    }

    @AfterEach
    void tearDownDownloader()
    {
        if (downloader != null)
            downloader.close();
    }

    /**
     * Look up the URL for a Windows Update download without actually downloading it.
     */
    @Test
    void downloadUrlTest()
    throws IOException, BrowserBoxException
    {
        URL result = downloader.lookUpDownloadUrl(new WindowsUpdateKB("4056564",
                      "2018-05 Security Update for Windows Server 2008 for x86-based Systems (KB4056564)"));

        log.info("Download URL: " + result.toExternalForm());

        assertThat(result.toExternalForm()).endsWith(".msu");
    }

    /**
     * Try to look up a KB that does not exist.
     */
    @Test
    void notFoundTest()
    {
        UpdateNotFoundException ex = catchThrowableOfType(() ->
        {
            //Should not be found
            downloader.lookUpDownloadUrl(new WindowsUpdateKB("galah", "galah"));
        }, UpdateNotFoundException.class);

        assertThat(ex).isNotNull();
    }

    @Test
    @Disabled("Multi megabyte download - only do this manually")
    void fullDownloadTest()
    throws IOException, BrowserBoxException
    {
        Path downloadFile = Paths.get("vista-update.msu");

        try (OutputStream out = Files.newOutputStream(downloadFile))
        {
            log.info("Download file...");
            downloader.download(new WindowsUpdateKB("4056564",
                "2018-05 Security Update for Windows Server 2008 for x86-based Systems (KB4056564)"),
                out);
        }

        long downloadSize = Files.size(downloadFile);

        log.info("Downloaded file " + downloadFile + ": " + downloadSize + " bytes");

        //Verify the file size is at least 1 MB
        assertThat(downloadSize).isGreaterThan(1_000_000L);
    }

    @Test
    @Disabled("an experiment to go directly to do the last page")
    void test2()
    throws Exception
    {
        try (WebClient client = new WebClient())
        {
            //Do nothing for warnings
            client.setIncorrectnessListener((message, origin) -> {});
            client.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());

            client.getOptions().setThrowExceptionOnScriptError(false);
            client.getOptions().setJavaScriptEnabled(true);
            client.getOptions().setThrowExceptionOnFailingStatusCode(false);
            client.getOptions().setPrintContentOnFailingStatusCode(false);

            WebRequest request = new WebRequest(new URL("https://www.catalog.update.microsoft.com/DownloadDialog.aspx"), HttpMethod.POST);
            request.setEncodingType(FormEncodingType.URL_ENCODED);
            request.setRequestParameters(ImmutableList.of(
                    //new NameValuePair("updateIds", "[{\"size\":0,\"languages\":\"\",\"uidInfo\":\"b317cee6-38f0-4a82-8c6f-cc61a264629c\",\"updateID\":\"b317cee6-38f0-4a82-8c6f-cc61a264629c\"}]")
                    new NameValuePair("updateIds", "[{\"size\":0,\"languages\":\"\",\"uidInfo\":\"b317cee6-38f0-4a82-8c6f-cc61a264629c\",\"updateID\":\"b317cee6-38f0-4a82-8c6f-cc61a264629c\"}]")
            ));////[{"size":0,"languages":"","uidInfo":"b317cee6-38f0-4a82-8c6f-cc61a264629c","updateID":"b317cee6-38f0-4a82-8c6f-cc61a264629c"}] from chrome so numbers consistent

            HtmlPage page = client.getPage(request);

            //System.out.println(page.asXml());

            HtmlAnchor downloadLink = page.getAnchors().get(0);
            System.out.println("Download this: " + downloadLink.getHrefAttribute());


            //client.getPage();
        }
    }
}
