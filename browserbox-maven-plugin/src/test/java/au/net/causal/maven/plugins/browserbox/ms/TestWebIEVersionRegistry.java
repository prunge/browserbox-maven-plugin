package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.JerseyTestTools;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

/**
 * Use fake web endpoint - does not use live data so we can guarantee the same results over time.
 */
class TestWebIEVersionRegistry
{
    @Test
    void retrieveAll()
    throws Exception
    {
        WebIEVersionRegistry resolver = makeResolver();
        Collection<Item> results = ImmutableList.copyOf(resolver.readAllItems(new Query()));
        
        assertThat(results).containsExactlyInAnyOrder(
                new Item("8", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20150916/Vagrant/IE8/IE8.Win7.Vagrant.zip")),
                new Item("9", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20150916/Vagrant/IE9/IE9.Win7.Vagrant.zip")),
                new Item("10", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20150916/Vagrant/IE10/IE10.Win7.Vagrant.zip")),
                new Item("11", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20150916/Vagrant/IE11/IE11.Win7.Vagrant.zip")));
    }
    
    private WebIEVersionRegistry makeResolver()
    {
        return new WebIEVersionRegistry(
                JerseyTestTools.forJsonResponseFromResource(TestWebIEVersionRegistry.class, "vms.json"));
    }
}
