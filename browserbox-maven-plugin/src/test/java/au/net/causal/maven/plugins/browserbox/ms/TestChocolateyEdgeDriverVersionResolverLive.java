package au.net.causal.maven.plugins.browserbox.ms;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestChocolateyEdgeDriverVersionResolverLive
{
    private static final Logger log = LoggerFactory.getLogger(TestChocolateyEdgeDriverVersionResolverLive.class);
    
    @Test
    void someVersionsExist()
    throws Exception
    {
        //Uses real URL
        ChocolateyEdgeDriverVersionResolver resolver = new ChocolateyEdgeDriverVersionResolver();
        List<String> versions = resolver.readVersions();

        log.info("Available Edge driver versions: " + versions);

        //Verify there are at least these items in the list, there may be additional later versions too
        assertThat(versions).contains("3.14393", "4.15063", "5.16299");
    }
}
