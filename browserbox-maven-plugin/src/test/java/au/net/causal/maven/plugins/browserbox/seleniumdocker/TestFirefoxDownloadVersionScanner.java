package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;

class TestFirefoxDownloadVersionScanner
{
    private final DockerNaming dockerNaming = new DockerNaming();
    
    @Test
    void testDesktopBrowser()
    throws IOException, BrowserBoxException
    {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        
        try 
        {
            Log log = new SystemStreamLog();
            FirefoxDownloadVersionScanner scanner = new FirefoxDesktopDownloadVersionScanner(log, new EmptyBrowserVersionBlacklist(), executor);
            Set<String> results = scanner.readAvailableVersions(dockerNaming.firefoxInstallerUrl().toURL());
            log.info(results.toString());

            assertThat(results).isNotEmpty();
        }
        finally
        {
            executor.shutdown();
        }
    }

    @Test
    void testAndroidBrowser()
    throws IOException, BrowserBoxException
    {
        ExecutorService executor = Executors.newFixedThreadPool(8);

        try
        {
            Log log = new SystemStreamLog();
            FirefoxDownloadVersionScanner scanner = new FirefoxAndroidDownloadVersionScanner(log, new EmptyBrowserVersionBlacklist(), executor);
            Set<? extends FirefoxDownloadVersionScanner.AvailableVersion> results = scanner.readAvailableVersionsData(dockerNaming.firefoxMobileInstallerUrl().toURL(), ImmutableSet.of(), ImmutableMap.of()).getAvailableVersionDownloads();
            log.info(results.toString());

            assertThat(results).isNotEmpty();
        }
        finally
        {
            executor.shutdown();
        }
    }
}
