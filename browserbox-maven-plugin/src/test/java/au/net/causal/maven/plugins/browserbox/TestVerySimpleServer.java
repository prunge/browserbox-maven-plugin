package au.net.causal.maven.plugins.browserbox;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class TestVerySimpleServer 
{
    public static void main(String... args) throws Exception
    {
        HttpServer httpServer = HttpServer.create();
        httpServer.setExecutor(null);
        httpServer.createContext("/galah", new HttpHandler() 
        {
            @Override
            public void handle(HttpExchange h) throws IOException 
            {
                byte[] content = "<h1>Good Morning</h1><p id='galah'>what will be for eating?</p>".getBytes();
                h.sendResponseHeaders(200, content.length);
                h.getResponseBody().write(content);
                h.close();
            }
        });
       
        httpServer.bind(new InetSocketAddress(8080), 0);
        httpServer.start();
        
        Thread.sleep(10_000_000);
    }
}
