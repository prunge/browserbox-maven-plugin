package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.JerseyTestTools;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

/**
 * Use fake web endpoint - does not use live data so we can guarantee the same results over time.
 */
class TestWebEdgeVersionRegistry
{
    /**
     * Older file with a specific name pattern.
     */
    @Test
    void retrieveAll()
    throws Exception
    {
        WebEdgeVersionRegistry resolver = makeResolver();
        Collection<Item> results = ImmutableList.copyOf(resolver.readAllItems(new Query()));
        assertThat(results).containsExactlyInAnyOrder(
                new Item("15.15063", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20170320/Vagrant/MSEdge/MSEdge.Win10.RS2.Vagrant.zip")),
                new Item("16.16257", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20170804/Vagrant/MSEdge/MSEdge.Win10_preview.Vagrant.zip")));
    }

    /**
     * Test with a newer file with different name pattern.
     */
    @Test
    void retrieveAll2()
    throws Exception
    {
        WebEdgeVersionRegistry resolver = makeResolver2();
        Collection<Item> results = ImmutableList.copyOf(resolver.readAllItems(new Query()));
        assertThat(results).containsExactlyInAnyOrder(
                new Item("1809", new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20190311/Vagrant/MSEdge/MSEdge.Win10.Vagrant.zip")));
    }
    
    private WebEdgeVersionRegistry makeResolver()
    {
        return new WebEdgeVersionRegistry(
                JerseyTestTools.forJsonResponseFromResource(TestWebEdgeVersionRegistry.class, "vms.json"));
    }

    private WebEdgeVersionRegistry makeResolver2()
    {
        return new WebEdgeVersionRegistry(
                JerseyTestTools.forJsonResponseFromResource(TestWebEdgeVersionRegistry.class, "vms-2.json"));
    }
}
