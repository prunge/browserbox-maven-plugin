package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;

class TestChromeDriverWebVersionRegistryLive
{
    @Test
    void test()
    throws BrowserBoxException
    {
        ChromeDriverWebVersionRegistry registry = new ChromeDriverWebVersionRegistry(Platform.WINDOWS, new SystemStreamLog());
        Collection<? extends Item> items = registry.readAllItems(new Query());

        for (Item item : items)
        {
            WebDriverItem c = (WebDriverItem)item;
            System.out.println(c.toString() + " " + c.getCompatibilityInfo());
        }

        //Should have some versions
        assertThat(items).hasSizeGreaterThanOrEqualTo(10);
        
        //Ensure there's at least 10 instances of compat info
        assertThat(items.stream().map(WebDriverItem.class::cast).map(WebDriverItem::getCompatibilityInfo).filter(Objects::nonNull)).hasSizeGreaterThanOrEqualTo(10);
    }
}
