package au.net.causal.maven.plugins.browserbox.ms;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.*;

class TestChocolateyDownloader
{
    @Test
    @Disabled("Don't download full chocolatey by default in tests because it would put load on servers")
    void testFullDownload(@TempDir Path tempDir)
    throws IOException
    {
        Path file = tempDir.resolve("chocolatey.nupkg");
        ChocolateyDownloader downloader = new ChocolateyDownloader();
        downloader.downloadChocolatey(file);

        System.out.println("Target file size: " + Files.size(file));
    }

    @Test
    @Disabled("Don't download full chocolatey by default in tests because it would put load on servers")
    void testFullDownloadAndExtract(@TempDir Path tempDir)
    throws IOException
    {
        ChocolateyDownloader downloader = new ChocolateyDownloader();
        downloader.downloadAndExtractChocolatey(tempDir);

        //Ensure the install script exists and is a decent size
        Path installerScript = tempDir.resolve("tools").resolve("chocolateyInstall.ps1");
        assertThat(installerScript).exists();
        assertThat(Files.size(installerScript)).isGreaterThan(256);
    }

    @Test
    void checkChocolateyDownloadExists()
    throws IOException
    {
        ChocolateyDownloader downloader = new ChocolateyDownloader();
        HttpURLConnection con = downloader.createDownloadSession();

        try
        {
            //Download should be at least 1 MB
            assertThat(con.getContentLengthLong()).isGreaterThan(1_000_000);

            //and not be an HTML page (like an error)
            assertThat(con.getContentType()).isEqualTo(MediaType.APPLICATION_OCTET_STREAM);

            //and have OK response
            assertThat(con.getResponseCode()).isEqualTo(200);

            //actual file name should be *.nupkg
            assertThat(con.getURL().getPath()).endsWith(".nupkg");
        }
        finally
        {
            con.disconnect();
        }
    }
}
