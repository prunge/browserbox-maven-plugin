package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOError;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

public abstract class DownloadTestTools
{
    private static final Logger log = LoggerFactory.getLogger(DownloadTestTools.class);

    private static long downloadSize(URL url)
    {
        try
        {
            URLConnection con = url.openConnection();
            try
            {
                return con.getContentLengthLong();
            }
            finally
            {
                con.getInputStream().close();
            }
        }
        catch (IOException e)
        {
            throw new IOError(e);
        }
    }

    public static void verifyAllDownloadsAreOfADecentSize(Collection<? extends Item> items, long minimumAcceptableSizeInBytes)
    {
        for (Item download : items)
        {
            long size = downloadSize(download.getUrl());
            log.info("Download for " + download.getUrl().toExternalForm() + " - " + size + " bytes");

            //At least 100MB (to ensure it's not an error page or something)
            assertThat(size).isGreaterThan(minimumAcceptableSizeInBytes);
        }
    }

    public static void verifyAllDownloadsAreOfADecentSize(VersionRegistry resolver, long minimumAcceptableSizeInBytes)
    throws BrowserBoxException
    {
        verifyAllDownloadsAreOfADecentSize(resolver.readAllItems(new Query()), minimumAcceptableSizeInBytes);
    }
}
