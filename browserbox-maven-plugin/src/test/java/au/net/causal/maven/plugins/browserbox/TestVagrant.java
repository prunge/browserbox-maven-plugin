package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.vagrant.BoxDefinition;
import au.net.causal.maven.plugins.browserbox.vagrant.LocalVagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.BoxListOptions;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.codehaus.plexus.util.cli.Commandline;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

@Disabled("test is run manually only")
class TestVagrant
{
    @Test
    void testList()
    throws Exception
    {
        Log log = new SystemStreamLog();

        Commandline base = new Commandline("vagrant");
        Vagrant vagrant = new LocalVagrant(base, log);

        BoxListOptions options = new BoxListOptions();
        List<? extends BoxDefinition> result = vagrant.boxList(options);
        
        System.out.println(result);
    }
}
