package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.versionstore.DownloadTestTools;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

class TestChromeApkMirrorVersionRegistryLive
{
    @Test
    void testSomeApksExistAndAreDownloadable()
    throws Exception
    {
        //Intentionally limit so we don't get rate limited by APK mirror site
        //Three samples should be enough to prove it's working
        ChromeApkMirrorVersionRegistry resolver = new ChromeApkMirrorVersionRegistry(new SystemStreamLog())
        {
            @Override protected int getResultLimit()
            {
                return 3;
            }
        };
        Collection<? extends Item> downloads = resolver.readAllItems(new Query());

        assertThat(downloads).hasSize(3);

        System.out.println("Results: " + downloads);

        //Also verify the files are downloadable
        DownloadTestTools.verifyAllDownloadsAreOfADecentSize(downloads, 1_000_000);
    }
}
