package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.versionstore.FileDownloader.Download;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;

class TestResumableFileDownloader
{
    @TempDir
    Path tempDirectory;

    @Test
    @Disabled("tests a real download that is large")
    void test()
    throws IOException
    {
        System.out.println(tempDirectory.getRoot());
        ResumableFileDownloader d = new ResumableFileDownloader(tempDirectory);
        URL fileToDownload = new URL("https://az792536.vo.msecnd.net/vms/VMBuild_20150916/Vagrant/IE11/IE11.Win7.Vagrant.zip");
        Download file = d.downloadFile(fileToDownload);
        System.out.println("Download completed: " + file);
    }
}
