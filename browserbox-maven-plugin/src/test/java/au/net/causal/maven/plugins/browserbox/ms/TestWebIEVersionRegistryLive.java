package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.versionstore.DownloadTestTools;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

/**
 * These hit the live Microsoft website, so we can't test anything specific (like version number input) because these
 * change over time.
 */
class TestWebIEVersionRegistryLive
{
    private static final Logger log = LoggerFactory.getLogger(TestWebIEVersionRegistryLive.class);

    /**
     * Must have at least some versions in the list.
     */
    @Test
    void hasSomeVersions()
    throws Exception
    {
        WebIEVersionRegistry resolver = new WebIEVersionRegistry();
        Collection<String> versions = resolver.readAllVersions(new Query());
        log.info("Versions: " + versions);
        
        assertThat(versions).contains("8", "9", "10", "11");
    }

    /**
     * Check that each download in the list gives a download URL whose file is of a decent size suitable for a VM.
     */
    @Test
    void eachDownloadHasDecentlySizedFile()
    throws Exception
    {
        WebIEVersionRegistry resolver = new WebIEVersionRegistry();
        DownloadTestTools.verifyAllDownloadsAreOfADecentSize(resolver, 100_000_000);
    }
}
