package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestChocolateyEdgeDriverVersionResolver
{
    @Test
    void versionsAreParsedAndReturned()
    throws BrowserBoxException
    {
        URL url = TestChocolateyEdgeDriverVersionResolver.class.getResource("edgedriverfeed.xml");
        ChocolateyEdgeDriverVersionResolver resolver = new ChocolateyEdgeDriverVersionResolver(url);
        List<String> versions = resolver.readVersions();
        assertThat(versions).containsExactlyInAnyOrder("3.14393", "4.15063", "5.16299", "5.16299.20171103");
    }
    
    @Test
    void testParsedEdgeVersions()
    throws BrowserBoxException
    {
        List<? extends EdgeDriverVersionResolver.Version> versions =
                EdgeDriverVersionResolver.Version.parseVersions(Arrays.asList("3.14393", "4.15063", "5.16299", "5.16299.20171103"));
        
        assertThat(versions).extracting(EdgeDriverVersionResolver.Version::getRawVersion)
                            .containsExactly("3.14393", "4.15063", "5.16299", "5.16299.20171103");
        assertThat(versions).extracting(EdgeDriverVersionResolver.Version::getMajorVersion)
                            .containsExactly("3", "4", "5", "5");
        assertThat(versions).extracting(EdgeDriverVersionResolver.Version::getEdgeVersion)
                            .containsExactly("14393", "15063", "16299", "16299");
    }
    
    @Test
    void testResolveKnownVersions()
    throws BrowserBoxException
    {
        URL url = TestChocolateyEdgeDriverVersionResolver.class.getResource("edgedriverfeed.xml");
        ChocolateyEdgeDriverVersionResolver resolver = new ChocolateyEdgeDriverVersionResolver(url);

        assertThat(resolver.resolveWithFallback("14.14393").getRawVersion()).isEqualTo("3.14393");
        assertThat(resolver.resolveWithFallback("15.15063").getRawVersion()).isEqualTo("4.15063");
        assertThat(resolver.resolveWithFallback("16.16299").getRawVersion()).isEqualTo("5.16299.20171103");
    }
    
    @Test
    void testResolvePartialMatch()
    throws Exception
    {
        URL url = TestChocolateyEdgeDriverVersionResolver.class.getResource("edgedriverfeed.xml");
        ChocolateyEdgeDriverVersionResolver resolver = new ChocolateyEdgeDriverVersionResolver(url);

        assertThat(resolver.resolveWithFallback("16.16257").getRawVersion()).isEqualTo("5.16299.20171103");
    }

    @Test
    void testResolveNoMatch()
    throws Exception
    {
        URL url = TestChocolateyEdgeDriverVersionResolver.class.getResource("edgedriverfeed.xml");
        ChocolateyEdgeDriverVersionResolver resolver = new ChocolateyEdgeDriverVersionResolver(url);

        assertThat(resolver.resolveWithFallback("13.10586").getRawVersion()).isEqualTo("5.16299.20171103");
    }
}
