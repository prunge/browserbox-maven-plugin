package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class TestDockerRunBrowserVersionReader
{
    @Test
    void userAgentStringParsing()
    {
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0";
        String browserName = "firefox";
        String result = DockerRunBrowserVersionReader.readVersionFromUserAgentString(browserName, userAgent);
        assertThat(result).isEqualTo("59.0");
    }
}
