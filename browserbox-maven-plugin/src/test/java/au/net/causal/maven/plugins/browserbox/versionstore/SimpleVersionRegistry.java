package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableMap;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleVersionRegistry implements VersionRegistry
{
    private final Map<String, URL> versionUrlMap;
    
    public SimpleVersionRegistry(Map<String, URL> versionUrlMap)
    {
        this.versionUrlMap = ImmutableMap.copyOf(versionUrlMap);
    }
    
    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        List<Item> items = versionUrlMap.entrySet().stream()
                                        .filter(entry -> !query.getIgnoredVersions().contains(entry.getKey()))
                                        .map(entry -> new Item(entry.getKey(), entry.getValue()))
                                        .collect(Collectors.toList());
        return new ItemList(items, Collections.emptyList());
    }
}
