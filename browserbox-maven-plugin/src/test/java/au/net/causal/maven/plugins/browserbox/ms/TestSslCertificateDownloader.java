package au.net.causal.maven.plugins.browserbox.ms;

import org.junit.jupiter.api.Test;

import java.net.URL;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

//These SSL certificates might change over time and break the tests
//but we can't anticipate ahead of time what it might change to
//so if the tests break, eyeball the results to make sure they are OK and then update them

class TestSslCertificateDownloader
{
    @Test
    void test()
    throws Exception
    {
        SslCertificateDownloader downloader = new SslCertificateDownloader();
        URL url = new URL("https://selenium-release.storage.googleapis.com");
        X509Certificate result = downloader.downloadRootCertificate(url);
        assertThat(result.getSubjectDN().getName()).contains("CN=GlobalSign");
    }

    @Test
    void test2()
    throws Exception
    {
        SslCertificateDownloader downloader = new SslCertificateDownloader();
        URL url = new URL("https://javadl.oracle.com");
        X509Certificate result = downloader.downloadRootCertificate(url);
        assertThat(result.getSubjectDN().getName()).contains("CN=DigiCert Global Root CA");
    }

    @Test
    void test3()
    throws Exception
    {
        SslCertificateDownloader downloader = new SslCertificateDownloader();
        URL url = new URL("https://stackoverflow.com");
        X509Certificate result = downloader.downloadRootCertificate(url);
        assertThat(result.getSubjectDN().getName()).contains("CN=DST Root CA X3");
    }

    @Test
    void testRedirects()
    throws Exception
    {
        SslCertificateDownloader downloader = new SslCertificateDownloader();
        URL url = new URL("https://bintray.com/tigervnc/stable/download_file?file_path=tigervnc-1.9.0.exe");
        Set<? extends X509Certificate> results = downloader.downloadRootCertificates(url);

        Set<String> rootNames = results.stream().map(X509Certificate::getSubjectDN)
                                                .map(Principal::getName)
                                                .collect(Collectors.toSet());

        assertThat(rootNames).anyMatch(s -> s.contains("DigiCert Global Root CA"))
                             .anyMatch(s -> s.contains("VeriSign Class 3 Public Primary Certification Authority - G5"));
    }
}
