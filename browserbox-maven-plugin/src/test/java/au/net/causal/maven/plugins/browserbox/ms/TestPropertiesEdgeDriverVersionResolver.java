package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class TestPropertiesEdgeDriverVersionResolver
{
    @Test
    void testResolveExactKnownVersions()
    throws Exception
    {
        Properties props = new Properties();
        props.setProperty("15063", "4.15063");
        props.setProperty("16299", "5.16299.20171103");
        
        MyPropertiesEdgeDriverVersionResolver resolver = new MyPropertiesEdgeDriverVersionResolver(props);

        assertThat(resolver.resolveWithFallback("15.15063").getRawVersion()).isEqualTo("4.15063");
        assertThat(resolver.resolveWithFallback("16.16299").getRawVersion()).isEqualTo("5.16299.20171103");
    }
    
    @Test
    void testResolvePartialMatch()
    throws Exception
    {
        Properties props = new Properties();
        props.setProperty("15063", "4.15063");
        props.setProperty("16299", "5.16299.20171103");

        MyPropertiesEdgeDriverVersionResolver resolver = new MyPropertiesEdgeDriverVersionResolver(props);

        assertThat(resolver.resolveWithFallback("16.16257").getRawVersion()).isEqualTo("5.16299.20171103");
    }

    @Test
    void testResolveNoMatch()
    throws Exception
    {
        Properties props = new Properties();
        props.setProperty("15063", "4.15063");
        props.setProperty("16299", "5.16299.20171103");

        MyPropertiesEdgeDriverVersionResolver resolver = new MyPropertiesEdgeDriverVersionResolver(props);

        //Picks the highest version
        assertThat(resolver.resolveWithFallback("13.10586").getRawVersion()).isEqualTo("5.16299.20171103");
    }

    @Test
    void testResolveNoMatchWithOnDemand()
    throws Exception
    {
        Properties props = new Properties();
        props.setProperty("15063", "4.15063");
        props.setProperty("16299", "5.16299.20171103");
        props.setProperty("17763", "ondemand:7.17763");

        MyPropertiesEdgeDriverVersionResolver resolver = new MyPropertiesEdgeDriverVersionResolver(props);

        //Picks the highest version
        assertThat(resolver.resolveWithFallback("13.10586").getRawVersion()).isEqualTo("ondemand:7.17763");
    }

    private static class MyPropertiesEdgeDriverVersionResolver extends PropertiesEdgeDriverVersionResolver
    {
        private final Properties props;
        
        public MyPropertiesEdgeDriverVersionResolver(Properties props)
        {
            this.props = props;
        }
        
        @Override
        protected Properties readProperties() 
        throws BrowserBoxException 
        {
            return props;
        }
    }
}
