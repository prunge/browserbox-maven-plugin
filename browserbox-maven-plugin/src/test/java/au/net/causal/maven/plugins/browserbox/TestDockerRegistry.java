package au.net.causal.maven.plugins.browserbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

@Disabled("test is run manually only")
class TestDockerRegistry
{
    private TokenResponse savedToken;
    
    /*
    https://docs.docker.com/registry/spec/api/#listing-repositories
    https://stackoverflow.com/questions/28320134/how-to-list-all-tags-for-a-docker-image-on-a-remote-registry
    https://registry.hub.docker.com/v2/repositories/library/postgres/tags/
    https://docs.docker.com/registry/spec/auth/token/#how-to-authenticate
    https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/latest
    */
    
    @Test
    void test()
    {
        Client client = ClientBuilder.newClient();
        TagsResponse response = getWithAuth(client,
                URI.create("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/tags/list"),
                TagsResponse.class);

        Multimap<String, String> imageIdToTagMap = ArrayListMultimap.create();
        
        for (String tag : response.getTags())
        {
            System.out.println("Reading manifest for tag " + tag);
            Response genericManifestResponse = genericGetWithAuth(client,
                                                    URI.create("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/" + tag));
            
            //System.out.println(genericManifestResponse);
            
            //System.out.println(genericManifestResponse.getMediaType());
            if (genericManifestResponse.getMediaType().getSubtype().contains("+prettyjws"))
                System.out.println("Wrong response content type, possibly V1 manifest, ignoring...");
                //System.out.println("Wrong response content type, possibly V1 manifest, ignoring... " + genericManifestResponse.getStringHeaders() + "\n" + genericManifestResponse.readEntity(String.class));
            else 
            {
                ManifestResponse manifestResponse = genericManifestResponse.readEntity(ManifestResponse.class);
                if (manifestResponse.getConfig() == null)
                    System.out.println("Missing manifest for " + tag);
                else
                    imageIdToTagMap.put(manifestResponse.getConfig().getDigest(), tag);
            }
        }
        
        System.out.println(imageIdToTagMap);

    }

    @Test
    void testManifest()
    {
        Client client = ClientBuilder.newClient();
        ManifestResponse response = getWithAuth(client,
                URI.create("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/3.5.0"),
                ManifestResponse.class);

        System.out.println("Response: " + response.getConfig().getDigest());
        
        //config.digest is the image ID
        //sha256:116407e123328927e7e9501fc60ea5545e906e77085f02c9f041ee19200e3bdd -> 116407e12332
    }

    @Test
    void testManifestString()
    {
        Client client = ClientBuilder.newClient();
        Response response = genericGetWithAuth(client,
                    URI.create("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/2.45.0"));
        
        //System.out.println("Response: " + response);
        
        System.out.println(response.getStringHeaders());
        
        System.out.println(response.readEntity(ManifestResponse.class));
    }

    @Test
    void testTagList()
    {
        //client.target("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/latest").request()
        
        Client client = ClientBuilder.newClient();
        TagsResponse response = getWithAuth(client, 
                                    URI.create("https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/tags/list"),
                                    TagsResponse.class);
        
        System.out.println("Response: " + response.getTags());
    }
    
    private <T> T getWithAuth(Client client, URI requestUri, Class<T> responseType)
    {
        try 
        {
            Invocation.Builder b = client.target(requestUri)
                                    .request(MediaType.APPLICATION_JSON, 
                                            "application/vnd.docker.distribution.manifest.v2+json", 
                                            //"application/vnd.docker.distribution.manifest.v1+prettyjws", 
                                            "application/vnd.docker.distribution.manifest.v1+json");
            if (savedToken != null)
                b = b.header(HttpHeaders.AUTHORIZATION, "Bearer " + savedToken.getToken());
                    
            return b.get(responseType);
        }
        catch (NotAuthorizedException e)
        {
            Map<String, String> bearerParameters = new LinkedHashMap<>();
            for (Object challenge : e.getChallenges())
            {
                String sChallenge = Objects.toString(challenge, "");
                if (sChallenge.toLowerCase(Locale.ENGLISH).startsWith("bearer "))
                {
                    String bearerContent = sChallenge.substring("bearer ".length(), sChallenge.length());
                    bearerParameters.clear();
                    parseBearerContent(bearerContent, bearerParameters);
                }
                
                //System.out.println(challenge.getClass().getCanonicalName() + ": " + challenge);
                //Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:selenium/standalone-firefox-debug:pull"
            }

            String realm = bearerParameters.remove("realm");
            if (realm == null)
                throw e;
            
            /*
            Map<String, String> params = ImmutableMap.of("service", "registry.docker.io",
                    "scope", "repository:selenium/standalone-firefox-debug:pull");
            */
            TokenResponse token = authenticate(client, URI.create(realm), bearerParameters);
            savedToken = token;
            
            return client.target(requestUri)
                    .request(MediaType.APPLICATION_JSON,
                            "application/vnd.docker.distribution.manifest.v2+json",
                            //"application/vnd.docker.distribution.manifest.v1+prettyjws",
                            "application/vnd.docker.distribution.manifest.v1+json")
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token.getToken())
                    .get(responseType);
        }
    }

    private Response genericGetWithAuth(Client client, URI requestUri)
    {
        //TODO copy paste
        try
        {
            Invocation.Builder b = client.target(requestUri)
                    .request(MediaType.APPLICATION_JSON,
                            "application/vnd.docker.distribution.manifest.v2+json",
                            //"application/vnd.docker.distribution.manifest.v1+prettyjws",
                            "application/vnd.docker.distribution.manifest.v1+json");
            if (savedToken != null)
                b = b.header(HttpHeaders.AUTHORIZATION, "Bearer " + savedToken.getToken());

            return b.get();
        }
        catch (NotAuthorizedException e)
        {
            Map<String, String> bearerParameters = new LinkedHashMap<>();
            for (Object challenge : e.getChallenges())
            {
                String sChallenge = Objects.toString(challenge, "");
                if (sChallenge.toLowerCase(Locale.ENGLISH).startsWith("bearer "))
                {
                    String bearerContent = sChallenge.substring("bearer ".length(), sChallenge.length());
                    bearerParameters.clear();
                    parseBearerContent(bearerContent, bearerParameters);
                }

                //System.out.println(challenge.getClass().getCanonicalName() + ": " + challenge);
                //Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:selenium/standalone-firefox-debug:pull"
            }

            String realm = bearerParameters.remove("realm");
            if (realm == null)
                throw e;
            
            /*
            Map<String, String> params = ImmutableMap.of("service", "registry.docker.io",
                    "scope", "repository:selenium/standalone-firefox-debug:pull");
            */
            TokenResponse token = authenticate(client, URI.create(realm), bearerParameters);
            savedToken = token;

            return client.target(requestUri)
                    .request(MediaType.APPLICATION_JSON,
                            "application/vnd.docker.distribution.manifest.v2+json",
                            //"application/vnd.docker.distribution.manifest.v1+prettyjws",
                            "application/vnd.docker.distribution.manifest.v1+json")
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token.getToken())
                    .get();
        }
    }
    
    private static void parseBearerContent(String bearerContent, Map<? super String, ? super String> parameterMap)
    {
        Pattern splitter = Pattern.compile(Pattern.quote("="));
        
        //e.g. realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:selenium/standalone-firefox-debug:pull"
        for (String part : bearerContent.split(Pattern.quote("\",")))
        {
            //realm="https://auth.docker.io/token
            String[] keyValue = splitter.split(part, 2);
            if (keyValue.length >= 2)
            {
                String key = keyValue[0];
                String value = keyValue[1];
                if (value.startsWith("\""))
                    value = value.substring(1);
                if (value.endsWith("\""))
                    value = value.substring(0, value.length() - 1);
                
                parameterMap.put(key, value);
            }
        }
    }
    
    private static TokenResponse authenticate(Client client, URI uri, Map<String, String> parameters)
    {
        WebTarget target = client.target(uri);
        for (Map.Entry<String, String> entry : parameters.entrySet())
        {
            target = target.queryParam(entry.getKey(), entry.getValue());
        }
        return target.request(MediaType.APPLICATION_JSON_TYPE)
                     .get(TokenResponse.class);
    }

    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TagsResponse
    {
        //TODO pagination?
        
        private String name;
        private final List<String> tags = new ArrayList<>();

        public String getName() 
        {
            return name;
        }

        public void setName(String name) 
        {
            this.name = name;
        }

        public List<String> getTags() 
        {
            return tags;
        }
        
        public void setTags(List<String> tags)
        {
            this.tags.clear();
            this.tags.addAll(tags);
        }
    }
    
    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ManifestResponse
    {
        private Config config = new Config();

        public Config getConfig() 
        {
            return config;
        }

        public void setConfig(Config config) 
        {
            this.config = config;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Config
        {
            private String digest;

            public String getDigest() 
            {
                return digest;
            }

            public void setDigest(String digest) 
            {
                this.digest = digest;
            }
        }
    }

    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TokenResponse
    {
        private String token;
        
        @JsonProperty("expires_in")
        private long expiresIn;
        
        //TODO could also read date in here
        //"issued_at":"2017-09-20T21:16:31.931887312Z"

        public String getToken() 
        {
            return token;
        }

        public void setToken(String token) 
        {
            this.token = token;
        }

        public long getExpiresIn() 
        {
            return expiresIn;
        }

        public void setExpiresIn(long expiresIn) 
        {
            this.expiresIn = expiresIn;
        }
    }
}
