package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.ConnectionType;
import au.net.causal.maven.plugins.browserbox.box.MicrosoftVagrantBrowserBox.VagrantConnectionType;
import au.net.causal.maven.plugins.browserbox.box.StandardConnectionType;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestShowMojo
{
    @Test
    void selectConnectionTypesAllUisSupportedByBox()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI, 
                                                                   StandardConnectionType.VNC,
                                                                   StandardConnectionType.SELENIUM, //A non-UI one
                                                                   StandardConnectionType.RDP);
        List<String> preferredViewers = ImmutableList.of();
        List<String> viewerBlacklist = ImmutableList.of();
        
        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));
        
        assertThat(selected).containsExactly(VagrantConnectionType.VIRTUALBOX_UI, StandardConnectionType.VNC, StandardConnectionType.RDP);
    }

    @Test
    void selectConnectionTypesSomeUisSupportedByBox()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(StandardConnectionType.VNC,
                                                                   VagrantConnectionType.VIRTUALBOX_UI,
                                                                   StandardConnectionType.SSH); //A non-UI one;
        List<String> preferredViewers = ImmutableList.of();
        List<String> viewerBlacklist = ImmutableList.of();

        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));

        assertThat(selected).containsExactly(StandardConnectionType.VNC, VagrantConnectionType.VIRTUALBOX_UI);
    }

    @Test
    void selectConnectionTypesAllUisSupportedByBoxWithBlacklist()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI,
                                                                   StandardConnectionType.VNC,
                                                                   StandardConnectionType.SELENIUM, //A non-UI one
                                                                   StandardConnectionType.RDP);
        List<String> preferredViewers = ImmutableList.of();
        List<String> viewerBlacklist = ImmutableList.of(StandardConnectionType.VNC.name());

        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));

        assertThat(selected).containsExactly(VagrantConnectionType.VIRTUALBOX_UI, StandardConnectionType.RDP);
    }

    @Test
    void selectConnectionTypesSomeUisSupportedByBoxWithBlacklist()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(StandardConnectionType.VNC,
                                                                   VagrantConnectionType.VIRTUALBOX_UI,
                                                                   StandardConnectionType.SSH); //A non-UI one;
        List<String> preferredViewers = ImmutableList.of();
        List<String> viewerBlacklist = ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI.name());

        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));

        assertThat(selected).containsExactly(StandardConnectionType.VNC);
    }

    @Test
    void selectConnectionTypesAllUisSupportedByBoxUserSelection()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI,
                                                                   StandardConnectionType.VNC,
                                                                   StandardConnectionType.SELENIUM, //A non-UI one
                                                                   StandardConnectionType.RDP);
        List<String> preferredViewers = ImmutableList.of(StandardConnectionType.RDP.name(), StandardConnectionType.VNC.name());
        List<String> viewerBlacklist = ImmutableList.of();

        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));

        assertThat(selected).containsExactly(StandardConnectionType.RDP, StandardConnectionType.VNC);
    }

    @Test
    void selectConnectionTypesSomeUisSupportedByBoxUserSelection()
    {
        List<ConnectionType> boxConnectionTypes = ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI,
                                                                   StandardConnectionType.SELENIUM, //A non-UI one
                                                                   StandardConnectionType.RDP);
        List<String> preferredViewers = ImmutableList.of(StandardConnectionType.RDP.name(), StandardConnectionType.VNC.name());
        List<String> viewerBlacklist = ImmutableList.of();

        List<ConnectionType> selected = ImmutableList.copyOf(ShowMojo.selectConnectionTypes(boxConnectionTypes, preferredViewers, viewerBlacklist));

        assertThat(selected).containsExactly(StandardConnectionType.RDP);
    }
}
