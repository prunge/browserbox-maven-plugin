package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.assertj.core.api.Assertions.*;

class TestWindowsBoxMaker
{
    @Test
    void testFileNameFromUrl()
    throws BrowserBoxException, MalformedURLException
    {
        URL url = new URL("http://galah.galah.galah/path/myfile.txt");
        String fileName = WindowsBoxMaker.fileNameFromUrl(url);
        assertThat(fileName).isEqualTo("myfile.txt");
    }
}
