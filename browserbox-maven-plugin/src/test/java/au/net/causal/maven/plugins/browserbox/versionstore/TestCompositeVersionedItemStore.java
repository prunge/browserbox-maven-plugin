package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestCompositeVersionedItemStore
{
    @Test
    void noItemStoreImplementationShouldThrowError()
    {
        IllegalArgumentException thrown = catchThrowableOfType(() -> {
                new CompositeVersionedItemStore(
                        new SimpleVersionRegistry(ImmutableMap.of()),
                        new SimpleVersionRegistry(ImmutableMap.of()));
        }, IllegalArgumentException.class);
        assertThat(thrown).isNotNull();
    }

    @Test
    void shouldSaveToFirstChildItemStore()
    throws IOException, BrowserBoxException
    {
        MyStore myStore = new MyStore();
        VersionedItemStore store = new CompositeVersionedItemStore(
                new SimpleVersionRegistry(ImmutableMap.of()),
                myStore);

        Item saveItem = new Item("2.0", new URL("http://galah.galah/galah/myfile"));

        store.saveItemContents(saveItem);

        //Verify the item was saved to the child store
        assertThat(myStore.getSavedItems()).containsExactly(saveItem);
    }

    private static class MyStore implements VersionedItemStore
    {
        private final List<Item> savedItems = new ArrayList<>();

        public List<Item> getSavedItems()
        {
            return savedItems;
        }

        @Override
        public URL saveItemContents(Item item) throws BrowserBoxException
        {
            savedItems.add(item);
            try
            {
                return new URL(item.getUrl().toExternalForm() + ".saved");
            }
            catch (MalformedURLException e)
            {
                throw new BrowserBoxException(e);
            }
        }

        @Override
        public ItemList readAllItemsAllowFailures(Query query) throws BrowserBoxException
        {
            return new ItemList(Collections.emptyList(), Collections.emptyList());
        }
    }
}
