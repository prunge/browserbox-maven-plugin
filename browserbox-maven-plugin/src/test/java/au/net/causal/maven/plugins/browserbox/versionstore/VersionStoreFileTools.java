package au.net.causal.maven.plugins.browserbox.versionstore;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class VersionStoreFileTools
{
    /**
     * Creates a temporary file of the specified size filled with zeroes.
     *
     * @param tempDirectory temporary folder for testing.
     * @param size the size of the file to create.
     *
     * @return the created file.
     *
     * @throws IOException if an error occurs.
     */
    public static Path createTempFile(Path tempDirectory, long size)
    throws IOException
    {
        Path tempFile = Files.createTempFile(tempDirectory, "browserbox", ".tmp");
        try (SeekableByteChannel c = Files.newByteChannel(tempFile, StandardOpenOption.WRITE))
        {
            c.position(size - 1);
            ByteBuffer buf = ByteBuffer.allocate(1);
            buf.put((byte)0);
            buf.flip();
            c.write(buf);
        }
        return tempFile;
    }
}
