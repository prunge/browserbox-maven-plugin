package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import au.net.causal.maven.plugins.browserbox.box.StandardConnectionType;
import au.net.causal.maven.plugins.browserbox.execute.FinderRegistry;
import au.net.causal.maven.plugins.browserbox.execute.ToolFinder;
import org.codehaus.plexus.util.cli.DefaultConsumer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Optional;

@Disabled("test is run manually only")
class TestVncFinder
{
    @Test
    void test() throws Exception
    {
        ConnectionInfo ci = new ConnectionInfo(URI.create("vnc://localhost:15900"));

        FinderRegistry r = new FinderRegistry();
        Optional<ToolFinder.ToolRunner> result = r.platformCombinedFinder(StandardConnectionType.VNC).findTool(ci, null, null, null, null);
        
        System.out.println(result);

        result.get().run(ci, null, null, null, null, new DefaultConsumer(), new DefaultConsumer(), null);
    }
}
