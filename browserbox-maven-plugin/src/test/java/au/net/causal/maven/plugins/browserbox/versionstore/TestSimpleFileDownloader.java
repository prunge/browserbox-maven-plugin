package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.versionstore.FileDownloader.Download;
import au.net.causal.maven.plugins.browserbox.versionstore.SimpleFileDownloader.NonTemporaryDownload;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import static org.assertj.core.api.Assertions.*;

class TestSimpleFileDownloader
{
    @TempDir
    Path temp;

    @Test
    void localFilesAreNotDownloadedToTemp()
    throws IOException
    {
        Path localFile = Files.createTempFile(temp, "browserbox", ".tmp");
        URL localFileUrl = localFile.toUri().toURL();

        SimpleFileDownloader downloader = new SimpleFileDownloader(temp);
        Download download = downloader.downloadFile(localFileUrl);

        assertThat(download).isInstanceOf(NonTemporaryDownload.class);
        assertThat(download.getUrl()).isEqualTo(localFileUrl);
        assertThat(download.getFile()).isEqualTo(localFile);
    }

    @Test
    void remoteFilesAreDownloadedToTemp()
    throws IOException
    {
        String fileContent = "This is a test file";

        Path localFile = Files.createTempFile(temp, "browserbox", ".tmp");
        Files.write(localFile, Collections.singleton(fileContent));
        URL localFileUrl = localFile.toUri().toURL();
        URL testFileUrl = new URL(null, "http://galah.galah.galah/content.txt",
                                  new CustomContentURLStreamHandler(localFileUrl));

        SimpleFileDownloader downloader = new SimpleFileDownloader(temp);
        Download download = downloader.downloadFile(testFileUrl);

        assertThat(download.getUrl()).isEqualTo(testFileUrl);

        //This file should be downloaded so it must be a different temp file
        //But file content must match
        assertThat(download.getFile()).isNotEqualTo(localFile)
                                      .hasContent(fileContent);
    }

    /**
     * A custom content handler to allow content for a URL to be fetched from a completely different source.
     */
    private static class CustomContentURLStreamHandler extends URLStreamHandler
    {
        private final URL actualContent;

        public CustomContentURLStreamHandler(URL actualContent)
        {
            this.actualContent = actualContent;
        }

        @Override
        protected URLConnection openConnection(URL u, Proxy p) throws IOException
        {
            return actualContent.openConnection(p);
        }

        @Override
        protected URLConnection openConnection(URL u) throws IOException
        {
            return actualContent.openConnection();
        }
    }
}
