package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class TestVersionPropertiesArchiveResolver
{
    @Test
    void testAvailableBrowserVersions()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();
        
        versions.putAll(ImmutableMap.of("tag-1", "23.4.8", 
                                        "tag-2", "23.5.0", 
                                        "tag-3", "24.0.0"));
        
        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);
        
        assertThat(resolver.availableBrowserVersions()).containsExactly("23.4.8", "23.5.0", "24.0.0");
    }
    
    @Test
    void findMajorVersion()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "24.5.0",
                                        "tag-3", "25.0.0"));
        images.putAll(ImmutableMap.of("tag-1", "111", 
                                      "tag-2", "222", 
                                      "tag-3", "333"));

        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        TagDetails t1 = resolver.tagForBrowserVersion("24");
        assertThat(t1.getName()).isEqualTo("tag-2");
        assertThat(t1.getDigest()).isEqualTo("222");
    }

    @Test
    void findMajorVersionSelectsHighestMinorVersion()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "23.5.0",
                                        "tag-3", "25.0.0"));
        images.putAll(ImmutableMap.of("tag-1", "111",
                                        "tag-2", "222",
                                        "tag-3", "333"));

        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        TagDetails t1 = resolver.tagForBrowserVersion("23");
        assertThat(t1.getName()).isEqualTo("tag-2");
        assertThat(t1.getDigest()).isEqualTo("222");
    }

    @Test
    void findVersionExact()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "23.5.0",
                                        "tag-3", "25.0.0"));
        images.putAll(ImmutableMap.of("tag-1", "111",
                                        "tag-2", "222",
                                        "tag-3", "333"));

        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        TagDetails t1 = resolver.tagForBrowserVersion("23.4.8");
        assertThat(t1.getName()).isEqualTo("tag-1");
        assertThat(t1.getDigest()).isEqualTo("111");
    }

    @Test
    void findVersionWithoutImage()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "23.5.0",
                                        "tag-3", "25.0.0"));
        
        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        TagDetails t1 = resolver.tagForBrowserVersion("23.4.8");
        assertThat(t1.getName()).isEqualTo("tag-1");
        assertThat(t1.getDigest()).isNull();
    }
    
    @Test
    void findVersionWithSpecialSuffix()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "23.5.0",
                                        "tag-3", "25.0.0"));
        images.putAll(ImmutableMap.of("tag-1", "111",
                                        "tag-2", "222",
                                        "tag-3", "333"));
        
        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        //Should not find
        TagDetails t1 = resolver.tagForBrowserVersion("23.4-stable");
        assertThat(t1).isNull();
    }

    @Test
    void findVersionSpecialSuffixOnData()
    throws BrowserBoxException
    {
        Properties versions = new Properties();
        Properties images = new Properties();

        versions.putAll(ImmutableMap.of("tag-1", "23.4.8",
                                        "tag-2", "23.5.0-special",
                                        "tag-3", "25.0.0"));
        images.putAll(ImmutableMap.of("tag-1", "111",
                                        "tag-2", "222",
                                        "tag-3", "333"));

        TestingVersionPropertiesArchiveResolver resolver = new TestingVersionPropertiesArchiveResolver(versions, images);

        TagDetails t1 = resolver.tagForBrowserVersion("23");
        assertThat(t1.getName()).isEqualTo("tag-2");
        assertThat(t1.getDigest()).isEqualTo("222");
    }
    
    private static class TestingVersionPropertiesArchiveResolver extends VersionPropertiesArchiveResolver
    {
        private final VersionsResources r;
        
        public TestingVersionPropertiesArchiveResolver(Properties versions, Properties images) 
        {
            r = new VersionsResources(versions, images);
        }

        @Override
        protected VersionsResources readBrowserVersionResources() 
        throws BrowserBoxException 
        {
            return r;
        }
    }
}
