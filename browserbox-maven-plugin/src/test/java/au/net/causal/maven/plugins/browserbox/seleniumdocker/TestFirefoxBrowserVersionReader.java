package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;

@Disabled("manually run only test")
class TestFirefoxBrowserVersionReader
{
    @TempDir
    Path tempDir;
    
    @Test
    void test()
    throws BrowserBoxException
    {
        FirefoxBrowserVersionReader reader = new FirefoxBrowserVersionReader(tempDir);
        
        List<Tag> tags = ImmutableList.of(new Tag("3.5.3-boron"), new Tag("3.5.0-argon"), new Tag("3.4.0-chromium"));

        for (Tag tag : tags)
        {
            String result = reader.readBrowserVersion(tag);
            System.out.println(tag.getName() + ": " + result);
        }
    }
}
