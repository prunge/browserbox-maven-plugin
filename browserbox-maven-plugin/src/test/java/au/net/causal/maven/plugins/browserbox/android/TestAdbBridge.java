package au.net.causal.maven.plugins.browserbox.android;

import com.android.SdkConstants;
import com.android.annotations.NonNull;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.BrowserBoxAdbHelper;
import com.android.ddmlib.EmulatorConsole;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.FileListingService.FileEntry;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.ScreenRecorderOptions;
import com.android.prefs.AndroidLocation;
import com.android.repository.api.Channel;
import com.android.repository.api.ConsoleProgressIndicator;
import com.android.repository.api.Downloader;
import com.android.repository.api.Installer;
import com.android.repository.api.InstallerFactory;
import com.android.repository.api.LocalPackage;
import com.android.repository.api.RemotePackage;
import com.android.repository.api.RepoManager;
import com.android.repository.api.SettingsController;
import com.android.repository.io.FileOp;
import com.android.repository.io.impl.FileOpImpl;
import com.android.resources.ScreenOrientation;
import com.android.sdklib.devices.Device;
import com.android.sdklib.devices.DeviceManager;
import com.android.sdklib.internal.avd.AvdInfo;
import com.android.sdklib.internal.avd.AvdManager;
import com.android.sdklib.internal.avd.GpuMode;
import com.android.sdklib.internal.avd.HardwareProperties;
import com.android.sdklib.repository.AndroidSdkHandler;
import com.android.sdklib.repository.installer.SdkInstallerUtil;
import com.android.sdklib.repository.legacy.LegacyDownloader;
import com.android.sdklib.repository.meta.DetailsTypes.SysImgDetailsType;
import com.android.sdklib.repository.targets.SystemImage;
import com.android.sdklib.repository.targets.SystemImageManager;
import com.android.utils.ILogger;
import com.android.utils.IReaderLogger;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.base.MoreObjects;
import org.codehaus.plexus.util.Os;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.DosFileAttributes;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

class TestAdbBridge
{
    private WebClient webClient;

    @BeforeEach
    private void setUpWebClient()
    {
        webClient = new WebClient(BrowserVersion.BEST_SUPPORTED);
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setCssEnabled(false);
        
    }

    @AfterEach
    private void tearDownWebClient()
    {
        if (webClient != null)
            webClient.close();
    }

    @Disabled("test is run manually only")
    @Test
    void testDownloadFromApkMirror()
    throws Exception
    {
        
        URL u = new URL("https://www.apkmirror.com/uploads/?q=chrome");
        
        HtmlPage p1 = webClient.getPage(u);
        for (DomNode item : p1.querySelectorAll("#primary .appRowTitle a"))
        {
            if (item instanceof HtmlAnchor) 
            {
                HtmlAnchor link = (HtmlAnchor)item;
                System.out.println("Item: " + link.getTextContent() + " - " + link.getHrefAttribute());
                readApkLink(new URL(u, link.getHrefAttribute()));
                //return; //TODO
            }
        }
    }
    
    private void readApkLink(URL pageUrl)
    throws Exception
    {
        HtmlPage page = webClient.getPage(pageUrl);
        
        URL downloadPageUrl = null;
        for (DomNode row : page.querySelectorAll(".variants-table .table-row"))
        {
            boolean x86Match = false;
            for (DomNode cell : row.querySelectorAll(".table-cell"))
            {
                if (cell.getTextContent().trim().equalsIgnoreCase("x86"))
                    x86Match = true;
            }
            if (x86Match)
            {
                HtmlAnchor link = row.querySelector("a");
                
                //Use the last one found, it is likely the latest
                downloadPageUrl = new URL(pageUrl, link.getHrefAttribute());
            }
        }
        
        if (downloadPageUrl != null)
        {
            System.out.println("Download page URL: " + downloadPageUrl);
            
            HtmlPage downloadPage = webClient.getPage(downloadPageUrl);
            HtmlAnchor downloadLink = downloadPage.querySelector("a.downloadButton");
            URL downloadFinalPageUrl = new URL(downloadPageUrl, downloadLink.getHrefAttribute());
            System.out.println("Download final page APK: " + downloadFinalPageUrl);
            
            HtmlPage downloadFinalPage = webClient.getPage(downloadFinalPageUrl);
            HtmlForm downloadForm = downloadFinalPage.querySelector("#filedownload");
            WebRequest request = downloadForm.getWebRequest(null);
            URL downloadUrl = request.getUrl();
            System.out.println("Download URL: " + downloadUrl);
        }
    }

    @Disabled("test is run manually only")
    @Test
    void testReadChromeVersionFromImage()
    throws Exception
    {
        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);

        AndroidSdkHandler sdk = AndroidSdkHandler.getInstance(androidHome.toFile());
        AvdManager avdManager = AvdManager.getInstance(sdk, createLogger());
        System.out.println("Available AVDs:");
        for (AvdInfo avd : avdManager.getValidAvds())
        {
            System.out.println(avd.getName());
        }
        //avdManager.createAvd(...)

        SystemImageManager systemImageManager = sdk.getSystemImageManager(new ConsoleProgressIndicator());
        System.out.println("Available system images:");
        for (SystemImage image : systemImageManager.getImages())
        {
            System.out.println(image.getAndroidVersion() + " " + image.getAbiType() + " " + image.getTag());
        }

        RepoManager sdkManager = sdk.getSdkManager(new ConsoleProgressIndicator());
        System.out.println("Packages:\n" + sdkManager.getPackages().getLocalPackages().keySet().stream().collect(Collectors.joining("\n")));
        LocalPackage p = sdk.getLatestLocalPackageForPrefix("system-images", false, new ConsoleProgressIndicator());
        System.out.println("Is this what we want: " + p.getDisplayName());

        //Should we download?
        SettingsController settings = new SettingsController()
        {
            @Override
            public boolean getForceHttp()
            {
                return false;
            }

            @Override
            public void setForceHttp(boolean force)
            {

            }

            @Override
            public Channel getChannel()
            {
                return Channel.DEFAULT;
            }
        };
        Downloader downloader = new LegacyDownloader(sdk.getFileOp(), settings);
        sdkManager.loadSynchronously(0L, new ConsoleProgressIndicator(), downloader, settings);
        //RemotePackage latestRemoteSystemImage = sdk.getLatestRemotePackageForPrefix("system-images", false, new ConsoleProgressIndicator());
        //System.out.println("Remotes: " + sdkManager.getPackages().getRemotePackages().keySet());

        System.out.println("Remotes:");

        List<RemotePackage> remoteSystemImages = sdkManager.getPackages().getRemotePackagesForPrefix("system-images").stream()
                .filter(rp -> (SdkConstants.ABI_INTEL_ATOM.equals(((SysImgDetailsType)rp.getTypeDetails()).getAbi())))
                .filter(rp -> ("google_apis".equals(((SysImgDetailsType)rp.getTypeDetails()).getTag().getId())))
                .sorted(packageVersionComparator())
                .collect(Collectors.toList());
        
        for (RemotePackage remoteSystemImage : remoteSystemImages)
        {
            SysImgDetailsType details = (SysImgDetailsType)remoteSystemImage.getTypeDetails();
            System.out.println(remoteSystemImage.getPath() + " - " + details.getAndroidVersion());
        }
        //System.out.println("Latest remote: " + latestRemoteSystemImage.getDisplayName() + " " + ((SysImgDetailsType)latestRemoteSystemImage.getTypeDetails()).getAndroidVersion());
                  
        /*
        for (RemotePackage rp : sdkManager.getPackages().getRemotePackagesForPrefix("system-images"))
        {
            SysImgDetailsType details = (SysImgDetailsType)rp.getTypeDetails();
            System.out.println(rp.getDisplayName() + " - " + details.getAndroidVersion() + " - " + details.getCodename());
        }
        */
    }

    @Disabled("test is run manually only")
    @Test
    void testVideoRecord()
    throws Exception
    {
        //Emulator already running, just tunnel ports

        AndroidDebugBridge.initIfNeeded(false);

        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        Path osLocation = androidHome.resolve("platform-tools").resolve("adb");
        AndroidDebugBridge bridge = AndroidDebugBridge.createBridge(osLocation.toAbsolutePath().toString(), false);
        System.out.println(bridge);

        //Wait till bridge is connected
        while (!bridge.isConnected() || !bridge.hasInitialDeviceList())
        {
            Thread.sleep(100L);
        }

        List<IDevice> devices = Stream.of(bridge.getDevices()).filter(IDevice::isEmulator).collect(Collectors.toList());

        if (devices.isEmpty())
            throw new RuntimeException("No devices found");

        for (IDevice device : devices) 
        {
            System.out.println("AVD: " + device.getAvdName());
            
            long startTime = System.currentTimeMillis();
            
            System.out.println("Start recording");
            device.startScreenRecorder("/sdcard/galah.mp4", 
                    new ScreenRecorderOptions.Builder().build(),
                    new ConsoleOutputReceiver()
                    {
                        @Override
                        public boolean isCancelled() 
                        {
                            //Record for 10 seconds
                            //TODO how to solve the 3 minute limit problem?
                            long time = System.currentTimeMillis() - startTime;
                            return (time >= 10_000);
                            //return false;
                        }
                    });
            
            System.out.println("Came back from recording call");
            
            //Wait a bit for video to be fully written - to do this properly we should use file listing service and poll
            //until file size stops changing
            //Thread.sleep(3000L);


            FileListingService files = device.getFileListingService();
            FileEntry sdCardDir = Stream.of(files.getChildrenSync(files.getRoot())).filter(e -> e.isDirectory() && e.getName().equals("sdcard")).findFirst().orElseThrow(FileNotFoundException::new);
            
            
            for (int i = 0; i < 5; i++)
            {
                FileEntry videoFile = Stream.of(files.getChildrenSync(sdCardDir)).filter(e -> !e.isDirectory() && e.getName().equals("galah.mp4")).findFirst()
                                            .orElseThrow(FileNotFoundException::new);
                System.out.println("Video file size: " + videoFile.getSizeValue());
                Thread.sleep(100L);
            }
            




            Path localVideo = Paths.get("galah-android.mp4");
            device.pullFile("/sdcard/galah.mp4", localVideo.toAbsolutePath().toString());
            System.out.println("Saved video to " + localVideo.toAbsolutePath().toString());
        }
        
    }

    @Disabled("test is run manually only")
    @Test
    void testTunnelPortsToEmulator()
    throws Exception
    {
        //Emulator already running, just tunnel ports

        AndroidDebugBridge.initIfNeeded(false);

        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        Path osLocation = androidHome.resolve("platform-tools").resolve("adb");
        AndroidDebugBridge bridge = AndroidDebugBridge.createBridge(osLocation.toAbsolutePath().toString(), false);
        System.out.println(bridge);

        //Wait till bridge is connected
        while (!bridge.isConnected() || !bridge.hasInitialDeviceList())
        {
            Thread.sleep(100L);
        }

        List<IDevice> devices = Stream.of(bridge.getDevices()).filter(IDevice::isEmulator).collect(Collectors.toList());

        if (devices.isEmpty())
            throw new RuntimeException("No devices found");

        for (IDevice device : devices)
        {
            System.out.println("AVD: " + device.getAvdName());

            System.out.println("Forward ports...");
            //device.createForward(8090, 8090);

            BrowserBoxAdbHelper.createReverse(AndroidDebugBridge.getSocketAddress(), device,
                    String.format("tcp:%d", 8090),     //$NON-NLS-1$
                    String.format("tcp:%d", 8090));   //$NON-NLS-1$

            System.out.println("Port forward registered");
        }
    }

    @Disabled("test is run manually only")
    @Test
    void testDeleteAvd()
    throws Exception
    {
        AndroidDebugBridge.initIfNeeded(false);
        
        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("ANDROID_HOME"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        String avdName = "browserbox.causal.net.au_chromeX";

        Path osLocation = androidHome.resolve("platform-tools").resolve("adb");
        AndroidDebugBridge bridge = AndroidDebugBridge.createBridge(osLocation.toAbsolutePath().toString(), false);

        //Wait till bridge is connected
        while (!bridge.isConnected() || !bridge.hasInitialDeviceList())
        {
            Thread.sleep(100L);
        }
        
        AndroidSdkHandler sdk = AndroidSdkHandler.getInstance(androidHome.toFile());
        //AvdManager avdManager = AvdManager.getInstance(sdk, createLogger());
        FileOp fop = new MyFileOpImpl();
        AvdManager avdManager = AvdManager.getInstance(sdk, new File(AndroidLocation.getAvdFolder()), createLogger(), fop);

        AvdInfo avd = avdManager.getAvd(avdName, true);
        if (avd == null)
        {
            System.out.println("Creating AVD");
            
            //Create if needed
            SystemImageManager systemImageManager = sdk.getSystemImageManager(new ConsoleProgressIndicator());

            SystemImage systemImage = systemImageManager.getImages().stream()
                    .filter(si -> SdkConstants.ABI_INTEL_ATOM.equals(si.getAbiType()))
                    .filter(si -> SystemImage.GOOGLE_APIS_TAG.equals(si.getTag()))
                    .max(Comparator.comparing(SystemImage::getAndroidVersion))
                    .orElse(null);

            if (systemImage == null)
            {
                //TODO download form remote
                throw new RuntimeException("No system image locally found");
            }

            DeviceManager deviceManager = DeviceManager.createInstance(sdk.getLocation(), sdk.getAndroidFolder(), createLogger(), sdk.getFileOp());
            
            Device nexusDevice = deviceManager.getDevice("Nexus 6P", "Google");
            System.out.println("Nexus: " + nexusDevice);

            Map<String, String> hardwareConfig = DeviceManager.getHardwareProperties(nexusDevice);
            System.out.println("Hardware: " + hardwareConfig);

            hardwareConfig.put("hw.gpu.enabled", "yes");
            hardwareConfig.put("hw.gpu.mode", GpuMode.AUTO.getGpuSetting());
            hardwareConfig.put("hw.cpu.ncore", "4");
            hardwareConfig.put(HardwareProperties.HW_KEYBOARD, "yes"); //So real PC keyboard works

            Dimension screenSize = nexusDevice.getScreenSize(ScreenOrientation.PORTRAIT);
            hardwareConfig.put("hw.lcd.height", String.valueOf(screenSize.height));
            hardwareConfig.put("hw.lcd.width", String.valueOf(screenSize.width));

            File avdFolder = AvdInfo.getDefaultAvdFolder(avdManager, avdName, sdk.getFileOp(), true);
            avd = avdManager.createAvd(avdFolder, avdName, systemImage, null, null, null, hardwareConfig, nexusDevice.getBootProps(), false, false, false, createLogger());

            System.out.println("Created AVD: " + avd.getName());
        }

        //Run the emulator
        Path emulatorExe = androidHome.resolve("tools").resolve("emulator" + (Os.isFamily(Os.FAMILY_WINDOWS) ? ".exe" : ""));
        Commandline commandLine = new Commandline(emulatorExe.toAbsolutePath().toString());
        commandLine.addArguments(new String[] {"-avd", avdName});
        commandLine.addEnvironment("ANDROID_HOME", androidHome.toAbsolutePath().toString());
        CommandLineUtils.StringStreamConsumer out = new CommandLineUtils.StringStreamConsumer();
        CommandLineUtils.StringStreamConsumer err = new CommandLineUtils.StringStreamConsumer();

        //int exitCode = CommandLineUtils.executeCommandLine(commandLine, out, err);
        Callable<Integer> r = CommandLineUtils.executeCommandLineAsCallable(commandLine, null, out, err, -1);
        //r.call();

        System.out.println("Started the emulator, waiting for startup");
        Thread.sleep(60_000L);

        System.out.println("Kill the emulator");
        List<IDevice> devices = Stream.of(bridge.getDevices()).filter(IDevice::isEmulator).collect(Collectors.toList());
        for (IDevice device : devices)
        {
            System.out.println("Device: " + device.getName());
            System.out.println("AVD: " + device.getAvdName());

            //device.installPackage();

            System.out.println("Killing emulator...");
            EmulatorConsole console = EmulatorConsole.getConsole(device);
            console.kill();
        }
        
        Integer emulatorExitCode = r.call();
        System.out.println("Emulator exit: " + emulatorExitCode);

        System.out.println("Delete the AVD");
        boolean deleted = avdManager.deleteAvd(avd, createLogger());
        assertThat(deleted).as("Check AVD is deleted.").isTrue();
    }

    @Disabled("test is run manually only")
    @Test
    void testLaunchEmulator()
    throws Exception
    {
        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("ANDROID_HOME"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        String avdName = "browserbox.causal.net.au_chromeX";
        
        Path emulatorExe = androidHome.resolve("tools").resolve("emulator" + (Os.isFamily(Os.FAMILY_WINDOWS) ? ".exe" : ""));
        Commandline commandLine = new Commandline(emulatorExe.toAbsolutePath().toString());
        commandLine.addArguments(new String[] {"-avd", avdName});
        commandLine.addEnvironment("ANDROID_HOME", androidHome.toAbsolutePath().toString());
        CommandLineUtils.StringStreamConsumer out = new CommandLineUtils.StringStreamConsumer();
        CommandLineUtils.StringStreamConsumer err = new CommandLineUtils.StringStreamConsumer();
        
        //int exitCode = CommandLineUtils.executeCommandLine(commandLine, out, err);
        Callable<Integer> r = CommandLineUtils.executeCommandLineAsCallable(commandLine, null, out, err, -1);
        //r.call();
        
        System.out.println("Started the emulator");
        Thread.sleep(10_000L);
        
        System.out.println("Waiting for it to finish");
        int exitCode = r.call();
        
        System.out.println("Exit code: " + exitCode);
    }

    @Disabled("test is run manually only")
    @Test
    void testCreateAvd()
    throws Exception
    {
        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);

        AndroidSdkHandler sdk = AndroidSdkHandler.getInstance(androidHome.toFile());
        AvdManager avdManager = AvdManager.getInstance(sdk, createLogger());

        String avdName = "browserbox.causal.net.au_chromeX";
        
        //System.out.println("Existing: " + avdManager.getValidAvds()[0].getProperties());
        
        AvdInfo avd = avdManager.getAvd(avdName, true);
        if (avd != null)
        {
            System.out.println("Existing AVD found, not creating");
            return;
        }

        SystemImageManager systemImageManager = sdk.getSystemImageManager(new ConsoleProgressIndicator());
        
        /*
        Collection<SystemImage> availableImages = systemImageManager.getImages().stream()
                .filter(si -> SdkConstants.ABI_INTEL_ATOM.equals(si.getAbiType()))
                .filter(si -> SystemImage.GOOGLE_APIS_TAG.equals(si.getTag()))
                .sorted(Comparator.comparing(SystemImage::getAndroidVersion))
                .collect(Collectors.toList()); 
        for (SystemImage si : availableImages)
        {
            System.out.println(si.getPackage().getPath());
        }
        */
        
        SystemImage systemImage = systemImageManager.getImages().stream()
                                    .filter(si -> SdkConstants.ABI_INTEL_ATOM.equals(si.getAbiType()))
                                    .filter(si -> SystemImage.GOOGLE_APIS_TAG.equals(si.getTag()))
                                    .max(Comparator.comparing(SystemImage::getAndroidVersion))
                                    .orElse(null);

        if (systemImage == null)
        {
            //TODO download form remote
            throw new RuntimeException("No system image locally found");
        }

        DeviceManager deviceManager = DeviceManager.createInstance(sdk.getLocation(), sdk.getAndroidFolder(), createLogger(), sdk.getFileOp());
        
        /*
        Collection<Device> devices = deviceManager.getDevices(DeviceManager.ALL_DEVICES);
        for (Device device : devices)
        {
            System.out.println(device);
        }
        */
        
        Device nexusDevice = deviceManager.getDevice("Nexus 6P", "Google");
        System.out.println("Nexus: " + nexusDevice);
        
        Map<String, String> hardwareConfig = DeviceManager.getHardwareProperties(nexusDevice);
        System.out.println("Hardware: " + hardwareConfig);
        
        hardwareConfig.put("hw.gpu.enabled", "yes");
        hardwareConfig.put("hw.gpu.mode", GpuMode.AUTO.getGpuSetting());
        hardwareConfig.put("hw.cpu.ncore", "4");
        hardwareConfig.put(HardwareProperties.HW_KEYBOARD, "yes"); //So real PC keyboard works
        
        Dimension screenSize = nexusDevice.getScreenSize(ScreenOrientation.PORTRAIT);
        hardwareConfig.put("hw.lcd.height", String.valueOf(screenSize.height));
        hardwareConfig.put("hw.lcd.width", String.valueOf(screenSize.width));
        
        File avdFolder = AvdInfo.getDefaultAvdFolder(avdManager, avdName, sdk.getFileOp(), true);
        avd = avdManager.createAvd(avdFolder, avdName, systemImage, null, null, null, hardwareConfig, nexusDevice.getBootProps(), false, false, false, createLogger());
        
        System.out.println("Created AVD: " + avd.getName());
        
        System.out.println("Final: " + avd.getProperties());
    }

    @Disabled("test is run manually only")
    @Test
    void testAvdManager()
    throws Exception
    {
        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        
        AndroidSdkHandler sdk = AndroidSdkHandler.getInstance(androidHome.toFile());
        AvdManager avdManager = AvdManager.getInstance(sdk, createLogger());
        System.out.println("Available AVDs:");
        for (AvdInfo avd : avdManager.getValidAvds())
        {
            System.out.println(avd.getName());
        }
        //avdManager.createAvd(...)
        
        SystemImageManager systemImageManager = sdk.getSystemImageManager(new ConsoleProgressIndicator());
        System.out.println("Available system images:");
        for (SystemImage image : systemImageManager.getImages())
        {
            System.out.println(image.getAndroidVersion() + " " + image.getAbiType() + " " + image.getTag());
        }
        
        RepoManager sdkManager = sdk.getSdkManager(new ConsoleProgressIndicator());
        System.out.println("Packages:\n" + sdkManager.getPackages().getLocalPackages().keySet().stream().collect(Collectors.joining("\n")));
        LocalPackage p = sdk.getLatestLocalPackageForPrefix("system-images", false, new ConsoleProgressIndicator());
        System.out.println("Is this what we want: " + p.getDisplayName());
        
        //Should we download?
        SettingsController settings = new SettingsController()
        {
            @Override
            public boolean getForceHttp()
            {
                return false;
            }

            @Override
            public void setForceHttp(boolean force)
            {

            }

            @Override
            public Channel getChannel()
            {
                return Channel.DEFAULT;
            }
        };
        Downloader downloader = new LegacyDownloader(sdk.getFileOp(), settings);
        sdkManager.loadSynchronously(0L, new ConsoleProgressIndicator(), downloader, settings);
        //RemotePackage latestRemoteSystemImage = sdk.getLatestRemotePackageForPrefix("system-images", false, new ConsoleProgressIndicator());
        //System.out.println("Remotes: " + sdkManager.getPackages().getRemotePackages().keySet());
        
        System.out.println("Remotes:");

        RemotePackage latestRemoteSystemImage = sdkManager.getPackages().getRemotePackagesForPrefix("system-images").stream()
                                                .filter(rp -> (SdkConstants.ABI_INTEL_ATOM.equals(((SysImgDetailsType)rp.getTypeDetails()).getAbi())))
                                                .filter(rp -> ("google_apis".equals(((SysImgDetailsType)rp.getTypeDetails()).getTag().getId()))) 
                                                .max(packageVersionComparator()).orElseThrow(() -> new RuntimeException("No versions available"));
        System.out.println("Latest remote: " + latestRemoteSystemImage.getDisplayName() + " " + ((SysImgDetailsType)latestRemoteSystemImage.getTypeDetails()).getAndroidVersion());
                  
        /*
        for (RemotePackage rp : sdkManager.getPackages().getRemotePackagesForPrefix("system-images"))
        {
            SysImgDetailsType details = (SysImgDetailsType)rp.getTypeDetails();
            System.out.println(rp.getDisplayName() + " - " + details.getAndroidVersion() + " - " + details.getCodename());
        }
        */

        InstallerFactory installerFactory = SdkInstallerUtil.findBestInstallerFactory(latestRemoteSystemImage, sdk);
        Installer installer = installerFactory.createInstaller(latestRemoteSystemImage, sdkManager, downloader, sdk.getFileOp());
        
        System.out.println("Installing package...");
        long startTime = System.nanoTime();
        installer.prepare(new ConsoleProgressIndicator());
        installer.complete(new ConsoleProgressIndicator());
        long time = System.nanoTime() - startTime;
        System.out.println("Install complete in " + time / 1.0E9 + "s");
        
    }
    
    private Comparator<RemotePackage> packageVersionComparator()
    {
        return Comparator.comparing(rp -> ((SysImgDetailsType)rp.getTypeDetails()).getAndroidVersion());
    }

    @Disabled("test is run manually only")
    @Test
    void test()
    throws Exception
    {
        AndroidDebugBridge.initIfNeeded(false);

        Path androidHome = Paths.get(MoreObjects.firstNonNull(System.getenv("android_home"), "/Users/prunge/java/android-sdk-macosx"));
        System.out.println("Android home: " + androidHome);
        Path osLocation = androidHome.resolve("platform-tools").resolve("adb");
        AndroidDebugBridge bridge = AndroidDebugBridge.createBridge(osLocation.toAbsolutePath().toString(), false);
        System.out.println(bridge);
        
        //Wait till bridge is connected
        while (!bridge.isConnected() || !bridge.hasInitialDeviceList())
        {
            Thread.sleep(100L);
        }
        
        System.out.println(Arrays.asList(bridge.getDevices()));

        List<IDevice> devices = Stream.of(bridge.getDevices()).filter(IDevice::isEmulator).collect(Collectors.toList());
        
        if (devices.isEmpty())
            throw new RuntimeException("No devices found");
        
        for (IDevice device : devices) 
        {
            System.out.println("Device: " + device.getName());
            System.out.println("AVD: " + device.getAvdName());
         
            //device.installPackage();
            
            System.out.println("Killing emulator...");
            EmulatorConsole console = EmulatorConsole.getConsole(device);
            console.kill();
        }
    }

    private static ILogger createLogger() {
        return new IReaderLogger() {
            @Override
            public void error(Throwable t, String errorFormat, Object... args) {
                if (errorFormat != null) {
                    System.err.printf("Error: " + errorFormat, args);
                    if (!errorFormat.endsWith("\n"))
                        System.err.print("\n");
                }
                if (t != null)
                    System.err.printf("Error: %s\n", t.getMessage());
            }

            @Override
            public void warning(@NonNull String warningFormat, Object... args) {
                System.out.printf("Warning: " + warningFormat, args);
                if (!warningFormat.endsWith("\n"))
                    System.out.print("\n");
            }

            @Override
            public void info(@NonNull String msgFormat, Object... args) {
                System.out.printf(msgFormat, args);
            }

            @Override
            public void verbose(@NonNull String msgFormat, Object... args) {
                System.out.printf(msgFormat, args);
            }

            /**
             * Used by UpdaterData.acceptLicense() to prompt for license acceptance
             * when updating the SDK from the command-line.
             * <p/>
             * {@inheritDoc}
             */
            @Override
            public int readLine(byte[] inputBuffer) throws IOException {
                return System.in.read(inputBuffer);
            }
        };
    }
    
    private static class MyFileOpImpl extends FileOpImpl
    {
        @Override
        public boolean delete(File file) 
        {
            try 
            {
                DosFileAttributeView attributeView = Files.getFileAttributeView(toPath(file), DosFileAttributeView.class);
                if (attributeView != null)
                {
                    DosFileAttributes attributes = attributeView.readAttributes();
                    if (attributes.isReadOnly())
                    {
                        System.out.println("Warning: file " + file.getAbsolutePath() + " is read only, resetting...");
                        attributeView.setReadOnly(false);
                    }
                }
                Files.delete(toPath(file));
                return true;
            } 
            catch (IOException e) 
            {
                e.printStackTrace(); //Some debugging for deletion failures
                return false;
            }
        }
    }
    
    private static class ConsoleOutputReceiver implements IShellOutputReceiver
    {
        @Override
        public void addOutput(byte[] data, int offset, int length) 
        {
            System.out.write(data, offset, length);
        }

        @Override
        public void flush() 
        {
            System.out.flush();
        }

        @Override
        public boolean isCancelled() 
        {
            return false;
        }
    }
}
