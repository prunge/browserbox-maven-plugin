package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.versionstore.DownloadTestTools;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

class TestWebIEWithFallbackRegistryLive
{
    private static final Logger log = LoggerFactory.getLogger(TestWebIEWithFallbackRegistryLive.class);

    /**
     * Must have at least some versions in the list.
     */
    @Test
    void hasSomeVersions()
    throws Exception
    {
        WebIEWithEdgeFallbackRegistry resolver = new WebIEWithEdgeFallbackRegistry();
        Collection<String> versions = resolver.readAllVersions(new Query());
        log.info("Versions: " + versions);
        
        assertThat(versions).contains("11");
    }

    /**
     * Check that each download in the list gives a download URL whose file is of a decent size suitable for a VM.
     */
    @Test
    void eachDownloadHasDecentlySizedFile()
    throws Exception
    {
        WebIEWithEdgeFallbackRegistry resolver = new WebIEWithEdgeFallbackRegistry();
        DownloadTestTools.verifyAllDownloadsAreOfADecentSize(resolver, 100_000_000);
    }
}
