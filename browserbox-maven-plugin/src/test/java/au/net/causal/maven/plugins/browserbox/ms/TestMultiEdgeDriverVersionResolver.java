package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ms.EdgeDriverVersionResolver.Version;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class TestMultiEdgeDriverVersionResolver
{
    @Test
    void resolveExact()
    throws Exception
    {
        Properties props1 = new Properties();
        Properties props2 = new Properties();
        
        props1.setProperty("15063", "3.15063");
        props1.setProperty("16384", "4.16384.201712");
        
        props2.setProperty("14000", "2.14000");
        props2.setProperty("16384", "4.16384");
        
        MultiEdgeDriverVersionResolver resolver = new MultiEdgeDriverVersionResolver(
                new MyPropertiesEdgeDriverVersionResolver(props1), 
                new MyPropertiesEdgeDriverVersionResolver(props2));
                
        assertThat(resolver.resolveWithFallback("14.14000").getRawVersion()).isEqualTo("2.14000");
        assertThat(resolver.resolveWithFallback("15.15063").getRawVersion()).isEqualTo("3.15063");
        assertThat(resolver.resolveWithFallback("16.16384").getRawVersion()).isEqualTo("4.16384.201712");
        assertThat(resolver.resolveWithFallback("17.17345").getRawVersion()).isEqualTo("4.16384.201712");
    }

    @Test
    void resolveExactWithOndemand()
    throws Exception
    {
        Properties props1 = new Properties();
        Properties props2 = new Properties();

        props1.setProperty("15063", "3.15063");
        props1.setProperty("16384", "4.16384.201712");

        props2.setProperty("14000", "2.14000");
        props2.setProperty("16384", "4.16384");
        props2.setProperty("17763", "ondemand:7.17763");

        MultiEdgeDriverVersionResolver resolver = new MultiEdgeDriverVersionResolver(
                new MyPropertiesEdgeDriverVersionResolver(props1),
                new MyPropertiesEdgeDriverVersionResolver(props2));

        assertThat(resolver.resolveWithFallback("14.14000").getRawVersion()).isEqualTo("2.14000");
        assertThat(resolver.resolveWithFallback("15.15063").getRawVersion()).isEqualTo("3.15063");
        assertThat(resolver.resolveWithFallback("16.16384").getRawVersion()).isEqualTo("4.16384.201712");
        assertThat(resolver.resolveWithFallback("17.17763").getRawVersion()).isEqualTo("ondemand:7.17763");
        assertThat(resolver.resolveWithFallback("18.18900").getRawVersion()).isEqualTo("ondemand:7.17763");

        Version v13 = resolver.resolveWithFallback("13.10586");

        //version 13 should not be found
        assertThat(v13).isNull();
    }

    private static class MyPropertiesEdgeDriverVersionResolver extends PropertiesEdgeDriverVersionResolver
    {
        private final Properties props;

        public MyPropertiesEdgeDriverVersionResolver(Properties props)
        {
            this.props = props;
        }

        @Override
        protected Properties readProperties()
        throws BrowserBoxException
        {
            return props;
        }
    }
}
