package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class TestImageReference
{
    @Test
    void testParse()
    {
        ImageReference ref = ImageReference.parse("selenium/standalone-firefox-debug:3.14.0-europium");
        assertThat(ref.getRepository()).isEqualTo("selenium/standalone-firefox-debug");
        assertThat(ref.getTag().getName()).isEqualTo("3.14.0-europium");
        assertThat(ref.getName()).isEqualTo("selenium/standalone-firefox-debug:3.14.0-europium");
    }

    @Test
    void testParseNoTag()
    {
        ImageReference ref = ImageReference.parse("repository-by-itself");
        assertThat(ref.getRepository()).isEqualTo("repository-by-itself");
        assertThat(ref.getTag().getName()).isEmpty();

        //Garbage in, garbage out, but it's consistent
        assertThat(ref.getName()).isEqualTo("repository-by-itself");
    }
}
