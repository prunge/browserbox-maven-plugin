package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionScanner.TaggedBrowserVersion;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.List;

@Disabled("manually run test")
class TestBrowserVersionScanner
{
    @TempDir
    Path tempDir;
    
    @Test
    void test()
    throws BrowserBoxException
    {
        Log log = new SystemStreamLog();
        BrowserVersionScanner mapper = new BrowserVersionScanner(log);

        List<? extends TaggedBrowserVersion> results =
            mapper.process("selenium/standalone-firefox-debug", new FirefoxBrowserVersionReader(tempDir));
        
        for (TaggedBrowserVersion result : results)
        {
            System.out.println(result);
        }
    }
    
    @Test
    void testReadTags()
    {
        Log log = new SystemStreamLog();
        BrowserVersionScanner mapper = new BrowserVersionScanner(log);
        List<? extends Tag> tags = mapper.readTags("selenium/standalone-firefox-debug");
        tags.forEach(System.out::println);
    }
}
