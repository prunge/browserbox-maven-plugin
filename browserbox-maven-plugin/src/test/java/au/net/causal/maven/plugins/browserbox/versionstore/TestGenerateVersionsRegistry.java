package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;

class TestGenerateVersionsRegistry
{
    @TempDir
    Path temp;

    private GenerateVersionsContext context;
    private URL knownWorkingUrl;
    
    private Set<String> nonWorkingUrls = new HashSet<>();

    @BeforeEach
    private void setUpContext()
    throws IOException, BrowserBoxException
    {
        Path versionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path imagesPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path knownVersionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path downloadsPath =Files.createTempFile(temp, "browserbox", ".tmp");
        Path driverVersionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path driverCompatibilityPath = Files.createTempFile(temp, "browserbox", ".tmp");
        context = new GenerateVersionsContext(versionsPath, imagesPath, knownVersionsPath, downloadsPath,
                                              driverVersionsPath, ImmutableMap.of(), driverCompatibilityPath);
    }

    @BeforeEach
    private void setUpWorkingUrl()
    throws IOException
    {
        knownWorkingUrl = VersionStoreFileTools.createTempFile(temp, 10_000_000).toUri().toURL();
    }

    @Test
    void addToEmptyProperties()
    throws IOException, BrowserBoxException
    {
        GenerateVersionsRegistry registry = new GenerateVersionsRegistryX(context);
        registry.saveItems(ImmutableList.of(
                new Item("1.0", workingUrl("http://galah.galah.galah/file1")),
                new Item("1.1", workingUrl("http://galah.galah.galah/file2"))
        ));

        Properties results = registry.readDownloadProperties();
        assertThat(results.entrySet()).hasSize(2);
        assertThat(results).containsEntry("1.0", "http://galah.galah.galah/file1")
                           .containsEntry("1.1", "http://galah.galah.galah/file2");
    }
    
    @Test
    void addToExistingProperties()
    throws IOException, BrowserBoxException
    {
        GenerateVersionsRegistry registry = new GenerateVersionsRegistryX(context);

        context.getDownloads().setProperty("1.3", "http://galah.galah.galah/file3");

        registry.saveItems(ImmutableList.of(
                new Item("1.0", workingUrl("http://galah.galah.galah/file1")),
                new Item("1.1", workingUrl("http://galah.galah.galah/file2"))
        ));

        Properties results = registry.readDownloadProperties();
        assertThat(results.entrySet()).hasSize(3);
        assertThat(results).containsEntry("1.0", "http://galah.galah.galah/file1")
                           .containsEntry("1.1", "http://galah.galah.galah/file2")
                           .containsEntry("1.3", "http://galah.galah.galah/file3");
    }

    @Test
    void replaceExistingProperty()
    throws IOException, BrowserBoxException
    {
        GenerateVersionsRegistry registry = new GenerateVersionsRegistryX(context);

        context.getDownloads().setProperty("1.0", "http://galah.galah.galah/file1");

        registry.saveItems(ImmutableList.of(
                new Item("1.0", workingUrl("http://galah.galah.galah/file2")),
                new Item("1.1", workingUrl("http://galah.galah.galah/file3"))
        ));

        Properties results = registry.readDownloadProperties();
        assertThat(results.entrySet()).hasSize(2);
        assertThat(results).containsEntry("1.0", "http://galah.galah.galah/file2")
                           .containsEntry("1.1", "http://galah.galah.galah/file3");
    }

    @Test
    void nonWorkingNewUrlsAreFilteredOut()
    throws IOException, BrowserBoxException
    {
        GenerateVersionsRegistry registry = new GenerateVersionsRegistryX(context);

        context.getDownloads().setProperty("1.3", "http://galah.galah.galah/file3");

        registry.saveItems(ImmutableList.of(
                new Item("1.0", nonWorkingUrl("http://galah.galah.galah/file1")),
                new Item("1.1", workingUrl("http://galah.galah.galah/file2"))
        ));

        Properties results = registry.readDownloadProperties();
        assertThat(results.entrySet()).hasSize(2);
        assertThat(results).containsEntry("1.1", "http://galah.galah.galah/file2")
                           .containsEntry("1.3", "http://galah.galah.galah/file3");
    }

    @Test
    void nonWorkingExistingUrlsAreFilteredOut()
    throws IOException, BrowserBoxException
    {
        GenerateVersionsRegistry registry = new GenerateVersionsRegistryX(context);

        context.getDownloads().setProperty("1.3", nonWorkingUrl("http://galah.galah.galah/file3").toExternalForm());
        context.getDownloads().setProperty("1.4", workingUrl("http://galah.galah.galah/file4").toExternalForm());

        registry.saveItems(ImmutableList.of(
                new Item("1.0", workingUrl("http://galah.galah.galah/file1")),
                new Item("1.1", workingUrl("http://galah.galah.galah/file2"))
        ));

        Properties results = registry.readDownloadProperties();
        assertThat(results.entrySet()).hasSize(3);
        assertThat(results).containsEntry("1.0", "http://galah.galah.galah/file1")
                           .containsEntry("1.1", "http://galah.galah.galah/file2")
                           .containsEntry("1.4", "http://galah.galah.galah/file4");
    }

    private URL workingUrl(String spec)
    throws MalformedURLException
    {
        return new URL(null, spec, new AlwaysWorkingURLStreamHandler());
    }

    private URL nonWorkingUrl(String spec)
    throws MalformedURLException
    {
        nonWorkingUrls.add(spec);
        return new URL(null, spec, new NeverWorkingURLStreamHandler());
    }

    private class GenerateVersionsRegistryX extends GenerateVersionsRegistry
    {
        public GenerateVersionsRegistryX(GenerateVersionsContext generateContext)
        {
            super(generateContext, new SystemStreamLog());
        }

        @Override
        protected URL createUrlForDownload(String urlString) throws MalformedURLException
        {
            if (nonWorkingUrls.contains(urlString))
                return nonWorkingUrl(urlString);
            else
                return workingUrl(urlString);
        }
    }

    private class AlwaysWorkingURLStreamHandler extends URLStreamHandler
    {
        @Override
        protected URLConnection openConnection(URL u, Proxy p) throws IOException
        {
            return openConnection(u);
        }

        @Override
        protected URLConnection openConnection(URL u) throws IOException
        {
            return knownWorkingUrl.openConnection();
        }
    }

    private static class NeverWorkingURLStreamHandler extends URLStreamHandler
    {
        private final URL resourceThatNeverExists = new URL("http://galah.galah.galah");

        public NeverWorkingURLStreamHandler() throws MalformedURLException
        {
        }

        @Override
        protected URLConnection openConnection(URL u, Proxy p) throws IOException
        {
            return openConnection(u);
        }

        @Override
        protected URLConnection openConnection(URL u) throws IOException
        {
            return resourceThatNeverExists.openConnection();
        }
    }
}
