package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestBrowserVersionTools
{
    @Test
    void createVersionRangeThreeTokens()
    {
        String s = BrowserVersionTools.createVersionRangeString(1, 2, 3);
        assertThat(s).isEqualTo("[1.2.3, 1.2.4)");
    }

    @Test
    void createVersionRangeOneToken()
    {
        String s = BrowserVersionTools.createVersionRangeString(23);
        assertThat(s).isEqualTo("[23, 24)");
    }

    @Test
    void findClosestVersionLessThan()
    throws BrowserBoxException
    {
        BrowserVersionTools tools = new BrowserVersionTools();

        List<String> versions = ImmutableList.of("1.0", "1.1", "2.0", "3.0");

        //Simple results
        assertThat(tools.findClosestVersionLessThan("1.0.1", versions)).isEqualTo("1.0");
        assertThat(tools.findClosestVersionLessThan("1.2", versions)).isEqualTo("1.1");
        assertThat(tools.findClosestVersionLessThan("1.9.9", versions)).isEqualTo("1.1");
        assertThat(tools.findClosestVersionLessThan("2.0.1", versions)).isEqualTo("2.0");
        assertThat(tools.findClosestVersionLessThan("3.1", versions)).isEqualTo("3.0");
        assertThat(tools.findClosestVersionLessThan("4.0", versions)).isEqualTo("3.0");

        //Should not find since it's lower than lowest
        assertThat(tools.findClosestVersionLessThan("0.9", versions)).isNull();

        //Matching values
        assertThat(tools.findClosestVersionLessThan("1.0", versions)).isNull();
        assertThat(tools.findClosestVersionLessThan("1.1", versions)).isEqualTo("1.0");
        assertThat(tools.findClosestVersionLessThan("2.0", versions)).isEqualTo("1.1");
        assertThat(tools.findClosestVersionLessThan("3.0", versions)).isEqualTo("2.0");

        //Some not-fully-numeric versions
        assertThat(tools.findClosestVersionLessThan("3.0b10", versions)).isEqualTo("2.0");
        assertThat(tools.findClosestVersionLessThan("70.0b9", versions)).isEqualTo("3.0");
    }
}
