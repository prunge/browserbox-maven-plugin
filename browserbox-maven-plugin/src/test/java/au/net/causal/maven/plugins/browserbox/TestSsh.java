package au.net.causal.maven.plugins.browserbox;

import com.google.common.io.ByteStreams;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

@Disabled("test is run manually only")
class TestSsh
{
    @Test
    void test()
    throws Exception
    {
        String user = "IEUser";
        String password = "Passw0rd!";
        String host = "localhost";
        int port = 2222;

        JSch jsch = new JSch();
        Session sshSession = jsch.getSession(user, host, port);
        sshSession.setPassword(password);

        //Disables known hosts checking - good enough for Selenium testing
        sshSession.setConfig("StrictHostKeyChecking", "no");

        sshSession.connect();




        String command = "echo Good Morning 1>&2";
        
        Channel channel = sshSession.openChannel("exec");
        ((ChannelExec)channel).setCommand(command);
        
        InputStream commandOutput = channel.getInputStream();
        InputStream commandErr = ((ChannelExec) channel).getErrStream();
        channel.connect();

        ByteStreams.copy(commandOutput, System.out);
        ByteStreams.copy(commandErr, System.err);
        System.out.println("Exit status: " + channel.getExitStatus());
        
        channel.disconnect();
        
        
        
        
        //Verify connect
        //TODO

        sshSession.disconnect();
    }
}
