package au.net.causal.maven.plugins.browserbox;

import org.codehaus.plexus.util.IOUtil;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientRequest;
import org.glassfish.jersey.client.ClientResponse;
import org.glassfish.jersey.client.spi.AsyncConnectorCallback;
import org.glassfish.jersey.client.spi.Connector;
import org.glassfish.jersey.client.spi.ConnectorProvider;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Future;

/**
 * Utilities for unit testing with Jersey.
 */
public final class JerseyTestTools 
{
    public static ClientBuilder forJsonResponseFromResource(Class<?> resourceBase, String resourceName)
    {
        URL resourceUrl = resourceBase.getResource(resourceName);
        if (resourceUrl == null)
            throw new IOError(new FileNotFoundException("Missing resource: " + resourceName));

        try (InputStream is = resourceUrl.openStream())
        {
            byte[] resourceData = IOUtil.toByteArray(is);
            return ClientBuilder.newBuilder()
                    .withConfig(new ClientConfig().connectorProvider(new FixedResponseConnectorProvider(resourceData, MediaType.APPLICATION_JSON_TYPE)));

        }
        catch (IOException e)
        {
            throw new IOError(e);
        }

    }

    public static class FixedResponseConnectorProvider implements ConnectorProvider
    {
        private final byte[] responseContent;
        private final MediaType contentType;

        public FixedResponseConnectorProvider(byte[] responseContent, MediaType contentType)
        {
            this.responseContent = responseContent;
            this.contentType = contentType;
        }

        @Override
        public Connector getConnector(Client client, Configuration configuration)
        {
            return new FixedResponseConnector(responseContent, contentType);
        }
    }

    public static class FixedResponseConnector implements Connector
    {
        private final byte[] responseContent;
        private final MediaType contentType;

        public FixedResponseConnector(byte[] responseContent, MediaType contentType)
        {
            this.responseContent = responseContent;
            this.contentType = contentType;
        }

        @Override
        public ClientResponse apply(ClientRequest clientRequest)
        {
            ClientResponse responseContext = new ClientResponse(Response.Status.OK, clientRequest);
            responseContext.setEntityStream(new ByteArrayInputStream(responseContent));
            responseContext.header(HttpHeaders.CONTENT_TYPE, contentType.toString());
            return responseContext;
        }

        @Override
        public Future<?> apply(ClientRequest clientRequest, AsyncConnectorCallback asyncConnectorCallback)
        {
            throw new UnsupportedOperationException("async not implemented");
        }

        @Override
        public String getName()
        {
            return "test";
        }

        @Override
        public void close()
        {
        }
    }
}
