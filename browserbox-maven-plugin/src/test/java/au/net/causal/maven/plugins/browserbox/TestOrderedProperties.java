package au.net.causal.maven.plugins.browserbox;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

class TestOrderedProperties
{
    @Test
    void propertiesAreOrderedWhenStored()
    throws IOException
    {
        Properties p = new OrderedProperties();
        
        p.setProperty("a", "*");
        p.setProperty("c", "*");
        p.setProperty("b", "*");
        p.setProperty("d", "*");
        p.setProperty("e", "*");
        p.setProperty("g", "*");
        p.setProperty("f", "*");
        p.setProperty("one", "*");
        p.setProperty("two", "*");
        p.setProperty("three", "*");
        p.setProperty("four", "*");
        p.setProperty("five", "*");
        
        StringWriter buf = new StringWriter();
        p.store(buf, null);

        List<String> lines = new BufferedReader(new StringReader(buf.toString()))
                                    .lines()
                                    .filter(line -> !line.trim().startsWith("#"))
                                    .collect(Collectors.toList());
        
        assertThat(lines).contains(
           "a=*",
           "b=*",
           "c=*",
           "d=*",
           "e=*",
           "f=*",
           "five=*",
           "four=*",
           "g=*",
           "one=*",
           "three=*",
           "two=*"
        );
    }
}
