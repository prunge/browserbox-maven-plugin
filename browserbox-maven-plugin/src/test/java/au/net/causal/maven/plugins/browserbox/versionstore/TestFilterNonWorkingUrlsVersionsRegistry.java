package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

class TestFilterNonWorkingUrlsVersionsRegistry
{
    @TempDir
    Path temp;
    
    @Test
    void missingFilesAreFilteredOut()
    throws IOException, BrowserBoxException
    {
        //Should always exist - must be above size threshold in the filter
        URL tempFile = VersionStoreFileTools.createTempFile(temp, 10_000_000).toUri().toURL();
        
        //Does not exist
        URL missingFile = new URL("http://galah.galah.galah/idonotexist");
        
        FilterNonWorkingUrlsVersionsRegistry registry = new FilterNonWorkingUrlsVersionsRegistry(
                new SimpleVersionRegistry(ImmutableMap.of("1.0", missingFile, "2.0", tempFile)), new SystemStreamLog());
        
        Collection<? extends Item> results = registry.readAllItems(new Query());

        assertThat(results).hasSize(1);
        Item result = results.iterator().next();
        assertThat(result.getVersion()).isEqualTo("2.0");
        assertThat(result.getUrl()).isEqualTo(tempFile);
    }
}
