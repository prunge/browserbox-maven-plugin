package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

class TestCompositeVersionRegistry
{
    @Test
    void testEmpty()
    throws BrowserBoxException
    {
        CompositeVersionRegistry registry = new CompositeVersionRegistry();
        assertThat(registry.readAllItems(new Query())).isEmpty();
        assertThat(registry.readAllVersions(new Query())).isEmpty();
    }

    @Test
    void testSingleChild()
    throws IOException, BrowserBoxException
    {
        VersionRegistry r1 = new SimpleVersionRegistry(ImmutableMap.of(
                "1.0", new URL("http://galah.galah.galah/v1"),
                "2.0", new URL("http://galah.galah.galah/v2")));
        CompositeVersionRegistry registry = new CompositeVersionRegistry(r1);

        assertThat(registry.readAllVersions(new Query())).containsExactlyInAnyOrder("1.0", "2.0");

        Map<String, URL> results = registry.readAllItems(new Query()).stream()
                                           .collect(Collectors.toMap(Item::getVersion, Item::getUrl));

        assertThat(results).containsEntry("1.0", new URL("http://galah.galah.galah/v1"))
                           .containsEntry("2.0", new URL("http://galah.galah.galah/v2"));
    }

    @Test
    void multipleChildrenResultsAreCombined()
    throws IOException, BrowserBoxException
    {
        VersionRegistry r1 = new SimpleVersionRegistry(ImmutableMap.of(
                "1.0", new URL("http://galah.galah.galah/v1"),
                "2.0", new URL("http://galah.galah.galah/v2")));
        VersionRegistry r2 = new SimpleVersionRegistry(ImmutableMap.of(
                "3.0", new URL("http://galah.galah.galah/v3"),
                "4.0", new URL("http://galah.galah.galah/v4")));
        CompositeVersionRegistry registry = new CompositeVersionRegistry(r1, r2);

        assertThat(registry.readAllVersions(new Query())).containsExactlyInAnyOrder("1.0", "2.0", "3.0", "4.0");

        Map<String, URL> results = registry.readAllItems(new Query()).stream()
                                           .collect(Collectors.toMap(Item::getVersion, Item::getUrl));

        assertThat(results).containsEntry("1.0", new URL("http://galah.galah.galah/v1"))
                           .containsEntry("2.0", new URL("http://galah.galah.galah/v2"))
                           .containsEntry("3.0", new URL("http://galah.galah.galah/v3"))
                           .containsEntry("4.0", new URL("http://galah.galah.galah/v4"));
    }

    @Test
    void multipleChildrenResultsOfSameVersionAreNotDuplicated()
    throws IOException, BrowserBoxException
    {
        VersionRegistry r1 = new SimpleVersionRegistry(ImmutableMap.of(
                "1.0", new URL("http://galah.galah.galah/v1"),
                "2.0", new URL("http://galah.galah.galah/v2")));
        VersionRegistry r2 = new SimpleVersionRegistry(ImmutableMap.of(
                "1.0", new URL("http://galah.galah.galah/other/v1"), //Dupe, won't be used
                "3.0", new URL("http://galah.galah.galah/v3")));
        CompositeVersionRegistry registry = new CompositeVersionRegistry(r1, r2);

        assertThat(registry.readAllVersions(new Query())).containsExactlyInAnyOrder("1.0", "2.0", "3.0");

        Map<String, URL> results = registry.readAllItems(new Query()).stream()
                                           .collect(Collectors.toMap(Item::getVersion, Item::getUrl));

        assertThat(results).containsEntry("1.0", new URL("http://galah.galah.galah/v1"))
                           .containsEntry("2.0", new URL("http://galah.galah.galah/v2"))
                           .containsEntry("3.0", new URL("http://galah.galah.galah/v3"));
    }
}
