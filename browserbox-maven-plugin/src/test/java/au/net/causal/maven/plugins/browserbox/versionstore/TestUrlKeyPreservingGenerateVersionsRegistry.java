package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import static org.assertj.core.api.Assertions.*;

class TestUrlKeyPreservingGenerateVersionsRegistry
{
    @TempDir
    Path temp;

    private GenerateVersionsContext context;

    @BeforeEach
    private void setUpContext()
    throws IOException, BrowserBoxException
    {
        Path versionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path imagesPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path knownVersionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path downloadsPath =Files.createTempFile(temp, "browserbox", ".tmp");
        Path driverVersionsPath = Files.createTempFile(temp, "browserbox", ".tmp");
        Path driverCompatibilityPath = Files.createTempFile(temp, "browserbox", ".tmp");
        context = new GenerateVersionsContext(versionsPath, imagesPath, knownVersionsPath, downloadsPath,
                                              driverVersionsPath, ImmutableMap.of(), driverCompatibilityPath);
    }

    @Test
    void existingVersionsAreNotRemapped()
    throws IOException, BrowserBoxException
    {
        //Add an existing download under a different key that should be preserved and not used for new mappings
        context.getDownloads().setProperty("7.0-known", "http://galah.galah.galah/file1");

        UrlKeyPreservingGenerateVersionsRegistry registry = new UrlKeyPreservingGenerateVersionsRegistry(context, null,
                                                                                                         false);
        registry.saveItems(ImmutableList.of(
                new Item("1.0", new URL("http://galah.galah.galah/file1")),
                new Item("1.1", new URL("http://galah.galah.galah/file2"))
        ));

        Properties results = registry.readDownloadProperties();

        assertThat(results).hasSize(2)
                           .containsEntry("7.0-known", "http://galah.galah.galah/file1")
                           .containsEntry("1.1", "http://galah.galah.galah/file2");
    }
}
