import au.net.causal.maven.plugins.browserbox.Mp4Checker

//Verify that video files for each tested version exists and are of a decent size

File buildDir = new File(basedir, 'target')
File videoDir = new File(buildDir, 'video')
assert videoDir.exists()

//This list must be kept in sync with the versions specified in invoker.properties
List<String> versionList = ['11', '10', '9', '8', '7']
versionList.each {
    File videoFile = new File(videoDir, "ie-${it}.mp4")
    assert videoFile.exists()
    assert videoFile.length() > 10000 //Make sure video is a decent size

    Mp4Checker.VideoDetails videoDetails = Mp4Checker.check(videoFile)

    //Validates video actually is a video file and the width/height are recording full screen
    //Old IEs have lower resolution so need to accept both
    assert videoDetails.width in [1024, 800]
    assert videoDetails.height in [768, 600]
    assert videoDetails.framesPerSecond == 25.0d
}

return
