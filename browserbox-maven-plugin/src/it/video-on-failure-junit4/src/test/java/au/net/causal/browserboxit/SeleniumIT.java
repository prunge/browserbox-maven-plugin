package au.net.causal.browserboxit;

import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class SeleniumIT
{
    private static RemoteWebDriver driver;
    private static WebDriverWait wait;
    
    @BeforeClass
    public static void setUpSelenium()
    throws Exception
    {
        String seleniumUrl = System.getProperty("selenium.url");

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        URL server = new URL(seleniumUrl);
        driver = new RemoteWebDriver(server, capabilities);

        long timeoutInSeconds = 20L;
        wait = new WebDriverWait(driver, timeoutInSeconds);
    }
    
    @AfterClass
    public static void shutDownSelenium()
    throws Exception
    {
        if (driver != null)
            driver.quit();
    }
    
    private void verifyBrowser(RemoteWebDriver driver)
    {
        Capabilities caps = driver.getCapabilities();
        assertThat(caps.getBrowserName(), is("firefox"));
        assertThat(String.valueOf(caps.getCapability("browserVersion")), startsWith("57."));

        String userAgent = (String)driver.executeScript("return navigator.userAgent");
        assertThat(userAgent, containsString("Firefox/57."));
    }
    
    @Test
    public void testSelenium()
    throws Exception
    {
        verifyBrowser(driver);
        
        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText(), is("Good Morning"));
        assertThat(driver.findElement(By.id("content")).getText(), is("Good morning, what will be for eating?"));
    }

    @Test
    public void testIntentionalFailure()
    throws Exception
    {
        verifyBrowser(driver);

        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText(), is("Good Morning"));
        assertThat(driver.findElement(By.id("content")).getText(), is("Good morning, what will be for eating?"));

        fail("Intentionally failing");
    }
    
    @Test
    public void testError()
    throws Exception
    {
        verifyBrowser(driver);

        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText(), is("Good Morning"));
        assertThat(driver.findElement(By.id("content")).getText(), is("Good morning, what will be for eating?"));

        throw new Exception("Intentionally throw error");
    }
}
