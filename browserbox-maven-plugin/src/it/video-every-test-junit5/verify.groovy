File buildDir = new File(basedir, 'target')

//Verify videos created for every test
File videoBaseDir = new File(buildDir, 'testvideo')
assert videoBaseDir.exists()
File videoDir = new File(videoBaseDir, 'browserbox-firefox-57.0')
assert videoDir.exists()
assert videoDir.listFiles().count { it.name.endsWith('.mp4') } == 3
File videoFile1 = new File(videoDir, 'au.net.causal.browserboxit.SeleniumIT-testIntentionalFailure.mp4')
assert videoFile1.exists()
assert videoFile1.size() > 0
File videoFile2 = new File(videoDir, 'au.net.causal.browserboxit.SeleniumIT-testError.mp4')
assert videoFile2.exists()
assert videoFile2.size() > 0
File videoFile3 = new File(videoDir, 'au.net.causal.browserboxit.SeleniumIT-testSelenium.mp4')
assert videoFile3.exists()
assert videoFile3.size() > 0
