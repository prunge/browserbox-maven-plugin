package au.net.causal.browserboxit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class SeleniumIT
{
    private static RemoteWebDriver driver;
    private static WebDriverWait wait;

    @BeforeAll
    private static void setUpSelenium()
    throws Exception
    {
        String seleniumUrl = System.getProperty("selenium.url");

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        URL server = new URL(seleniumUrl);
        driver = new RemoteWebDriver(server, capabilities);

        long timeoutInSeconds = 20L;
        wait = new WebDriverWait(driver, timeoutInSeconds);
    }

    @AfterAll
    private static void shutDownSelenium()
    {
        if (driver != null)
            driver.quit();
    }

    private void verifyBrowser(RemoteWebDriver driver)
    {
        Capabilities caps = driver.getCapabilities();
        assertThat(caps.getBrowserName()).isEqualTo("firefox");
        assertThat(String.valueOf(caps.getCapability("browserVersion"))).startsWith("57.");

        String userAgent = (String)driver.executeScript("return navigator.userAgent");
        assertThat(userAgent).contains("Firefox/57.");
    }

    @Test
    void testSelenium()
    {
        verifyBrowser(driver);

        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText()).isEqualTo("Good Morning");
        assertThat(driver.findElement(By.id("content")).getText()).isEqualTo("Good morning, what will be for eating?");
    }

    @Test
    void testIntentionalFailure()
    {
        verifyBrowser(driver);

        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText()).isEqualTo("Good Morning");
        assertThat(driver.findElement(By.id("content")).getText()).isEqualTo("Good morning, what will be for eating?");

        fail("Intentionally failing");
    }

    @Test
    void testError()
    throws Exception
    {
        verifyBrowser(driver);

        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText()).isEqualTo("Good Morning");
        assertThat(driver.findElement(By.id("content")).getText()).isEqualTo("Good morning, what will be for eating?");

        throw new Exception("Intentionally throw error");
    }
}