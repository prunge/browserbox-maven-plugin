import au.net.causal.maven.plugins.browserbox.Mp4Checker

//Verify that video files for each tested version exists and are of a decent size

File buildDir = new File(basedir, 'target')
File videoDir = new File(buildDir, 'video')
assert videoDir.exists()

//This list must be kept in sync with the versions specified in invoker.properties
List<String> versionList = ['69.0', '57.0', '58.0b5', '47.0', '38.0', '6.0']
versionList.each {
    File videoFile = new File(videoDir, "firefox-${it}.mp4")
    assert videoFile.exists()
    assert videoFile.size() > 10000 //Make sure video is a decent size

    Mp4Checker.VideoDetails videoDetails = Mp4Checker.check(videoFile)

    //Validates video actually is a video file and the width/height are recording full screen (Selenium default resolution)
    assert videoDetails.width == 1360
    assert videoDetails.height == 1020
    assert videoDetails.framesPerSecond == 25.0d
}

return
