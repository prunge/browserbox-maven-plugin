package au.net.causal.browserboxit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.openqa.selenium.remote.CapabilityType.ACCEPT_INSECURE_CERTS;

public class SeleniumIT
{
    private void verifyBrowser(RemoteWebDriver driver)
    {
        Capabilities caps = driver.getCapabilities();
        assertThat(caps.getBrowserName(), is("chrome"));
        assertThat(caps.getVersion(), startsWith("76."));

        String userAgent = (String)driver.executeScript("return navigator.userAgent");
        System.out.println("User agent: " + userAgent);
        assertThat(userAgent, containsString("Chrome/76."));
        assertThat(userAgent, containsString("Android"));
    }
    
    @Test
    public void testSelenium()
    throws Exception
    {
        String seleniumUrl = System.getProperty("selenium.url");
        String capabilitiesString = System.getProperty("selenium.capabilities");

        MutableCapabilities capabilities = DesiredCapabilities.chrome();
        if (capabilitiesString != null && !capabilitiesString.isEmpty())
            capabilities = new Json().toType(capabilitiesString, DesiredCapabilities.class);

        capabilities.setCapability(ACCEPT_INSECURE_CERTS, false);

        //Add a Chrome capability to turn off the option to ignore cert errors
        //If this is not done, the default behaviour to ignore all SSL errors is used which is not what we want
        //since we want to test whether our SSL certs are actually installed
        Map<String, ?> existingChromeOptions = (Map<String, ?>)capabilities.getCapability(ChromeOptions.CAPABILITY);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        if (existingChromeOptions != null)
            chromeOptions.putAll(existingChromeOptions);

        chromeOptions.put("excludeSwitches", Collections.singletonList("ignore-certificate-errors"));
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        URL server = new URL(seleniumUrl);
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        verifyBrowser(driver);
        
        driver.get("https://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        
        long timeoutInSeconds = 20L;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText(), is("Good Morning"));
        assertThat(driver.findElement(By.id("content")).getText(), is("Good morning, what will be for eating?"));
        
        Files.write(Paths.get("full-screenshot.png"), driver.getScreenshotAs(OutputType.BYTES));

        driver.quit();
    }
}
