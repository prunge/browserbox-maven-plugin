package au.net.causal.browserboxit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class SeleniumIT
{
    private void verifyBrowser(RemoteWebDriver driver)
    {
        Capabilities caps = driver.getCapabilities();
        assertThat(caps.getBrowserName(), is("chrome"));
        assertThat(caps.getVersion(), startsWith("76."));

        String userAgent = (String)driver.executeScript("return navigator.userAgent");
        System.out.println("User agent: " + userAgent);
        assertThat(userAgent, containsString("Chrome/76."));
        assertThat(userAgent, containsString("Linux; Android 6.0;"));
    }
    
    @Test
    public void testSelenium()
    throws Exception
    {
        String seleniumUrl = System.getProperty("selenium.url");
        String capabilitiesString = System.getProperty("selenium.capabilities");

        Capabilities capabilities = DesiredCapabilities.chrome();
        if (capabilitiesString != null)
            capabilities = new Json().toType(capabilitiesString, DesiredCapabilities.class);

        URL server = new URL(seleniumUrl);
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        verifyBrowser(driver);
        
        driver.get("http://localhost:" + System.getProperty("test.server.port") + "/index.jsp");
        
        long timeoutInSeconds = 20L;
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

        assertThat(driver.findElement(By.id("header")).getText(), is("Good Morning"));
        assertThat(driver.findElement(By.id("content")).getText(), is("Good morning, what will be for eating?"));
        
        Files.write(Paths.get("full-screenshot.png"), driver.getScreenshotAs(OutputType.BYTES));

        driver.quit();
    }
}
