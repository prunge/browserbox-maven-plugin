import au.net.causal.maven.plugins.browserbox.Mp4Checker
import groovy.transform.Immutable

@Immutable class VideoTest {
    String browserVersion
    int androidApiLevel
}

//Verify that video files for each tested version exists and are of a decent size

File buildDir = new File(basedir, 'target')
File videoDir = new File(buildDir, 'video')
assert videoDir.exists()

//This list must be kept in sync with the versions specified in invoker.properties
List<VideoTest> videoTests = [
        new VideoTest(browserVersion: '76.0', androidApiLevel: 29),
        new VideoTest(browserVersion: '76.0', androidApiLevel: 26),
        new VideoTest(browserVersion: '76.0', androidApiLevel: 23),
        new VideoTest(browserVersion: '62.0', androidApiLevel: 23),
]
videoTests.each {
    File videoFile = new File(videoDir, "android-${it.androidApiLevel}-chrome-${it.browserVersion}.mp4")
    assert videoFile.exists()
    assert videoFile.size() > 10000 //Make sure video is a decent size

    Mp4Checker.VideoDetails videoDetails = Mp4Checker.check(videoFile)

    //Validates video actually is a video file and the width/height are recording full screen (Selenium default resolution)
    //or in half-resolution, which will happen on older Android builds that don't support full resolution video recording
    assert videoDetails.width in [1440, 720]
    assert videoDetails.height in [2560, 1280]

    //FPS is all over the place, looks like it adapts so don't check
    //assert videoDetails.framesPerSecond == 25.0d
}

return
