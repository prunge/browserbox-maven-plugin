package au.net.causal.browserboxit;

import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;
import static org.openqa.selenium.remote.CapabilityType.ACCEPT_INSECURE_CERTS;
import static org.openqa.selenium.remote.CapabilityType.ACCEPT_SSL_CERTS;

class SeleniumIT
{
    /**
     * Maps Android OS API level to user agent string fragments.
     *
     * Reference: <a href="https://source.android.com/setup/start/build-numbers">Android Build Numbers</a>
     */
    private static final Map<String, String> VERSIONED_USER_AGENT_STRINGS = ImmutableMap.of(
            "29", "Android 10",
            "26", "Android 8.0.0",
            "23", "Android 6.0"
    );

    private final String seleniumUrl = System.getProperty("selenium.url");
    private final String seleniumCapabilitiesString = System.getProperty("selenium.capabilities");
    private final String expectedBrowserVersion = System.getProperty("test.browser.version");
    private final String expectedAndroidOsApiLevel = System.getProperty("test.androidOsApiLevel");
    private final String httpServerPort = System.getProperty("test.server.http.port");
    private final String httpsServerPort = System.getProperty("test.server.https.port");

    private final String expectedBrowserMajorVersion = parseMajorVersion(expectedBrowserVersion);

    private static String parseMajorVersion(String version)
    {
        if (version == null)
            return null;

        return version.split(Pattern.quote("."))[0];
    }

    @BeforeEach
    private void checkPropertiesAreConfigured()
    {
        Objects.requireNonNull(seleniumUrl, "Selenium URL system property not configured.");
        Objects.requireNonNull(seleniumCapabilitiesString, "Selenium capabilities system property not configured.");
        Objects.requireNonNull(expectedBrowserVersion, "Expected browser version system property not configured.");
        Objects.requireNonNull(expectedAndroidOsApiLevel, "Expected Android OS API level property not configured.");
        Objects.requireNonNull(httpServerPort, "HTTP server port system property not configured.");
        Objects.requireNonNull(httpsServerPort, "HTTPS server port system property not configured");
    }

    private void verifyBrowser(RemoteWebDriver driver)
    {
        Capabilities caps = driver.getCapabilities();
        String capsBrowserName = caps.getBrowserName();
        Object capsBrowserVersionObj = caps.getCapability("browserVersion");
        if (capsBrowserVersionObj == null) //Version stored under 'version' for older Selenium remotes
            capsBrowserVersionObj = caps.getCapability("version");
        String capsBrowserVersion = String.valueOf(capsBrowserVersionObj);
        String userAgent = (String)driver.executeScript("return navigator.userAgent");

        //System.out.println("All capabilities: " + caps.asMap());
        System.out.println("Capabilities browser name: " + capsBrowserName);
        System.out.println("Capabilities browser version: " + capsBrowserVersion);
        System.out.println("User agent: " + userAgent);

        assertThat(capsBrowserName).isEqualTo("chrome");
        assertThat(capsBrowserVersion).startsWith(expectedBrowserMajorVersion + ".");
        assertThat(userAgent).contains("Chrome/" + expectedBrowserMajorVersion + ".");
        assertThat(userAgent).contains("Android");
        String expectedAndroidVersionString = VERSIONED_USER_AGENT_STRINGS.get(expectedAndroidOsApiLevel);
        assertThat(userAgent).contains(expectedAndroidVersionString);
    }

    private void runSeleniumOnUrl(String classifier, String url)
    throws IOException
    {
        MutableCapabilities capabilities = DesiredCapabilities.chrome();
        if (seleniumCapabilitiesString != null && !seleniumCapabilitiesString.isEmpty())
            capabilities = new Json().toType(seleniumCapabilitiesString, DesiredCapabilities.class);

        capabilities.setCapability(ACCEPT_INSECURE_CERTS, false);
        capabilities.setCapability(ACCEPT_SSL_CERTS, false);

        //Add a Chrome capability to turn off the option to ignore cert errors
        //If this is not done, the default behaviour to ignore all SSL errors is used which is not what we want
        //since we want to test whether our SSL certs are actually installed
        Map<String, ?> existingChromeOptions = (Map<String, ?>)capabilities.getCapability(ChromeOptions.CAPABILITY);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        if (existingChromeOptions != null)
            chromeOptions.putAll(existingChromeOptions);

        chromeOptions.put("excludeSwitches", Collections.singletonList("ignore-certificate-errors"));
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        URL server = new URL(seleniumUrl);
        RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);

        try
        {
            verifyBrowser(driver);

            driver.get(url);

            long timeoutInSeconds = 20L;
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("header")));

            assertThat(driver.findElement(By.id("header")).getText()).isEqualTo("Good Morning");
            assertThat(driver.findElement(By.id("content")).getText()).isEqualTo("Good morning, what will be for eating?");

            Files.write(Paths.get("full-screenshot-" + classifier + ".png"), driver.getScreenshotAs(OutputType.BYTES));
        }
        catch (Exception e)
        {
            //Dump page source when there is a failure
            System.out.println("Page source: " + driver.getPageSource());

            //...and rethrow original error/assertion failure
            throw e;
        }
        finally
        {
            driver.quit();
        }
    }

    @Test
    void testHttp()
    throws IOException
    {
        runSeleniumOnUrl("http", "http://localhost:" + httpServerPort + "/index.jsp");
    }

    @Test
    void testHttps()
    throws IOException
    {
        runSeleniumOnUrl("https", "https://localhost:" + httpsServerPort + "/index.jsp");
    }
}
