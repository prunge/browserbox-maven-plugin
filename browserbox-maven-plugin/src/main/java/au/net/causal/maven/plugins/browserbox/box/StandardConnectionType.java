package au.net.causal.maven.plugins.browserbox.box;

public enum StandardConnectionType implements ConnectionType
{
    SSH,
    SELENIUM,
    VNC,
    RDP;
}