package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.service.ImagePullManager;
import io.fabric8.maven.docker.service.QueryService;
import io.fabric8.maven.docker.service.RegistryService;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Utility class for pulling Docker images using a pull policy configured on the Maven project.
 */
public final class DockerPuller
{
    /**
     * Private constructor to prevent instantiation.
     */
    private DockerPuller()
    {
    }

    /**
     * Pull an image using Docker if needed.  This uses the pull image policy configured in Maven, which controls whether
     * images are checked and updated if needed.
     *
     * @param imageName the name of the Docker image to pull.
     * @param projectConfiguration project configuration.
     * @param context context.
     *
     * @throws BrowserBoxException if an error occurs.
     * @throws DockerAccessException if a Docker error occurs.
     * @throws MojoExecutionException if there is a problem with configuration in Maven.
     */
    public static void pullImage(String imageName, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BrowserBoxException, DockerAccessException, MojoExecutionException
    {
        QueryService queryService = context.getDockerServiceHub().getQueryService();
        RegistryService.RegistryConfig registryConfig = getRegistryConfig(projectConfiguration, context, null); //TODO should we allow specific registry to pull from to be configured?
        ImagePullManager pullManager = context.getImagePullCacheManager();
        context.getDockerServiceHub().getRegistryService().pullImageWithPolicy(imageName, pullManager, registryConfig,
                                                      queryService.hasImage(imageName));
    }

    /**
     * Builds Docker registry configuration from Maven configuration.
     *
     * @param projectConfiguration Maven project configuration for the current project.
     * @param context box context.
     * @param specificRegistry the Docker registry specifically configured, or null to use the default registry.
     *
     * @return registry configuration.
     */
    private static RegistryService.RegistryConfig getRegistryConfig(ProjectConfiguration projectConfiguration, BoxContext context, String specificRegistry)
    {

        return new RegistryService.RegistryConfig.Builder()
                    .settings(projectConfiguration.getSettings())
                    .authConfig(context.getDockerAuthConfiguration() != null ? context.getDockerAuthConfiguration().toMap() : null)
                    .authConfigFactory(context.getAuthConfigFactory())
                    .skipExtendedAuth(context.isDockerSkipExtendedAuth())
                    .registry(specificRegistry)
                    .build();
    }


}
