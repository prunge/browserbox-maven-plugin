package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.net.URL;
import java.util.List;
import java.util.stream.Stream;

/**
 * Versioned item store implmementation that, given multiple implementations of version registries, will find the first
 * writable one and propogate writes to that one.  Reading is treated the same as in {@link CompositeVersionRegistry}.
 */
public class CompositeVersionedItemStore extends CompositeVersionRegistry implements VersionedItemStore
{
    private final VersionedItemStore store;

    /**
     * Creates a composite store.
     *
     * @param registries version registries, one of which must be an implementation of {@link VersionedItemStore}.
     *
     * @throws IllegalArgumentException if no registry implements {@link VersionedItemStore}.
     */
    public CompositeVersionedItemStore(List<? extends VersionRegistry> registries)
    {
        super(registries);
        this.store = findStore(registries);
    }

    /**
     * Creates a composite store.
     *
     * @param registries version registries, one of which must be an implementation of {@link VersionedItemStore}.
     *
     * @throws IllegalArgumentException if no registry implements {@link VersionedItemStore}.
     */
    public CompositeVersionedItemStore(VersionRegistry... registries)
    {
        this(new ImmutableList.Builder<VersionRegistry>().add(registries).build());
    }

    /**
     * Finds the first registry that implements <code>VersionedItemStore</code> and returns it, throwing an
     * <code>IllegalArgumentException</code> if none was found.
     */
    private VersionedItemStore findStore(List<? extends VersionRegistry> registries)
    {
        return registries.stream()
                .flatMap(r -> (r instanceof VersionedItemStore ? Stream.of((VersionedItemStore)r) : Stream.empty()))
                .findFirst().orElseThrow(() -> new IllegalArgumentException("No versioned item store in registry list."));
    }

    @Override
    public URL saveItemContents(Item item) throws BrowserBoxException
    {
        return store.saveItemContents(item);
    }
}
