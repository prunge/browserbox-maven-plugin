package au.net.causal.maven.plugins.browserbox.virtualbox;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class LocalVirtualBoxManager implements VirtualBoxManager 
{
    private final Commandline baseCommandLine;
    private final Log log;

    /**
     * @param baseCommandLine command line that has just the VBoxManage executable, no parameters.
     * @param log log that will receive output from the process.
     */
    public LocalVirtualBoxManager(Commandline baseCommandLine, Log log)
    {
        Objects.requireNonNull(baseCommandLine, "baseCommandLine == null");
        Objects.requireNonNull(log, "log == null");

        this.baseCommandLine = baseCommandLine;
        this.log = log;
    }

    /**
     * Convenience converter varargs to String array.
     */
    private static String[] args(String... args)
    {
        return args;
    }

    @Override
    public Map<String, String> showVmInfo(ShowVmInfoOptions options) throws VirtualBoxException 
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        command.addArguments(args("showvminfo", "--machinereadable", options.getVmName()));
        String result = executeCommand(command, options.getTimeout(), false);
        
        //Parse result
        Map<String, String> map = new LinkedHashMap<>();
        try (BufferedReader r = new BufferedReader(new StringReader(result))) 
        {
            r.lines().forEach(line -> parseVmInfoLine(line, map));
        }
        catch (IOException e)
        {
            //Shouldn't actually happen since it's in memory
            throw new VirtualBoxException("Result parse error: " + e, e);
        }
        
        return map;
    }
    
    private void parseVmInfoLine(String line, Map<? super String, ? super String> map)
    {
        //"Something"="Something Else"
        //Intentionally simplified to not cope with '=' in keys
        List<String> tokens = Splitter.on('=').limit(2).trimResults().splitToList(line);
        if (tokens.size() > 1)
        {
            String key = trimQuotes(tokens.get(0));
            String value = trimQuotes(tokens.get(1));
            map.put(key, value);
        }
    }
    
    private static String trimQuotes(Object value)
    {
        if (value == null)
            return null;
        
        String s = value.toString();
        if (s.startsWith("\"") && s.endsWith("\""))
            s = s.substring(1, s.length() - 1);
        
        return s;
    }

    @Override
    public String guestControlRun(GuestControlRunOptions options) 
    throws VirtualBoxException
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        configureGuestControlArguments(options, command);
        
        //Run-arguments
        command.addArguments(args("run", options.getProgram()));
        if (!options.getProgramArgs().isEmpty())
        {
            command.addArguments(args("--"));
            command.addArguments(options.getProgramArgs().toArray(new String[options.getProgramArgs().size()]));
        }
        
        int attempts = 0;
        VirtualBoxException firstException = null;
        while (attempts < 10)
        {
            try
            {
                attempts++;
                return executeCommand(command, options.getTimeout(), options.isLogOutput());
            }
            catch (VirtualBoxException e)
            {
                if (firstException == null)
                    firstException = e;
                
                //Error 126 with no output is safe to try again
                if (e.getExitCode() != 126 || !Strings.nullToEmpty(e.getOut()).trim().isEmpty() || 
                                              !Strings.nullToEmpty(e.getErr()).trim().isEmpty())
                {
                    throw e;
                }
                
                log.warn("Attempting retry after VBoxManage error 126...");
                try 
                {
                    Thread.sleep(200L);
                }
                catch (InterruptedException ex)
                {
                    log.warn("Interrupted waiting to retry VBoxManage run.", e);
                }
            }
        }
        
        //Max attempts reached
        throw firstException;
    }

    @Override
    public void modifyVm(ModifyVmOptions options) throws VirtualBoxException 
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        command.addArguments(args("modifyvm", options.getVmName(), 
                "--" + options.getOptionName(), options.getOptionValue()));
        executeCommand(command, options.getTimeout(), true);
    }

    @Override
    public void storageAttach(StorageAttachOptions options) throws VirtualBoxException
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        command.addArguments(args("storageattach", options.getVmName(),
                                  "--storagectl", options.getControllerName(), 
                                  "--device", String.valueOf(options.getDevice()), 
                                  "--port", String.valueOf(options.getPort()), 
                                  "--type", options.getType(), 
                                  "--medium", options.getMedium()));
        executeCommand(command, options.getTimeout(), true);
    }

    @Override
    public void setVideoModeHint(SetVideoModeHintOptions options) 
    throws VirtualBoxException 
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        command.addArguments(args("controlvm", options.getVmName(), 
                                "setvideomodehint", String.valueOf(options.getWidth()), 
                                String.valueOf(options.getHeight()), String.valueOf(options.getBitsPerPixel())));
        executeCommand(command, options.getTimeout(), true);
    }

    private void configureGuestControlArguments(GuestControlOptions options, Commandline command) 
    {
        command.addArguments(args("guestcontrol", options.getVmName()));
        if (!Strings.isNullOrEmpty(options.getUserName()))
            command.addArguments(args("--username", options.getUserName()));
        if (!Strings.isNullOrEmpty(options.getPassword()))
            command.addArguments(args("--password", options.getPassword()));
    }

    private String executeCommand(Commandline commandLine, Duration timeout, boolean logOutput)
    throws VirtualBoxException
    {
        CommandLineUtils.StringStreamConsumer out;
        CommandLineUtils.StringStreamConsumer err;
        if (logOutput) 
        {
            out = new LoggingConsumer(log, "VBoxManage> ");
            err = new LoggingConsumer(log, "VBoxManage> ");
            
        }
        else 
        {
            out = new CommandLineUtils.StringStreamConsumer();
            err = new CommandLineUtils.StringStreamConsumer();
        }
        
        try
        {
            log.debug("Execute command line: " + commandLine);
            int exitCode = CommandLineUtils.executeCommandLine(commandLine, out, err, Math.toIntExact(timeout.getSeconds()));
            if (exitCode != 0) 
            {
                throw new VirtualBoxException(exitCode, out.getOutput(), err.getOutput(), "VBoxManage error " + exitCode + ": " + 
                        err.getOutput() + "\n" + out.getOutput());
                        //(err.getOutput().isEmpty() ? out.getOutput() : err.getOutput()));
            }
        }
        catch (CommandLineException e)
        {
            throw new VirtualBoxException("Error running VBoxManage: " + e, e);
        }

        return out.getOutput();
    }

    /**
     * Every line of output from the VBoxManage process is sent to the log at INFO level.
     */
    private static class LoggingConsumer extends CommandLineUtils.StringStreamConsumer 
    {
        private final Log log;
        private final String prefix;

        public LoggingConsumer(Log log, String prefix)
        {
            this.log = log;
            this.prefix = prefix;
        }

        @Override
        public void consumeLine(String line)
        {
            super.consumeLine(line);
            log.info(prefix + line);
        }
    }
}
