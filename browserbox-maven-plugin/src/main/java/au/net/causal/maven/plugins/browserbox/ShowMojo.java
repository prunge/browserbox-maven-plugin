package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import au.net.causal.maven.plugins.browserbox.box.ConnectionType;
import au.net.causal.maven.plugins.browserbox.box.MicrosoftVagrantBrowserBox.VagrantConnectionType;
import au.net.causal.maven.plugins.browserbox.box.StandardConnectionType;
import au.net.causal.maven.plugins.browserbox.execute.FinderRegistry;
import au.net.causal.maven.plugins.browserbox.execute.ToolFinder;
import au.net.causal.maven.plugins.browserbox.execute.ToolFinder.ToolRunner;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.cli.StreamConsumer;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

@Mojo(name="show", defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST, requiresProject = false)
public class ShowMojo extends StartAndWaitMojo 
{
    /**
     * A list of viewer names to use when displaying the browser.  If not specified, all possible viewers will
     * be attempted to be used.
     *
     * @see StandardConnectionType#RDP
     * @see StandardConnectionType#VNC
     * @see VagrantConnectionType#VIRTUALBOX_UI
     */
    @Parameter(property = "browserbox.viewer")
    private List<String> preferredViewers = new ArrayList<>();

    /**
     * A list of viewer names to never use when displaying the browser.
     *
     * @see StandardConnectionType#RDP
     * @see StandardConnectionType#VNC
     * @see VagrantConnectionType#VIRTUALBOX_UI
     */
    @Parameter(property = "browserbox.viewer.blacklist")
    private List<String> viewerBlacklist = new ArrayList<>();

    /**
     * Normalize a user-entered viewer name to match one of the known viewer names.  Strings are upper-cased and '-'/space
     * is converted to '_'.
     *
     * @see StandardConnectionType
     * @see VagrantConnectionType
     */
    private static String normalizeViewerName(String viewerName)
    {
        return viewerName.replace('-', '_')
                         .replace(' ', '_')
                         .toUpperCase(Locale.ENGLISH);
    }

    @VisibleForTesting
    static List<? extends ConnectionType> selectConnectionTypes(List<? extends ConnectionType> boxSupportedConnectionTypes, 
                                                                List<String> preferredViewers,
                                                                Collection<String> viewerBlacklist)
    {
        List<String> normalizedPreferredViewers = preferredViewers.stream()
                                                                  .map(ShowMojo::normalizeViewerName)
                                                                  .collect(Collectors.toList());
        Set<String> normalizedViewerBlacklist = viewerBlacklist.stream()
                                                               .map(ShowMojo::normalizeViewerName)
                                                               .collect(Collectors.toSet());

        Set<ConnectionType> uiConnectionTypes = new HashSet<>(Arrays.asList(
                StandardConnectionType.VNC,
                StandardConnectionType.RDP,
                VagrantConnectionType.VIRTUALBOX_UI));

        //Remove any that are in the blacklist
        uiConnectionTypes.removeIf(connectionType -> normalizedViewerBlacklist.contains(connectionType.name()));

        //If the user specified the viewer list, they want to use them in the order they specified
        if (!normalizedPreferredViewers.isEmpty())
        {
            Map<String, ConnectionType> uiConnectionTypesByName = uiConnectionTypes.stream().collect(Collectors.toMap(
                    ConnectionType::name, Function.identity()));

            //Keep only user specified ones
            //and only ones supported by the box
            return normalizedPreferredViewers.stream().map(uiConnectionTypesByName::get)
                                                      .filter(Objects::nonNull)
                                                      .filter(boxSupportedConnectionTypes::contains)
                                                      .collect(Collectors.toList());
        }
        else
        {
            //Use box preferred ordering otherwise
            return boxSupportedConnectionTypes.stream()
                                              .filter(uiConnectionTypes::contains)
                                              .collect(Collectors.toList());
        }
    }
    
    private DisplayToolDetails findDisplayTool(BrowserBox box, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BrowserBoxException, IOException
    {
        FinderRegistry finderRegistry = new FinderRegistry();

        List<? extends ConnectionType> selectedConnectionTypes = selectConnectionTypes(box.getSupportedConnectionTypes(), preferredViewers, viewerBlacklist);
        for (ConnectionType displayType : selectedConnectionTypes)
        {
            ConnectionInfo connectionInfo = box.getConnectionInfo(displayType);
            Optional<ToolFinder.ToolRunner> runner = finderRegistry.platformCombinedFinder(displayType)
                                                        .findTool(connectionInfo, box, boxConfiguration, projectConfiguration, context);
            if (runner.isPresent())
                return new DisplayToolDetails(displayType, connectionInfo, runner.get());
        }
        
        throw new BrowserBoxException("Browser box display cannot be established - selected " + selectedConnectionTypes + " but no user tools were found.");
    }
    
    @Override
    protected void registerHookAndStartBrowserBox(ExceptionalSupplier<DockerService, BrowserBoxException> dockerService)
    throws MojoExecutionException, MojoFailureException, DockerAccessException
    {
        super.registerHookAndStartBrowserBox(dockerService);

        try
        {
            BrowserBox box = browserBox(dockerService);
            BoxConfiguration boxConfiguration = this.box;
            BoxContext context = boxContext(dockerService);
            ProjectConfiguration projectConfiguration = projectConfiguration();

            DisplayToolDetails tool = findDisplayTool(box, boxConfiguration, projectConfiguration, context);

            getLog().info("Running " + tool.getConnectionType() + ": " + tool.getRunner());

            StreamConsumer out = new LoggingConsumer(getLog(), tool.getConnectionType() + "> ");
            StreamConsumer err = new LoggingConsumer(getLog(), tool.getConnectionType() + "> ");

            ToolRunner toolRunner = tool.getRunner();
            toolRunner.prepareRun(tool.getConnectionInfo(), box, boxConfiguration, projectConfiguration, context);

            ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
            executor.submit(() -> runCommand(toolRunner, tool.getConnectionInfo(), box, boxConfiguration, projectConfiguration, context, out, err, executor));
            
            box.browserControl().launch(URI.create("about:blank"));
        }
        catch (BrowserBoxException | IOException e)
        {
            throw new MojoExecutionException("Error showing browser: " + e.getMessage(), e);
        }
    }
    
    private void runCommand(ToolFinder.ToolRunner runner, ConnectionInfo ci, BrowserBox box,
                            BoxConfiguration boxConfiguration,
                            ProjectConfiguration projectConfiguration, BoxContext context,
                            StreamConsumer out, StreamConsumer err, ListeningExecutorService executor)
    {
        try 
        {
            int result = runner.run(ci, box, boxConfiguration, projectConfiguration, context, out, err, executor);
            getLog().info("Command result: " + result);
            System.exit(0);
        }
        catch (IOException | BrowserBoxException e)
        {
            getLog().error("Error executing tool: " + e.getMessage(), e);
        }
    }

    @Override
    protected String getWaitMessage() 
    {
        return "Press control+c or close the remote viewer to stop the browser container...";
    }

    /**
     * Every line of output from the Vagrant process is sent to the log at INFO level.
     */
    private static class LoggingConsumer implements StreamConsumer
    {
        private final Log log;
        private final String prefix;

        public LoggingConsumer(Log log, String prefix)
        {
            this.log = log;
            this.prefix = prefix;
        }

        @Override
        public void consumeLine(String line)
        {
            log.info(prefix + line);
        }
    }
    
    private static class DisplayToolDetails
    {
        private final ConnectionType connectionType;
        private final ConnectionInfo connectionInfo;
        private final ToolFinder.ToolRunner runner;

        public DisplayToolDetails(ConnectionType connectionType, ConnectionInfo connectionInfo, ToolFinder.ToolRunner runner) 
        {
            this.connectionType = connectionType;
            this.connectionInfo = connectionInfo;
            this.runner = runner;
        }

        public ConnectionType getConnectionType() 
        {
            return connectionType;
        }

        public ConnectionInfo getConnectionInfo() 
        {
            return connectionInfo;
        }

        public ToolFinder.ToolRunner getRunner() 
        {
            return runner;
        }
    }
}
