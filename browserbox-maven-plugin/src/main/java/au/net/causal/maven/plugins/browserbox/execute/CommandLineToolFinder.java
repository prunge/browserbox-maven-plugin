package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.util.concurrent.ListeningExecutorService;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;
import org.codehaus.plexus.util.cli.StreamConsumer;

import java.io.IOException;
import java.util.Optional;

public abstract class CommandLineToolFinder implements ToolFinder 
{
    @Override
    public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box, 
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration, 
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Commandline> clio = find(connectionInfo, box, boxConfiguration, projectConfiguration, context);
        return clio.map(CommandLineToolRunner::new);
    }

    /**
     * Convenience converter varargs to String array.
     */
    protected static String[] args(String... args)
    {
        return args;
    }
    
    protected abstract Optional<Commandline> find(ConnectionInfo connectionInfo,
                                                  BrowserBox box, 
                                                  BoxConfiguration boxConfiguration,
                                                  ProjectConfiguration projectConfiguration, 
                                                  BoxContext context)
    throws IOException, BrowserBoxException;

    private static class CommandLineToolRunner implements ToolRunner
    {
        private final Commandline cli;

        public CommandLineToolRunner(Commandline cli)
        {
            this.cli = cli;
        }

        @Override
        public int run(ConnectionInfo connectionInfo, BrowserBox box,
                       BoxConfiguration boxConfiguration,
                       ProjectConfiguration projectConfiguration, BoxContext context,
                       StreamConsumer out, StreamConsumer err, ListeningExecutorService executor)
        throws IOException, BrowserBoxException
        {
            try
            {
                return CommandLineUtils.executeCommandLine(cli, out, err);
            }
            catch (CommandLineException e)
            {
                throw new IOException(e.getMessage(), e);
            }
        }

        @Override
        public String toString() 
        {
            return cli.toString();
        }
    }
}
