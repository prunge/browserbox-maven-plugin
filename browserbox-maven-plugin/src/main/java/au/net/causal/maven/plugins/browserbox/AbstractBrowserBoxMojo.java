package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.android.Platform;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxLookup;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.BrowserBoxFactory;
import au.net.causal.maven.plugins.browserbox.box.Resolution;
import au.net.causal.maven.plugins.browserbox.box.SslTrustConfiguration;
import au.net.causal.maven.plugins.browserbox.box.TrustStoreConfiguration;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.RepositoryUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.filtering.MavenReaderFilter;
import org.apache.maven.shared.filtering.MavenResourcesFiltering;
import org.apache.maven.shared.invoker.Invoker;
import org.codehaus.plexus.archiver.manager.ArchiverManager;
import org.codehaus.plexus.component.repository.ComponentDependency;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.DependencyResolutionException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public abstract class AbstractBrowserBoxMojo extends AbstractDockerBoxMojo 
{
    protected static final String TUNNEL_SESSION_KEY_PREFIX = "au.net.causal.maven.plugins.browserbox.tunnelsession.";
    protected static final String VIDEO_RECORDING_SESSION_KEY_PREFIX = "au.net.causal.maven.plugins.browserbox.videorecording";
    protected static final String TOOL_SERVER_KEY_PREFIX = "au.net.causal.maven.plugins.browserbox.toolserver";
    
    @Parameter(property = "browser.type")
    private String browserType;
    
    @Parameter(property = "browser.version")
    private String browserVersion;

    @Parameter(property = "browser.containerName")
    private String containerName;
    
    @Parameter(property = "browser.tunnelPorts")
    private List<String> tunnelPorts = new ArrayList<>();

    /**
     * Configures the screen resolution to run the browser at.  This is a hint, it may not be supported by all
     * browser types.
     *
     * The format for this value is <code>widthxheight</code>, e.g. '1920x1080'.  If not specified, a browser-specific
     * default is used.
     *
     * @see BoxConfiguration#getResolution()
     */
    @Parameter(property = "browser.resolution")
    private String browserResolution;

    /**
     * Shortcut for configuring browser box properties through a property.  Normally this would be configured through
     * the {@link #box} property, but this property can be used as a convenience when configuring on the command
     * line.  Each entry takes the form key=value.
     */
    @Parameter(property = "browser.properties")
    private List<String> browserPropertyList = new ArrayList<>();

    /**
     * Configures graphics/DPI scaling for the browser UI.  This can be used to make things appear larger on high-DPI
     * displays or simply to test with browsers running in high-DPI mode.
     *
     * The format for this value is a floating point number that scales the UI by that amount.  For example, "2.0" scales
     * the UI by a factor of 2, making it twice as big.  Defaults to no scaling.
     *
     * @see BoxConfiguration#getScaleFactor()
     */
    @Parameter(property = "browser.scaleFactor")
    private String browserScaleFactor;

    /**
     * Configures a trust store with certificates that will be installed into the browser.  The store will be
     * in PKCS12 format and all aliases will be used.
     *
     * This is a shortcut for specifying the trust store configuration from a file.  Use a box configuration
     * for more fine-grained control over SSL certificates.
     *
     * @see #sslTrustStorePassword
     * @see BoxConfiguration#getSslTrust()
     * @see SslTrustConfiguration#getTrustStores()
     * @see au.net.causal.maven.plugins.browserbox.box.TrustStoreConfiguration
     */
    @Parameter(property = "browser.sslTrustStore.file")
    private File sslTrustStoreFile;

    /**
     * Password for the SSL trust store file.  This is a shortcut for specifying this in a box configuration.
     *
     * @see #sslTrustStoreFile
     */
    @Parameter(property = "browser.sslTrustStore.password")
    private String sslTrustStorePassword;

    /**
     * Alias(es) to select in the SSL trust store file.  This is a shortcut for specifying this in a box configuration.
     * If not specified, all aliases are selected from the SSL trust store file.
     *
     * @see #sslTrustStoreFile
     */
    @Parameter(property = "browser.sslTrustStore.alias")
    private List<String> sslTrustStoreAliases = new ArrayList<>();

    //Can't do this because project.build.directory is not resolved in non-project builds
    //@Parameter(defaultValue = "${project.build.directory}/browserbox", required = true)
    @Parameter
    private File workDirectory;

    @Parameter(defaultValue = "${user.home}/.browserbox", required = true)
    protected File globalConfigDirectory;
    
    @Parameter(defaultValue = "${android.sdk.path}")
    protected File androidHome;

    /**
     * A properties file that is written by the plugin with remote Selenium URL, etc.
     * Can be used by other tools to pick up a running browser box instance.
     * This file is deleted when browser box exits.
     */
    @Parameter(property = "browserbox.active.properties.file", defaultValue = "${project.build.directory}/browserbox.properties")
    protected File activePropertiesFile;

    @Parameter(property = "browserbox.imageUpdateMode", defaultValue = "ON", required = true)
    private ImageUpdateMode imageUpdateMode;

    /**
     * Which intermediate/temporary artifacts should be saved to the local machine.  By default, only downloads are
     * saved.  Can be turned off or configured if necessary.
     */
    @Parameter(property = "browserbox.build.keepIntermediates")
    private Set<BuildIntermediates> keepBuildIntermediates = EnumSet.of(BuildIntermediates.DOWNLOAD_ARTIFACTS);

    @Component
    protected MavenResourcesFiltering resourcesFiltering;

    @Component
    protected MavenReaderFilter readerFilter;

    @Component
    protected RepositorySystem repositorySystem;

    @Component
    protected ArchiverManager archiverManager;
    
    @Component
    protected Invoker invoker;

    @Parameter(defaultValue = "${repositorySystemSession}", readonly = true)
    protected RepositorySystemSession repoSession;

    @Parameter(defaultValue = "${project.remotePluginRepositories}", readonly = true)
    protected List<RemoteRepository> remoteRepos;

    @Parameter
    protected BoxConfiguration box = new BoxConfiguration();

    /**
     * Controls whether the browser box is deleted when it is stopped.
     */
    @Parameter(property = "browserbox.deleteOnStop", defaultValue = "true")
    protected boolean deleteOnStop;

    /**
     * Amount of time in milliseconds to wait between polling when checking container results, etc.
     * When polling is used depends on browser type.
     */
    @Parameter(property = "browserbox.pollTimeMillis", defaultValue = "300", required = true)
    protected long pollTimeMillis;

    /**
     * Additional Maven artifacts to search for <code>BrowserBox</code> implementations.
     * Each element in the list is a string in the form <i>groupId</i>:<i>artifactId</i>:<i>version</i>.
     * <p>
     *
     * This can be a handy alternative to specifying additional <code>dependency</code> elements when configuring the
     * plugin, especially when running this Maven plugin without a Maven project.
     */
    @Parameter(property = "browserbox.artifacts")
    private List<String> additionalBoxArtifacts = new ArrayList<>();

    @Parameter(defaultValue = "${plugin}", readonly = true)
    private PluginDescriptor thisPlugin;

    protected File getWorkDirectory()
    {
        if (workDirectory != null)
            return workDirectory;

        File buildDirectory;
        if (project == null || project.getBasedir() == null) //Standalone project has null basedir but build directory is ${...}
            buildDirectory = new File("target");
        else
            buildDirectory = new File(project.getBuild().getDirectory());

        return new File(buildDirectory, "browserbox");
    }

    protected ImageUpdateMode getImageUpdateMode()
    {
        return imageUpdateMode;
    }

    /**
     * Read the Selenium project from the plugin's dependency metadata.  By default this will resolve to whatever
     * version of Selenium is defined in the plugin's POM, but this can be overridden by users by explicitly overriding
     * the Selenium server plugin dependency.
     */
    private String resolvePluginSeleniumVersion()
    throws BrowserBoxException
    {
        return thisPlugin.getDependencies().stream().filter(dep -> "org.seleniumhq.selenium".equals(dep.getGroupId()) &&
                                                                   "selenium-server".equals(dep.getArtifactId()))
                                                    .findAny()
                                                    .map(ComponentDependency::getVersion)
                                                    .orElseThrow(() -> new BrowserBoxException("Could not find Selenium dependency of the plugin"));
    }

    private Artifact resolveInternalVncViewerArtifact()
    {
        //Just use the same version of the VNC viewer as the plugin, since they are released together
        return new DefaultArtifact(thisPlugin.getGroupId(), "browserbox-vnc-viewer", "jar", thisPlugin.getVersion());
    }

    protected ProjectConfiguration projectConfiguration()
    {
        return new ProjectConfiguration(project, settings, getKeepBuildIntermediates(), Duration.ofMillis(pollTimeMillis));
    }
    
    protected BoxContext boxContext(ExceptionalSupplier<DockerService, BrowserBoxException> serviceHub)
    throws BrowserBoxException
    {
        return new BoxContext(serviceHub, authConfigFactory, getWorkDirectory().toPath(), globalConfigDirectory.toPath(),
                              getImageUpdateMode(), getLog(), logSpecFactory, session, resourcesFiltering,
                              readerFilter, repositorySystem, repoSession, remoteRepos, archiverManager, getImagePullManager(),
                              authConfig, skipExtendedAuth,
                              invoker,
                              androidHome == null ? null : androidHome.toPath(), Platform.current(),
                              resolvePluginSeleniumVersion(),
                              RepositoryUtils.toArtifact(thisPlugin.getPluginArtifact()), resolveInternalVncViewerArtifact(),
                              dockerSharedMemorySize);
    }

    protected BrowserBox browserBox(ExceptionalSupplier<DockerService, BrowserBoxException> serviceHub)
    throws MojoExecutionException, BrowserBoxException
    {
        BrowserBoxFactory factory = browserBoxFactory(serviceHub);
        ProjectConfiguration projectConfiguration = projectConfiguration();
        return factory.create(box, projectConfiguration, boxContext(serviceHub));
    }
    
    protected BoxLookup boxLookup()
    throws MojoExecutionException
    {
        ClassLoader boxClassLoader = createBoxClassLoader();
        return new BoxLookup(getLog(), boxClassLoader);
    }
    
    protected BrowserBoxFactory browserBoxFactory(ExceptionalSupplier<DockerService, BrowserBoxException> serviceHub)
    throws MojoExecutionException, BrowserBoxException
    {
        String browserType = box.getBrowserType();
        BoxLookup lookup = boxLookup();
        BrowserBoxFactory factory = lookup.findFactory(browserType);
        if (factory == null)
        {
            throw new MojoExecutionException("Browser type '" + browserType + "' not supported.  Available browser types: " +
                    lookup.getAvailableBoxFactoryNames());
        }

        return factory;
    }

    protected ClassLoader createBoxClassLoader()
    throws MojoExecutionException
    {
        getLog().debug("Additional artifacts: " + additionalBoxArtifacts);

        if (additionalBoxArtifacts == null || additionalBoxArtifacts.isEmpty())
            return AbstractBrowserBoxMojo.class.getClassLoader();

        try
        {
            List<Path> jarFiles = DependencyUtils.resolveDependenciesFromStrings(additionalBoxArtifacts, repositorySystem,
                    repoSession, remoteRepos);

            List<URL> jarUrls = new ArrayList<>(jarFiles.size());
            for (Path jarFile : jarFiles)
            {
                jarUrls.add(jarFile.toUri().toURL());
            }

            URL[] jarUrlArray = jarUrls.stream().toArray(URL[]::new);
            return URLClassLoader.newInstance(jarUrlArray, AbstractBrowserBoxMojo.class.getClassLoader());
        }
        catch (DependencyResolutionException e)
        {
            throw new MojoExecutionException("Failed to resolve additional box artifacts: " + e.getMessage(), e);
        }
        catch (MalformedURLException e)
        {
            throw new MojoExecutionException("Failed to generate URLs from dependency JARs: " + e, e);
        }
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException 
    {
        BuildIntermediates.expand(keepBuildIntermediates);
        
        if (browserType != null)
            box.setBrowserType(browserType);
        if (browserVersion != null)
            box.setBrowserVersion(browserVersion);
        if (containerName != null)
            box.setContainerName(containerName);
        if (!tunnelPorts.isEmpty())
            box.setTunnelPorts(tunnelPorts);
        if (browserResolution != null)
            box.setResolution(parseResolution(browserResolution));
        if (browserScaleFactor != null)
            box.setScaleFactor(parseScaleFactor(browserScaleFactor));
        if (sslTrustStoreFile != null)
        {
            if (box.getSslTrust() == null)
                box.setSslTrust(new SslTrustConfiguration());
            
            TrustStoreConfiguration trustStoreConfig = new TrustStoreConfiguration();
            trustStoreConfig.setFile(sslTrustStoreFile);
            trustStoreConfig.setPassword(sslTrustStorePassword);
            trustStoreConfig.setAliases(sslTrustStoreAliases);
            box.getSslTrust().getTrustStores().add(trustStoreConfig);
        }
        if (!browserPropertyList.isEmpty())
            box.setConfiguration(parseMapEntries(browserPropertyList));

        //Android home default set from environment variable if possible
        if (androidHome == null)
        {
            String androidHomeEnv = System.getenv("ANDROID_HOME");
            if (androidHomeEnv != null)
                androidHome = new File(androidHomeEnv);
        }
        
        getLog().debug("Keep build intermediates: " + keepBuildIntermediates);
        
        super.execute();
    }

    /**
     * Each entry takes the from key=value.
     */
    private Map<String, String> parseMapEntries(List<String> stringEntries)
    throws MojoExecutionException
    {
        ImmutableMap.Builder<String, String> map = ImmutableMap.builder();

        for (String stringEntry : stringEntries)
        {
            stringEntry = stringEntry.trim();
            if (!stringEntry.isEmpty())
            {
                String[] keyValuePair = stringEntry.split(Pattern.quote("="), 2);
                if (keyValuePair.length != 2)
                    throw new MojoExecutionException("Error parsing map property: " + stringEntry);

                map.put(keyValuePair[0], keyValuePair[1]);
            }
        }

        return map.build();
    }
    
    private Resolution parseResolution(String s)
    throws MojoExecutionException
    {
        String[] tokens = s.split("\\D");
        if (tokens.length != 2)
            throw new MojoExecutionException("Failed to parse resolution string: " + s);
        
        try
        {
            return new Resolution(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]));
        }
        catch (NumberFormatException e)
        {
            throw new MojoExecutionException("Failed to parse resolution string: " + s, e);
        }
    }

    private Double parseScaleFactor(String s)
    throws MojoExecutionException
    {
        try
        {
            return Double.parseDouble(s);
        }
        catch (NumberFormatException e)
        {
            throw new MojoExecutionException("Failed to parse scale-factor string: " + s, e);
        }
    }

    protected void stopBrowserContainer(BrowserBox browserBox)
    throws MojoExecutionException, BrowserBoxException
    {
        //Clean up video recording session
        VideoRecordingSession videoSession = (VideoRecordingSession)getPluginContext().remove(VIDEO_RECORDING_SESSION_KEY_PREFIX + browserBox.getName());
        if (videoSession != null)
        {
            getLog().info("Ending video recording, saving to " + videoSession.getVideoFile().toAbsolutePath());
            try 
            {
                videoSession.getRecording().stopAndSave(videoSession.getVideoFile());
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Error saving video to " + videoSession.getVideoFile().toAbsolutePath() + ": " + e.getMessage(), e);
            }
        }
        
        boolean exists = true;
        if (!browserBox.exists())
        {
            getLog().warn("Browser box '" + browserBox.getName() + "' does not exist");
            exists = false;
        }
        else if (!browserBox.isRunning())
            getLog().info("Browser box '" + browserBox.getName() + "' is not running");
        else
        {
            getLog().info("Stopping browser box '" + browserBox.getName() + "'");
            browserBox.stop();
            getLog().info("Browser box stopped.");
        }

        if (exists && deleteOnStop)
        {
            getLog().info("Deleting browser box '" + browserBox.getName() + "'");
            browserBox.delete();
        }

        //Delete active properties file if it exists
        try
        {
            Files.deleteIfExists(activePropertiesFile.toPath());
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error deleting active properties file " + activePropertiesFile.toPath() + ": " + e.getMessage(), e);
        }
    }

    public Set<BuildIntermediates> getKeepBuildIntermediates() 
    {
        return keepBuildIntermediates;
    }
    
    protected String getBrowserType()
    {
        return browserType;
    }
}
