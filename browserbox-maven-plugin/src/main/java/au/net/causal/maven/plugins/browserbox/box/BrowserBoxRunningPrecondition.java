package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import io.fabric8.maven.docker.wait.WaitUtil;

public class BrowserBoxRunningPrecondition implements WaitUtil.Precondition 
{
    private final BrowserBox browserBox;
    private final BoxContext context;

    public BrowserBoxRunningPrecondition(BrowserBox browserBox, BoxContext context) 
    {
        this.browserBox = browserBox;
        this.context = context;
    }

    @Override
    public boolean isOk() 
    {
        try 
        {
            return browserBox.isRunning();
        } 
        catch (BrowserBoxException e) 
        {
            context.getLog().error("Error retrieving status while waiting: " + e, e);
            return false;
        }
    }

    @Override
    public void cleanup() 
    {
        //Nothing to clean up
    }
}
