package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.vagrant.BoxDefinition;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.BoxAddOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.VagrantException;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MicrosoftVagrantBoxInstaller
{
    private final ProjectConfiguration projectConfiguration;
    private final BoxContext context;
    private final Vagrant vagrant;
    
    public MicrosoftVagrantBoxInstaller(ProjectConfiguration projectConfiguration, BoxContext context, 
                                        Vagrant vagrant)
    {
        Objects.requireNonNull(projectConfiguration, "projectConfiguration == null");
        Objects.requireNonNull(context, "context == null");
        Objects.requireNonNull(vagrant, "vagrant == null");
        this.projectConfiguration = projectConfiguration;
        this.context = context;
        this.vagrant = vagrant;
    }
    
    protected Log getLog()
    {
        return context.getLog();
    }
    
    private Path extractBoxFromZip(URL vagrantBoxZipFile)
    throws BrowserBoxException
    {
        //Extract box file from ZIP
        Path targetBoxFile = null;
        try (ZipInputStream is = new ZipInputStream(vagrantBoxZipFile.openStream()))
        {
            ZipEntry entry;
            do
            {
                entry = is.getNextEntry();
                if (entry != null)
                {
                    if (!entry.isDirectory() && entry.getName().toLowerCase(Locale.ENGLISH).endsWith(".box"))
                    {
                        targetBoxFile = context.getTempDirectory().resolve(entry.getName());
                        Files.copy(is, targetBoxFile);
                    }
                }
            }
            while (entry != null);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("I/O error reading Vagrant box ZIP file " + vagrantBoxZipFile + ": " + e.getMessage(), e);
        }

        if (targetBoxFile == null)
            throw new BrowserBoxException("Could not find box file in ZIP: " + vagrantBoxZipFile);
        
        return targetBoxFile;
    }

    /**
     * Installs a base unmodified Vagrant image for a browser type.
     * 
     * @param boxZipUrl URL of the box file zip.
     * @param boxName the Vagrant image name to install to.
     *                
     * @throws BrowserBoxException if an error occurs.
     */
    public void install(URL boxZipUrl, String boxName)
    throws BrowserBoxException
    {
        Objects.requireNonNull(boxZipUrl, "boxZipUrl == null");
        
        Path vagrantBoxFile = extractBoxFromZip(boxZipUrl);
        getLog().info("Registering Vagrant box " + boxName);
        BoxAddOptions addOptions = new BoxAddOptions(new BoxDefinition(boxName, "0", "virtualbox"), vagrantBoxFile);
        addOptions.setForce(true);
        try
        {
            vagrant.boxAdd(addOptions);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Error adding Vagrant box: " + e.getMessage(), e);
        }
        
        //Delete extracted box file
        try 
        {
            Files.deleteIfExists(vagrantBoxFile);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Failed to delete box file " + vagrantBoxFile + ": " + e.getMessage(), e);
        }
    }
}
