package au.net.causal.maven.plugins.browserbox.vagrant;

import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Interface to the Vagrant executable.
 */
public interface Vagrant
{
    public void up(UpOptions options)
    throws VagrantException;

    public void provision(ProvisionOptions options)
    throws VagrantException;

    public void destroy(DestroyOptions options)
    throws VagrantException;

    public void halt(HaltOptions options)
    throws VagrantException;

    public BoxStatus status(StatusOptions options)
    throws VagrantException;

    public List<? extends BoxDefinition> boxList(BoxListOptions options)
    throws VagrantException;

    public void boxRemove(BoxRemoveOptions options)
    throws VagrantException;
    
    public void boxAdd(BoxAddOptions options)
    throws VagrantException;
    
    public void createPackage(CreatePackageOptions options)
    throws VagrantException;

    public List<? extends Plugin> pluginList(PluginListOptions options)
    throws VagrantException;

    public void pluginInstall(PluginInstallOptions options)
    throws VagrantException;
    
    public static class PluginListOptions extends BaseOptions
    {
    }

    public static class PluginInstallOptions extends BaseOptions
    {
        private String pluginName;

        public PluginInstallOptions(String pluginName)
        {
            this.pluginName = pluginName;
        }

        public String getPluginName()
        {
            return pluginName;
        }

        public void setPluginName(String pluginName)
        {
            this.pluginName = pluginName;
        }
    }

    public static class BoxListOptions extends BaseOptions
    {
    }

    public static class UpOptions extends InstanceOptions
    {
        public UpOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }
    }

    public static class ProvisionOptions extends InstanceOptions
    {
        private final List<String> provisioners = new ArrayList<>();

        public ProvisionOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }

        public List<String> getProvisioners()
        {
            return provisioners;
        }

        public void setProvisioners(List<String> provisioners)
        {
            this.provisioners.clear();
            this.provisioners.addAll(provisioners);
        }

        public void setProvisioners(String... provisioners)
        {
            setProvisioners(Arrays.asList(provisioners));
        }
    }

    public static class DestroyOptions extends InstanceOptions
    {
        public DestroyOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }
    }

    public static class HaltOptions extends InstanceOptions
    {
        public HaltOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }
    }

    public static class StatusOptions extends InstanceOptions
    {
        public StatusOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }
    }
    
    public static class BoxRemoveOptions extends BaseOptions
    {
        private final BoxDefinition box;
        private boolean force;

        public BoxRemoveOptions(BoxDefinition box)
        {
            this.box = box;
        }

        public BoxDefinition getBox()
        {
            return box;
        }

        public boolean isForce()
        {
            return force;
        }

        public void setForce(boolean force)
        {
            this.force = force;
        }
    }

    public static class BoxAddOptions extends BaseOptions
    {
        private final BoxDefinition box;
        private final Path boxFile;
        private boolean force;
        
        public BoxAddOptions(BoxDefinition box, Path boxFile)
        {
            this.box = box;
            this.boxFile = boxFile;
        }

        public BoxDefinition getBox() 
        {
            return box;
        }

        public boolean isForce() 
        {
            return force;
        }

        public void setForce(boolean force) 
        {
            this.force = force;
        }

        public Path getBoxFile() 
        {
            return boxFile;
        }
    }

    public static abstract class BaseOptions
    {
        private final Map<String, String> environment = new LinkedHashMap<>();
        private Duration timeout = Duration.ofHours(8L);

        public void env(String name, String value)
        {
            environment.put(name, value);
        }

        public Duration getTimeout()
        {
            return timeout;
        }

        public void setTimeout(Duration timeout)
        {
            this.timeout = timeout;
        }

        public Map<String, String> getEnvironment()
        {
            return environment;
        }
    }

    public static abstract class InstanceOptions extends BaseOptions
    {
        private Path baseDirectory;
        private String vagrantFileName;
        private String boxName;

        public InstanceOptions(Path baseDirectory, String boxName)
        {
            this.baseDirectory = baseDirectory;
            this.boxName = boxName;
        }

        public Path getBaseDirectory()
        {
            return baseDirectory;
        }

        public void setBaseDirectory(Path baseDirectory)
        {
            this.baseDirectory = baseDirectory;
        }

        public String getVagrantFileName()
        {
            return vagrantFileName;
        }

        public void setVagrantFileName(String vagrantFileName)
        {
            this.vagrantFileName = vagrantFileName;
        }

        public String getBoxName()
        {
            return boxName;
        }

        public void setBoxName(String boxName)
        {
            this.boxName = boxName;
        }
    }
    
    public static class CreatePackageOptions extends InstanceOptions
    {
        private String base;

        public CreatePackageOptions(Path baseDirectory, String boxName)
        {
            super(baseDirectory, boxName);
        }

        public String getBase() 
        {
            return base;
        }

        public void setBase(String base) 
        {
            this.base = base;
        }
    }

    public static enum BoxStatus
    {
        RUNNING,
        STOPPED,
        NOT_CREATED,
        ABORTED,
        STOPPING,
        PAUSED
    }

    public static class Plugin
    {
        private final String name;
        private final String version;

        public Plugin(String name, String version)
        {
            this.name = name;
            this.version = version;
        }

        public String getName()
        {
            return name;
        }

        public String getVersion()
        {
            return version;
        }

        @Override
        public String toString()
        {
            final StringBuilder sb = new StringBuilder("Plugin{");
            sb.append("name='").append(name).append('\'');
            sb.append(", version='").append(version).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}
