package au.net.causal.maven.plugins.browserbox.execute;

import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class FileNamePatternMatcher implements Predicate<Path> 
{
    private final Pattern pattern;
    
    public FileNamePatternMatcher(Pattern pattern)
    {
        Objects.requireNonNull(pattern, "pattern == null");
        this.pattern = pattern;
    }
    
    @Override
    public boolean test(Path path) 
    {
        return pattern.matcher(path.getFileName().toString()).matches();
    }
}
