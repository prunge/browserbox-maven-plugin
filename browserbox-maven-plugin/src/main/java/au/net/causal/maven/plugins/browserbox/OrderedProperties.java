package au.net.causal.maven.plugins.browserbox;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Properties but with keys that are in natural order.  When written to file, keys will be written in order.
 */
public class OrderedProperties extends Properties 
{
    //For Java 8
    @Override
    public synchronized Enumeration<Object> keys() 
    {
        return Collections.enumeration(new TreeSet<>(keySet()));
    }
    
    //For Java 10 (and maybe 9)
    @Override
    public Set<Entry<Object, Object>> entrySet()
    {
        return super.entrySet().stream().collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(entry -> entry.getKey().toString()))));
    }
}
