package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ExceptionalSupplier;
import org.apache.maven.plugin.logging.Log;

import java.time.Duration;
import java.util.List;
import java.util.Objects;

public class RetryEdgeDriverVersionResolver implements EdgeDriverVersionResolver
{
    private final EdgeDriverVersionResolver resolver;
    private final int maxTries;
    private final Duration retryWaitTime;
    private final Log log;

    public RetryEdgeDriverVersionResolver(EdgeDriverVersionResolver resolver, int maxTries, Duration retryWaitTime, Log log) 
    {
        Objects.requireNonNull(resolver, "resolver == null");
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(retryWaitTime, "retryWaitTime == null");
        this.resolver = resolver;
        this.maxTries = maxTries;
        this.retryWaitTime = retryWaitTime;
        this.log = log;
    }
    
    private <T> T retry(ExceptionalSupplier<T, BrowserBoxException> body)
    throws BrowserBoxException
    {
        BrowserBoxException lastException = null;
        for (int i = 0; i < maxTries; i++)
        {
            try
            {
                return body.get();
            }
            catch (BrowserBoxException e)
            {
                lastException = e;
                
                log.warn("Failed call #" + (i + 1) + " of " + maxTries + " to " + resolver.getClass().getCanonicalName() + ": " + e.getMessage());
                log.debug(e);

                try
                {
                    Thread.sleep(retryWaitTime.toMillis());
                }
                catch (InterruptedException ex)
                {
                    //Ignore
                }
            }
        }

        throw lastException;
    }

    @Override
    public Version resolve(String edgeVersion) throws BrowserBoxException 
    {
        return retry(() -> resolver.resolve(edgeVersion));
    }

    @Override
    public Version resolveWithFallback(String edgeVersion) 
    throws BrowserBoxException 
    {
        return retry(() -> resolver.resolveWithFallback(edgeVersion));
    }

    @Override
    public List<? extends Version> allVersions() 
    throws BrowserBoxException 
    {
        return retry(resolver::allVersions);
    }
}
