package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.collect.ImmutableList;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Optional;
import java.util.regex.Pattern;

public class MacRealVncFinder extends GenericPathBasedFinder
{
    private static final Pattern REALVNC_PATTERN = Pattern.compile("^RealVNC$", Pattern.CASE_INSENSITIVE);
    private static final Pattern REALVNC_VIEWER_PATTERN = Pattern.compile("^VNC\\sViewer\\.app$", Pattern.CASE_INSENSITIVE);
    
    private final RealVncTools realVnc = new RealVncTools();
    
    public MacRealVncFinder()
    {
        super(ImmutableList.of(Paths.get("/Applications"), Paths.get(System.getProperty("user.home")).resolve("Applications")));
    }

    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box,
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration,
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> dir = findInSearchPath(REALVNC_PATTERN);
        if (!dir.isPresent())
            return Optional.empty();
        
        Optional<Path> executable = findInSpecifiedPaths(Collections.singleton(dir.get()), REALVNC_VIEWER_PATTERN);
        if (!executable.isPresent())
            return Optional.empty();

        Commandline commandline =  new Commandline("open");
        Path vncFile = realVnc.generateVncFile(connectionInfo, context);
        commandline.addArguments(args("-W", executable.get().toAbsolutePath().toString(), "--args",
                vncFile.toAbsolutePath().toString()));
        
        return Optional.of(commandline);
    }
}
