package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.concurrent.TimeUnit;

/**
 * Starts the browser box and waits until termination with control+c.  Shuts down cleanly in shutdown hook.
 */
@Mojo(name="startwait", requiresProject = false)
public class StartAndWaitMojo extends StartMojo
{
    protected void registerHookAndStartBrowserBox(ExceptionalSupplier<DockerService, BrowserBoxException> dockerService)
    throws MojoExecutionException, MojoFailureException, DockerAccessException
    {
	    try
	    {
	        BrowserBox box = browserBox(dockerService);

            try
            {
                super.executeInternal(dockerService);
            }
            catch (Throwable t)
            {
                //If something goes wrong, run the shutdown stuff immediately instead of in a shutdown hook
                //I know there are classloader issues when running from hooks with Maven plugins
                //(see https://issues.apache.org/jira/browse/MNG-5589) but we do it anyway and it's more likely
                //more classes need loading if an error occurs early than if everything runs smoothly
                try
                {
                    if (shouldStopContainer())
                        stopBrowserContainer(box);
                }
                catch (Throwable stopError)
                {
                    t.addSuppressed(stopError);
                }

                throw t;
            }

            //If we get here nothing went wrong starting the browser so register the shutdown hook now
            //This shutdown hook runs when control+c is pressed and cleanly shuts down the box
            //There are known problems with shutdown hooks and Maven plugins
            //(see https://issues.apache.org/jira/browse/MNG-5589)
            //but we'll do it anyway for now - it's only a convenience for users to stop with control+c
            Thread stopperThread = new Thread(new Stopper(box), "browserbox-shutdown-hook-thread");
            Runtime.getRuntime().addShutdownHook(stopperThread);
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException("Error setting up browserbox: " + e, e);
        }
    }

    /**
     * @return true if the container should be stopped on error or on control+c, false if not.
     */
    protected boolean shouldStopContainer()
    {
        return true;
    }

    protected void waitForControlC()
    {
        //Now that everything is set up, block until control+c is pressed
        getLog().info(getWaitMessage());
        try
        {
            while (true)
            {
                Thread.sleep(TimeUnit.DAYS.toMillis(10000L));
            }
        }
        catch (InterruptedException e)
        {
            getLog().info("Interrupted waiting.", e);
        }
    }

    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> dockerService)
    throws DockerAccessException, MojoExecutionException, MojoFailureException
    {
        registerHookAndStartBrowserBox(dockerService);
        waitForControlC();
    }

    protected String getWaitMessage()
    {
        return "Press control+c to stop the browser container...";
    }

    private class Stopper implements Runnable
    {
        private final BrowserBox browserBox;

        public Stopper(BrowserBox browserBox)
        {
            this.browserBox = browserBox;
        }

        @Override
        public void run()
        {
            try
            {
                if (shouldStopContainer())
                    stopBrowserContainer(browserBox);
            }
            catch (BrowserBoxException | MojoExecutionException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
}
