package au.net.causal.maven.plugins.browserbox.ms;

import com.google.common.collect.ImmutableList;

import javax.ws.rs.client.ClientBuilder;
import java.util.regex.Pattern;

public class WebEdgeVersionRegistry extends WebVersionRegistry
{
    //e.g. MSEdge on Win10 (x64) Preview (16.16257)
    private static final Pattern ITEM_NAME_PATTERN =   Pattern.compile("^MSEdge\\s+.*\\s+\\((\\d+\\.\\d+)\\)$", Pattern.CASE_INSENSITIVE);

    //e.g. MSEdge on Win10 (x64) Stable 1809
    //TODO this is not actually the Edge version number but it's the best we can do with what we have
    //   at least it gives an indication that a new version is available
    private static final Pattern ITEM_NAME_PATTERN_2 = Pattern.compile("^MSEdge\\s+.*\\s+(\\d+)$");

    public WebEdgeVersionRegistry(ClientBuilder clientBuilder)
    {
        super(clientBuilder, ImmutableList.of(ITEM_NAME_PATTERN, ITEM_NAME_PATTERN_2));
    }

    public WebEdgeVersionRegistry()
    {
        super(ImmutableList.of(ITEM_NAME_PATTERN, ITEM_NAME_PATTERN_2));
    }

    @Override
    protected String getPreKnownVersionForItem(DownloadItem item)
    {
        //Sigh - Microsoft took away all the VMs and the catalog only has one Edge version as of 2020/05/07, it probably won't be updated again since Edge is EOL
        //so hardcode this version.  Really wish they would have left the old versions alone.
        if ("Win10 with MsEdge".equals(item.getName()))
            return "18.17763";

        return super.getPreKnownVersionForItem(item);
    }
}

