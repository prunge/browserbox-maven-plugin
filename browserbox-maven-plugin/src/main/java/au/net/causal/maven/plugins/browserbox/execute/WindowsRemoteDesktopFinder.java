package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

public class WindowsRemoteDesktopFinder extends EnvironmentPathBasedFinder
{
    private static final Pattern REMOTE_DESKTOP_EXE_PATTERN = Pattern.compile("^mstsc.exe$", Pattern.CASE_INSENSITIVE);
    
    private final WindowsRemoteDesktopTools windowsRemoteDesktopTools = new WindowsRemoteDesktopTools();
    
    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                                         ProjectConfiguration projectConfiguration, BoxContext context) 
    throws IOException, BrowserBoxException
    {
        Optional<Path> executable = findInSearchPath(REMOTE_DESKTOP_EXE_PATTERN);
        if (!executable.isPresent())
            return Optional.empty();
        
        Commandline commandline = new Commandline(executable.get().toAbsolutePath().toString());
        Path rdpFile = windowsRemoteDesktopTools.generateRdpFile(connectionInfo, WindowsRemoteDesktopTools.RdpToolMode.MICROSOFT, box, boxConfiguration, projectConfiguration, context);
        
        commandline.addArguments(args(rdpFile.toAbsolutePath().toString()));
        
        return Optional.of(commandline);
    }
}
