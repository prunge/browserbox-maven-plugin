package au.net.causal.maven.plugins.browserbox.execute;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

public class ExecutableFileMatcher implements Predicate<Path>
{
    @Override
    public boolean test(Path path) 
    {
        return Files.isExecutable(path);
    }
}
