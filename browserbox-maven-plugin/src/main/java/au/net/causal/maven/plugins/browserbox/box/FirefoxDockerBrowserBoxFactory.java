package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionBlacklist;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionResolver;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionTools;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.EmptyBrowserVersionBlacklist;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.FirefoxDesktopDownloadVersionScanner;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.FirefoxNonLinux64BitMavenBlacklist;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.GeneratedImageFirefoxVersionResolver;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.Tag;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.model.Image;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionScheme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FirefoxDockerBrowserBoxFactory extends SeleniumDockerBrowserBoxFactory 
{
    private final BrowserVersionTools versionTools = new BrowserVersionTools();
    
    public FirefoxDockerBrowserBoxFactory() 
    {
        super("firefox");
    }

    protected BrowserVersionResolver imageGeneratorVersionResolver(BoxContext context)
    {
        BrowserVersionBlacklist versionBlacklist = new FirefoxNonLinux64BitMavenBlacklist(context);
        return new GeneratedImageFirefoxVersionResolver(context.getLog(), versionBlacklist);
    }

    /**
     * Searches for an existing image with an exact match of the browser version.
     * 
     * @param browserVersion the browser version to search for.
     *                       
     * @return a matching image reference for this browser version, or null if none was found.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    private ImageReference findExistingGeneratedImage(String browserVersion, BoxContext context)
    throws BrowserBoxException
    {
        try 
        {
            for (Image image : context.getDockerServiceHub().getDockerAccess().listImages(false))
            {
                //image = dockerHacks.fixImage(image);
                if (dockerNaming.isCustomVersionFirefoxImage(image, browserVersion))
                {
                    //Reference is by tag, so return the first one
                    if (!image.getRepoTags().isEmpty())
                        return ImageReference.parse(image.getRepoTags().get(0)); //Any image is OK
                }
            }
        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException(e);
        }
        
        return null;
    }
    
    @Override
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) 
    throws BrowserBoxException 
    {
        initializeDefaults(boxConfiguration, context);

        BrowserVersionResolver versionResolver = versionResolver(context);
        
        //If there is a version exact match built as a Docker image already, then use that
        ImageReference existingImage = findExistingGeneratedImage(boxConfiguration.getBrowserVersion(), context);
        if (existingImage != null)
        {
            context.getLog().info("Found existing image for Firefox version " + boxConfiguration.getBrowserVersion() + ": " + existingImage.getName());
            Tag fromTag = versionResolver.tagForBrowserVersion(versionResolver.defaultVersion());
            return new SeleniumFirefoxCustomVersionBrowserBox(boxConfiguration, projectConfiguration, context, fromTag, 
                            boxConfiguration.getBrowserVersion(), existingImage);
        }
        
        BrowserVersionResolver customVersionResolver = imageGeneratorVersionResolver(context);
        
        //Determine if we need to build the base image - version, as filled in by defaults, should never be null
        String version = boxConfiguration.getBrowserVersion();
        Tag versionTag = versionResolver.tagForBrowserVersion(version);
        context.getLog().info("Resolved browser version " + version + " to tag " + versionTag);
        if (versionTag == null)
        {
            context.getLog().info("Pre-built Selenium image for this version of Firefox does not exist, custom image will be generated.");
            
            //Resolve the correct version
            List<String> downloadableVersions = customVersionResolver.availableBrowserVersions();
            String matchingBrowserVersion = versionTools.findBrowserVersion(version, downloadableVersions, 
                                                    Function.identity(), Function.identity());
            if (matchingBrowserVersion == null)
                throw new BrowserBoxException("Could not find any matching version for '" + version + "'.");
            
            //Find closest previous tag for this version
            //Need to do this because there often there are Selenium version incompatibilities
            //so if we're replacing Firefox choose a base image that already had a version that was pretty close
            //Special exception is for really old versions where we just take the latest base image - this seems to
            //work (probably because GeckoDriver goes into pre-marionette mode) better
            String fromVersion = versionTools.findClosestVersionLessThan(version, versionResolver.availableBrowserVersions());
            if (fromVersion == null) //If it's too old, use the latest version - this works well enough from experiments
                fromVersion = versionResolver.defaultVersion();

            Tag fromTag = versionResolver.tagForBrowserVersion(fromVersion);

            ImageReference endBaseImageName = new ImageReference(dockerNaming.customVersionRepositoryName("standalone-firefox-debug"), 
                                                    dockerNaming.firefoxCustomVersionTag(fromTag, matchingBrowserVersion));
            return new SeleniumFirefoxCustomVersionBrowserBox(boxConfiguration, projectConfiguration, context, fromTag, matchingBrowserVersion, endBaseImageName);
        }
        
        ImageReference baseImageName = dockerBaseImageName(boxConfiguration, projectConfiguration, context);
        return new FirefoxDockerBrowserBox(boxConfiguration, projectConfiguration, context, baseImageName);
    }

    @Override
    public List<String> availableKnownVersions(String type, BoxContext context)
    throws BrowserBoxException 
    {
        List<String> versions = super.availableKnownVersions(type, context);
        List<String> moreVersions = imageGeneratorVersionResolver(context).availableBrowserVersions();

        //Sorting
        List<Version> sortableVersions = new ArrayList<>();
        VersionScheme versionScheme = new GenericVersionScheme();
        try 
        {
            for (String version : versions) 
            {
                sortableVersions.add(versionScheme.parseVersion(version));
            }
            for (String version : moreVersions) 
            {
                sortableVersions.add(versionScheme.parseVersion(version));
            }
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new BrowserBoxException(e);
        }
        
        return sortableVersions.stream().sorted().map(Version::toString).distinct().collect(Collectors.toList());
    }

    /* No longer works since they start putting 'latest' into the source
    protected TagBrowserVersionReader versionReaderForBrowserType(String browserType,
                                                                  ProjectConfiguration projectConfiguration,
                                                                  BoxContext context)
    throws BrowserBoxException, IOException
    {
            Path tempDir = context.getTempDirectory();
            return new MultiBrowserVersionReader(new FirefoxBrowserVersionReader(tempDir),
                        super.versionReaderForBrowserType(browserType, projectConfiguration, context));
    }
    */

    @Override
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, 
                                      BoxContext context, GenerateVersionsContext generateVersionsContext) 
    throws BrowserBoxException, IOException 
    {
        super.generateVersionsFiles(type, projectConfiguration, context, generateVersionsContext);

        //Do blacklist only for Firefox
        context.getLog().info("Reading known download versions for " + type);
        firefoxDownloadVersionScanner(context, generateVersionsContext).generateBlacklist(dockerNaming.firefoxInstallerUrl().toURL(), generateVersionsContext.getKnownVersions());
        generateVersionsContext.saveKnownVersions();
        context.getLog().debug(generateVersionsContext.getKnownVersions().size() + " versions recorded");
    }

    private FirefoxDesktopDownloadVersionScanner firefoxDownloadVersionScanner(BoxContext context,
                                                                               GenerateVersionsContext generateVersionsContext)
    {
        return new FirefoxDesktopDownloadVersionScanner(context.getLog(), new EmptyBrowserVersionBlacklist(),
                                                        generateVersionsContext.getDownloadExecutor());
    }
}
