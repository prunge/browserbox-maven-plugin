package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.DockerPuller;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.access.DockerAccess;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.access.ExecException;
import io.fabric8.maven.docker.access.PortMapping;
import io.fabric8.maven.docker.config.ImageConfiguration;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import io.fabric8.maven.docker.config.WaitConfiguration;
import io.fabric8.maven.docker.model.Container;
import io.fabric8.maven.docker.service.QueryService;
import io.fabric8.maven.docker.service.RunService;
import io.fabric8.maven.docker.util.GavLabel;
import io.fabric8.maven.docker.wait.HttpPingChecker;
import io.fabric8.maven.docker.wait.PreconditionFailedException;
import io.fabric8.maven.docker.wait.WaitChecker;
import io.fabric8.maven.docker.wait.WaitUtil;
import org.apache.maven.plugin.MojoExecutionException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Read the browser version by spinning up the image in Docker and reading the version using Selenium.
 */
public class DockerRunBrowserVersionReader implements TagBrowserVersionReader 
{
    private final String browserType;
    private final ProjectConfiguration projectConfiguration;
    private final BoxContext context;
    
    public DockerRunBrowserVersionReader(String browserType, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        Objects.requireNonNull(browserType, "browserType == null");
        Objects.requireNonNull(projectConfiguration, "projectConfiguration == null");
        Objects.requireNonNull(context, "context == null");
        this.browserType = browserType;
        this.projectConfiguration = projectConfiguration;
        this.context = context;
    }
    
    @Override
    public String readBrowserVersion(Tag tag) 
    throws BrowserBoxException 
    {
        context.getLog().info("Reading version for " + tag.getName());
        
        String version = tag.getName();
        
        try 
        {
            ImageConfiguration imageConfiguration = imageConfiguration(version);

            //Pull image
            boolean imageWasDownloaded = pullRemoteImage(imageConfiguration);

            try 
            {
                //Run image
                runImage(imageConfiguration);

                try 
                {
                    //Wait until fully started
                    waitUntilStarted(Duration.ofMinutes(5L));
                    
                    //Pull out the version
                    String result =  readVersionFromRunningImage();
                    
                    context.getLog().info("Browser version: " + result);
                    
                    return result;
                }
                finally
                {
                    stopAndDeleteContainer(imageConfiguration);   
                }
            }
            finally
            {
                if (imageWasDownloaded)
                    deleteImage(imageConfiguration);
            }
        }
        catch (DockerAccessException | MojoExecutionException | TimeoutException e)
        {
            throw new BrowserBoxException("Error pulling image: " + e.getMessage(), e);
        }
    }
    
    private String readVersionFromRunningImage()
    throws BrowserBoxException
    {
        DesiredCapabilities capabilities = (browserType.equalsIgnoreCase("chrome") ? DesiredCapabilities.chrome() : DesiredCapabilities.firefox());
        try 
        {
            URL server = new URL(seleniumUrl());
            RemoteWebDriver driver = new RemoteWebDriver(server, capabilities);
            try
            {
                Capabilities serverCaps = driver.getCapabilities();
                System.out.println("Browser name: " + serverCaps.getBrowserName());
                String version = serverCaps.getVersion();
                String userAgent = (String)driver.executeScript("return navigator.userAgent");
                if (version == null || version.trim().isEmpty())
                    version = readVersionFromUserAgentString(serverCaps.getBrowserName(), userAgent);

                System.out.println("Browser version: " + version);



                System.out.println("User agent: " + userAgent);

                return version;
            }
            finally 
            {
                driver.quit();    
            }
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    @VisibleForTesting
    static String readVersionFromUserAgentString(String browserName, String userAgent)
    {
        Pattern p = Pattern.compile(Pattern.quote(browserName + "/") + "(\\S+)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(userAgent);
        if (!m.find())
            return "";

        return m.group(1);
    }
    
    protected BoxContext getContext()
    {
        return context;
    }
    
    protected ProjectConfiguration getProjectConfiguration()
    {
        return projectConfiguration;
    }
    
    private void stopAndDeleteContainer(ImageConfiguration imageConfiguration)
    throws BrowserBoxException
    {
        try 
        {
            DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();

            //Resolve container name to ID first
            //There's what looks like a bug in RunService.shutdown() that does a substring for a log message and assumes
            //it's an  ID instead of a name and has a particular length
            Container container = dockerAccess.getContainer(containerName());

            //No need to stop a container that doesn't exist
            if (container != null)
            {
                String containerId = container.getId();
                context.getDockerServiceHub().getRunService().stopContainer(containerId, imageConfiguration, true, false);
            }

            boolean removeVolumes = true;
            dockerAccess.removeContainer(containerName(), removeVolumes);
        }
        catch (DockerAccessException | ExecException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    protected void deleteImage(ImageConfiguration imageConfiguration)
    throws BrowserBoxException
    {
        DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();
        try
        {
            boolean force = false;
            dockerAccess.removeImage(imageConfiguration.getName(), force);
        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    protected void runImage(ImageConfiguration imageConfiguration)
    throws BrowserBoxException
    {
        RunService runService = getContext().getDockerServiceHub().getRunService();
        Properties projProperties = getProjectConfiguration().getProjectProperties();
        GavLabel pomLabel = getProjectConfiguration().getPomLabel();
        PortMapping portMapping = runService.createPortMapping(imageConfiguration.getRunConfiguration(), projProperties);
        try
        {
            Date buildTimestamp = new Date();
            String containerId = runService.createAndStartContainer(imageConfiguration, portMapping, pomLabel, projProperties, getProjectConfiguration().getBaseDirectory(), null, buildTimestamp);
            getContext().getLog().info("Created docker container: " + containerId);

        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }

    }

    /**
     * @return true if a fresh image was pulled, false if the image already existed locally.
     */
    private boolean pullRemoteImage(ImageConfiguration imageConfiguration)
    throws BrowserBoxException, DockerAccessException, MojoExecutionException
    {
        QueryService queryService = getContext().getDockerServiceHub().getQueryService();
        String imageName = imageConfiguration.getName();

        //Check if image already exists
        //OK since we are just reading browser versions and they are unlikely to change for stable tags
        //We are only using this return value to check if we need to delete the image afterwards
        boolean alreadyExists = queryService.hasImage(imageName);
        if (!alreadyExists)
            DockerPuller.pullImage(imageName, getProjectConfiguration(), getContext());

        return !alreadyExists;
    }

    protected ImageConfiguration imageConfiguration(String version)
    {
        RunImageConfiguration.Builder runBuilder = new RunImageConfiguration.Builder();
        
        configureRunImage(runBuilder);
        RunImageConfiguration runConfig = runBuilder.build();

        ImageConfiguration.Builder imageBuilder = new ImageConfiguration.Builder();
        imageBuilder.runConfig(runConfig);
        configureImage(imageBuilder, version);
        return imageBuilder.build();
    }

    /**
     * Customize the docker run image for the browser box.
     * To augment default settings, call <code>super.configureRunImage()</code> first.
     *
     * @param builder the builder to configure.
     */
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        int mappedWebDriverPort = 24444;
        int containerWebDriverPort = containerWebDriverPort();
        builder.containerNamePattern("%a")
               .ports(ImmutableList.of(mappedWebDriverPort + ":" + containerWebDriverPort));
    }

    protected void configureImage(ImageConfiguration.Builder builder, String version)
    {
        builder.name(dockerImageName(version))
                .alias(containerName());
    }
    
    protected String dockerImageName(String version)
    {
        return "selenium/standalone-" + browserType + "-debug:" + version;
    }
    
    protected String containerName()
    {
        return "browserbox-" + browserType + "-versioncheck";
    }
    
    protected int containerWebDriverPort()
    {
        return 4444;
    }
    
    protected int webDriverPort()
    {
        return 24444;
    }
    
    protected String seleniumUrl()
    throws BrowserBoxException
    {
        return "http://" + context.getDockerHostAddress() + ":" + webDriverPort() + "/wd/hub";
    }

    public void waitUntilStarted(Duration maxTimeToWait)
    throws TimeoutException, BrowserBoxException
    {
        int waitMillis;
        if (maxTimeToWait == null)
            waitMillis = Integer.MAX_VALUE;
        else
            waitMillis = Math.toIntExact(Math.min(maxTimeToWait.toMillis(), Integer.MAX_VALUE));

        String url = seleniumUrl();
        WaitChecker checker = new HttpPingChecker(url, "GET", WaitConfiguration.DEFAULT_STATUS_RANGE);
        try
        {
            WaitUtil.wait(new WaitUtil.Precondition() {
                @Override
                public boolean isOk() 
                {
                    return true;
                }

                @Override
                public void cleanup() 
                {
                }
            }, waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BrowserBoxException("Error occurred while waiting for browser box to start up: " + e, e);
        }
    }

}
