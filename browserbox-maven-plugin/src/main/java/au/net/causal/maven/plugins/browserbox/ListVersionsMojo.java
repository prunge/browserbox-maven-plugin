package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BoxLookup;
import au.net.causal.maven.plugins.browserbox.box.BrowserBoxFactory;
import com.google.common.base.Strings;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.List;

@Mojo(name="versions", requiresProject = false)
public class ListVersionsMojo extends AbstractBrowserBoxMojo 
{
    private void listVersions(BrowserBoxFactory boxFactory, String browserType, ExceptionalSupplier<DockerService, BrowserBoxException> service)
    throws MojoExecutionException
    {
        try
        {
            List<String> versions = boxFactory.availableKnownVersions(browserType, boxContext(service));
            getLog().info("Browser versions for '" + browserType + "': " + versions);
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }
    
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        String browserType = getBrowserType();
        if (Strings.isNullOrEmpty(browserType))
        {
            ClassLoader boxClassLoader = createBoxClassLoader();
            BoxLookup lookup = new BoxLookup(getLog(), boxClassLoader);
            for (BrowserBoxFactory boxFactory : lookup.getAvailableBoxFactories())
            {
                for (String curBrowserType : boxFactory.getKnownTypes())
                {
                    listVersions(boxFactory, curBrowserType, service);    
                }
            }
        }
        else 
        {
            try 
            {
                listVersions(browserBoxFactory(service), browserType, service);
            }
            catch (BrowserBoxException e)
            {
                throw new MojoExecutionException(e.getMessage(), e);
            }
        }
    }
}
