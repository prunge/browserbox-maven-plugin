package au.net.causal.maven.plugins.browserbox.box;

import java.io.File;

/**
 * Individual certificate files that are registered as trusted in browsers.
 */
public class TrustCertificateFileConfiguration
{
    private File file;

    /**
     * @return the file containing the certificate.
     */
    public File getFile()
    {
        return file;
    }

    public void setFile(File file)
    {
        this.file = file;
    }
}
