package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import au.net.causal.maven.plugins.browserbox.box.Resolution;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenReaderFilterRequest;
import org.codehaus.plexus.util.IOUtil;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Properties;

public class WindowsRemoteDesktopTools
{
    public Resolution getDefaultResolution()
    {
        return new Resolution(1280, 800);
    }
    
    public Path generateRdpFile(ConnectionInfo connectionInfo, RdpToolMode mode, BrowserBox box, BoxConfiguration boxConfiguration,
                                 ProjectConfiguration projectConfiguration, BoxContext context)
    throws IOException, BrowserBoxException
    {
        Path tempDirectory = context.getTempDirectory();
        Path rdpFile = tempDirectory.resolve("browserbox.rdp");
        Properties extraProperties = new Properties();
        extraProperties.setProperty("connection.host", connectionInfo.getUri().getHost());
        extraProperties.setProperty("connection.port", String.valueOf(connectionInfo.getUri().getPort()));
        if (connectionInfo.getUsername() != null)
            extraProperties.setProperty("connection.user", connectionInfo.getUsername());
        else
            extraProperties.setProperty("connection.user", "");
        if (connectionInfo.getPassword() != null)
            extraProperties.setProperty("connection.password", connectionInfo.getPassword());
        else
            extraProperties.setProperty("connection.password", "");

        if (boxConfiguration.getResolution() != null &&
            boxConfiguration.getResolution().getWidth() > 0 &&
            boxConfiguration.getResolution().getHeight() > 0)
        {
            extraProperties.setProperty("box.resolution.width", String.valueOf(boxConfiguration.getResolution().getWidth()));
            extraProperties.setProperty("box.resolution.height", String.valueOf(boxConfiguration.getResolution().getHeight()));
        }
        else
        {
            extraProperties.setProperty("box.resolution.width", String.valueOf(getDefaultResolution().getWidth()));
            extraProperties.setProperty("box.resolution.height", String.valueOf(getDefaultResolution().getHeight()));
        }

        try (Reader r = new InputStreamReader(WindowsRemoteDesktopTools.class.getResource(mode.getTemplateResourceName()).openStream(), StandardCharsets.UTF_8);
             Writer writer = Files.newBufferedWriter(rdpFile))
        {
            MavenReaderFilterRequest request = new MavenReaderFilterRequest(r, true, projectConfiguration.getProject(),
                                                                            Collections.emptyList(), true, context.getSession(),
                                                                            extraProperties);
            try (Reader fr = context.getReaderFilter().filter(request))
            {
                IOUtil.copy(fr, writer);
            }
            catch (MavenFilteringException e)
            {
                throw new BrowserBoxException("Error filtering RDP template: " + e.getMessage(), e);
            }
        }

        return rdpFile;
    }
    
    public static enum RdpToolMode
    {
        MICROSOFT("template-ms.rdp"),
        MAC("template-mac.rdp");
        
        private final String templateResourceName;
        
        private RdpToolMode(String templateResourceName)
        {
            this.templateResourceName = templateResourceName;
        }

        public String getTemplateResourceName() 
        {
            return templateResourceName;
        }
    }
}
