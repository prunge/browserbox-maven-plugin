package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.execute.BrowserControl;
import au.net.causal.maven.plugins.browserbox.execute.SeleniumBrowserControl;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.sdklib.internal.avd.AvdInfo;
import org.codehaus.plexus.util.xml.pull.MXParser;
import org.codehaus.plexus.util.xml.pull.XmlPullParser;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AndroidChromeBrowserBox extends AndroidBrowserBox
{
    //TODO think about if anything extra needs to be done to bootstrap Android ITs with the APK
    //TODO start cataloging which system images have which Chrome versions

    /**
     * Creates an Android browser box for Chrome.
     *
     * @param boxConfiguration configuration for the box itself.
     * @param projectConfiguration project-level configuration.
     * @param context runtime context.
     * @param chromeApkVersionRegistry version registry for downloading Chrome APKs.
     * @param chromeDriverVersionRegistry version registry for downloading Chrome Webdriver artifacts.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public AndroidChromeBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context,
                                   VersionedItemStore chromeApkVersionRegistry, VersionedItemStore chromeDriverVersionRegistry)
    throws BrowserBoxException
    {
        super(boxConfiguration, projectConfiguration, context, chromeApkVersionRegistry, chromeDriverVersionRegistry);
    }

    @Override
    protected String avdName()
    {
        return "browserbox.causal.net.au_chromeX";
        //TODO
    }

    @Override
    protected void beforeApkInstall(IDevice emulatorDevice, AvdInfo avd) throws BrowserBoxException
    {
        super.beforeApkInstall(emulatorDevice, avd);

        //Find the lowest installed package version which indicates a pre-installed version of Chrome in the image
        //Android won't let us install a version lower than this so if the user attempts to do so give a helpful
        //error message instead of just bombing out
        Version installedSystemChromeVersion = readInstalledPackageVersion("com.android.chrome", emulatorDevice);
        if (installedSystemChromeVersion != null)
        {
            try
            {
                Version targetChromeVersion = versionScheme.parseVersion(getBoxConfiguration().getBrowserVersion());
                getContext().getLog().info("Target Chrome version: " + targetChromeVersion);
                getContext().getLog().info("System Image Chrome version: " + installedSystemChromeVersion);
                if (targetChromeVersion.compareTo(installedSystemChromeVersion) < 0)
                {
                    throw new BrowserBoxException("System image's Chrome version (" + installedSystemChromeVersion.toString() +
                                                  ") cannot be replaced by an older version (" +
                                                  getBoxConfiguration().getBrowserVersion() +
                                                  ").  Use an older Android version (configure with '" + ANDROID_OS_API_LEVEL +
                                                  "' browser box property, currently  is " +
                                                  avd.getSystemImage().getAndroidVersion().getApiLevel() +
                                                  ") to test this older version of Chrome or select a higher version of Chrome to test with.");
                }
            }
            catch (InvalidVersionSpecificationException e)
            {
                throw new BrowserBoxException("Error parsing Chrome version '" + getBoxConfiguration().getBrowserVersion() + "': " + e.getMessage(), e);
            }
        }
    }

    @Override
    protected void afterApkInstall(IDevice emulatorDevice, AvdInfo avd) throws BrowserBoxException
    {
        super.afterApkInstall(emulatorDevice, avd);

        //Configure keyboard preferences to avoid popups
        configureGBoard(emulatorDevice);
        configureChromePreferences(emulatorDevice);
    }

    private Version readInstalledPackageVersion(String packageName, IDevice emulatorDevice)
    throws BrowserBoxException
    {
        CollectingOutputReceiver out = new CollectingOutputReceiver();

        try
        {
            emulatorDevice.executeShellCommand("dumpsys package " + packageName + " | grep versionName\\=", out);
            String result = out.getOutput();

            //Results will be in the form of 'versionName=X', one per line
            List<String> resultLines;
            try (BufferedReader r = new BufferedReader(new StringReader(result)))
            {
                resultLines = r.lines()
                               .map(String::trim)
                               .filter(s -> !s.isEmpty())
                               .collect(Collectors.toList());
            }

            List<Version> versions = new ArrayList<>();
            for (String resultLine : resultLines)
            {
                if (resultLine.startsWith("versionName="))
                {
                    String versionString = resultLine.substring("versionName=".length());
                    try
                    {
                        versions.add(versionScheme.parseVersion(versionString));
                    }
                    catch (InvalidVersionSpecificationException e)
                    {
                        throw new BrowserBoxException("Error parsing version '" + versionString + "' of package " +
                                                      packageName + ": " + e.getMessage(), e);
                    }
                }
            }

            Collections.sort(versions);
            if (versions.isEmpty())
                return null;

            //If there are multiple versions, the lowest version will be the system installed version which will be
            //the lower limit of installable package versions
            //(e.g. possible for a user to have installed a later version of the package)
            return versions.get(0);
        }
        catch (IOException | com.android.ddmlib.TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException e)
        {
            throw new BrowserBoxException("Error reading version: " + e, e);
        }
    }

    private void configureChromePreferences(IDevice emulatorDevice)
    throws BrowserBoxException
    {
        //Don't hack the preferences since there is a better way
        //configureApplication(emulatorDevice, "com.android.chrome", "chromepref.xsl");

        try
        {
            //Update command line to disable first-run-experience in Chrome
            emulatorDevice.executeShellCommand("echo \"chrome --disable-fre --no-default-browser-check --no-first-run\" > /data/local/tmp/chrome-command-line", new ConsoleOutputReceiver());

            //Restart application so new settings are picked up
            emulatorDevice.executeShellCommand("su root am force-stop com.google.chrome", new ConsoleOutputReceiver());

        }
        catch (IOException | AdbCommandRejectedException | ShellCommandUnresponsiveException | com.android.ddmlib.TimeoutException e)
        {
            throw new BrowserBoxException("Error configuring Chrome preferences: " + e.getMessage(), e);
        }
    }

    /**
     * Configure the Google Keyboard to not show the initial annoying popups.
     *
     * @param emulatorDevice emulator device running.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private void configureGBoard(IDevice emulatorDevice)
    throws BrowserBoxException
    {
        //GBoard is a system thing and its preferences can be initialized at any time
        //so wait for those preferences to be fully initialized
        configureApplication(emulatorDevice, "com.google.android.inputmethod.latin", "gboard.xsl");
    }

    private void configureApplication(IDevice emulatorDevice, String appName, String configTransformResourceName)
    throws BrowserBoxException
    {
        try
        {
            //If the application might not exist, do a check to see if it actually is present
            boolean applicationExists = isApplicationInstalledInEmulator(emulatorDevice, appName);
            if (applicationExists)
                getContext().getLog().info("Application " + appName + " exists.");
            else
            {
                getContext().getLog().info("Application " + appName + " does not exist, no need to configure.");
                return;
            }

            CollectingOutputReceiver prefOut = new CollectingOutputReceiver();
            String preferencesFilePath = "/data/data/" + appName + "/shared_prefs/" + appName + "_preferences.xml";

            long startTime = System.nanoTime();
            getContext().getLog().debug("Waiting for file " + preferencesFilePath);
            waitForFileToBePresentInEmulator(emulatorDevice, preferencesFilePath);
            long time = System.nanoTime() - startTime;
            getContext().getLog().info("File is now present (" + time / 1.0E9 + "s): " + preferencesFilePath);

            emulatorDevice.executeShellCommand("su root cat " + preferencesFilePath, prefOut);

            String prefsXml = prefOut.getOutput();

            getContext().getLog().debug("App preferences for " + appName + ": " + prefsXml);

            if (!isXml(prefsXml))
            {
                prefsXml = "<map />";
                getContext().getLog().info("Using empty map for preferences");
            }

            //Attempt to parse and transform
            URL appTransformResource = AndroidBrowserBox.class.getResource(configTransformResourceName);
            Source appTransform = new StreamSource(appTransformResource.toExternalForm());
            StringWriter prefsXmlWriter = new StringWriter();
            TransformerFactory.newInstance().newTransformer(appTransform).transform(new StreamSource(new StringReader(prefsXml)), new StreamResult(prefsXmlWriter));

            String newPrefsXml = prefsXmlWriter.toString();

            Path tempXmlFile = getContext().getTempDirectory().resolve(appName + ".xml");
            Files.createDirectories(tempXmlFile.getParent());
            Files.write(tempXmlFile, newPrefsXml.getBytes(StandardCharsets.UTF_8));

            emulatorDevice.pushFile(tempXmlFile.toAbsolutePath().toString(), "/sdcard/Download/" + appName + ".xml");

            emulatorDevice.executeShellCommand("su root mkdir -p /data/data/" + appName + "/shared_prefs",
                                               new ConsoleOutputReceiver());
            emulatorDevice.executeShellCommand("su root cp /sdcard/Download/" + appName + ".xml /data/data/" + appName + "/shared_prefs/" + appName + "_preferences.xml",
                                               new ConsoleOutputReceiver());

            getContext().getLog().debug("New app preferences for " + appName + ": " + new String(Files.readAllBytes(tempXmlFile), StandardCharsets.UTF_8));

            //Restart application so new settings are picked up
            emulatorDevice.executeShellCommand("su root am force-stop " + appName,
                                               new ConsoleOutputReceiver());
        }
        catch (IOException | AdbCommandRejectedException | ShellCommandUnresponsiveException | com.android.ddmlib.TimeoutException | SyncException | TransformerException e)
        {
            throw new BrowserBoxException("Error configuring " + appName + " preferences: " + e.getMessage(), e);
        }
    }

    /**
     * Checks whether an application is installed in the emulator Android system according to its package manager.
     *
     * @param emulatorDevice the emulator running the OS.
     * @param applicationName application name.
     *
     * @return true if the application is installed, false if not.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private boolean isApplicationInstalledInEmulator(IDevice emulatorDevice, String applicationName)
    throws BrowserBoxException
    {
        CollectingOutputReceiver out = new CollectingOutputReceiver();

        try
        {
            emulatorDevice.executeShellCommand("pm list packages " + applicationName, out);

            //If the package exists, there will be a line of output, otherwise not
            return !out.getOutput().trim().isEmpty();
        }
        catch (IOException | com.android.ddmlib.TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException e)
        {
            throw new BrowserBoxException("Error listing files: " + e, e);
        }
    }

    private void waitForFileToBePresentInEmulator(IDevice emulatorDevice, String filePath)
    throws BrowserBoxException
    {
        int attempts = 0;

        try
        {
            String result;
            do
            {
                CollectingOutputReceiver out = new CollectingOutputReceiver();
                attempts++;
                emulatorDevice.executeShellCommand("su root sh -c \"((ls " + filePath + " > /dev/null 2>&1 && echo true) || echo false)\"", out);
                result = out.getOutput().trim();

                if (!result.equals("true"))
                    Thread.sleep(200L);
                if (attempts > 200)
                    throw new BrowserBoxException("Timeout waiting for file to be present: " + filePath);
            }
            while (!result.equals("true"));
        }
        catch (IOException | com.android.ddmlib.TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | InterruptedException e)
        {
            throw new BrowserBoxException("Error listing files: " + e, e);
        }
    }

    private boolean isXml(String text)
    throws BrowserBoxException
    {
        try (Reader reader = new StringReader(text))
        {
            XmlPullParser parser = new MXParser();
            parser.setInput( reader );
            parser.nextToken();

            return true;
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
        catch (XmlPullParserException e)
        {
            //Means it's not XML
            return false;
        }
    }

    @Override
    public BrowserControl browserControl() throws BrowserBoxException
    {
        String deviceId = "emulator-5554";
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "com.android.chrome");
        chromeOptions.put("androidDeviceSerial", deviceId);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        try
        {
            return new SeleniumBrowserControl(getWebDriverUrl(), capabilities);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error creating browser control: " + e.getMessage(), e);
        }
    }

    @Override
    public Capabilities getSeleniumDesiredCapabilities()
    throws BrowserBoxException
    {
        IDevice device = findAvdDevice();
        String deviceId = device.getSerialNumber();

        DesiredCapabilities capabilities = new DesiredCapabilities(BrowserType.CHROME, "", Platform.ANDROID);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "com.android.chrome");
        chromeOptions.put("androidDeviceSerial", deviceId);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        return capabilities;
    }

    @Override
    public String getSeleniumBrowserType()
    {
        return BrowserType.CHROME;
    }
}
