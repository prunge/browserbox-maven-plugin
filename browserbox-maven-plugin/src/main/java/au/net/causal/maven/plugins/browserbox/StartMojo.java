package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.SslTrustConfiguration;
import au.net.causal.maven.plugins.browserbox.box.TunnelSession;
import au.net.causal.maven.plugins.browserbox.box.VideoControl;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

@Mojo(name="start", defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST, requiresProject = false)
public class StartMojo extends AbstractBrowserBoxMojo 
{
    //TODO
    //Non hardcode timeouts
    //Alternate tunnel mechanism via Vagrant (without going through to VBox direct)

    /**
     * If specified, record video of the entire session to this file.  Video will be saved when the box is stopped.
     */
    @Parameter(property = "browserbox.video.file")
    private File sessionVideoFile;

    @Parameter(property = "browserbox.toolserver.port", defaultValue = "4454")
    private int toolServerPort;

    /**
     * If true, turns on the browserbox tool server, which can be used to control video recording from 
     * tests.
     */
    @Parameter(property = "browserbox.toolserver.use", defaultValue = "false")
    private boolean toolServerUsed;
    
    @Parameter(property = "browserbox.test.video.directory", defaultValue = "${project.build.directory}/testvideo")
    private File videoDirectory;

    /**
     * If true, additional verification will be performed by performing a simple Selenium command to the server
     * and only returning when it succeeds.
     */
    @Parameter(property = "browserbox.seleniumWaiting")
    private boolean useSeleniumWaiting;

    /**
     * Maximum time to wait in seconds for browser box to start up.
     */
    @Parameter(property = "browserbox.startupTimeout", defaultValue = "1800", required = true)
    private long startupTimeout;

    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        Duration maxWaitTime = Duration.ofSeconds(startupTimeout);

        try
        {
            BrowserBox browserBox = browserBox(service);
            if (!browserBox.exists())
            {
                getLog().info("Creating browser box " + browserBox.getName());
                browserBox.createAndStart();

                browserBox.waitUntilStarted(maxWaitTime);

                //If desired, do additional waiting
                if (useSeleniumWaiting)
                {
                    getLog().info("Waiting for Selenium to be ready...");
                    waitForSelenium(browserBox, boxContext(service), maxWaitTime, maxWaitTime);
                    getLog().info("Selenium ready.");
                }

                //Install SSL certificates
                if (box.getSslTrust() != null)
                    installSslCertificates(browserBox, box.getSslTrust());

                getLog().info("Browser box started.");
            }

            TunnelSession tunnelSession = browserBox.establishTunnels();
            TunnelSession oldSession = (TunnelSession) getPluginContext().put(TUNNEL_SESSION_KEY_PREFIX + browserBox.getName(), tunnelSession);
            if (oldSession != null)
                oldSession.close();

            //Configure project properties
            configureBrowserBoxExportedProperties(browserBox, project.getProperties());

            //Also potentially write out a file with this information
            writeActivePropertiesFile(browserBox);

            //Start video recording if desired
            if (sessionVideoFile != null && isVideoStartAutomatic())
                startVideoSession(browserBox, sessionVideoFile.toPath());

            //Tool server
            if (toolServerUsed)
                startToolServer(browserBox);
        }
        catch (BrowserBoxException | TimeoutException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private void writeActivePropertiesFile(BrowserBox browserBox)
    throws BrowserBoxException, MojoExecutionException
    {
        if (activePropertiesFile == null)
            return;

        //Create parent directories of the properties file if needed
        try
        {
            Files.createDirectories(activePropertiesFile.toPath().getParent());
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Error creating directory for active properties file: " + e.getMessage(), e);
        }

        try (OutputStream os = Files.newOutputStream(activePropertiesFile.toPath()))
        {
            Properties p = new Properties();
            configureBrowserBoxExportedProperties(browserBox, p);
            p.store(os, null);
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Error writing active properties file " +
                                             activePropertiesFile.toPath() + ": " + e.getMessage(), e);
        }
    }

    /**
     * Writes properties that allow tests to configure themselves to talk to Selenium running in the browser box.
     *
     * @param browserBox the browser box being used.
     * @param p properties to write to.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private void configureBrowserBoxExportedProperties(BrowserBox browserBox, Properties p)
    throws BrowserBoxException
    {
        //URL the possibly non-local Selenium server is running on
        p.setProperty("browserbox.selenium.url", browserBox.getWebDriverUrl().toString());

        //Browser type
        p.setProperty("browserbox.browser.type", browserBox.getSeleniumBrowserType());

        //Capabilities string, which may be needed for some boxes such as Android
        Capabilities capabilities = browserBox.getSeleniumDesiredCapabilities();
        if (capabilities != null)
        {
            String capabilitiesString = new Json().toJson(capabilities);
            p.setProperty("browserbox.capabilities", capabilitiesString);
        }
    }
    
    protected boolean isVideoStartAutomatic()
    {
        return true;
    }
    
    private void startToolServer(BrowserBox browserBox)
    throws MojoExecutionException
    {
        Path testVideoDirectory = videoDirectory.toPath().resolve(box.getContainerName());
        ToolServer toolServer = new ToolServer(toolServerPort, browserBox, testVideoDirectory);
        getPluginContext().put(TOOL_SERVER_KEY_PREFIX + browserBox.getName(), toolServer);
        try
        {
            getLog().info("BrowserBox tool server started");
            toolServer.start();
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Error starting tool server: " + e.getMessage(), e);
        }
    }
    
    protected void startVideoSession(BrowserBox browserBox, Path sessionVideoPath)
    throws MojoExecutionException
    {
        try
        {
            Files.createDirectories(sessionVideoPath.getParent());
        }
        catch (IOException e)
        {
            throw new MojoExecutionException("Failed to create directory for video file " + sessionVideoPath.toAbsolutePath() + ": " + e.getMessage(), e);
        }

        try
        {
            getLog().info("Recording video for browser session to " + sessionVideoPath.toAbsolutePath());
            VideoControl.Recording recording = browserBox.video().startRecording();
            VideoRecordingSession videoSession = new VideoRecordingSession(recording, sessionVideoPath);
            VideoRecordingSession oldSession = (VideoRecordingSession)getPluginContext().put(VIDEO_RECORDING_SESSION_KEY_PREFIX + browserBox.getName(), videoSession);
            if (oldSession != null) 
                oldSession.getRecording().stopAndSave(oldSession.getVideoFile());
        }
        catch (IOException | BrowserBoxException e)
        {
            throw new MojoExecutionException("Error recording video: " + e.getMessage(), e);
        }
    }

    private void waitForSelenium(BrowserBox box, BoxContext context, Duration maxWaitTime, Duration pollTime)
    throws TimeoutException, BrowserBoxException
    {
        long startTime = System.currentTimeMillis();
        long maxEndTime = startTime + maxWaitTime.toMillis();
        WebDriverException lastError = null;

        while (System.currentTimeMillis() < maxEndTime)
        {
            try
            {
                attemptSeleniumConnection(box, context);
                //If we get here - connection successful
                return;
            }
            catch (WebDriverException e)
            {
                lastError = e;
                context.getLog().warn("Selenium connection error: " + e, e);

                try
                {
                    Thread.sleep(pollTime.toMillis());
                }
                catch (InterruptedException ex)
                {
                    BrowserBoxException bex = new BrowserBoxException("Interrupted waiting for Selenium.", ex);
                    bex.addSuppressed(e);
                    throw bex;
                }
            }
        }

        TimeoutException ex = new TimeoutException("Timeout waiting for Selenium.");
        if (lastError != null)
            ex.addSuppressed(lastError);
        throw ex;
    }

    private void attemptSeleniumConnection(BrowserBox box, BoxContext context)
    throws BrowserBoxException
    {
        Capabilities capabilities = box.getSeleniumDesiredCapabilities();
        if (capabilities == null)
            capabilities = new DesiredCapabilities(box.getSeleniumBrowserType(), "", Platform.ANY);

        URL seleniumServer;
        try
        {
            seleniumServer = box.getWebDriverUrl().toURL();
        }
        catch (MalformedURLException e)
        {
            throw new BrowserBoxException("Invalid webdriver URL: " + box.getWebDriverUrl());
        }

        RemoteWebDriver driver = new RemoteWebDriver(seleniumServer, capabilities);
        try
        {
            String userAgent = (String)driver.executeScript("return navigator.userAgent");
            if (userAgent == null || userAgent.trim().isEmpty())
                throw new BrowserBoxException("Empty user agent response from Selenium.");
        }
        finally
        {
            try
            {
                driver.close();
            }
            catch (WebDriverException e)
            {
                //Ignore error on close
                context.getLog().debug("Error closing webdriver: " + e.getMessage(), e);
            }
        }
    }

    private void installSslCertificates(BrowserBox box, SslTrustConfiguration sslTrust)
    throws BrowserBoxException
    {
        //Most users won't use SSL so create this lazily here as opposed to Mojo constructor
        SslCertificateReader sslCertificateReader = new SslCertificateReader();

        Collection<? extends X509Certificate> certificates = sslCertificateReader.read(sslTrust);
        if (!certificates.isEmpty())
        {
            getLog().info("Installing " + certificates.size() + " certificate(s) into " + box.getName());
            box.installCertificates(certificates);
            getLog().info("Certificates installed into " + box.getName());
        }
    }
}
