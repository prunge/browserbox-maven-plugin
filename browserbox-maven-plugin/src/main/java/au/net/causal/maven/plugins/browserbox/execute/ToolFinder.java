package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.util.concurrent.ListeningExecutorService;
import org.codehaus.plexus.util.cli.StreamConsumer;

import java.io.IOException;
import java.util.Optional;

public interface ToolFinder 
{
    public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                                         ProjectConfiguration projectConfiguration, BoxContext context)
    throws IOException, BrowserBoxException;
    
    public static interface ToolRunner
    {
        /**
         * Prepares to run the tool on the main thread.  Might do things like download dependencies.
         * By default does nothing.
         */
        public default void prepareRun(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                                       ProjectConfiguration projectConfiguration,
                                       BoxContext context)
        throws BrowserBoxException
        {
            //By default, do nothing
        }

        public int run(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                       ProjectConfiguration projectConfiguration,
                       BoxContext context, StreamConsumer out, StreamConsumer err,
                       ListeningExecutorService executor)
        throws IOException, BrowserBoxException;
    }
}
