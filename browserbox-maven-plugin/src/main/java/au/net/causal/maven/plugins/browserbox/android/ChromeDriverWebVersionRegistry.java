package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import com.google.common.io.Resources;
import org.apache.maven.plugin.logging.Log;
import org.glassfish.jersey.internal.util.SimpleNamespaceResolver;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOError;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChromeDriverWebVersionRegistry implements VersionRegistry
{
    private final URL siteUrl;
    private final XPathFactory xPathFactory;
    private final Platform defaultPlatform;
    private final Log log;

    public ChromeDriverWebVersionRegistry(URL siteUrl, Platform defaultPlatform, Log log)
    {
        Objects.requireNonNull(siteUrl, "siteUrl == null");
        Objects.requireNonNull(defaultPlatform, "defaultPlatform == null");
        Objects.requireNonNull(log, "log == null");
        this.siteUrl = siteUrl;
        this.defaultPlatform = defaultPlatform;
        this.log = log;
        this.xPathFactory = XPathFactory.newInstance();
    }

    public ChromeDriverWebVersionRegistry(Platform defaultPlatform, Log log)
    {
        this(defaultSiteUrl(), defaultPlatform, log);
    }

    private static URL defaultSiteUrl()
    {
        try
        {
            return new URL("https://chromedriver.storage.googleapis.com");
        }
        catch (MalformedURLException e)
        {
            //Should always be a valid URL
            throw new IOError(e);
        }
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        XPath xPath = xPathFactory.newXPath();
        xPath.setNamespaceContext(new SimpleNamespaceResolver("a", "http://doc.s3.amazonaws.com/2006-03-01"));

        Pattern chromeDriverFilePattern = Pattern.compile("^(\\d+\\.\\d+\\.?\\d*\\.?\\d*)/chromedriver_([\\p{Alnum}]+)\\.zip$");
        Pattern notesFilePattern = Pattern.compile("^(\\d+\\.\\d+)/notes.txt"); //Only read notes for the old two-digit chromedriver versions

        try
        {
            Map<String, CompatibilityInfo> compatibilityInfoMap = new HashMap<>();
            List<WebDriverItem> results = new ArrayList<>();
            String currentVersion = null;
            Map<Platform, URL> platformSpecificVersions = null;

            //The items are in order of version, then platform
            NodeList items = (NodeList)xPath.evaluate("//a:Contents/a:Key", new InputSource(siteUrl.toExternalForm()), XPathConstants.NODESET);
            for (int i = 0; i < items.getLength(); i++)
            {
                Element item = (Element)items.item(i);
                String fileName = item.getTextContent();

                //Expected form: 2.8/chromedriver_linux32.zip
                Matcher chromeDriverZipMatcher = chromeDriverFilePattern.matcher(fileName);
                Matcher notesMatcher = notesFilePattern.matcher(fileName);
                if (chromeDriverZipMatcher.matches())
                {
                    String version = chromeDriverZipMatcher.group(1);
                    String platformLabel = chromeDriverZipMatcher.group(2);
                    Platform platform = Platform.forLabel(platformLabel);

                    if (platform != null) //only record supported platforms
                    {
                        if (!version.equals(currentVersion))
                        {
                            if (currentVersion != null && platformSpecificVersions.keySet().containsAll(Arrays.asList(Platform.values())))
                                results.add(new WebDriverItem(currentVersion, defaultPlatform, platformSpecificVersions));

                            currentVersion = version;
                            platformSpecificVersions = new EnumMap<>(Platform.class);
                        }

                        try
                        {
                            platformSpecificVersions.put(platform, new URL(siteUrl, fileName));
                        }
                        catch (MalformedURLException e)
                        {
                            throw new BrowserBoxException(e);
                        }

                        //For more modern ChromeDriver with > 2 segments in the version number, assume it is for the
                        //major version number corresponding to the browser
                        //(e.g. ChromeDriver 70.0.3538.16 is for Chrome 70)
                        String[] versionSegments = version.split(Pattern.quote("."));
                        if (versionSegments.length > 2)
                        {
                            int majorVersion = Integer.parseInt(versionSegments[0]);
                            compatibilityInfoMap.put(version, new CompatibilityInfo(majorVersion, majorVersion));
                        }
                    }
                }
                else if (notesMatcher.matches())
                {
                    String version = notesMatcher.group(1);

                    if (!version.equals(currentVersion))
                    {
                        if (currentVersion != null && platformSpecificVersions.keySet().containsAll(Arrays.asList(Platform.values())))
                            results.add(new WebDriverItem(currentVersion, defaultPlatform, platformSpecificVersions));

                        currentVersion = version;
                        platformSpecificVersions = new EnumMap<>(Platform.class);
                    }

                    //Read the notes file only if needed
                    try
                    {
                        if (!compatibilityInfoMap.containsKey(version))
                            readNotesFile(new URL(siteUrl, fileName), compatibilityInfoMap);
                    }
                    catch (MalformedURLException e)
                    {
                        throw new BrowserBoxException(e);
                    }
                }
                else
                    log.debug(fileName + ": no match");
            }

            //Final entry
            if (currentVersion != null && platformSpecificVersions.keySet().containsAll(Arrays.asList(Platform.values())))
                results.add(new WebDriverItem(currentVersion, defaultPlatform, platformSpecificVersions));

            //Fill in compatibility info
            for (WebDriverItem result : results)
            {
                CompatibilityInfo compatibilityInfo = compatibilityInfoMap.get(result.getVersion());
                if (compatibilityInfo == null)
                    log.debug("Warning: no compatiblity info for " + result.getVersion());
                result.setCompatibilityInfo(compatibilityInfo);
            }


            return new ItemList(results, Collections.emptyList());
        }
        catch (XPathExpressionException e)
        {
            throw new BrowserBoxException("Error reading Chrome Driver versions: " + e.getMessage(), e);
        }
    }
    
    private void readNotesFile(URL notesFile, Map<? super String, ? super CompatibilityInfo> versionCompatibilityMap)
    throws BrowserBoxException
    {
        Pattern versionHeaderPattern = Pattern.compile("^-{4,}ChromeDriver\\s+[vV](\\d+\\.\\d+)\\s+.*-{4,}$");
        Pattern compatPattern = Pattern.compile("^Supports Chrome v(\\d+)-(\\d+)$");

        //TODO should actually read encoding from the URL headers
        try
        {
            String currentVersion = null;
            for (String line : Resources.readLines(notesFile, StandardCharsets.UTF_8))
            {
                Matcher versionMatcher = versionHeaderPattern.matcher(line);
                if (versionMatcher.matches())
                    currentVersion = versionMatcher.group(1);
                else if (currentVersion != null)
                {
                    Matcher compatMatcher = compatPattern.matcher(line);
                    if (compatMatcher.matches())
                    {
                        String minVersionStr = compatMatcher.group(1);
                        String maxVersionStr = compatMatcher.group(2);
                        int minVersion = Integer.parseInt(minVersionStr);
                        int maxVersion = Integer.parseInt(maxVersionStr);
                        versionCompatibilityMap.putIfAbsent(currentVersion, new CompatibilityInfo(minVersion, maxVersion));
                    }
                }
            }
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error reading notes file " + notesFile.toExternalForm() + ": " + e.getMessage(), e);
        }
    }
}
