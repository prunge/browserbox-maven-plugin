package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Reads data from a properties file that maps version numbers to download URLs.  Subclasses define the actual
 * properties source.
 */
public abstract class PropertiesBasedVersionRegistry implements VersionRegistry
{
    /**
     * Reads properties that map version numbers to download URLs.
     *
     * @return a fully populated properties object.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    protected abstract Properties readDownloadProperties()
    throws BrowserBoxException;

    protected URL createUrlForDownload(String urlString)
    throws MalformedURLException
    {
        return new URL(urlString);
    }
    
    @Override
    public ItemList readAllItemsAllowFailures(Query query) throws BrowserBoxException
    {
        Properties downloadProperties = readDownloadProperties();

        List<Item> results = new ArrayList<>();
        for (String version : downloadProperties.stringPropertyNames())
        {
            if (!query.getIgnoredVersions().contains(version))
            {
                String downloadUrlStr = downloadProperties.getProperty(version);
                try
                {
                    results.add(new Item(version, createUrlForDownload(downloadUrlStr)));
                }
                catch (MalformedURLException e)
                {
                    //This is a critical error throw and don't log in the result
                    throw new BrowserBoxException("Error parsing URL in download properties: " + downloadUrlStr, e);
                }
            }
        }

        return new ItemList(results, Collections.emptyList());
    }
}
