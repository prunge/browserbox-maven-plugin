package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.RunnerDependency;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResolutionException;
import org.eclipse.aether.resolution.DependencyResult;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DependencyUtils
{
    public static List<Path> resolveDependenciesFromStrings(List<String> dependencySpecs,
                                                            RepositorySystem repositorySystem,
                                                            RepositorySystemSession repositorySystemSession,
                                                            List<RemoteRepository> remoteRepositories)
    throws DependencyResolutionException
    {
        List<Dependency> dependencyList = dependencySpecs.stream()
                .map(DefaultArtifact::new)
                .map(artifact -> new Dependency(artifact, org.apache.maven.artifact.Artifact.SCOPE_COMPILE))
                .collect(Collectors.toList());

        DependencyRequest dRequest = new DependencyRequest();
        dRequest.setCollectRequest(new CollectRequest(dependencyList, Collections.emptyList(), remoteRepositories));

        DependencyResult dResult = repositorySystem.resolveDependencies(repositorySystemSession, dRequest);

        return dResult.getArtifactResults().stream()
                .map(ArtifactResult::getArtifact)
                .map(Artifact::getFile)
                .map(File::toPath)
                .collect(Collectors.toList());
    }

	public static List<Path> resolveDependencies(List<? extends RunnerDependency> dependencies,
	                                             RepositorySystem repositorySystem,
	                                             RepositorySystemSession repositorySystemSession,
	                                             List<RemoteRepository> remoteRepositories)
	throws DependencyResolutionException
	{
		List<Dependency> dependencyList = dependencies.stream()
				.map(rd -> new DefaultArtifact(rd.getGroupId(), rd.getArtifactId(), rd.getClassifier(), rd.getType(), rd.getVersion()))
				.map(artifact -> new Dependency(artifact, org.apache.maven.artifact.Artifact.SCOPE_COMPILE))
				.collect(Collectors.toList());

		DependencyRequest dRequest = new DependencyRequest();
		dRequest.setCollectRequest(new CollectRequest(dependencyList, Collections.emptyList(), remoteRepositories));

		DependencyResult dResult = repositorySystem.resolveDependencies(repositorySystemSession, dRequest);

		return dResult.getArtifactResults().stream()
				.map(ArtifactResult::getArtifact)
				.map(Artifact::getFile)
				.map(File::toPath)
				.collect(Collectors.toList());
	}
}
