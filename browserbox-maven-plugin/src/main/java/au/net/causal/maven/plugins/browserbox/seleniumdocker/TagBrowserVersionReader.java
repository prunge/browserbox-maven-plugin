package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

public interface TagBrowserVersionReader 
{
    public String readBrowserVersion(Tag tag)
    throws BrowserBoxException;
}
