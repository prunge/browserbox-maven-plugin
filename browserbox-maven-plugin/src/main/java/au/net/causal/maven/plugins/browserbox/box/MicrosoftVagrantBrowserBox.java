package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.BuildIntermediates;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.execute.BrowserControl;
import au.net.causal.maven.plugins.browserbox.execute.SeleniumBrowserControl;
import au.net.causal.maven.plugins.browserbox.execute.ToolFinder;
import au.net.causal.maven.plugins.browserbox.ms.BrowserSelector;
import au.net.causal.maven.plugins.browserbox.ms.ChocolateyEdgeDriverVersionResolver;
import au.net.causal.maven.plugins.browserbox.ms.EdgeDriverVersionResolver;
import au.net.causal.maven.plugins.browserbox.ms.EdgeDriverVersionResolver.Version;
import au.net.causal.maven.plugins.browserbox.ms.MavenArtifactPropertiesEdgeDriverVersionResolver;
import au.net.causal.maven.plugins.browserbox.ms.MicrosoftVagrantBoxInstaller;
import au.net.causal.maven.plugins.browserbox.ms.MicrosoftVagrantBoxManager;
import au.net.causal.maven.plugins.browserbox.ms.MultiEdgeDriverVersionResolver;
import au.net.causal.maven.plugins.browserbox.ms.WindowsBoxMaker;
import au.net.causal.maven.plugins.browserbox.ms.WindowsUpdateKB;
import au.net.causal.maven.plugins.browserbox.vagrant.BoxDefinition;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.BoxListOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.VagrantException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxException;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxExecutable;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager.ShowVmInfoOptions;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxSDL;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import io.fabric8.maven.docker.config.WaitConfiguration;
import io.fabric8.maven.docker.wait.HttpPingChecker;
import io.fabric8.maven.docker.wait.PreconditionFailedException;
import io.fabric8.maven.docker.wait.WaitChecker;
import io.fabric8.maven.docker.wait.WaitUtil;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenReaderFilterRequest;
import org.apache.maven.shared.utils.io.FileUtils;
import org.apache.maven.shared.utils.io.Java7Support;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.cli.StreamConsumer;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@BrowserBoxBundled
public class MicrosoftVagrantBrowserBox implements BrowserBox 
{
    private final BoxContext context;
    private final BoxConfiguration boxConfiguration;
    private final ProjectConfiguration projectConfiguration;
    
    private final MicrosoftVagrantBoxManager microsoftVagrantBoxManager = new MicrosoftVagrantBoxManager();
    private final VersionedItemStore versionRegistry;
    private final EdgeDriverVersionResolver edgeDriverVersionResolver;
    
    private final VirtualBoxManager virtualBoxManager;
    private final VirtualBoxExecutable virtualBox;
    private final VirtualBoxSDL vboxSdl;
    private final Vagrant vagrant;
    
    private final BrowserSelector browserSelector;
    
    private final Path vagrantDirectory;
    
    //TODO unhardcode
    private final Duration commandTimeout = Duration.ofMinutes(30L);

    public MicrosoftVagrantBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                                      BoxContext context,
                                      VirtualBoxManager virtualBoxManager, VirtualBoxExecutable virtualBox, VirtualBoxSDL vboxSdl,
                                      Vagrant vagrant, Path vagrantDirectory, VersionedItemStore versionRegistry)
    throws BrowserBoxException
    {
        this.boxConfiguration = boxConfiguration;
        this.projectConfiguration = projectConfiguration;
        this.context = context;
        this.virtualBoxManager = virtualBoxManager;
        this.virtualBox = virtualBox;
        this.vboxSdl = vboxSdl;
        this.vagrant = vagrant;
        this.vagrantDirectory = vagrantDirectory;
        this.versionRegistry = versionRegistry;

        try
        {
            browserSelector = BrowserSelector.parse(boxConfiguration);
        }
        catch (ParseException e)
        {
            throw new BrowserBoxException("Invalid box definition, type=" + boxConfiguration.getBrowserType() + ", version=" + boxConfiguration.getBrowserVersion(), e);
        }
        
        try 
        {
            edgeDriverVersionResolver = new MultiEdgeDriverVersionResolver(
                    new MavenArtifactPropertiesEdgeDriverVersionResolver(context),
                    new ChocolateyEdgeDriverVersionResolver());
            //edgeDriverVersionResolver = new ChocolateyEdgeDriverVersionResolver();
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error creating Edge driver version resolver: " + e.getMessage(), e);
        }
        
        //This is a hack to initialize FileUtils class - otherwise what can happen is that under shutdown hook
        //delete is called on this box, the class is yet to be loaded (because Maven is torn down) and deletion does not complete
        FileUtils.extension("galah.txt");
        Java7Support.isAtLeastJava7();
    }

    @Override
    public boolean exists() throws BrowserBoxException
    {
        return getVagrantStatus() != Vagrant.BoxStatus.NOT_CREATED;
    }
        
    protected Vagrant.BoxStatus getVagrantStatus()
    throws BrowserBoxException
    {
        context.getLog().debug("Getting status for " + vagrantDirectory);

        //If there is no Vagrantfile we know the box isn't set up yet
        if (Files.notExists(vagrantDirectory.resolve("Vagrantfile")))
            return Vagrant.BoxStatus.NOT_CREATED;
        
        Vagrant.StatusOptions options = new Vagrant.StatusOptions(vagrantDirectory, containerName());
        configureInstanceOptions(options);
        try
        {
            return vagrant.status(options);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to get Vagrant status: " + e.getMessage(), e);
        }
    }

    protected void configureInstanceOptions(Vagrant.InstanceOptions options)
    {
        configureBaseOptions(options);
        options.setBaseDirectory(getVagrantDirectory());
        options.setBoxName(containerName());
    }

    protected void configureBaseOptions(Vagrant.BaseOptions options)
    {
        options.env("CONTAINERNAME", containerName());
    }

    @Override
    public boolean isRunning() throws BrowserBoxException 
    {
        return getVagrantStatus() == Vagrant.BoxStatus.RUNNING;
    }

    @Override
    public void start() throws BrowserBoxException 
    {
        //If the image does not exist, create it
        if (!hasImage())
            prepareImage();
        
        //Generate Vagrantfile to vagrant directory
        BoxDefinition browserBoxDefinition = browserBoxDefinition();
        generateVagrantFile(browserBoxDefinition, vagrantDirectory);
        
        //Now create/start
        Vagrant.UpOptions options = new Vagrant.UpOptions(vagrantDirectory, containerName());
        configureInstanceOptions(options);
        try
        {
            vagrant.up(options);
            configureVmVideoMode(boxConfiguration);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to start Vagrant: " + e.getMessage(), e);
        }
    }

    private void configureVmVideoMode(BoxConfiguration boxConfiguration)
    throws BrowserBoxException
    {
        if (boxConfiguration.getResolution() != null && boxConfiguration.getResolution().getWidth() > 0 &&
            boxConfiguration.getResolution().getHeight() > 0)
        {
            try
            {
                context.getLog().debug("Set video mode hint: " + boxConfiguration.getResolution());
                virtualBoxManager.setVideoModeHint(new VirtualBoxManager.SetVideoModeHintOptions(containerName(),
                                                                                                 boxConfiguration.getResolution().getWidth(),
                                                                                                 boxConfiguration.getResolution().getHeight(),
                                                                                                 32));
            }
            catch (VirtualBoxException e)
            {
                throw new BrowserBoxException("Error setting resolution: " + e.getMessage(), e);
            }
        }
    }
    
    private void generateVagrantFile(BoxDefinition boxDefinition, Path vagrantDirectory)
    throws BrowserBoxException
    {
        Path vagrantFile = vagrantDirectory.resolve("Vagrantfile");
        URL vagrantFileResource = MicrosoftVagrantBrowserBox.class.getResource("ms.Vagrantfile");
        if (vagrantFileResource == null)
            throw new Error("Missing bundled resource ms.Vagrantfile");
        
        Properties properties = new Properties();
        properties.setProperty("boxDefinition.name", boxDefinition.getName());
        properties.setProperty("box.name", containerName());
        
        copyFilteredResource(vagrantFileResource, vagrantFile, properties);
    }
    
    private void copyFilteredResource(URL resource, Path targetFile, Properties extraProperties)
    throws BrowserBoxException
    {
        try (Reader fromReader = new InputStreamReader(resource.openStream(), StandardCharsets.UTF_8)) 
        {
            MavenReaderFilterRequest request = new MavenReaderFilterRequest(fromReader, true, projectConfiguration.getProject(),
                    Collections.emptyList(), true, context.getSession(),
                    extraProperties);

            try (Reader filteredReader = context.getReaderFilter().filter(request);
                 Writer fileWriter = new OutputStreamWriter(Files.newOutputStream(targetFile))) 
            {
                IOUtil.copy(filteredReader, fileWriter);
            }
        }
        catch (IOException | MavenFilteringException e)
        {
            throw new BrowserBoxException("Error generating Vagrantfile: " + e.getMessage(), e);
        }
    }
    
    private void createBrowserBoxVagrantImage(String fromBoxName)
    throws BrowserBoxException
    {
        String targetBoxName = microsoftVagrantBoxManager.browserBoxVagrantBoxName(browserSelector);

        WindowsBoxMaker boxMaker = new WindowsBoxMaker(virtualBoxManager, vagrant, projectConfiguration, context, commandTimeout);
        WindowsBoxMaker.BoxSource fromBox;

        if (browserSelector.getType() == BrowserSelector.Type.EDGE) 
        {
            EdgeDriverVersionResolver.Version edgeDriverVersion = edgeDriverVersionResolver.resolve(boxConfiguration.getBrowserVersion());
            fromBox = new WindowsBoxMaker.BoxSource(fromBoxName, "0", edgeDriverVersion, ImmutableList.of(), ImmutableList.of(), ImmutableList.of());
        }
        else
        {
            List<WindowsUpdateKB> windowsUpdates = new ArrayList<>();
            List<URL> sslCertificateSites = new ArrayList<>();
            List<URL> codeSigningCertificates = new ArrayList<>();
            if ("7".equals(browserSelector.getVersion()))
            {
                //KB4056564 for Vista - enables newer versions of TLS
                //Required to allow chocolatey to download tools from some sites
                windowsUpdates.add(new WindowsUpdateKB("4056564",
                                                       "2018-05 Security Update for Windows Server 2008 for x86-based Systems (KB4056564)"));


                try
                {
                    //The Vista box for IE7 is very old so needs to have some root certs installed to make the scripted installs work
                    sslCertificateSites.add(new URL("https://selenium-release.storage.googleapis.com")); //For IE and Edge driver
                    sslCertificateSites.add(new URL("https://javadl.oracle.com")); //For JDK 8

                    //Vista is too old so needs the Microsoft root code signing cert installed
                    //And this is needed for code signing the VirtualBox drivers
                    //Apparently needed even if Oracle certs are installed as root since this one is in the chain
                    codeSigningCertificates.add(new URL("https://www.microsoft.com/pki/certs/MicrosoftCodeVerifRoot.crt"));
                }
                catch (MalformedURLException e)
                {
                    throw new BrowserBoxException(e);
                }
            }

            //Need this for all versions of IE for some chocolatey ie/edge driver downloads
            //Some old versions of Windows don't have necessary root certificate
            try
            {
                sslCertificateSites.add(new URL("https://download.microsoft.com"));
            }
            catch (MalformedURLException e)
            {
                throw new BrowserBoxException(e);
            }

            //Just a dummy default version for now, not needed for IE anyway
            EdgeDriverVersionResolver.Version edgeDriverVersion = new Version("5.16299.20171103");
            fromBox = new WindowsBoxMaker.BoxSource(fromBoxName, "0", edgeDriverVersion, windowsUpdates,
                                                    sslCertificateSites, codeSigningCertificates);
        }
        
        WindowsBoxMaker.PrototypeBox prototypeBox = defaultPrototypeBox();
        boxMaker.run(fromBox, prototypeBox, targetBoxName);
    }

    private WindowsBoxMaker.PrototypeBox defaultPrototypeBox()
    {
        //TODO maybe we can generate prototype box with suffix on actual box name
        return new WindowsBoxMaker.PrototypeBox("WinIEPrototype", "IEUser", "Passw0rd!");
    }
    
    private Path saveUrlToTempFile(URL zipFileUrl)
    throws IOException
    {
        Path tempFile = Files.createTempFile(context.getTempDirectory(), 
                            "download-" + browserSelector.getType().name().toLowerCase(Locale.ENGLISH) + "-" + browserSelector.getVersion(), 
                            ".zip");
        URLConnection con = zipFileUrl.openConnection();
        long size = con.getContentLengthLong();
        try (InputStream is = con.getInputStream())
        {
            context.getLog().info("Saving " + zipFileUrl + " to file " + tempFile.toAbsolutePath().toString() + " (" + size + " bytes)...");
            Files.copy(is, tempFile, StandardCopyOption.REPLACE_EXISTING);
        }
        
        //Verify size if content length was sent (detect truncation)
        if (size >= 0L && Files.size(tempFile) != size)
            throw new IOException("Downloaded file is incomplete - expected " + size + " bytes but downloaded " + Files.size(tempFile) + " bytes");
        
        return tempFile;
    }
    
    /**
     * Generates and installs the original Microsoft Vagrant box in the system.
     * 
     * @param originalBoxName the name of the box to install the original image to.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    private void installOriginalImage(String originalBoxName)
    throws BrowserBoxException
    {
        Item download =
                versionRegistry.itemForVersion(boxConfiguration.getBrowserVersion());
        if (download == null)
            throw new BrowserBoxException("No available downloads found for browser version " + boxConfiguration.getBrowserVersion());

        URL boxZipUrl = download.getUrl();
        
        boolean keepDownloadArtifacts = projectConfiguration.getSaveBuildIntermediates().contains(BuildIntermediates.DOWNLOAD_ARTIFACTS);
        if (keepDownloadArtifacts)
        {
            context.getLog().info("Saving artifact " + boxZipUrl.toExternalForm() + " to local Maven repository...");
            boxZipUrl = versionRegistry.saveItemContents(new Item(boxConfiguration.getBrowserVersion(), boxZipUrl));
        }

        MicrosoftVagrantBoxInstaller installer = new MicrosoftVagrantBoxInstaller(projectConfiguration, context, vagrant);
        installer.install(boxZipUrl, originalBoxName);
    }
    
    private void deleteOriginalImage(String originalBoxName)
    throws BrowserBoxException
    {
        context.getLog().info("Deleting Vagrant image " + originalBoxName);
        
        BoxDefinition box = new BoxDefinition(originalBoxName, "0", "virtualbox");
        Vagrant.BoxRemoveOptions options = new Vagrant.BoxRemoveOptions(box);
        configureBaseOptions(options);
        try 
        {
            vagrant.boxRemove(options);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to remove box " + originalBoxName + ": " + e.getMessage(), e);
        }
    }

    @Override
    public void stop() 
    throws BrowserBoxException 
    {
        Vagrant.HaltOptions options = new Vagrant.HaltOptions(vagrantDirectory, containerName());
        configureInstanceOptions(options);
        try
        {
            vagrant.halt(options);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to stop Vagrant: " + e.getMessage(), e);
        }
    }

    @Override
    public void createAndStart() throws BrowserBoxException 
    {
        start();
    }

    @Override
    public TunnelSession establishTunnels() throws BrowserBoxException 
    {
        ConnectionInfo info = getConnectionInfo(StandardConnectionType.SSH);
        return establishSshPortTunnels(info.getUri().getHost(), info.getUri().getPort(), info.getUsername(), info.getPassword());
    }

    /**
     * Queries Virtualbox for the logs directory of an existing VM if it exists.  Used so that after shutdown
     * the VM directory can be cleaned up if it is not cleaned up due to a Vagrant bug.
     * <p>
     *
     * Sometimes, after a vagrant destroy, a VM is not properly cleaned up, and that the logs directory with some
     * log files is left.  This causes problems if a VM with the same name is attempted to be created in the future,
     * so BrowserBox will attempt to properly clean up.
     *
     * @param name the VM name.
     *
     * @return the Virtualbox VM logs directory if it exists, or null otherwise.
     */
    private Path findVirtualBoxVmLogsDirectory(String name)
    {
        try
        {
            Map<String, String> vmInfo = virtualBoxManager.showVmInfo(new ShowVmInfoOptions(name));

            String logDirStr = vmInfo.get("LogFldr");
            if (logDirStr == null)
                return null;

            Path logDir = Paths.get(logDirStr);

            //These will typically be:
            //logDir: <vm base dir>/Logs

            //Ensure the dir exists
            if (!Files.isDirectory(logDir))
                return null;

            return logDir;
        }
        catch (VirtualBoxException e)
        {
            //Couldn't get VM info, maybe it doesn't exist or is already deleted?
            context.getLog().debug("Did not find Virtualbox logs directory of " + name + ": " + e, e);

            return null;
        }
    }

    /**
     * Given a Virtualbox VM logs directory, will attempt to delete the VM base directory only if the logs directory
     * is the only thing left and it only contains log files.
     * <p>
     *
     * This is very conservative, as we don't want to delete / clean in any case apart from the specialised case where
     * Vagrant failed to clean up the VM log files.
     *
     * @param logDir the Virtualbox VM log directory, whose base directory will be possibly cleaned up.
     */
    @VisibleForTesting
    void cleanUpVirtualboxVmFromLogDirectory(Path logDir)
    {
        if (logDir == null)
            return;
        if (!Files.isDirectory(logDir))
            return;

        context.getLog().warn("Virtualbox directory " + logDir + " was not cleaned up properly after Vagrant destroy, will manually clean.");

        try
        {
            //Log directory still exists
            //Verify it contains ONLY log files
            try (DirectoryStream<Path> logFiles = Files.newDirectoryStream(logDir))
            {
                for (Path logFile : logFiles)
                {
                    if (!logFile.getFileName().toString().endsWith(".log") || Files.isDirectory(logFile))
                    {
                        context.getLog().warn("Not cleaning up " + logDir + " because there were non-logfiles detected in it: " + logFile);
                        return;
                    }
                }
            }

            //Check that the parent's only child is the log directory
            //If any other stuff is in there we don't want to clean it up
            Path vmBaseDir = logDir.getParent();

            if (!Files.list(vmBaseDir).collect(Collectors.toList()).equals(ImmutableList.of(logDir)))
            {
                context.getLog().warn("Extra files detected in VM directory " + vmBaseDir + ", not cleaning up");
                return;
            }

            //If we get here, there is a VM base dir with a single log directory in it, and that log directory
            //only has *.log files in it
            //Now it's safe to clean up
            try (DirectoryStream<Path> logFiles = Files.newDirectoryStream(logDir))
            {
                for (Path logFile : logFiles)
                {
                    if (logFile.getFileName().toString().endsWith(".log"))
                        Files.delete(logFile);
                }
            }
            Files.delete(logDir);
            Files.delete(vmBaseDir);

            context.getLog().info("Virtualbox VM directory " + vmBaseDir + " cleaned up");
        }
        catch (IOException e)
        {
            context.getLog().warn("Failed to clean up Virtualbox directory: " + e.getMessage(), e);
        }
    }

    @Override
    public void delete() throws BrowserBoxException 
    {
        //Special handling for a possible bug in Vagrant where Virtualbox directories are not always cleaned up
        Path virtualBoxLogDir = findVirtualBoxVmLogsDirectory(containerName());
        context.getLog().debug("Virtualbox log directory: " + virtualBoxLogDir);

        Vagrant.DestroyOptions options = new Vagrant.DestroyOptions(vagrantDirectory, containerName());
        configureInstanceOptions(options);
        try
        {
            vagrant.destroy(options);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to perform Vagrant destroy: " + e.getMessage(), e);
        }

        //Also delete the Vagrant directory
        if (Files.exists(vagrantDirectory))
        {
            context.getLog().info("Deleting Vagrant directory " + vagrantDirectory.toString());
            try
            {
                FileUtils.deleteDirectory(vagrantDirectory.toFile());
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Failed to delete container directory " + vagrantDirectory.toString() + ": " + e, e);
            }
        }

        //Post-destroy cleanup - sometimes Virtualbox directories are not fully cleaned up
        cleanUpVirtualboxVmFromLogDirectory(virtualBoxLogDir);
    }
    
    private BoxDefinition browserBoxDefinition()
    throws BrowserBoxException
    {
        return new BoxDefinition(microsoftVagrantBoxManager.browserBoxVagrantBoxName(browserSelector),
                                    "0", "virtualbox");
    }

    @Override
    public void deleteImage() throws BrowserBoxException 
    {
        Vagrant.BoxRemoveOptions options = new Vagrant.BoxRemoveOptions(browserBoxDefinition());
        configureBaseOptions(options);
        options.setForce(true);
        try
        {
            vagrant.boxRemove(options);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to remove Vagrant box: " + e.getMessage(), e);
        }
    }

    /**
     * Creates the browser box Vagrant image by downloading and installing the original Microsoft image, augmenting it,
     * and saving the modified image.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    @Override
    public void prepareImage() throws BrowserBoxException 
    {
        BoxListOptions boxListOptions = new BoxListOptions();
        configureBaseOptions(boxListOptions);
        List<? extends BoxDefinition> currentVagrantBoxes;
        try
        {
            currentVagrantBoxes = vagrant.boxList(boxListOptions);
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Error getting box image list from Vagrant: " + e.getMessage(), e);
        }

        //Check to see if the base image exists
        String originalName = microsoftVagrantBoxManager.originalVagrantBoxName(browserSelector);
        if (currentVagrantBoxes.stream().noneMatch(def -> def.getName().equalsIgnoreCase(originalName)))
            installOriginalImage(originalName);

        createBrowserBoxVagrantImage(originalName);

        if (!projectConfiguration.getSaveBuildIntermediates().contains(BuildIntermediates.BOX_IMAGES))
            deleteOriginalImage(originalName);
    }

    @Override
    public boolean hasImage() throws BrowserBoxException 
    {
        //If the image does not exist, create it
        BoxListOptions boxListOptions = new BoxListOptions();
        configureBaseOptions(boxListOptions);
        try 
        {
            List<? extends BoxDefinition> boxes = vagrant.boxList(boxListOptions);
            BoxDefinition browserBoxDefinition = browserBoxDefinition();
            String imageName = browserBoxDefinition.getName(); //ignore version/type for matching
            return boxes.stream().anyMatch(def -> def.getName().equalsIgnoreCase(imageName));
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Error getting box image list from Vagrant: " + e.getMessage(), e);
        }
    }

    @Override
    public String getName() 
    {
        return containerName();
    }

    @Override
    public void waitUntilStarted(Duration maxTimeToWait) throws TimeoutException, BrowserBoxException 
    {
        int waitMillis;
        if (maxTimeToWait == null)
            waitMillis = Integer.MAX_VALUE;
        else
            waitMillis = Math.toIntExact(Math.min(maxTimeToWait.toMillis(), Integer.MAX_VALUE));
        
        String url = getWebDriverUrl().toString();
        WaitChecker checker = new HttpPingChecker(url, "GET", WaitConfiguration.DEFAULT_STATUS_RANGE);
        try
        {
            WaitUtil.wait(new BrowserBoxRunningPrecondition(this, context), waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BrowserBoxException("Error occurred while waiting for browser box to start up: " + e, e);
        }
    }

    /**
     * @return the name of the container.  Defaults to the name given in the configuration.
     */
    protected String containerName()
    {
        return boxConfiguration.getContainerName();
    }

    protected Path getVagrantDirectory()
    {
        return vagrantDirectory;
    }

    protected TunnelSession establishSshPortTunnels(String host, int browserBoxSshPort, String sshUser, String sshPassword)
    throws BrowserBoxException
    {
        List<BoxConfiguration.TunnelPort> tunnelPorts = boxConfiguration.tunnelPortConfiguration();
        if (tunnelPorts.isEmpty())
            return new NullTunnelSession();
        
        TunnelSession tunnelSession = SshTunnelSession.create(tunnelPorts, host, browserBoxSshPort, sshUser, sshPassword);
        context.getLog().info("SSH tunnel(s) established: " + tunnelPorts);
        return tunnelSession;
    }

    @Override
    public List<? extends ConnectionType> getSupportedConnectionTypes() 
    throws BrowserBoxException 
    {
        return ImmutableList.of(VagrantConnectionType.VIRTUALBOX_UI, StandardConnectionType.RDP,
                                StandardConnectionType.VNC, StandardConnectionType.SELENIUM,
                                StandardConnectionType.SSH);
    }

    @Override
    public ConnectionInfo getConnectionInfo(ConnectionType connectionType) 
    throws BrowserBoxException 
    {
        if (connectionType == StandardConnectionType.SELENIUM)
            return new ConnectionInfo(URI.create("http://localhost:" + boxConfiguration.getWebDriverPort() + "/wd/hub"));
        else if (connectionType == StandardConnectionType.SSH)
            return new ConnectionInfo(URI.create("ssh://localhost:2222"), defaultPrototypeBox().getUser(), defaultPrototypeBox().getPassword());
        else if (connectionType == StandardConnectionType.VNC)
            return new ConnectionInfo(URI.create("vnc://localhost:15900"), null, "secret");
        else if (connectionType == StandardConnectionType.RDP)
            return new ConnectionInfo(URI.create("rdp://localhost:13389"), "IEUser", "Passw0rd!");
        else if (connectionType == VagrantConnectionType.VIRTUALBOX_UI)
            return new ConnectionInfo(URI.create("virtualboxui:" + getName()));
        else 
            throw new BrowserBoxException("Unknown connection type " + connectionType);
    }

    @Override
    public BrowserControl browserControl() 
    throws BrowserBoxException 
    {
        try 
        {
            return new SeleniumBrowserControl(getWebDriverUrl(), 
                        browserSelector.getType() == BrowserSelector.Type.EDGE ? DesiredCapabilities.edge() : DesiredCapabilities.internetExplorer());
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }

    @Override
    public VideoControl video() 
    throws BrowserBoxException 
    {
        return new MicrosoftFfmpegVideoControl(this, boxConfiguration);
    }

    @Override
    public String getSeleniumBrowserType()
    {
        switch (browserSelector.getType())
        {
            case EDGE:
                return BrowserType.EDGE;
            case IE:
                return BrowserType.IE;
            default:
                throw new Error("Unknown browser type: " + browserSelector.getType());
        }
    }

    @Override
    public void installCertificates(Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        //Copy certificates to target machine using SSH

        //Start SSH session
        ConnectionInfo info = getConnectionInfo(StandardConnectionType.SSH);
        String sshUser = info.getUsername();
        String sshPassword = info.getPassword();
        String browserBoxSshHost = info.getUri().getHost();
        int browserBoxSshPort = info.getUri().getPort();

        JSch jsch = new JSch();

        try
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            try
            {
                //Save certificates to remote machine
                List<String> remoteCertificateFileNames = new ArrayList<>();

                ChannelSftp sftpChannel = (ChannelSftp)sshSession.openChannel("sftp");
                sftpChannel.connect();

                int certificateIndex = 0;
                for (X509Certificate certificate : certificates)
                {
                    try (InputStream is = new ByteArrayInputStream(certificate.getEncoded()))
                    {
                        String remoteCertificateFileName = "cert" +  + certificateIndex + ".cer";
                        remoteCertificateFileNames.add(remoteCertificateFileName);
                        sftpChannel.put(is, remoteCertificateFileName);
                        certificateIndex++;
                    }
                    catch (CertificateEncodingException | IOException | SftpException e)
                    {
                        throw new BrowserBoxException("Error saving certificates: " + e.getMessage(), e);
                    }
                }
                sftpChannel.disconnect();

                context.getLog().debug("Now have to install certificates: " + remoteCertificateFileNames);

                for (String remoteCertificateFileName : remoteCertificateFileNames)
                {
                    ChannelExec exec = (ChannelExec)sshSession.openChannel("exec");
                    exec.setCommand("certutil -AddStore \"root\" " + remoteCertificateFileName);

                    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
                         ByteArrayOutputStream err = new ByteArrayOutputStream())
                    {
                        exec.setOutputStream(out);
                        exec.setErrStream(err);

                        exec.connect();
                        while (exec.getExitStatus() < 0)
                        {
                            Thread.sleep(projectConfiguration.getPollTime().toMillis());
                        }

                        if (out.size() > 0)
                            context.getLog().info("certutil> " + new String(out.toByteArray(), StandardCharsets.UTF_8));
                        if (err.size() > 0)
                            context.getLog().error("certutil> " + new String(err.toByteArray(), StandardCharsets.UTF_8));

                        if (exec.getExitStatus() != 0)
                            throw new BrowserBoxException("Error installing certificates: " + exec.getExitStatus());

                        exec.disconnect();
                    }
                    catch (IOException e)
                    {
                        throw new BrowserBoxException("Error installing certificates: " + e.getMessage(), e);
                    }
                }
            }
            finally
            {
                sshSession.disconnect();
            }
        }
        catch (JSchException | InterruptedException e)
        {
            throw new BrowserBoxException("Error installing certificates: " + e.getMessage(), e);
        }
    }

    private static class MicrosoftFfmpegVideoControl extends FfmpegVideoControl
    {
        private final Path stopRecordingMarkerFile;
        private final boolean unixPaths;
        
        public MicrosoftFfmpegVideoControl(MicrosoftVagrantBrowserBox box, BoxConfiguration boxConfiguration)
        throws BrowserBoxException
        {
            super(box, boxConfiguration);
            stopRecordingMarkerFile = box.getVagrantDirectory().resolve("stopffmpeg");
            this.unixPaths = detectUnixShellType();
        }
        
        @Override
        public Recording startRecording() 
        throws IOException, BrowserBoxException 
        {
            //Clean up any stop marker
            Files.deleteIfExists(stopRecordingMarkerFile);
            
            return super.startRecording();
        }

        @Override
        protected void stopRecording(OutputStream ffmpegInputWriter, Session sshSession)
        throws IOException
        {
            //Create empty stop marker file
            if (Files.notExists(stopRecordingMarkerFile))
                Files.createFile(stopRecordingMarkerFile);
            
            ffmpegInputWriter.close();
        }

        @Override
        protected String ffmpegCommand() 
        {
            return "psexec -accepteula -i 1 -u IEUser -p Passw0rd! powershell -Window minimized -File c:\\\\scripts\\\\runffmpeg.ps1 " + ffmpegArguments();
        }

        @Override
        protected String ffmpegArguments() 
        {
            //Actually want to run this through ssh:
            //psexec -i 1 -u IEUser -p Passw0rd! powershell -File c:\\scripts\\runffmpeg.ps1 -y -f gdigrab -framerate 25 -i desktop -pix_fmt yuv420p c:\\vagrant\\galah.mp4
            
            StringBuilder commandBuf = new StringBuilder();

            Resolution boxResolution = getBoxConfiguration().getResolution();
            if (boxResolution != null && boxResolution.getWidth() > 0 && boxResolution.getHeight() > 0)
                commandBuf.append("-video_size " + boxResolution.getWidth() + "x" + boxResolution.getHeight() + " ");

            commandBuf.append("-y -framerate 25 -f gdigrab -i desktop -pix_fmt yuv420p -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" c:\\\\vagrant\\\\output.mp4");

            return commandBuf.toString();
        }

        @Override
        protected String videoFileName() 
        {
            if (unixPaths)
                return "/cygdrive/c/Vagrant/output.mp4";
            else
                return "/C:/Vagrant/output.mp4";
        }

        /**
         * @return true to use UNIX style paths, false to use windows style paths.  All depends on which version of SSH
         * is installed on the Windows box.
         */
        private boolean detectUnixShellType()
        throws BrowserBoxException
        {
            try
            {
                int shellTypeCheckResult = sshExecute("export TEST", new NullOutputStream());
                boolean isUnixShellType = (shellTypeCheckResult == 0);
                //System.out.println("UNIX shell type: " + isUnixShellType);

                return isUnixShellType;
            }
            catch (IOException | JSchException e)
            {
                throw new BrowserBoxException("Failed to detect shell type: " + e.getMessage(), e);
            }
        }

        private int sshExecute(String command, OutputStream result)
        throws JSchException, IOException, BrowserBoxException
        {
            ConnectionInfo info = getBox().getConnectionInfo(StandardConnectionType.SSH);
            String sshUser = info.getUsername();
            String sshPassword = info.getPassword();
            String browserBoxSshHost = info.getUri().getHost();
            int browserBoxSshPort = info.getUri().getPort();

            JSch jsch = new JSch();

            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            try
            {
                //Execute command
                Channel channel = sshSession.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);

                InputStream commandOutput = channel.getInputStream();
                InputStream commandErr = ((ChannelExec) channel).getErrStream();

                channel.connect();
                try
                {
                    ByteArrayOutputStream err = new ByteArrayOutputStream();
                    ByteStreams.copy(commandOutput, result);
                    ByteStreams.copy(commandErr, err);

                    //if (err.size() > 0)
                    //    System.err.println(new String(err.toByteArray(), StandardCharsets.UTF_8));

                    return channel.getExitStatus();
                }
                finally
                {
                    channel.disconnect();
                }
            }
            finally
            {
                sshSession.disconnect();
            }
        }
    }

    public static enum VagrantConnectionType implements ConnectionType
    {
        /**
         * A 'native' UI connection to VirtualBox that simply displays its UI on the host using VBoxSDL.
         */
        VIRTUALBOX_UI
    }

    public static class VirtualBoxUiFinder implements ToolFinder
    {
        @Override
        public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box,
                                             BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                                             BoxContext context)
        throws IOException, BrowserBoxException
        {
            //Only supported for Vagrant boxes
            if (!(box instanceof MicrosoftVagrantBrowserBox))
                return Optional.empty();

            MicrosoftVagrantBrowserBox mBox = (MicrosoftVagrantBrowserBox)box;

            return Optional.of(new VirtualBoxExecutableRunner());
            //return Optional.of(new VBoxSdlRunner(mBox.vboxSdl));
        }

        private static class VirtualBoxExecutableRunner implements ToolRunner
        {
            private void configureVideoModeAfterDelay(MicrosoftVagrantBrowserBox box, BoxConfiguration boxConfiguration,
                                                      BoxContext context, Duration delay)
            {
                //Launch as delayed in the background another call to set resolution
                try
                {
                    Thread.sleep(delay.toMillis());
                    box.configureVmVideoMode(boxConfiguration);
                }
                catch (BrowserBoxException | InterruptedException e)
                {
                    context.getLog().error("Error setting video mode: " + e.getMessage(), e);
                }
            }

            @Override
            public int run(ConnectionInfo connectionInfo, BrowserBox sbox, BoxConfiguration boxConfiguration,
                           ProjectConfiguration projectConfiguration, BoxContext context, StreamConsumer out,
                           StreamConsumer err, ListeningExecutorService executor)
            throws IOException, BrowserBoxException
            {
                MicrosoftVagrantBrowserBox box = (MicrosoftVagrantBrowserBox)sbox;

                //VirtualBox tool when spawned will reset the resolution for god knows what reason
                //so reset the resolution back after delay- horrible hack but I don't know what else I can do here
                executor.submit(() -> configureVideoModeAfterDelay(box, boxConfiguration, context, Duration.ofSeconds(1)));

                try
                {
                    VirtualBoxExecutable.StartVmOptions options = new VirtualBoxExecutable.StartVmOptions(box.getName());
                    options.setSeparate(true);
                    //options.setScale(true);
                    box.virtualBox.startVm(options);

                    //Non-zero exit code throws an exception
                    return 0;
                }
                catch (VirtualBoxException e)
                {
                    throw new BrowserBoxException("Error invoking VirtualBox tool: " + e.getMessage(), e);
                }
            }
        }

        private static class VBoxSdlRunner implements ToolRunner
        {
            private final VirtualBoxSDL vboxSdl;

            public VBoxSdlRunner(VirtualBoxSDL vboxSdl)
            {
                this.vboxSdl = vboxSdl;
            }

            @Override
            public int run(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                           ProjectConfiguration projectConfiguration, BoxContext context, StreamConsumer out,
                           StreamConsumer err, ListeningExecutorService executor)
            throws IOException, BrowserBoxException
            {
                try
                {
                    VirtualBoxSDL.StartVmOptions options = new VirtualBoxSDL.StartVmOptions(box.getName());
                    options.setSeparate(true);
                    vboxSdl.startVm(options);

                    //Non-zero exit code throws an exception
                    return 0;
                }
                catch (VirtualBoxException e)
                {
                    throw new BrowserBoxException("Error invoking VBoxSDL tool: " + e.getMessage(), e);
                }
            }
        }
    }
}
