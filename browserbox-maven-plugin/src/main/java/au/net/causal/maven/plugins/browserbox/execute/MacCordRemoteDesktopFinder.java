package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import au.net.causal.maven.plugins.browserbox.box.Resolution;
import com.google.common.collect.ImmutableList;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.regex.Pattern;

public class MacCordRemoteDesktopFinder extends GenericPathBasedFinder
{
    private static final Pattern APP_PATTERN = Pattern.compile("^Cord.app$", Pattern.CASE_INSENSITIVE);
    
    private final WindowsRemoteDesktopTools windowsRemoteDesktopTools = new WindowsRemoteDesktopTools();
    
    public MacCordRemoteDesktopFinder()
    {
        super(ImmutableList.of(Paths.get("/Applications"), Paths.get(System.getProperty("user.home")).resolve("Applications")));
    }

    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, 
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration, 
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> app = findInSpecifiedPaths(getSearchPaths(), APP_PATTERN);
        if (!app.isPresent())
            return Optional.empty();
        
        Path exe = app.get().resolve("Contents").resolve("MacOS").resolve("CoRD");
        if (!Files.isRegularFile(exe))
            return Optional.empty();

        Resolution resolution = boxConfiguration.getResolution();
        if (resolution == null || resolution.getWidth() <= 0 || resolution.getHeight() <= 0)
            resolution = windowsRemoteDesktopTools.getDefaultResolution();
        
        Commandline commandline =  new Commandline(exe.toAbsolutePath().toString());
        commandline.addArguments(args("-host", connectionInfo.getUri().getHost(), 
                                    "-port", String.valueOf(connectionInfo.getUri().getPort()), 
                                    "-a", "32", 
                                    "-width", String.valueOf(resolution.getWidth()), 
                                    "-height", String.valueOf(resolution.getHeight())));
        
        return Optional.of(commandline);
    }
}
