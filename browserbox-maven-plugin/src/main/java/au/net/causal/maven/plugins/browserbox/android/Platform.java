package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.SystemUtils;

import java.util.Set;

public enum Platform
{
    LINUX_64("linux64"),
    WINDOWS("win32"),
    MAC("mac64", "mac32");

    private final Set<String> labels;

    private Platform(String... labels)
    {
        this.labels = ImmutableSet.copyOf(labels);
    }

    public Set<String> getLabels()
    {
        return labels;
    }

    public static Platform forLabel(String label)
    {
        for (Platform platform : values())
        {
            if (platform.getLabels().contains(label))
                return platform;
        }

        return null;
    }
    
    public static Platform current()
    throws BrowserBoxException
    {
        if (SystemUtils.IS_OS_MAC_OSX)
            return MAC;
        else if (SystemUtils.IS_OS_WINDOWS)
            return WINDOWS;
        else if (SystemUtils.IS_OS_LINUX)
            return LINUX_64;
        else
            throw new BrowserBoxException("Unsupported platform: " + SystemUtils.OS_NAME + " " + SystemUtils.OS_VERSION);
    }
}
