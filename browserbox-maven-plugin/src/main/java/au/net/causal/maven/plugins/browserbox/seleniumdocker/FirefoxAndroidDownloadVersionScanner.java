package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import org.apache.maven.plugin.logging.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

public class FirefoxAndroidDownloadVersionScanner extends FirefoxDownloadVersionScanner
{
    public FirefoxAndroidDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist)
    {
        super(log, versionBlacklist);
    }

    public FirefoxAndroidDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist, ExecutorService executor)
    {
        super(log, versionBlacklist, executor);
    }

    @Override
    protected URL downloadUrlForVersion(URL versionUrl) throws IOException
    {
        URL x86Link = androidX86VersionDownloadUrl(versionUrl);
        if (x86Link == null)
            return null;

        URL multiLangLink = findMuliLangLink(x86Link);
        if (multiLangLink == null)
            return null;

        return findApkLink(multiLangLink);
    }

    private URL androidX86VersionDownloadUrl(URL versionUrl)
    throws IOException
    {
        //Next file listing should have something like 'android-x86'
        Document d = Jsoup.parse(versionUrl, Math.toIntExact(pageTimeout.toMillis()));
        for (String href : d.getElementsByTag("a").eachAttr("href"))
        {
            String operatingSystemToken = lastFilePathToken(href);
            if (operatingSystemToken.equalsIgnoreCase("android-x86"))
                return new URL(versionUrl, href);
        }

        return null;
    }

    private URL findMuliLangLink(URL platformUrl)
    throws IOException
    {
        Document d = Jsoup.parse(platformUrl, Math.toIntExact(pageTimeout.toMillis()));
        for (String href : d.getElementsByTag("a").eachAttr("href"))
        {
            String langToken = lastFilePathToken(href);
            if (langToken.equalsIgnoreCase("multi"))
                return new URL(platformUrl, href);
        }

        return null;
    }

    private URL findApkLink(URL langUrl)
    throws IOException
    {
        Document d = Jsoup.parse(langUrl, Math.toIntExact(pageTimeout.toMillis()));

        URL fennecApkUrl = null;
        URL apkUrl = null;

        for (String href : d.getElementsByTag("a").eachAttr("href"))
        {
            String apkToken = lastFilePathToken(href).toLowerCase(Locale.ENGLISH);
            if (apkToken.endsWith(".apk"))
            {
                apkUrl = new URL(langUrl, href);
                if (apkToken.contains("fennec"))
                    fennecApkUrl = apkUrl;
            }
        }

        //Prefer APKs with 'fennec' in the name
        if (fennecApkUrl != null)
            return fennecApkUrl;
        else
            return apkUrl;
    }
}
