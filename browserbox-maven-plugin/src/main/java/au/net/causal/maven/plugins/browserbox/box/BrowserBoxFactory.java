package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface BrowserBoxFactory 
{
    /**
     * Creates a browser box.
     *
     * @param boxConfiguration configuration for the box itself.
     * @param projectConfiguration generic project and global configuration.
     * @param context context for interacting with Maven and other parts of the system.
     *
     * @return the created box.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BrowserBoxException;

    public boolean supportsType(String type);
    
    public Set<String> getKnownTypes();
    
    public List<String> availableKnownVersions(String type, BoxContext context)
    throws BrowserBoxException;
    
    public void deleteAllImages(String type, BoxContext context)
    throws BrowserBoxException;
    
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, 
                                      BoxContext context, GenerateVersionsContext generateContext)
    throws BrowserBoxException, IOException;
}
