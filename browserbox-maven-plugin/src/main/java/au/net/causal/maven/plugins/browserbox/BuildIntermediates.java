package au.net.causal.maven.plugins.browserbox;

import java.util.EnumSet;
import java.util.Set;

/**
 * Types of intermediate files, artifacts or images that are produced during browser box image construction.
 */
public enum BuildIntermediates
{
    /**
     * Represents all other build intermediates.
     */
    ALL,

    /**
     * Downloaded VM artifacts that are kept in the local Maven repository.
     */
    DOWNLOAD_ARTIFACTS,

    /**
     * Box images for browser VMs, stored as either Docker images or Vagrant images.
     */
    BOX_IMAGES;

    /**
     * Expands 'ALL' value into all values if the set contains 'ALL'.
     * 
     * @param bi a set of build intermediates.
     */
    public static void expand(Set<BuildIntermediates> bi)
    {
        if (bi.contains(ALL)) 
        {
            bi.remove(ALL);
            bi.addAll(EnumSet.complementOf(EnumSet.of(ALL)));
        }
    }
}