package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import org.apache.maven.plugin.logging.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class UrlKeyPreservingGenerateVersionsRegistry extends GenerateVersionsRegistry
{
    public UrlKeyPreservingGenerateVersionsRegistry(GenerateVersionsContext generateContext)
    {
        super(generateContext);
    }

    public UrlKeyPreservingGenerateVersionsRegistry(GenerateVersionsContext generateContext, Log logger)
    {
        super(generateContext, logger);
    }

    public UrlKeyPreservingGenerateVersionsRegistry(GenerateVersionsContext generateContext, Log logger, boolean checkExistingUrls)
    {
        super(generateContext, logger, checkExistingUrls);
    }

    @Override
    public void saveItems(Collection<? extends Item> items)
    throws BrowserBoxException
    {
        Properties downloads = getGenerateContext().getDownloads();

        Map<String, String> downloadUrlsToVersions = new HashMap<>();
        downloads.forEach((k, v) -> downloadUrlsToVersions.put(v.toString(), k.toString()));

        List<Item> itemsToProcess = new ArrayList<>();

        for (Item item : items)
        {
            String versionForItemUrl = downloadUrlsToVersions.get(item.getUrl().toExternalForm());
            if (versionForItemUrl != null)
                getLogger().ifPresent(logger -> logger.debug("Looks like item " + item.toString() + " URL is already mapped to " + versionForItemUrl));
            else
                itemsToProcess.add(item);
        }

        super.saveItems(itemsToProcess);
    }
}
