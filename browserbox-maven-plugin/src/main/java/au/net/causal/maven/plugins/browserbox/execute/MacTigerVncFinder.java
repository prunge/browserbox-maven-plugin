package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.collect.ImmutableList;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.regex.Pattern;

public class MacTigerVncFinder extends GenericPathBasedFinder
{
    private static final Pattern TIGERVNC_PATTERN = Pattern.compile("^TigerVNC Viewer.*\\.app$", Pattern.CASE_INSENSITIVE);
    
    private final TigerVncTools tigerVnc = new TigerVncTools();
    
    public MacTigerVncFinder()
    {
        super(ImmutableList.of(Paths.get("/Applications"), Paths.get(System.getProperty("user.home")).resolve("Applications")));
    }

    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, 
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration,
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> executable = findInSearchPath(TIGERVNC_PATTERN);
        if (executable.isPresent())
        {
            Path vncPasswordFile = tigerVnc.generatePasswordFile(connectionInfo, context);
            
            Commandline commandline =  new Commandline("open");
            String host = connectionInfo.getUri().getHost();
            int port = connectionInfo.getUri().getPort();
            commandline.addArguments(args("-W", executable.get().toAbsolutePath().toString(), "--args", host + ":" + port, 
                                        "-PasswordFile", vncPasswordFile.toAbsolutePath().toString()));
            
            return Optional.of(commandline);
        }
        
        return Optional.empty();
    }
}
