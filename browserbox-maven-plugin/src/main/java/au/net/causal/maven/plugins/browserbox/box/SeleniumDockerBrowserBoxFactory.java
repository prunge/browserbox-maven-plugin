package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionResolver;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionScanner;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.DockerNaming;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.DockerRunBrowserVersionReader;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.MavenArtifactBrowserVersionResolver;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.Tag;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.TagBrowserVersionReader;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.TagDetails;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.model.Image;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class SeleniumDockerBrowserBoxFactory implements BrowserBoxFactory 
{
    private final String boxName;
    protected final DockerNaming dockerNaming = new DockerNaming();
    
    protected SeleniumDockerBrowserBoxFactory(String boxName)
    {
        Objects.requireNonNull(boxName, "boxName == null");
        this.boxName = boxName;
    }

    @Override
    public boolean supportsType(String type) 
    {
        return boxName.equals(type);
    }

    @Override
    public Set<String> getKnownTypes() 
    {
        return Collections.singleton(boxName);
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, BoxContext context)
    throws BrowserBoxException
    {
        if (boxConfiguration.getBrowserVersion() == null)
            boxConfiguration.setBrowserVersion(versionResolver(context).defaultVersion());

        if (boxConfiguration.getContainerName() == null)
        {
            if (boxConfiguration.getBrowserVersion() == null)
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType());
            else
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType() + "-" + boxConfiguration.getBrowserVersion());
        }
        if (boxConfiguration.getWebDriverPort() <= 0)
            boxConfiguration.setWebDriverPort(4444);
    }
    
    protected BrowserVersionResolver versionResolver(BoxContext context)
    {
        return new MavenArtifactBrowserVersionResolver(boxName, context);
    }

    @Override
    public List<String> availableKnownVersions(String type, BoxContext context) throws BrowserBoxException 
    {
        return versionResolver(context).availableBrowserVersions();
    }

    protected ImageReference dockerBaseImageName(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BrowserBoxException
    {
        BrowserVersionResolver versionResolver = versionResolver(context);

        String version = boxConfiguration.getBrowserVersion();
        if (version == null)
            version = versionResolver.defaultVersion();
        if (version == null)
            throw new BrowserBoxException("No version specified and no default version could be found.");

        Tag tag = versionResolver.tagForBrowserVersion(version);
        if (tag == null)
            throw new BrowserBoxException("Could not find a Selenium docker image for the specified browser version: " + version);
        
        return new ImageReference("selenium/standalone-" + boxName + "-debug", tag);
    }

    protected void deleteAllMatchingImages(BoxContext context, Function<Image, Set<ImageReference>> imageMatcher)
    throws BrowserBoxException
    {
        try
        {
            for (Image image : context.getDockerServiceHub().getDockerAccess().listImages(false))
            {
                Set<ImageReference> matchingImages = imageMatcher.apply(image);
                for (ImageReference matchingImage : matchingImages)
                {
                    context.getLog().info("Deleting image " + matchingImage);
                    context.getDockerServiceHub().getDockerAccess().removeImage(matchingImage.getName(), false);
                }
            }
        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException("Error processing Docker images: " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteAllImages(String type, BoxContext context)
    throws BrowserBoxException
    {
        deleteAllMatchingImages(context, this::matchingImages);
    }

    protected Set<ImageReference> matchingImages(Image image)
    {
        if (dockerNaming.labels().matchesAnyImageForBrowserType(image, boxName))
            return ImmutableSet.copyOf(image.getRepoTags().stream().map(ImageReference::parse).collect(Collectors.toSet()));
        else
            return Collections.emptySet();
    }

    protected TagBrowserVersionReader versionReaderForBrowserType(String browserType,
                                                                  ProjectConfiguration projectConfiguration,
                                                                  BoxContext context)
    throws BrowserBoxException, IOException
    {
        return new DockerRunBrowserVersionReader(browserType, projectConfiguration, context);
    }

    @Override
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, 
                                      BoxContext context, GenerateVersionsContext generateVersionsContext) 
    throws BrowserBoxException, IOException
    {
        TagBrowserVersionReader reader = versionReaderForBrowserType(type, projectConfiguration, context);
        BrowserVersionScanner mapper = new BrowserVersionScanner(context.getLog());
        
        context.getLog().info("Reading tags...");
        List<? extends TagDetails> tags = mapper.readTags("selenium/standalone-" + type + "-debug");
        context.getLog().info("Found " + tags.size() + " tags");
        context.getLog().debug(tags.stream().map(Tag::getName).collect(Collectors.toList()).toString());

        List<BrowserVersionScanner.TaggedBrowserVersion> results = new ArrayList<>(tags.size());
        List<TagDetails> unknownTags = new ArrayList<>();
        for (TagDetails tag : tags)
        {
            String existingBrowserVersion = generateVersionsContext.getVersions().getProperty(tag.getName());
            if (!Strings.isNullOrEmpty(existingBrowserVersion))
                context.getLog().debug("Tag " + tag.getName() + " browser version already known: " + existingBrowserVersion);
            else
            {
                BrowserVersionScanner.TaggedBrowserVersion result = mapper.processTag(tag, reader);
                if (result == null)
                {
                    unknownTags.add(tag);
                    context.getLog().info("Browser version not found for tag " + tag.getName());
                }
                else
                {
                    results.add(result);
                    context.getLog().info(tag.getName() + ": " + result);

                    generateVersionsContext.getVersions().setProperty(tag.getName(), result.getBrowserVersion());
                    if (tag.getDigest() != null)
                        generateVersionsContext.getImages().setProperty(tag.getName(), tag.getDigest());

                    generateVersionsContext.saveVersions();
                    generateVersionsContext.saveImages();
                }
            }
        }

        context.getLog().info("Version reading completed.");
        if (!unknownTags.isEmpty())
            context.getLog().info("Tags with unknown browser version: " + unknownTags);
        if (!results.isEmpty())
        {
            context.getLog().info("Tags with browser versions:");
            for (BrowserVersionScanner.TaggedBrowserVersion result : results)
            {
                context.getLog().info(result.toString());
            }
        }
    }
}
