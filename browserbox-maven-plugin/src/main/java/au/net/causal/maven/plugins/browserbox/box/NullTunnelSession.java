package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

public class NullTunnelSession implements TunnelSession
{
    @Override
    public void close() throws BrowserBoxException 
    {
    }
}
