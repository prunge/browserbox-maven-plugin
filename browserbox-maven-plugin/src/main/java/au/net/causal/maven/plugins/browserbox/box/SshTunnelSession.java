package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.util.List;
import java.util.Objects;

public class SshTunnelSession implements TunnelSession 
{
    private final Session sshSession;
    
    public SshTunnelSession(Session sshSession)
    {
        Objects.requireNonNull(sshSession, "sshSession == null");
        this.sshSession = sshSession;
    }

    public static SshTunnelSession create(List<? extends BoxConfiguration.TunnelPort> tunnelPorts,
                                          String browserBoxSshHost, int browserBoxSshPort,
                                          String sshUser, String sshPassword)
    throws BrowserBoxException
    {
        return create(tunnelPorts, ImmutableList.of(), browserBoxSshHost, browserBoxSshPort, sshUser, sshPassword);
    }

    public static SshTunnelSession create(List<? extends BoxConfiguration.TunnelPort> tunnelPorts,
                                          List<? extends BoxConfiguration.TunnelPort> reverseTunnelPorts,
                                          String browserBoxSshHost, int browserBoxSshPort,
                                          String sshUser, String sshPassword)
    throws BrowserBoxException
    {
        JSch jsch = new JSch();

        try
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            for (BoxConfiguration.TunnelPort tunnelPort : tunnelPorts)
            {
                sshSession.setPortForwardingR(tunnelPort.getLocalPort(), "localhost", tunnelPort.getBrowserPort());
            }
            for (BoxConfiguration.TunnelPort reverseTunnelPort : reverseTunnelPorts)
            {
                sshSession.setPortForwardingL(reverseTunnelPort.getLocalPort(), "localhost", reverseTunnelPort.getBrowserPort());
            }

            return new SshTunnelSession(sshSession);
        }
        catch (JSchException e)
        {
            throw new BrowserBoxException("Error creating SSH tunnel: " + e.getMessage(), e);
        }
    }

    @Override
    public void close() throws BrowserBoxException 
    {
        sshSession.disconnect();
    }
}
