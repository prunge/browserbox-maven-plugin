package au.net.causal.maven.plugins.browserbox.box;

import java.util.ArrayList;
import java.util.List;

/**
 * Specifies certificates that will be registered as trusted in browsers that are created.
 */
public class SslTrustConfiguration
{
    private final List<TrustStoreConfiguration> trustStores = new ArrayList<>();
    private final List<TrustCertificateFileConfiguration> certificateFiles = new ArrayList<>();

    public List<TrustStoreConfiguration> getTrustStores()
    {
        return trustStores;
    }

    /**
     * Configures trust stores that hold certificates in a Java key-store.
     */
    public void setTrustStores(List<TrustStoreConfiguration> trustStores)
    {
        this.trustStores.clear();
        this.trustStores.addAll(trustStores);
    }

    public List<TrustCertificateFileConfiguration> getCertificateFiles()
    {
        return certificateFiles;
    }

    /**
     * Configures individual certificate files.
     */
    public void setCertificateFiles(List<TrustCertificateFileConfiguration> certificateFiles)
    {
        this.certificateFiles.clear();
        this.certificateFiles.addAll(certificateFiles);
    }
}
