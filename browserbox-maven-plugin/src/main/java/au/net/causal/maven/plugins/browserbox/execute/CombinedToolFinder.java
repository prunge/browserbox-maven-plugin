package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class CombinedToolFinder implements ToolFinder
{
    private final List<? extends ToolFinder> finders;
    
    public CombinedToolFinder(Collection<? extends ToolFinder> finders)
    {
        this.finders = ImmutableList.copyOf(finders);
    }
    
    @Override
    public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box, 
                                         BoxConfiguration boxConfiguration,
                                         ProjectConfiguration projectConfiguration, 
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        for (ToolFinder finder : finders)
        {
            Optional<ToolRunner> result = finder.findTool(connectionInfo, box, boxConfiguration, projectConfiguration, context);
            if (result.isPresent())
                return result;
        }
        
        return Optional.empty();
    }
}
