package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.box.BrowserVersionsArtifactReader;

import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

public class FirefoxNonLinux64BitMavenBlacklist extends FirefoxNonLinux64BitBlacklist 
{
    private final BoxContext context;
    private final BrowserVersionsArtifactReader browserVersionsArtifactReader = new BrowserVersionsArtifactReader();
    
    public FirefoxNonLinux64BitMavenBlacklist(BoxContext context)
    {
        Objects.requireNonNull(context, "context == null");
        this.context = context;
    }
    
    @Override
    protected Properties getKnownVersions() throws BrowserBoxException 
    {
        Path versionsJar = browserVersionsArtifactReader.readBrowserVersionArtifact(context);
        return BlacklistResource.fromJarFile(versionsJar, "firefox");
    }
}
