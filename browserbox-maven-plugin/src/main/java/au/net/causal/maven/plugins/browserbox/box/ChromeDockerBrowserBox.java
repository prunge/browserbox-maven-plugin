package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.RemoteSslNssDatabaseGenerator.DatabaseType;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;

import java.security.cert.X509Certificate;
import java.util.Collection;

public class ChromeDockerBrowserBox extends SeleniumDockerBrowserBox 
{
    private final RemoteSslNssDatabaseGenerator sslNssDatabaseGenerator = new RemoteSslNssDatabaseGenerator(this);

    public ChromeDockerBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, 
                                  BoxContext context, ImageReference seleniumBaseImageName) 
    {
        super(boxConfiguration, projectConfiguration, context, seleniumBaseImageName, new ChromeOptions());
    }

    @Override
    public String getSeleniumBrowserType()
    {
        return BrowserType.CHROME;
    }

    private String getRemoteCertificateDbDirectory()
    {
        return "/home/seluser/.pki/nssdb";
    }

    @Override
    public void installCertificates(Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        sslNssDatabaseGenerator.generate(getRemoteCertificateDbDirectory(), DatabaseType.SQL, certificates);
    }
}
