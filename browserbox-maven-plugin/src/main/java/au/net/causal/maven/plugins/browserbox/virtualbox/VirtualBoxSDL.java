package au.net.causal.maven.plugins.browserbox.virtualbox;

import java.time.Duration;
import java.util.Objects;

/**
 * Interface to <code>VBoxSDL</code> tool.
 */
public interface VirtualBoxSDL
{
    public void startVm(StartVmOptions options)
    throws VirtualBoxException;
    
    public static abstract class BaseOptions
    {
        private Duration timeout = Duration.ofHours(8L);

        public Duration getTimeout()
        {
            return timeout;
        }

        public void setTimeout(Duration timeout)
        {
            this.timeout = timeout;
        }
    }

    public static class StartVmOptions extends BaseOptions
    {
        private final String vmName;
        private boolean separate;

        public StartVmOptions(String vmName)
        {
            Objects.requireNonNull(vmName, "vmName == null");
            this.vmName = vmName;
        }

        public String getVmName()
        {
            return vmName;
        }

        public boolean isSeparate()
        {
            return separate;
        }

        public void setSeparate(boolean separate)
        {
            this.separate = separate;
        }
    }
}