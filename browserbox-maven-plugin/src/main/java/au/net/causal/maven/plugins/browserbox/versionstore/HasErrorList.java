package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

/**
 * Superclass of a result list that might also have had some errors while producing the list.
 * This allows partial result lists to be consumed if needed.
 */
public abstract class HasErrorList
{
    private final List<BrowserBoxException> errors;
    
    protected HasErrorList(Collection<? extends BrowserBoxException> errors)
    {
        this.errors = ImmutableList.copyOf(errors);
    }

    public List<BrowserBoxException> getErrors()
    {
        return errors;
    }
}
