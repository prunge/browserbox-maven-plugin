package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Resolved and reads the browser versions artifact.  This is a separate artifact recording all the versions of various
 * browser types, and is typically updated more often than the plugin itself.
 */
public class BrowserVersionsArtifactReader 
{
    public Path readBrowserVersionArtifact(BoxContext context)
    throws BrowserBoxException
    {
        //Find latest version
        Artifact artifact = new DefaultArtifact("au.net.causal.maven.plugins", "browserbox-browser-versions", "jar", "[0.0,)");
        VersionRangeRequest versionRequest = new VersionRangeRequest(artifact, context.getRemoteRepositories(), null);
        try
        {
            VersionRangeResult result = context.getRepositorySystem().resolveVersionRange(context.getRepositorySystemSession(), versionRequest);
            if (result.getHighestVersion() == null)
            {
                context.getLog().warn("No browser versions artifact found.  For browser version resolution, the " + artifact.toString() + " artifact should be available.  This may happen if Maven is offline.");
                return null;
            }
            artifact = artifact.setVersion(result.getHighestVersion().toString());
        }
        catch (VersionRangeResolutionException e)
        {
            throw new BrowserBoxException("Error resolving latest version of browser versions artifact: " + e, e);
        }

        //Download this artifact
        ArtifactRequest request = new ArtifactRequest(artifact, context.getRemoteRepositories(), null);
        try
        {
            ArtifactResult result = context.getRepositorySystem().resolveArtifact(context.getRepositorySystemSession(), request);
            return result.getArtifact().getFile().toPath();
        }
        catch (ArtifactResolutionException e)
        {
            context.getLog().info("Failed to resolve browser versions artifact.", e);
            return null;
        }
    }
    
    public Properties readPropertiesFile(String resourceName, BoxContext context)
    throws BrowserBoxException
    {
        Path versionJar = readBrowserVersionArtifact(context);
        if (versionJar == null)
            return new Properties();

        try (URLClassLoader versionLoader = URLClassLoader.newInstance(new URL[] {versionJar.toUri().toURL()}, null))
        {
            URL downloadsResource = versionLoader.findResource(resourceName);
            if (downloadsResource == null)
            {
                //Don't throw an exception - it might be legit for a bootstrap build
                context.getLog().warn("Missing " + resourceName + " in " + versionJar);
                return new Properties();
            }

            Properties downloads = new Properties();
            try (InputStream versionsIs = downloadsResource.openStream())
            {
                downloads.load(versionsIs);
            }

            return downloads;
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("I/O error reading version JAR " + versionJar + ": " + e.getMessage(), e);
        }
    }
}
