package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.util.Optional;

public class MacScreenSharingVncFinder extends CommandLineToolFinder
{
    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box,
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration,
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        String vncUri;
        if (connectionInfo.getUsername() != null && connectionInfo.getPassword() != null)
            vncUri = "vnc://" + connectionInfo.getUsername() + ":" + connectionInfo.getPassword() + "@";
        else if (connectionInfo.getPassword() != null)
            vncUri = "vnc://" + connectionInfo.getPassword() + ":" + connectionInfo.getPassword() + "@";
        else
            vncUri = "vnc://";
        
        vncUri = vncUri + connectionInfo.getUri().getHost() + ":" + connectionInfo.getUri().getPort();

        Commandline commandline = new Commandline("open");
        commandline.addArguments(args("-W", vncUri));
        
        return Optional.of(commandline);
    }
}
