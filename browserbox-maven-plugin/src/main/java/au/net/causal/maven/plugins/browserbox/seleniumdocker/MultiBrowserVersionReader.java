package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MultiBrowserVersionReader implements TagBrowserVersionReader 
{
    private final List<? extends TagBrowserVersionReader> readers;
            
    public MultiBrowserVersionReader(Collection<? extends TagBrowserVersionReader> readers)
    {
        this.readers = ImmutableList.copyOf(readers);
    }
    
    public MultiBrowserVersionReader(TagBrowserVersionReader... readers)
    {
        this(Arrays.asList(readers));
    }
    
    @Override
    public String readBrowserVersion(Tag tag) 
    throws BrowserBoxException 
    {
        for (TagBrowserVersionReader reader : readers)
        {
            String result = reader.readBrowserVersion(tag);
            if (result != null)
                return result;
        }
        
        return null;
    }
}
