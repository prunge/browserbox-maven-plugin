package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.SslTrustConfiguration;
import au.net.causal.maven.plugins.browserbox.box.TrustCertificateFileConfiguration;
import au.net.causal.maven.plugins.browserbox.box.TrustStoreConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Reads SSL certificates from a trust store configuration.
 */
public class SslCertificateReader {
    private final CertificateFactory x509CertificateFactory;

    public SslCertificateReader()
    throws BrowserBoxException
    {
        try
        {
            this.x509CertificateFactory = CertificateFactory.getInstance("x.509");
        }
        catch (CertificateException e)
        {
            throw new BrowserBoxException("Error getting certificate factory: " + e.getMessage(), e);
        }
    }

    /**
     * Rread SSL certificates from a trust store configuration.
     *
     * @param configuration trust store configuration.
     *
     * @return collection of all certificates from the configuration.
     *
     * @throws BrowserBoxException if an error occurs reading the certificates.
     */
    public Collection<? extends X509Certificate> read(SslTrustConfiguration configuration)
    throws BrowserBoxException
    {
        List<X509Certificate> certificates = new ArrayList<>();

        //Handle trust stores
        for (TrustStoreConfiguration trustStoreConfig : configuration.getTrustStores())
        {
            readFromTrustStoreConfiguration(trustStoreConfig, certificates);
        }

        //Handle individual certificate files
        for (TrustCertificateFileConfiguration trustCertificateConfig : configuration.getCertificateFiles())
        {
            readFromTrustCertificateConfiguration(trustCertificateConfig, certificates);
        }

        return certificates;
    }

    private void readFromTrustCertificateConfiguration(TrustCertificateFileConfiguration trustCertificateConfig,
                                                       Collection<? super X509Certificate> certificates)
    throws BrowserBoxException
    {
        Path certificateFile = trustCertificateConfig.getFile().toPath();
        try (InputStream certificateIs = Files.newInputStream(certificateFile))
        {
            Collection<? extends Certificate> fileCertificates = x509CertificateFactory.generateCertificates(certificateIs);
            int count = 0;

            for (Certificate fileCertificate : fileCertificates)
            {
                if (fileCertificate instanceof X509Certificate)
                {
                    certificates.add((X509Certificate) fileCertificate);
                    count++;
                }
            }

            //User specified a file but it didn't have any X509 certificates, must be a problem
            if (count == 0)
                throw new BrowserBoxException("No X509 certificates found in file " + certificateFile);
        }
        catch (IOException | CertificateException e)
        {
            throw new BrowserBoxException("Error reading certificate file: " + certificateFile.toString() + ": " + e.getMessage(), e);
        }
    }

    private void readFromTrustStoreConfiguration(TrustStoreConfiguration trustStoreConfig,
                                                 Collection<? super X509Certificate> certificates)
    throws BrowserBoxException
    {
        try
        {
            KeyStore keyStore = KeyStore.getInstance(trustStoreConfig.getType());
            try (InputStream is = Files.newInputStream(trustStoreConfig.getFile().toPath()))
            {
                char[] passwordChars;
                if (trustStoreConfig.getPassword() == null)
                    passwordChars = null;
                else
                    passwordChars = trustStoreConfig.getPassword().toCharArray();

                keyStore.load(is, passwordChars);
            }

            List<String> aliases = trustStoreConfig.getAliases();
            boolean allAliases = aliases.isEmpty();
            if (allAliases)
                aliases = Collections.list(keyStore.aliases());

            int loadCount = 0;
            for (String alias : aliases)
            {
                Certificate certificate = keyStore.getCertificate(alias);

                if (certificate instanceof X509Certificate)
                {
                    certificates.add((X509Certificate)certificate);
                    loadCount++;
                }

                //Throw error if not found or wrong type only if the user explicitly wanted this alias
                //instanceof check above covers null case as well
                else if (!allAliases)
                    throw new BrowserBoxException("Could not find X509 certificate with alias " + alias + " in keystore " + trustStoreConfig.getFile());
            }

            //This has got to be an error so fail the build in this case
            //(maybe store is empty or full of non-certificate entries)
            if (loadCount < 1)
                throw new BrowserBoxException("No certificates were found in keystore " + trustStoreConfig.getFile());
        }
        catch (KeyStoreException | IOException | CertificateException | NoSuchAlgorithmException e)
        {
            throw new BrowserBoxException("Error loading trust store " + trustStoreConfig.getFile() + ": " + e.getMessage(), e);
        }
    }
}
