package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Reference used to identify and select Docker images.  i.e. <i>&lt;repository&gt;:&lt;tag&gt;</i>
 */
public class ImageReference 
{
    private final String repository;
    private final Tag tag;

    /**
     * Creates an image reference.
     *
     * @param repository the repository name.  e.g. selenium/standalone-firefox-debug
     * @param tag the image's tag.  e.g. 3.14.0-europium
     *
     * @throws NullPointerException if any parameter is null.
     */
    public ImageReference(String repository, Tag tag) 
    {
        Objects.requireNonNull(repository, "repository == null");
        Objects.requireNonNull(tag, "tag == null");
        
        this.repository = repository;
        this.tag = tag;
    }

    /**
     * Parses an image reference in the form of &lt;repository&gt;:&lt;tag&gt;.
     *
     * @param name the image reference string to parse.
     *
     * @return the parsed image reference.
     *
     * @throws NullPointerException if <code>name</code> is null.
     */
    public static ImageReference parse(String name)
    {
        String[] tokens = name.split(Pattern.quote(":"), 2);
        if (tokens.length < 2)
            return new ImageReference(tokens[0], new Tag(""));
        else
            return new ImageReference(tokens[0], new Tag(tokens[1]));
    }

    /**
     * @return the repository part of the image reference.
     */
    public String getRepository() 
    {
        return repository;
    }

    /**
     * @return the tag of the image reference.
     */
    public Tag getTag() 
    {
        return tag;
    }

    /**
     * @return the full name of the image - <i>&lt;repository&gt;:&lt;tag&gt;</i>
     */
    public String getName()
    {
        if (getTag().getName().isEmpty())
            return getRepository();
        else
            return getRepository() + ":" + getTag().getName();
    }

    /**
     * @return string representtation of the image reference.  This just returns the {@linkplain #getName() name}.
     */
    @Override
    public String toString() 
    {
        return getName();
    }

    @Override
    public boolean equals(Object o) 
    {
        if (this == o) return true;
        if (!(o instanceof ImageReference)) return false;
        ImageReference that = (ImageReference) o;
        return Objects.equals(repository, that.repository) &&
                Objects.equals(tag, that.tag);
    }

    @Override
    public int hashCode() 
    {
        return Objects.hash(repository, tag);
    }
}
