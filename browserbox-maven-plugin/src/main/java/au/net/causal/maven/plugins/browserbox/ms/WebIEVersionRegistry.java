package au.net.causal.maven.plugins.browserbox.ms;

import javax.ws.rs.client.ClientBuilder;
import java.util.regex.Pattern;

public class WebIEVersionRegistry extends WebVersionRegistry
{
    private static final Pattern ITEM_NAME_PATTERN = Pattern.compile("^IE(\\d{1,2})\\s+.*", Pattern.CASE_INSENSITIVE);

    public WebIEVersionRegistry(ClientBuilder clientBuilder)
    {
        super(clientBuilder, ITEM_NAME_PATTERN);
    }

    public WebIEVersionRegistry()
    {
        super(ITEM_NAME_PATTERN);
    }
}    

