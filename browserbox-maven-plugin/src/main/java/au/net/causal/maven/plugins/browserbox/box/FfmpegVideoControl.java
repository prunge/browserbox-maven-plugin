package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

//Do this through SSH or something:
//ffmpeg -video_size 1024x768 -framerate 25 -f x11grab -i :99.0 output.mp4
public abstract class FfmpegVideoControl implements VideoControl 
{
    private final BrowserBox box;
    private final BoxConfiguration boxConfiguration;

    //TODO ideally shut this down properly and clean up when finished
    private final ExecutorService sshExecutor = Executors.newCachedThreadPool();

    public FfmpegVideoControl(BrowserBox box, BoxConfiguration boxConfiguration) 
    {
        this.box = box;
        this.boxConfiguration = boxConfiguration;
    }
    
    protected BrowserBox getBox()
    {
        return box;
    }
    
    protected BoxConfiguration getBoxConfiguration()
    {
        return boxConfiguration;
    }
    
    protected abstract String ffmpegArguments();
    
    protected String ffmpegCommand() 
    {
        return "ffmpeg " + ffmpegArguments();
    }
    
    protected void stopRecording(OutputStream ffmpegInputWriter, Session sshSession)
    throws IOException
    {
        ffmpegInputWriter.write('q');
        ffmpegInputWriter.write('\n');
        ffmpegInputWriter.flush();
    }

    @Override
    public Recording startRecording() throws IOException, BrowserBoxException
    {
        ConnectionInfo info = box.getConnectionInfo(StandardConnectionType.SSH);
        String sshUser = info.getUsername();
        String sshPassword = info.getPassword();
        String browserBoxSshHost = info.getUri().getHost();
        int browserBoxSshPort = info.getUri().getPort();
        
        JSch jsch = new JSch();

        try 
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();
            
            String command = ffmpegCommand();
            ChannelExec exec = (ChannelExec)sshSession.openChannel("exec");
            exec.setCommand(command);
            exec.connect();

            Callable<Integer> runner = () ->
            {
                //TODO sysout/err to logs, maybe in another thread to avoid locks
                //IOUtil.copy(exec.getInputStream(), System.out);
                //IOUtil.copy(exec.getErrStream(), System.err);
                
                int result;
                do 
                {
                    Thread.sleep(200L);
                    result = exec.getExitStatus();
                }
                while (result < 0);

                exec.disconnect();
                
                return result;
            };
            Future<Integer> ffmpegResult = sshExecutor.submit(runner);
            
            return new FfmpegRecording(sshSession, exec.getOutputStream(), ffmpegResult);
        } 
        catch (JSchException e) 
        {
            throw new BrowserBoxException("Error creating SSH connection: " + e.getMessage(), e);
        }
    }

    /**
     * Throws an exception if ffmpeg did not return successfully.
     *
     * @param ffmpegExitCode the FFMPEG exit code.
     *
     * @throws BrowserBoxException if the exit code means FFMPEG execution failed.
     */
    protected void validateFfmpegExitCode(int ffmpegExitCode)
    throws BrowserBoxException
    {
        if (ffmpegExitCode != 0)
            throw new BrowserBoxException("Error during execution of ffmpeg, result=" + ffmpegExitCode);
    }

    protected abstract String videoFileName();

    private class FfmpegRecording implements Recording 
    {
        private final Session sshSession;
        private final OutputStream ffmpegInputWriter;
        private final Future<Integer> ffmpegResult;

        public FfmpegRecording(Session sshSession, OutputStream ffmpegInputWriter, Future<Integer> ffmpegResult) 
        {
            this.sshSession = sshSession;
            this.ffmpegInputWriter = ffmpegInputWriter;
            this.ffmpegResult = ffmpegResult;
        }

        @Override
        public void stopAndCancel() throws IOException, BrowserBoxException
        {
            try
            {
                stopRecording(ffmpegInputWriter, sshSession);

                //Wait for FFMPEG process to die
                try
                {
                    int ffmpegReturnCode = ffmpegResult.get(); //TODO might want timeout
                    System.err.println("ffmpeg return code: " + ffmpegReturnCode);
                    validateFfmpegExitCode(ffmpegReturnCode);
                }
                catch (InterruptedException | ExecutionException e)
                {
                    throw new BrowserBoxException("Error executing ffmpeg: " + e.getMessage(), e);
                }
            }
            finally
            {
                sshSession.disconnect();
            }
        }

        @Override
        public void stopAndSave(Path videoFile)
        throws IOException, BrowserBoxException 
        {
            try 
            {
                stopRecording(ffmpegInputWriter, sshSession);

                //Wait for FFMPEG process to die
                try 
                {
                    int ffmpegReturnCode = ffmpegResult.get(); //TODO might want timeout
                    System.err.println("ffmpeg return code: " + ffmpegReturnCode);
                    validateFfmpegExitCode(ffmpegReturnCode);
                } 
                catch (InterruptedException | ExecutionException e) 
                {
                    throw new BrowserBoxException("Error executing ffmpeg: " + e.getMessage(), e);
                }

                //Use Scp to save the file through SSH connection
                try (OutputStream videoWriter = Files.newOutputStream(videoFile))
                {
                    ChannelSftp sftpChannel = (ChannelSftp)sshSession.openChannel("sftp");
                    sftpChannel.connect();
                    sftpChannel.get(videoFileName(), videoWriter);
                    sftpChannel.rm(videoFileName());
                    sftpChannel.disconnect();
                }
                catch (SftpException e)
                {
                    throw new IOException(e);
                }
                catch (JSchException e)
                {
                    throw new BrowserBoxException("Error transferring video file: " + e.getMessage(), e);
                }
            }
            finally
            {
                sshSession.disconnect();
            }
        }
    }
}
