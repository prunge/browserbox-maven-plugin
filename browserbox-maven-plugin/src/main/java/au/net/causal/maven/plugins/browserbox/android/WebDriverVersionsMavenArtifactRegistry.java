package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenBrowserVersionsArtifactRegistry;
import com.google.common.collect.ImmutableMap;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;

public class WebDriverVersionsMavenArtifactRegistry extends MavenBrowserVersionsArtifactRegistry
{
    private final Platform platform;

    public WebDriverVersionsMavenArtifactRegistry(String browserType, BoxContext context, Platform platform)
    {
        super(browserType, context);
        Objects.requireNonNull(platform, "platform == null");
        this.platform = platform;
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        Properties downloadProperties = readWebDriverDownloadProperties();
        Properties compatibilityProperties = readCompatibilityProperties();

        List<WebDriverItem> results = new ArrayList<>();
        for (String version : downloadProperties.stringPropertyNames())
        {
            if (!query.getIgnoredVersions().contains(version))
            {
                String downloadUrlStr = downloadProperties.getProperty(version);
                try
                {
                    WebDriverItem item = new WebDriverItem(version, platform,
                                                           ImmutableMap.of(platform, createUrlForDownload(downloadUrlStr)));
                    String minVersionStr = compatibilityProperties.getProperty(version + ".minVersion");
                    String maxVersionStr = compatibilityProperties.getProperty(version + ".maxVersion");
                    /*
                    if (minVersionStr == null)
                        throw new BrowserBoxException("No minVersion specified for " + version + " in compatibility.properties.");
                    if (maxVersionStr == null)
                        throw new BrowserBoxException("No maxVersion specified for " + version + " in compatibility.properties");
                    */

                    if (minVersionStr != null && maxVersionStr != null)
                    {
                        try
                        {
                            int minVersion = Integer.parseInt(minVersionStr);
                            int maxVersion = Integer.parseInt(maxVersionStr);
                            item.setCompatibilityInfo(new CompatibilityInfo(minVersion, maxVersion));
                            results.add(item);
                        }
                        catch (NumberFormatException e)
                        {
                            throw new BrowserBoxException("Error parsing min/max versions in compatibility.properties: " + e, e);
                        }
                    }
                }
                catch (MalformedURLException e)
                {
                    //This is a critical error throw and don't log in the result
                    throw new BrowserBoxException("Error parsing URL in download properties: " + downloadUrlStr, e);
                }
            }
        }

        return new ItemList(results, Collections.emptyList());
    }

    protected Properties readWebDriverDownloadProperties()
    throws BrowserBoxException
    {
        return getBrowserVersionsArtifactReader().readPropertiesFile(getBrowserType() + "-driver-" + platform.name().toLowerCase(Locale.ENGLISH) + "-downloads.properties", getContext());
    }

    protected Properties readCompatibilityProperties()
    throws BrowserBoxException
    {
        return getBrowserVersionsArtifactReader().readPropertiesFile(getBrowserType() + "-compatibility.properties", getContext());
    }
}
