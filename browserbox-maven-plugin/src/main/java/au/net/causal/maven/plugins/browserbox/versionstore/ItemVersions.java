package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.Collection;

/**
 * Collection of versions for items with potential resolution or lookup errors.
 * Check if {@link #getErrors()} has any errors when using results of this type.
 */
public class ItemVersions extends HasErrorList
{
    private final Collection<String> versions;
    
    public ItemVersions(Collection<String> versions, Collection<? extends BrowserBoxException> errors)
    {
        super(errors);
        this.versions = ImmutableList.copyOf(versions);
    }
    
    public Collection<String> getVersions()
    {
        return versions;
    }
}
