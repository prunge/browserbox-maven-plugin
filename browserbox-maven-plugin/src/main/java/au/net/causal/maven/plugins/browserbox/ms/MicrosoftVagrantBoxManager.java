package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;

import java.util.Locale;

public class MicrosoftVagrantBoxManager 
{
    /**
     * Returns the name of the Vagrant box for the original VM (not modified by browserbox).
     * 
     * @param selector browser selector.
     *                
     * @return the name of the Vagrant box.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public String originalVagrantBoxName(BrowserSelector selector)
    throws BrowserBoxException
    {
        switch (selector.getType())
        {
            case EDGE:
                return "Microsoft/Edge-" + selector.getVersion();
            case IE:
                return "Microsoft/IE" + selector.getVersion();
            default:
                throw new Error("Unknown browser selector type: " + selector.getType());
        }
    }

    /**
     * Returns the name of the Vagrant box for the browser box VM (after being modified by browserbox).
     * 
     * @param selector browser selector.
     *                
     * @return the name of the Vagrant box.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public String browserBoxVagrantBoxName(BrowserSelector selector)
    throws BrowserBoxException
    {
        switch (selector.getType())
        {
            case EDGE:
                return "browserbox.causal.net.au/Edge-" + selector.getVersion();
            case IE:
                return "browserbox.causal.net.au/IE" + selector.getVersion();
            default:
                throw new Error("Unknown browser selector type: " + selector.getType());
        }
    }
    
    /**
     * Returns the artifact for the downloaded Vagrant box file for a browser type and version.
     * 
     * @param selector browser selector.
     *                
     * @return the artifact for the box file.  Not resolved in any repo, might not exist.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public Artifact mavenArtifactForOriginalBox(BrowserSelector selector)
    throws BrowserBoxException
    {
        return new DefaultArtifact("com.microsoft.browser.vm", selector.getType().name().toLowerCase(Locale.ENGLISH), "zip", selector.getVersion());
    }

    /**
     * Returns an artifact suitable for performing an artifact search with an open version range for retrieving all 
     * Vagrant box file artifacts for a browser type.
     * 
     * @param browserType the browser type.
     *                    
     * @return an artifact with open range suitable for searching for all versions.
     * 
     * @throws BrowserBoxException if an error occurs.
     * 
     * @see #mavenArtifactForOriginalBox(BrowserSelector) 
     */
    public Artifact mavenVersionSearchArtifactForOriginalBox(BrowserSelector.Type browserType)
    throws BrowserBoxException
    {
        return new DefaultArtifact("com.microsoft.browser.vm", browserType.name().toLowerCase(Locale.ENGLISH), "zip", "[0.0,)");
    }

    /**
     * Returns if a Vagrant box image name is a known browser box image name.
     * 
     * @param name the name to check.
     *             
     * @return true if the name is recognized, false if not.
     */
    public boolean isKnownVagrantBoxImageName(String name)
    {
        return name.startsWith("browserbox.causal.net.au/");
    }
}
