package au.net.causal.maven.plugins.browserbox.virtualbox;

public class VirtualBoxException extends Exception
{
    private final int exitCode;
    private final String out;
    private final String err;
    
    public VirtualBoxException()
    {
        this.exitCode = -1;
        this.out = null;
        this.err = null;
    }

    public VirtualBoxException(String message)
    {
        this(-1, null, null, message);
    }

    public VirtualBoxException(int exitCode, String out, String err, String message)
    {
        super(message);
        this.out = out;
        this.err = err;
        this.exitCode = exitCode;
    }

    public VirtualBoxException(String message, Throwable cause)
    {
        this(-1, null, null, message, cause);
    }

    public VirtualBoxException(int exitCode, String out, String err, String message, Throwable cause)
    {
        super(message, cause);
        this.exitCode = exitCode;
        this.out = out;
        this.err = err;
        
    }

    public VirtualBoxException(Throwable cause)
    {
        this(-1, null, null, cause);
    }

    public VirtualBoxException(int exitCode, String out, String err, Throwable cause)
    {
        super(cause);
        this.exitCode = exitCode;
        this.out = out;
        this.err = err;
    }

    public int getExitCode() 
    {
        return exitCode;
    }

    public String getOut() 
    {
        return out;
    }

    public String getErr() 
    {
        return err;
    }
}
