package au.net.causal.maven.plugins.browserbox.box;

import java.net.URI;

public class ConnectionInfo 
{
    private final URI uri;
    private final String username;
    private final String password;

    public ConnectionInfo(URI uri, String username, String password) 
    {
        this.uri = uri;
        this.username = username;
        this.password = password;
    }
    
    public ConnectionInfo(URI uri)
    {
        this(uri, null, null);
    }

    public URI getUri() 
    {
        return uri;
    }

    public String getUsername() 
    {
        return username;
    }

    public String getPassword() 
    {
        return password;
    }

    @Override
    public String toString() 
    {
        return getUri().toString();
    }
}
