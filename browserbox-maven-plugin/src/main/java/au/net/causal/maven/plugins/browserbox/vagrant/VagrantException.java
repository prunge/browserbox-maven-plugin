package au.net.causal.maven.plugins.browserbox.vagrant;

public class VagrantException extends Exception
{
    private final int exitCode;
    
    public VagrantException()
    {
        this.exitCode = -1;
    }

    public VagrantException(int exitCode, String message)
    {
        super(message);
        this.exitCode = exitCode;
    }

    public VagrantException(String message)
    {
        this(-1, message);
    }

    public VagrantException(int exitCode, String message, Throwable cause)
    {
        super(message, cause);
        this.exitCode = exitCode;
    }

    public VagrantException(String message, Throwable cause)
    {
        this(-1, message, cause);
    }

    public VagrantException(int exitCode, Throwable cause)
    {
        super(cause);
        this.exitCode = exitCode;
    }

    public VagrantException(Throwable cause)
    {
        this(-1, cause);
    }

    public int getExitCode() 
    {
        return exitCode;
    }
}
