package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.android.Platform;
import au.net.causal.maven.plugins.browserbox.box.BrowserBoxFactory;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Mojo(name="read-browser-versions", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresProject = false)
public class ReadBrowserVersionsMojo extends AbstractBrowserBoxMojo 
{
    /**
     * Default directory to save the versions files to.
     */
    @Parameter(property = "browserbox.versions.directory", defaultValue = "${project.build.directory}/browserversions", required = true)
    private File versionsDirectory;
    
    /**
     * Name of the versions file that maps Docker tags to browser versions.
     */
    @Parameter(property = "browserbox.versions.fileName", defaultValue = "${browser.type}-versions.properties", required = true)
    private String versionsFileName;

    /**
     * Name of the versions file that maps Docker tags to image IDs.
     */
    @Parameter(property = "browserbox.images.fileName", defaultValue = "${browser.type}-images.properties", required = true)
    private String imagesFileName;

    /**
     * Name of the known versions file that records the version blacklist.
     */
    @Parameter(property = "browserbox.knownversions.fileName", defaultValue = "${browser.type}-known-versions.properties", required = true)
    private String knownVersionsFileName;

    /**
     * Name of the downloads file that records IE/Edge browser download URLs.
     */
    @Parameter(property = "browserbox.downloads.fileName", defaultValue = "${browser.type}-downloads.properties", required = true)
    private String downloadsFileName;

    /**
     * Name of the driver versions file that records Edge driver versions.
     */
    @Parameter(property = "browserbox.driverversions.fileName", defaultValue = "${browser.type}-driver-downloads.properties", required = true)
    private String driverVersionsFileName;
    
    @Parameter(property = "browserbox.driverversions.osspecific.fileName", defaultValue = "${browser.type}-driver-${browser.os}-downloads.properties", required=true)
    private String driverOsSpecificVersionsFileName;

    @Parameter(property = "browserbox.drivercompatibility.fileName", defaultValue = "${browser.type}-compatibility.properties")
    private String driverCompatibilityFileName;

    /**
     * File matcher patterns, when specified, only displays summaries of particular files.
     *
     * @see java.nio.file.FileSystem#getPathMatcher(String)
     */
    @Parameter(property = "browserbox.versionsummary.include")
    private List<String> summaryIncludeFilter = new ArrayList<>();
    
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        ProjectConfiguration projectConfiguration = projectConfiguration();
        BoxContext context;
        try
        {
            context = boxContext(service);
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException(e.getMessage(),  e);
        }

        if (box == null || box.getBrowserType() == null)
            throw new MojoExecutionException("Browser type (browser.type property) must be specified.");
        
        //In case the browser type was specified through the box or plugin config instead of system property
        //Won't do anything if browser.type is already resolved by Maven
        versionsFileName = versionsFileName.replace("${browser.type}", box.getBrowserType());
        imagesFileName = imagesFileName.replace("${browser.type}", box.getBrowserType());
        knownVersionsFileName = knownVersionsFileName.replace("${browser.type}", box.getBrowserType());
        downloadsFileName = downloadsFileName.replace("${browser.type}", box.getBrowserType());
        driverVersionsFileName = driverVersionsFileName.replace("${browser.type}", box.getBrowserType());
        driverCompatibilityFileName = driverCompatibilityFileName.replace("${browser.type}", box.getBrowserType());
        
        String browserType = box.getBrowserType().toLowerCase(Locale.ENGLISH);

        Path basePath = versionsDirectory.toPath();
        Path versionsPath = basePath.resolve(versionsFileName);
        Path imagesPath = basePath.resolve(imagesFileName);
        Path knownVersionsPath = basePath.resolve(knownVersionsFileName);
        Path downloadsPath = basePath.resolve(downloadsFileName);
        Path driverVersionsPath = basePath.resolve(driverVersionsFileName);
        Path driverCompatibilityPath = basePath.resolve(driverCompatibilityFileName);

        Map<String, Path> platformDriverVersionsPaths = new LinkedHashMap<>();
        for (Platform platform : Platform.values()) 
        {
            String curDriverOsSpecificVersionsFileName = driverOsSpecificVersionsFileName.replace("${browser.type}", box.getBrowserType())
                                                                                         .replace("${browser.os}", platform.name().toLowerCase(Locale.ENGLISH));
            Path driverOsSpecificVersionsPath = basePath.resolve(curDriverOsSpecificVersionsFileName);
            platformDriverVersionsPaths.put(platform.name(), driverOsSpecificVersionsPath);
        }
        
        try 
        {
            try (GenerateVersionsContext generateVersionsContext =
                    new GenerateVersionsContext(versionsPath, imagesPath, knownVersionsPath, downloadsPath,
                                                driverVersionsPath, platformDriverVersionsPaths,
                                                driverCompatibilityPath))
            {
                BrowserBoxFactory boxFactory = browserBoxFactory(service);
                boxFactory.generateVersionsFiles(browserType, projectConfiguration, context, generateVersionsContext);

                PathMatcher matcher;
                if (summaryIncludeFilter.isEmpty())
                    matcher = file -> true;
                else
                    matcher = new OrPathMatcher(summaryIncludeFilter.stream().map(s -> s.replace("${browser.type}", box.getBrowserType()))
                                                                             .map(FileSystems.getDefault()::getPathMatcher)
                                                                             .collect(Collectors.toList()));

                GenerateVersionsContext.Summary summary = generateVersionsContext.getSummary().includingOnly(matcher);
                getLog().info("Summary: " + summary.toString());
            }
        }
        catch (BrowserBoxException | IOException e)
        {
            throw new MojoExecutionException("Error reading browser version: " + e.getMessage(), e);
        }
    }


    /**
     * Matches a file if any of the child matches match.
     */
    private static class OrPathMatcher implements PathMatcher
    {
        private final List<? extends PathMatcher> matchers;

        public OrPathMatcher(Collection<? extends PathMatcher> matchers)
        {
            this.matchers = ImmutableList.copyOf(matchers);
        }

        @Override
        public boolean matches(Path path)
        {
            for (PathMatcher matcher : matchers)
            {
                if (matcher.matches(path))
                    return true;
            }

            return false;
        }

        @Override
        public String toString()
        {
            return("OR" + matchers);
        }
    }
}
