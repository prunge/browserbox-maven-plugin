package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.android.CompatibilityInfo;
import au.net.causal.maven.plugins.browserbox.android.WebDriverItem;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration.TunnelPort;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.android.SdkConstants;
import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.BrowserBoxAdbHelper;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.EmulatorConsole;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.FileListingService.FileEntry;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ScreenRecorderOptions;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.prefs.AndroidLocation;
import com.android.repository.api.Channel;
import com.android.repository.api.ConsoleProgressIndicator;
import com.android.repository.api.Downloader;
import com.android.repository.api.Installer;
import com.android.repository.api.InstallerFactory;
import com.android.repository.api.LocalPackage;
import com.android.repository.api.RemotePackage;
import com.android.repository.api.RepoManager;
import com.android.repository.api.SettingsController;
import com.android.repository.api.Uninstaller;
import com.android.repository.impl.meta.TypeDetails;
import com.android.repository.io.FileOp;
import com.android.repository.io.impl.FileOpImpl;
import com.android.sdklib.devices.Device;
import com.android.sdklib.devices.DeviceManager;
import com.android.sdklib.internal.avd.AvdInfo;
import com.android.sdklib.internal.avd.AvdManager;
import com.android.sdklib.internal.avd.GpuMode;
import com.android.sdklib.internal.avd.HardwareProperties;
import com.android.sdklib.repository.AndroidSdkHandler;
import com.android.sdklib.repository.installer.SdkInstallerUtil;
import com.android.sdklib.repository.legacy.LegacyDownloader;
import com.android.sdklib.repository.meta.DetailsTypes;
import com.android.sdklib.repository.targets.SystemImage;
import com.android.sdklib.repository.targets.SystemImageManager;
import com.android.utils.ILogger;
import com.android.utils.IReaderLogger;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FilenameUtils;
import org.codehaus.plexus.util.Os;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionRange;
import org.eclipse.aether.version.VersionScheme;
import org.eclipse.jgit.annotations.NonNull;

import javax.security.auth.x500.X500Principal;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.Collection;
import java.util.Comparator;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public abstract class AndroidBrowserBox implements BrowserBox
{
    /**
     * Box configuration property that, if set, specifically chooses the Android OS API level for the browser to
     * run in (essentially selecting the version of the OS to run).  If not specified, the latest release version
     * is used.
     */
    public static final String ANDROID_OS_API_LEVEL = "androidOsApiLevel";

    private final BoxConfiguration boxConfiguration;
    private final ProjectConfiguration projectConfiguration;
    private final BoxContext context;

    private final Path androidHome;
    private final AndroidSdkHandler androidSdk;
    private final AvdManager avdManager;
    private final AndroidDebugBridge adb;

    /**
     * Version registry for the browser APK.
     */
    private final VersionedItemStore apkVersionRegistry;

    /**
     * Version registry for the webdriver that runs on the local machine and communicates with the browser in the emulator.
     * e.g. ChromeDriver or GeckoDriver.
     */
    private final VersionedItemStore webDriverVersionRegistry;

    private final ExecutorService videoRecordExecutor;
    private final ListeningExecutorService emulatorExecutor;
    private final ListeningExecutorService webDriverExecutor;

    protected final VersionScheme versionScheme = new GenericVersionScheme();

    /**
     * Creates an Android browser box.
     *
     * @param boxConfiguration configuration for the box itself.
     * @param projectConfiguration project-level configuration.
     * @param context runtime context.
     * @param apkVersionRegistry version registry for downloading the browser APKs.
     * @param webDriverVersionRegistry version registry for downloading Webdriver artifacts.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    protected AndroidBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context,
                                VersionedItemStore apkVersionRegistry, VersionedItemStore webDriverVersionRegistry)
    throws BrowserBoxException
    {
        this.boxConfiguration = boxConfiguration;
        this.projectConfiguration = projectConfiguration;
        this.context = context;

        try
        {
            androidHome = context.getAndroidHome();
            if (androidHome == null)
                throw new BrowserBoxException("Android home is not set.  Ensure the property 'android.sdk.path' or the environment variable ANDROID_HOME is set to the location of the Android SDK.");
            if (!Files.isDirectory(androidHome))
                throw new BrowserBoxException("Directory of SDK " + androidHome.toAbsolutePath() + " does not exist.  Ensure the property 'android.sdk.path' or the environment variable ANDROID_HOME is set to the location of the Android SDK.");

            //Hack for Java 9+ classloader issues with Maven ClassRealm and loading package annotations
            //If these classes are loaded using Class.forName() which is what JAXB does then this goes through a
            //code path that involves modules and ClassRealm doesn't cope well with that, refusing to load the class
            //So we pre-load these classes ourselves, and once they are in the classloader they can be loaded from
            //Class.forName() call because the classes are already defined
            ClassLoader loader = AndroidBrowserBox.class.getClassLoader();
            try
            {
                loader.loadClass("com.android.sdklib.repository.generated.addon.v1.package-info");
                loader.loadClass("com.android.sdklib.repository.generated.repository.v1.package-info");
                loader.loadClass("com.android.sdklib.repository.generated.sysimg.v1.package-info");
                loader.loadClass("com.android.sdklib.repository.generated.common.v1.package-info");
                loader.loadClass("com.android.sdklib.repository.sources.generated.v1.package-info");
                loader.loadClass("com.android.sdklib.repository.sources.generated.v2.package-info");
                loader.loadClass("com.android.sdklib.repository.sources.generated.v3.package-info");
                loader.loadClass("com.android.repository.impl.sources.generated.v1.package-info");
                loader.loadClass("com.android.repository.impl.generated.generic.v1.package-info");
                loader.loadClass("com.android.repository.impl.generated.v1.package-info");
            }
            catch (ClassNotFoundException e)
            {
                context.getLog().warn("Failed to load the class - " + e);
            }

            androidSdk = AndroidSdkHandler.getInstance(androidHome.toFile());
            FileOp fop = new DeletionFixingFileOpImpl(); //For the AVD manager, workaround the deletion problem on Windows
            try
            {
                File baseAvdFolder = new File(AndroidLocation.getAvdFolder()); //The default for the other AvdManager create method
                avdManager = AvdManager.getInstance(androidSdk, baseAvdFolder, createLogger(), fop);
            }
            catch (AndroidLocation.AndroidLocationException e)
            {
                throw new BrowserBoxException("Error initializing Android: " + e.getMessage(), e);
            }

            //ADB bridge initialization
            AndroidDebugBridge.initIfNeeded(false);
            Path osLocation = androidHome.resolve("platform-tools").resolve("adb");
            adb = AndroidDebugBridge.createBridge(osLocation.toAbsolutePath().toString(), false);

            //Wait till bridge is connected
            while (!adb.isConnected() || !adb.hasInitialDeviceList())
            {
                Thread.sleep(100L);
            }
        }
        catch (InterruptedException e)
        {
            throw new BrowserBoxException("Interrupted waiting for ADB connection.", e);
        }

        videoRecordExecutor = Executors.newCachedThreadPool(
                new ThreadFactoryBuilder().setDaemon(true).setNameFormat(getName() + "-videorecord-%d").build());
        emulatorExecutor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool(
                new ThreadFactoryBuilder().setDaemon(true).setNameFormat(getName() + "-emulator-executor-%d").build()));
        webDriverExecutor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool(
                new ThreadFactoryBuilder().setDaemon(true).setNameFormat(getName() + "-webdriver-executor-%d").build()));

        this.apkVersionRegistry = apkVersionRegistry;
        this.webDriverVersionRegistry = webDriverVersionRegistry;
    }

    protected BoxConfiguration getBoxConfiguration()
    {
        return boxConfiguration;
    }

    protected ProjectConfiguration getProjectConfiguration()
    {
        return projectConfiguration;
    }

    protected BoxContext getContext()
    {
        return context;
    }

    protected abstract String avdName();

    @Override
    public String getName()
    {
        return avdName();
    }

    @Override
    public boolean exists()
    throws BrowserBoxException
    {
        return avdManager.getAvd(avdName(), true) != null;
    }

    /**
     * @return the emulator device for this box from ADB, or null if it doesn't exist.
     */
    protected IDevice findAvdDevice()
    {
        return Stream.of(adb.getDevices()).filter(IDevice::isEmulator)
                     .filter(device -> avdName().equals(device.getAvdName()))
                     .findFirst().orElse(null);
    }

    @Override
    public boolean isRunning() throws BrowserBoxException
    {
        IDevice avdDevice = findAvdDevice();
        if (avdDevice == null)
            return false;

        return avdDevice.isOnline();
    }

    @Override
    public void prepareImage() throws BrowserBoxException
    {
        prepareSystemImage(true);
        resolveApk();
        resolveWebDriver();
    }

    @Override
    public boolean hasImage() throws BrowserBoxException
    {
        SystemImage systemImage = prepareSystemImage(false);
        return (systemImage != null && isApkDownloaded());
    }

    /**
     * Returns the user-specified Android OS API level if one was selected.  This allows the user to select
     * a specific versino of the OS as opposed to the plugin just selecting the latest stable release.
     *
     * @return the Android OS API level, or null if the user did not specifically select one.
     */
    protected Integer getSelectedAndroidOsApiLevel()
    {
        String apiLevelString = boxConfiguration.getConfiguration().get(ANDROID_OS_API_LEVEL);
        if (apiLevelString != null)
            return Integer.parseInt(apiLevelString);

        return null;
    }

    private static final String ANDROID_SDK_PREVIEW_LICENSE_ID = "android-sdk-preview-license";

    /**
     * Returns whether a system image is suitable for running the specified browser version.
     *
     * @param systemImage the system image to check.
     *
     * @return true if the system image can run the browser in an emulator, false if not.
     */
    protected boolean checkSuitableSystemImage(SystemImage systemImage)
    {
        if (!SdkConstants.ABI_INTEL_ATOM.equals(systemImage.getAbiType()))
            return false;
        if (!SystemImage.GOOGLE_APIS_TAG.equals(systemImage.getTag()))
            return false;

        //If the user specifically selected an API level, force only that one to be chosen
        Integer selectedApiLevel = getSelectedAndroidOsApiLevel();
        if (selectedApiLevel != null)
            return systemImage.getAndroidVersion().getApiLevel() == selectedApiLevel;

        //Not too early or else browser won't work
        //TODO use the browser version to pick this limit better
        if (systemImage.getAndroidVersion().getApiLevel() < 23)
            return false;

        //Don't select preview versions by default
        //The isPreview() check doesn't always return false for preview images but the license sometimes indicates it
        if (systemImage.getAndroidVersion().isPreview())
            return false;
        if (systemImage.getPackage().getLicense() != null && ANDROID_SDK_PREVIEW_LICENSE_ID.equals(systemImage.getPackage().getLicense().getId()))
            return false;

        return true;
    }

    protected boolean checkSuitableRemotePackage(RemotePackage remotePackage, String browserVersion)
    {
        TypeDetails details = remotePackage.getTypeDetails();
        if (!(details instanceof DetailsTypes.SysImgDetailsType))
            return false;

        DetailsTypes.SysImgDetailsType imageDetails = (DetailsTypes.SysImgDetailsType)details;

        if (!SdkConstants.ABI_INTEL_ATOM.equals(imageDetails.getAbi()))
            return false;
        if (!SystemImage.GOOGLE_APIS_TAG.equals(imageDetails.getTag()))
            return false;

        //If the user specifically selected an API level, force only that one to be chosen
        //If a version was specifically chosen then preview versions are OK
        Integer selectedApiLevel = getSelectedAndroidOsApiLevel();
        if (selectedApiLevel != null)
            return imageDetails.getAndroidVersion().getApiLevel() == selectedApiLevel;

        //Don't select preview versions by default
        //License type sometimes gives a hint as to whether a remote package is a preview version
        if (remotePackage.getLicense() != null && ANDROID_SDK_PREVIEW_LICENSE_ID.equals(remotePackage.getLicense().getId()))
            return false;

        return true;
    }

    /**
     * Ensure a system image for the AVD is available and if it isn't download it if specified.
     *
     * @param downloadIfNeeded if true and no suitable image exists locally, download it from Android repository.  If false and no suitable image exists,
     *                         null will be returned.
     *
     * @return the system image, or null if no suitable image could be found.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private SystemImage prepareSystemImage(boolean downloadIfNeeded)
    throws BrowserBoxException
    {
        SystemImageManager systemImageManager = androidSdk.getSystemImageManager(new ConsoleProgressIndicator());

        String browserVersion = boxConfiguration.getBrowserVersion();

        Optional<SystemImage> systemImage = systemImageManager.getImages().stream()
                                                              .filter(this::checkSuitableSystemImage)
                                                              .max(Comparator.comparing(SystemImage::getAndroidVersion));
        if (systemImage.isPresent())
            return systemImage.get();

        //Does not exist, download the image if we asked for it
        if (!downloadIfNeeded)
            return null;

        SettingsController settings = new SettingsController()
        {
            @Override
            public boolean getForceHttp()
            {
                return false;
            }

            @Override
            public void setForceHttp(boolean force)
            {

            }

            @Override
            public Channel getChannel()
            {
                return Channel.DEFAULT;
            }
        };

        context.getLog().info("Local compatible system image not found so one will be downloaded");
        RepoManager sdkManager = androidSdk.getSdkManager(new ConsoleProgressIndicator());
        Downloader downloader = new LegacyDownloader(androidSdk.getFileOp(), settings);

        //This loads the catalog
        sdkManager.loadSynchronously(0L, new ConsoleProgressIndicator(), downloader, settings);

        //Find a matching remote package
        RemotePackage selectedRemoteSystemImage = sdkManager.getPackages().getRemotePackagesForPrefix("system-images").stream()
                                                            .filter(rp -> checkSuitableRemotePackage(rp, browserVersion))
                                                            .max(remotePackageVersionComparator())
                                                            .orElseThrow(() -> new BrowserBoxException("No suitable remote system images found for browser."));

        context.getLog().info("Installing remote system image: " + selectedRemoteSystemImage.getDisplayName() + " " + ((DetailsTypes.SysImgDetailsType)selectedRemoteSystemImage.getTypeDetails()).getAndroidVersion());

        InstallerFactory installerFactory = SdkInstallerUtil.findBestInstallerFactory(selectedRemoteSystemImage, androidSdk);
        Installer installer = installerFactory.createInstaller(selectedRemoteSystemImage, sdkManager, downloader, androidSdk.getFileOp());

        long startTime = System.nanoTime();
        boolean ok = installer.prepare(new ConsoleProgressIndicator());
        if (!ok)
            throw new BrowserBoxException("Failed to install remote system image.");
        ok = installer.complete(new ConsoleProgressIndicator());
        if (!ok)
            throw new BrowserBoxException("Failed to install remote system image.");

        long time = System.nanoTime() - startTime;
        context.getLog().info("Remote system image install complete in " + time / 1.0E9 + "s");

        //Reload the local packages (no downloader since we're only refreshing local)
        sdkManager.loadSynchronously(0L, new ConsoleProgressIndicator(), null, settings);

        //Should be downloaded now, recurse to pick up local package
        return prepareSystemImage(false);
    }

    private Comparator<RemotePackage> remotePackageVersionComparator()
    {
        return Comparator.comparing(rp -> ((DetailsTypes.SysImgDetailsType)rp.getTypeDetails()).getAndroidVersion());
    }

    private Device deviceSpecification()
    {
        DeviceManager deviceManager = DeviceManager.createInstance(androidSdk.getLocation(),
                                                                   androidSdk.getAndroidFolder(),
                                                                   createLogger(),
                                                                   androidSdk.getFileOp());
        return deviceManager.getDevice("Nexus 6P", "Google");
    }

    private AvdInfo createAvd()
    throws BrowserBoxException
    {
        SystemImage systemImage = prepareSystemImage(false);
        if (systemImage == null) //TODO Maven offline check
            systemImage = prepareSystemImage(true);

        if (systemImage == null)
            throw new BrowserBoxException("Could not find any valid system image to create AVD from");

        context.getLog().info("Creating AVD using system image " + systemImage.getPackage().getPath());

        Device nexusDevice = deviceSpecification();
        Map<String, String> hardwareConfig = DeviceManager.getHardwareProperties(nexusDevice);

        hardwareConfig.put("hw.gpu.enabled", HardwareProperties.BOOLEAN_YES);
        hardwareConfig.put("hw.gpu.mode", GpuMode.AUTO.getGpuSetting());
        hardwareConfig.put("hw.cpu.ncore", "4");
        hardwareConfig.put(HardwareProperties.HW_KEYBOARD, HardwareProperties.BOOLEAN_YES); //So real PC keyboard works

        Dimension screenSize = nexusDevice.getScreenSize(com.android.resources.ScreenOrientation.PORTRAIT);
        hardwareConfig.put("hw.lcd.height", String.valueOf(screenSize.height));
        hardwareConfig.put("hw.lcd.width", String.valueOf(screenSize.width));

        try
        {
            File avdFolder = AvdInfo.getDefaultAvdFolder(avdManager, avdName(), androidSdk.getFileOp(), true);
            return avdManager.createAvd(avdFolder, avdName(), systemImage, null, null, null, hardwareConfig, nexusDevice.getBootProps(), false, false, false, createLogger());
        }
        catch (AndroidLocation.AndroidLocationException e)
        {
            throw new BrowserBoxException("Android error: " + e.getMessage(), e);
        }
    }

    private boolean isApkDownloaded()
    throws BrowserBoxException
    {
        //Resolve the APK we need
        Item apkDownload = apkVersionRegistry.itemForVersion(boxConfiguration.getBrowserVersion());
        if (apkDownload == null)
            return false;

        URL apkFileUrl = apkDownload.getUrl();
        try
        {
            if (apkFileUrl == null || !"file".equals(apkFileUrl.toURI().getScheme()))
                return false;
        }
        catch (URISyntaxException e)
        {
            throw new BrowserBoxException("APK download URI error: " + e, e);
        }

        return true;
    }

    /**
     * Resolves the APK of the browser itself.
     *
     * @return a local file for the browser APK, ready to install.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private Path resolveApk()
    throws BrowserBoxException
    {
        //Resolve the APK we need
        Item apkDownload = apkVersionRegistry.itemForVersion(boxConfiguration.getBrowserVersion());
        if (apkDownload == null)
            throw new BrowserBoxException("Browser APK with version " + boxConfiguration.getBrowserVersion() + " not found");

        //Save to local file
        URL url = apkVersionRegistry.saveItemContents(apkDownload);

        //Resolve to path, assume saving always produces a local file
        try
        {
            return Paths.get(url.toURI());
        }
        catch (URISyntaxException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    private WebDriverItem findHighestWebDriverVersionMatchingBrowser()
    throws BrowserBoxException
    {
        String fullBrowserVersion = boxConfiguration.getBrowserVersion();
        try
        {
            Version browserVersion = versionScheme.parseVersion(fullBrowserVersion);
            Version highestWebDriverVersion = null;
            WebDriverItem highestWebDriver = null;

            Collection<? extends Item> items = webDriverVersionRegistry.readAllItems(new Query());
            for (Item item : items)
            {
                WebDriverItem cdItem = (WebDriverItem)item;
                Version currentWebDriverVersion = versionScheme.parseVersion(cdItem.getVersion());
                CompatibilityInfo compatibilityInfo = cdItem.getCompatibilityInfo();

                //Use closed next version range because 56.0.23 > 56 whether or not 56 is inclusive
                VersionRange compatibilityRange = versionScheme.parseVersionRange("[" + compatibilityInfo.getMinMajorVersion() +
                                                                                  "," + (compatibilityInfo.getMaxMajorVersion() + 1) + ")");
                if (compatibilityRange.containsVersion(browserVersion))
                {
                    if (highestWebDriverVersion == null || highestWebDriverVersion.compareTo(currentWebDriverVersion) < 0)
                    {
                        highestWebDriver = cdItem;
                        highestWebDriverVersion = currentWebDriverVersion;
                    }
                }
            }

            return highestWebDriver;
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    /**
     * Resolves webdriver ZIP artifact to a local file.
     *
     * @return webdriver ZIP artifact.
     *
     * @throws BrowserBoxException if an error occurs resolving.
     */
    private Path resolveWebDriver()
    throws BrowserBoxException
    {
        WebDriverItem webDriverDownload = findHighestWebDriverVersionMatchingBrowser();
        if (webDriverDownload == null)
            throw new BrowserBoxException("No compatible Webdriver exists for browser APK " + boxConfiguration.getBrowserVersion());

        context.getLog().info(this.getName() + " webdriver for Android: " + webDriverDownload + " " + webDriverDownload.getCompatibilityInfo());

        //Save to local file
        URL url = webDriverVersionRegistry.saveItemContents(webDriverDownload);

        //Resolve to path, assume saving always produces a local file
        try
        {
            return Paths.get(url.toURI());
        }
        catch (URISyntaxException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    private static Path extractSingleFileFromTarGzArchive(Path archiveFile, Path extractionDirectory)
    throws BrowserBoxException
    {
        Path extractedExecutableFile = null;

        try (InputStream is = Files.newInputStream(archiveFile); TarArchiveInputStream tis = new TarArchiveInputStream(new GZIPInputStream(is)))
        {
            ArchiveEntry curEntry;
            do
            {
                curEntry = tis.getNextEntry();
                if (curEntry != null && !curEntry.isDirectory())
                {
                    extractedExecutableFile = extractionDirectory.resolve(FilenameUtils.getName(curEntry.getName()));
                    Files.copy(tis, extractedExecutableFile, StandardCopyOption.REPLACE_EXISTING);
                }
            }
            while (curEntry != null && extractedExecutableFile == null);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error reading tar.gz file " + archiveFile + ": " + e.getMessage(), e);
        }

        return extractedExecutableFile;
    }

    private static Path extractSingleFileFromZipArchive(Path archiveFile, Path extractionDirectory)
    throws BrowserBoxException
    {
        Path extractedExecutableFile = null;

        try (InputStream is = Files.newInputStream(archiveFile); ZipInputStream zis = new ZipInputStream(is))
        {
            ZipEntry curEntry;
            do
            {
                curEntry = zis.getNextEntry();
                if (curEntry != null && !curEntry.isDirectory())
                {
                    extractedExecutableFile = extractionDirectory.resolve(FilenameUtils.getName(curEntry.getName()));
                    Files.copy(zis, extractedExecutableFile, StandardCopyOption.REPLACE_EXISTING);
                }
            }
            while (curEntry != null && extractedExecutableFile == null);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error reading ZIP file " + archiveFile + ": " + e.getMessage(), e);
        }

        return extractedExecutableFile;
    }

    /**
     * Resolves webdriver ZIP from potentially a remote repository,  then extracts the executable.
     *
     * @return the extracted webdriver executable file.
     *
     * @throws BrowserBoxException if an error occurs resolving or extracting.
     */
    private Path resolveAndExtractWebDriverExecutable()
    throws BrowserBoxException
    {
        Path webDriverArchiveFile = resolveWebDriver();
        context.getLog().info("Resolved Webdriver: " + webDriverArchiveFile);

        Path webDriverDirectory;
        try
        {
            webDriverDirectory = context.getTempDirectory().resolve("webdriver");
            Files.createDirectories(webDriverDirectory);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error creating webdriver extraction directory: " + e.getMessage(),  e);
        }

        //Look for a single file in the ZIP/TAR.GZ archive, assume that is the executable
        Path extractedExecutableFile;

        if (webDriverArchiveFile.getFileName().toString().toLowerCase(Locale.ENGLISH).endsWith(".tar.gz"))
            extractedExecutableFile = extractSingleFileFromTarGzArchive(webDriverArchiveFile, webDriverDirectory);
        else
            extractedExecutableFile = extractSingleFileFromZipArchive(webDriverArchiveFile, webDriverDirectory);

        if (extractedExecutableFile == null)
            throw new BrowserBoxException("Webdriver executable not found in " + webDriverArchiveFile);

        //Set executable attribute on file if the file system supports it
        if (Files.getFileAttributeView(extractedExecutableFile, PosixFileAttributeView.class) != null)
        {
            try
            {
                Set<PosixFilePermission> permissions = Files.getPosixFilePermissions(extractedExecutableFile);
                permissions.add(PosixFilePermission.OWNER_EXECUTE);
                permissions.add(PosixFilePermission.GROUP_EXECUTE);
                permissions.add(PosixFilePermission.OTHERS_EXECUTE);
                Files.setPosixFilePermissions(extractedExecutableFile, permissions);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Error setting file permissions for extracted webdriver file: " + extractedExecutableFile + ": " + e.getMessage(), e);
            }
        }

        return extractedExecutableFile;
    }

    private void runWebDriverExecutableAsynchronously(Path webDriverExecutable)
    throws BrowserBoxException
    {
        Commandline commandLine = new Commandline(webDriverExecutable.toAbsolutePath().toString());
        //commandLine.addEnvironment("ANDROID_HOME", androidHome.toAbsolutePath().toString());
        CommandLineUtils.StringStreamConsumer out = new CommandLineUtils.StringStreamConsumer();
        CommandLineUtils.StringStreamConsumer err = new CommandLineUtils.StringStreamConsumer();
        context.getLog().info("Executing Webdriver: " + commandLine.toString());

        try
        {
            Callable<Integer> r = CommandLineUtils.executeCommandLineAsCallable(commandLine, null, out, err, -1);

            webDriverExecutor.execute(() ->
                                         {
                                             try
                                             {
                                                 Integer result = r.call();
                                                 context.getLog().info("Webdriver finished: " + result);
                                             }
                                             catch (Exception e)
                                             {
                                                 context.getLog().error("Error executing Webdriver: " + e.getMessage(), e);
                                             }
                                         });
        }
        catch (CommandLineException e)
        {
            throw new BrowserBoxException("Error running Webdriver: " + e.getMessage(), e);
        }
    }

    /**
     * Called just before the browser APK is installed in the emulator.
     * Subclasses can override to customize this behaviour.
     *
     * @param emulatorDevice the emulator device.
     * @param avd the AVD being run in the emulator.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    protected void beforeApkInstall(IDevice emulatorDevice, AvdInfo avd)
    throws BrowserBoxException
    {
        //Nothing to do, customized in subclasses
    }

    /**
     * Called just after the browser APK is installed in the emulator.
     * Subclasses can override to customize this behaviour.
     *
     * @param emulatorDevice the emulator device.
     * @param avd the AVD being run in the emulator.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    protected void afterApkInstall(IDevice emulatorDevice, AvdInfo avd)
    throws BrowserBoxException
    {
        //Nothing to do, customized in subclasses
    }

    @Override
    public void start() throws BrowserBoxException
    {
        Path webDriverExecutable = resolveAndExtractWebDriverExecutable();
        context.getLog().info("Starting Webdriver: " + webDriverExecutable);

        runWebDriverExecutableAsynchronously(webDriverExecutable);

        //TODO maybe we should wait for its port to be ready
        context.getLog().info("Webdriver is started");

        AvdInfo avd = avdManager.getAvd(avdName(), true);
        if (avd == null)
            avd = createAvd();

        runAvd(avd);

        //Download APK file for Browser
        Path browserApkFile = resolveApk();

        //Wait for emulator to fully start up
        //TODO hardcoded timeout
        try
        {
            waitUntilStarted(Duration.ofMinutes(5));
        }
        catch (TimeoutException e)
        {
            throw new BrowserBoxException("Timeout waiting for emulator to boot.", e);
        }

        IDevice emulatorDevice = findAvdDevice();
        if (emulatorDevice == null)
            throw new BrowserBoxException("Could not find emulator device.");

        beforeApkInstall(emulatorDevice, avd);

        //Install APK
        try
        {
            context.getLog().info("Installing browser APK " + boxConfiguration.getBrowserVersion() + " - " + browserApkFile);
            emulatorDevice.installPackage(browserApkFile.toAbsolutePath().toString(), true);
        }
        catch (InstallException e)
        {
            throw new BrowserBoxException("Failed to install browser APK: " + browserApkFile + ": " + e.getMessage(), e);
        }

        afterApkInstall(emulatorDevice, avd);
    }

    private void runAvd(AvdInfo avd)
    throws BrowserBoxException
    {
        Path emulatorExe = androidHome.resolve("emulator").resolve("emulator" + (Os.isFamily(Os.FAMILY_WINDOWS) ? ".exe" : ""));

        //Fallback for old Android SDKs
        if (Files.notExists(emulatorExe))
            emulatorExe = androidHome.resolve("tools").resolve("emulator" + (Os.isFamily(Os.FAMILY_WINDOWS) ? ".exe" : ""));

        Commandline commandLine = new Commandline(emulatorExe.toAbsolutePath().toString());
        commandLine.addArguments(new String[] {"-avd", avd.getName()});
        commandLine.addEnvironment("ANDROID_HOME", androidHome.toAbsolutePath().toString());
        CommandLineUtils.StringStreamConsumer out = new CommandLineUtils.StringStreamConsumer();
        CommandLineUtils.StringStreamConsumer err = new CommandLineUtils.StringStreamConsumer();
        context.getLog().info("Executing emulator: " + commandLine.toString());

        try
        {
            Callable<Integer> r = CommandLineUtils.executeCommandLineAsCallable(commandLine, null, out, err, -1);

            emulatorExecutor.execute(() ->
                                     {
                                         try
                                         {
                                             Integer result = r.call();
                                             context.getLog().info("Emulator finished: " + result);

                                             if (result != 0)
                                                 context.getLog().error(err.getOutput());

                                             fireManualShutdown();
                                         }
                                         catch (Exception e)
                                         {
                                             context.getLog().error("Error executing emulator: " + e.getMessage(), e);
                                         }
                                     });
        }
        catch (CommandLineException e)
        {
            throw new BrowserBoxException("Error running emulator: " + e.getMessage(), e);
        }
    }

    @Override
    public void stop() throws BrowserBoxException
    {
        IDevice avdDevice = findAvdDevice();
        if (avdDevice != null)
        {
            try
            {
                EmulatorConsole console = EmulatorConsole.getConsole(avdDevice);
                console.kill();
            }
            catch (Exception e)
            {
                context.getLog().warn("Failed to kill emulator: " + e, e);
            }
        }

        //If there are any emulators still executing, wait for them to finish
        try
        {
            emulatorExecutor.awaitTermination(1L, TimeUnit.MINUTES);
        }
        catch (InterruptedException e)
        {
            throw new BrowserBoxException("Interrupted waiting for emulators to finish running", e);
        }
    }

    @Override
    public void createAndStart() throws BrowserBoxException
    {
        start();
    }

    @Override
    public TunnelSession establishTunnels()
    throws BrowserBoxException
    {
        IDevice device = findAvdDevice();
        if (device == null)
            throw new BrowserBoxException("Device not found");

        for (TunnelPort tunnelPort : boxConfiguration.tunnelPortConfiguration())
        {
            try
            {
                BrowserBoxAdbHelper.createReverse(AndroidDebugBridge.getSocketAddress(), device,
                                                  String.format("tcp:%d", tunnelPort.getBrowserPort()),     //$NON-NLS-1$
                                                  String.format("tcp:%d", tunnelPort.getLocalPort()));   //$NON-NLS-1$
            }
            catch (IOException | AdbCommandRejectedException | com.android.ddmlib.TimeoutException e)
            {
                throw new BrowserBoxException("Error creating tunnels: " + e.getMessage(), e);
            }
        }

        return () -> {
            for (TunnelPort tunnelPort : boxConfiguration.tunnelPortConfiguration())
            {
                try
                {
                    BrowserBoxAdbHelper.removeReverse(AndroidDebugBridge.getSocketAddress(), device,
                                                      String.format("tcp:%d", tunnelPort.getBrowserPort()),     //$NON-NLS-1$
                                                      String.format("tcp:%d", tunnelPort.getLocalPort()));
                }
                catch (IOException | AdbCommandRejectedException | com.android.ddmlib.TimeoutException e)
                {
                    throw new BrowserBoxException("Error removing tunnels: " + e.getMessage(), e);
                }
            }
        };
    }

    @Override
    public void delete() throws BrowserBoxException
    {
        AvdInfo avd = avdManager.getAvd(avdName(), true);
        if (avd == null)
            return;

        boolean deleted = avdManager.deleteAvd(avd, createLogger());
        if (!deleted)
            throw new BrowserBoxException("Failed to delete AVD");
    }

    @Override
    public void deleteImage() throws BrowserBoxException
    {
        SystemImage systemImage = prepareSystemImage(false);
        if (systemImage == null)
            return;

        RepoManager sdkManager = androidSdk.getSdkManager(new ConsoleProgressIndicator());
        InstallerFactory installerFactory = SdkInstallerUtil.findBestInstallerFactory(systemImage.getPackage(), androidSdk);
        Uninstaller uninstaller = installerFactory.createUninstaller((LocalPackage)systemImage.getPackage(), sdkManager, androidSdk.getFileOp());
        boolean ok = uninstaller.prepare(new ConsoleProgressIndicator());
        if (!ok)
            throw new BrowserBoxException("Failed to uninstall system image");
        ok = uninstaller.complete(new ConsoleProgressIndicator());
        if (!ok)
            throw new BrowserBoxException("Failed to uninstall system image");
    }

    @Override
    public void waitUntilStarted(Duration maxTimeToWait)
    throws TimeoutException, BrowserBoxException
    {
        long maxTime = System.currentTimeMillis() + maxTimeToWait.toMillis();
        try
        {
            IDevice emulatorDevice;
            do
            {
                emulatorDevice = findAvdDevice();
                if (System.currentTimeMillis() > maxTime)
                    throw new TimeoutException("Timeout waiting for startup");

                Thread.sleep(100L);
            }
            while (emulatorDevice == null || !emulatorDevice.isOnline());

            //Now we have the emulator device, wait for it to completely boot up
            waitForEmulatorBoot(emulatorDevice, maxTimeToWait);
        }
        catch (InterruptedException e)
        {
            throw new BrowserBoxException("Interrupted waiting for startup.", e);
        }
    }

    private void waitForEmulatorBoot(IDevice emulatorDevice, Duration maxTimeToWait)
    throws BrowserBoxException, InterruptedException
    {
        Map<String, String> expectedBootProperties = ImmutableMap.of(
                "dev.bootcomplete", "1",
                "sys.boot_completed", "1",
                "init.svc.bootanim", "stopped"
        );
        Map<String, String> currentBootProperties = new HashMap<>();

        do
        {
            if (!emulatorDevice.isOnline())
                throw new BrowserBoxException("Device went offline while booting: " + emulatorDevice.getName());

            currentBootProperties.clear();
            for (String bootProperty : expectedBootProperties.keySet())
            {
                try
                {
                    currentBootProperties.put(bootProperty, emulatorDevice.getSystemProperty(bootProperty).get());
                }
                catch (ExecutionException e)
                {
                    context.getLog().warn("Failed to retrieve system property '" + bootProperty + "' from device: " + e.getMessage(), e);
                }
            }

            //If all the properties match, then we're done
            if (expectedBootProperties.equals(currentBootProperties))
                return;

            Thread.sleep(500L);
        }
        while (true);
    }

    @Override
    public List<? extends ConnectionType> getSupportedConnectionTypes()
    throws BrowserBoxException
    {
        return ImmutableList.of(StandardConnectionType.SELENIUM);
    }

    private final List<ManualShutdownListener> manualShutdownListeners = new CopyOnWriteArrayList<>();

    public void addManualShutdownListener(ManualShutdownListener listener)
    {
        manualShutdownListeners.add(listener);
    }

    protected void fireManualShutdown()
    {
        ManualShutdownEvent event = new ManualShutdownEvent(this);
        for (ManualShutdownListener listener : manualShutdownListeners)
        {
            listener.manualShutdownPerformed(event);
        }
    }

    @Override
    public ConnectionInfo getConnectionInfo(ConnectionType connectionType)
    throws BrowserBoxException
    {
        if (connectionType == StandardConnectionType.SELENIUM)
            return new ConnectionInfo(URI.create("http://localhost:9515"));
        else
            throw new BrowserBoxException("Unknown connection type " + connectionType);
    }

    @Override
    public VideoControl video() throws BrowserBoxException
    {
        return new VideoControl()
        {
            @Override
            public Recording startRecording() throws IOException, BrowserBoxException
            {
                IDevice device = findAvdDevice();
                if (device == null)
                    throw new BrowserBoxException("Emulator device not found.");

                AndroidVideoRecording recording = new AndroidVideoRecording(device);
                videoRecordExecutor.submit(() ->
                                           {
                                               try
                                               {
                                                   //Blocks until it finishes recording
                                                   recording.start();
                                               }
                                               catch (BrowserBoxException e)
                                               {
                                                   throw new RuntimeException("Failed to start screen recorder: " + e.getMessage(), e);
                                               }
                                           });
                return recording;
            }
        };
    }

    private static ILogger createLogger()
    {
        return new IReaderLogger()
        {
            @Override
            public void error(Throwable t, String errorFormat, Object... args)
            {
                if (errorFormat != null)
                {
                    System.err.printf("Error: " + errorFormat, args);
                    if (!errorFormat.endsWith("\n"))
                        System.err.print("\n");
                }
                if (t != null)
                    System.err.printf("Error: %s\n", t.getMessage());
            }

            @Override
            public void warning(@NonNull String warningFormat, Object... args)
            {
                System.out.printf("Warning: " + warningFormat, args);
                if (!warningFormat.endsWith("\n"))
                    System.out.print("\n");
            }

            @Override
            public void info(@NonNull String msgFormat, Object... args)
            {
                System.out.printf(msgFormat, args);
            }

            @Override
            public void verbose(@NonNull String msgFormat, Object... args)
            {
                System.out.printf(msgFormat, args);
            }

            /**
             * Used by UpdaterData.acceptLicense() to prompt for license acceptance
             * when updating the SDK from the command-line.
             * <p/>
             * {@inheritDoc}
             */
            @Override
            public int readLine(byte[] inputBuffer) throws IOException
            {
                return System.in.read(inputBuffer);
            }
        };
    }

    /**
     * Generate a certificate file name suitable for use on an Android device's cacerts store.
     * File name is an 8 character hex-encoded MD5 hash of the certificate subject principal.
     * Should have same result as OpenSSL's <code>-subject_hash_old</code> option.
     *
     * See <a href="https://github.com/google/conscrypt/blob/2.2.1/platform/src/main/java/org/conscrypt/TrustedCertificateStore.java#L557">Android's TrustedCertificateStore</a>
     * and <a href="https://github.com/google/conscrypt/blob/2.2.1/common/src/main/java/org/conscrypt/NativeCrypto.java#L364">NativeCrypto</a> implementation.
     */
    private String certificateFileName(X509Certificate cert)
    {
        X500Principal subject = cert.getSubjectX500Principal();
        String hash = Strings.padStart(
                Integer.toHexString(Hashing.md5().hashBytes(subject.getEncoded()).asInt()),
                8, '0');
        return hash + ".0";
    }

    @Override
    public void installCertificates(Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        IDevice device = findAvdDevice();
        if (device == null)
            throw new BrowserBoxException("No AVD device found.");

        CollectingOutputReceiver out = new CollectingOutputReceiver();

        //Generate hashes for certificates
        for (X509Certificate certificate : certificates)
        {
            try
            {
                //Generate cert file locally first
                String certFileName = certificateFileName(certificate);
                Path localCertFile = context.getTempDirectory().resolve(certFileName);
                Files.createDirectories(localCertFile.getParent());
                Files.write(localCertFile, certificate.getEncoded());

                //Copy file to emulator
                device.pushFile(localCertFile.toAbsolutePath().toString(), "/data/local/tmp/" + certFileName);
                context.getLog().info("Pushed certificate " + certFileName + " to emulator");

                //As root, copy the file to the cacerts directory
                //device.executeShellCommand("su 0 mv /data/local/tmp/" + certFileName + " /system/etc/security/cacerts/", out);
                device.executeShellCommand("su 0 mkdir -p /data/misc/user/0/cacerts-added", out);
                device.executeShellCommand("su 0 mv /data/local/tmp/" + certFileName + " /data/misc/user/0/cacerts-added/", out);
                device.executeShellCommand("su 0 chmod 0644 /data/misc/user/0/cacerts-added/" + certFileName, out);
            }
            catch (CertificateEncodingException e)
            {
                throw new BrowserBoxException("Failed to generate hash for certificate: " + e.getMessage(), e);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("I/O error adding certificate: " + e.getMessage(), e);
            }
            catch (AdbCommandRejectedException | com.android.ddmlib.TimeoutException | SyncException | ShellCommandUnresponsiveException e)
            {
                throw new BrowserBoxException("Android error adding certificate: " + e.getMessage() + " " + out.getOutput(), e);
            }
        }

        //At the end remount the system partition as read-only again
        try
        {
            device.executeShellCommand("su 0 mount -o ro,remount /", out);
        }
        catch (com.android.ddmlib.TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | IOException e)
        {
            throw new BrowserBoxException("Failed to make emulator device's system partition read-only after certificate installation: " + e.getMessage() + " " + out.getOutput(), e);
        }
    }

    /**
     * Fixes the deletion problem on Windows by ensuring files are not read-only before attempting to delete them.  On Windows platforms at least,
     * read-only files cannot be deleted and this caused problems with files used with certain versions of the Android emulator.
     */
    private class DeletionFixingFileOpImpl extends FileOpImpl
    {
        @Override
        public boolean delete(File file)
        {
            try
            {
                DosFileAttributeView attributeView = Files.getFileAttributeView(toPath(file), DosFileAttributeView.class);
                if (attributeView != null)
                {
                    DosFileAttributes attributes = attributeView.readAttributes();
                    if (attributes.isReadOnly())
                    {
                        context.getLog().debug("File " + file.getAbsolutePath() + " is read only, resetting...");
                        attributeView.setReadOnly(false);
                    }
                }
                Files.delete(toPath(file));
                return true;
            }
            catch (IOException e)
            {
                e.printStackTrace(); //Some debugging for deletion failures
                return false;
            }
        }
    }

    protected static class ConsoleOutputReceiver implements IShellOutputReceiver
    {
        @Override
        public void addOutput(byte[] data, int offset, int length)
        {
            System.out.write(data, offset, length);
        }

        @Override
        public void flush()
        {
            System.out.flush();
        }

        @Override
        public boolean isCancelled()
        {
            return false;
        }
    }

    private class AndroidVideoRecording implements VideoControl.Recording
    {
        private volatile boolean videoStopSignal;
        private final CountDownLatch videoStoppedLatch = new CountDownLatch(1);
        private final String deviceVideoFile = "/sdcard/browserboxvideo.mp4";
        private final String deviceVideoFileName = "browserboxvideo.mp4";
        private final IDevice device;

        public AndroidVideoRecording(IDevice device)
        {
            this.device = device;
        }

        public void start()
        throws BrowserBoxException
        {
            try
            {
                context.getLog().info("Starting video recording to " + deviceVideoFile);
                VideoShellOutputReceiver outputReceiver = new VideoShellOutputReceiver();
                device.startScreenRecorder(deviceVideoFile,
                                           new ScreenRecorderOptions.Builder().build(),
                                           outputReceiver);

                //If we get a known error, restart the recorder at a lower resolution
                //Known issue with older versions of Android
                if (outputReceiver.isExecutionFailureDueToVideoSize())
                {
                    context.getLog().info("This Android platform doesn't seem to support recording at native resolution.  Will attempt to record at lower resolution.");

                    //Halve the screen size for the purpose of video recording, which might work with older Android versions
                    Dimension recordSize = deviceSpecification().getScreenSize(com.android.resources.ScreenOrientation.PORTRAIT);
                    recordSize.setSize(recordSize.getWidth() / 2.0, recordSize.getHeight() / 2.0);

                    outputReceiver = new VideoShellOutputReceiver();
                    device.startScreenRecorder(deviceVideoFile,
                                               new ScreenRecorderOptions.Builder()
                                                       .setSize(recordSize.width, recordSize.height)
                                                       .build(),
                                               outputReceiver);
                }

                videoRecorderExecutionFinished();
            }
            catch (com.android.ddmlib.TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | IOException e)
            {
                throw new BrowserBoxException("Error recording video: " + e.getMessage(), e);
            }
        }

        @Override
        public void stopAndSave(Path videoFile) throws IOException, BrowserBoxException
        {
            videoStopSignal = true;
            waitAndFinish(videoFile);
        }

        @Override
        public void stopAndCancel() throws IOException, BrowserBoxException
        {
            videoStopSignal = true;
            waitAndFinish(null);
        }

        private void waitAndFinish(Path videoFile)
        throws BrowserBoxException
        {
            try
            {
                videoStoppedLatch.await();
            }
            catch (InterruptedException e)
            {
                throw new BrowserBoxException("Interrupted waiting for video recording to stop.", e);
            }

            context.getLog().info("Video recording done, saving: " + videoFile);

            //Wait a bit for video to be fully written - to do this properly we should use file listing service and poll
            //until file size stops changing
            try
            {
                FileListingService files = device.getFileListingService();
                FileEntry sdCardDir = Stream.of(files.getChildrenSync(files.getRoot())).filter(e -> e.isDirectory() && e.getName().equals("sdcard")).findFirst().orElseThrow(
                        FileNotFoundException::new);

                long lastSize;
                long curSize = 0L;
                int sameSizeCount = 0;
                do
                {
                    Thread.sleep(100L);
                    FileEntry videoFileEntry = Stream.of(files.getChildrenSync(sdCardDir)).filter(e -> !e.isDirectory() && deviceVideoFileName.equals(e.getName())).findFirst()
                                                     .orElseThrow(FileNotFoundException::new);
                    lastSize = curSize;
                    curSize = videoFileEntry.getSizeValue();
                    context.getLog().debug("Video file size: " + curSize);
                    if (lastSize == curSize)
                        sameSizeCount++;
                    else
                        sameSizeCount = 0;
                }
                //We really need to be sure the file stops changing, so only we're only sure
                //after we've polled a number of times and it still hasn't changed
                while (sameSizeCount < 10);

                if (videoFile != null)
                {
                    context.getLog().info("Preparing to save video to " + videoFile.toAbsolutePath().toString());
                    device.pullFile(deviceVideoFile, videoFile.toAbsolutePath().toString());
                    context.getLog().info("Saved video to " + videoFile.toAbsolutePath().toString());
                }

                //Clean up file on device regardless
                device.executeShellCommand("rm " + deviceVideoFile, new ConsoleOutputReceiver());
                context.getLog().info("Removed video from device " + deviceVideoFile);
            }
            catch (InterruptedException | com.android.ddmlib.TimeoutException | AdbCommandRejectedException | IOException | ShellCommandUnresponsiveException | SyncException e)
            {
                throw new BrowserBoxException("Error saving video file: " + e, e);
            }
        }

        private void videoRecorderExecutionFinished()
        {
            videoStoppedLatch.countDown();
        }

        private class VideoShellOutputReceiver extends ConsoleOutputReceiver
        {
            //Intentionally a thread-safe StringBuffer in case Android decides to multi-thread output
            private final StringBuffer outputBuf = new StringBuffer();

            @Override
            public boolean isCancelled()
            {
                return videoStopSignal;
            }

            @Override
            public void addOutput(byte[] data, int offset, int length)
            {
                super.addOutput(data, offset, length);
                outputBuf.append(new String(data, offset, length));
            }

            /**
             * Older versions of Android can't cope with recording video at native resolution, and will fail immediately
             * with a known error message.
             */
            public boolean isExecutionFailureDueToVideoSize()
            {
                return outputBuf.indexOf("Unable to get output buffers") >= 0;
            }
        }
    }

    public static interface ManualShutdownListener extends EventListener
    {
        public void manualShutdownPerformed(ManualShutdownEvent event);
    }

    public static class ManualShutdownEvent extends EventObject
    {
        private final AndroidBrowserBox box;

        public ManualShutdownEvent(AndroidBrowserBox source)
        {
            super(source);
            this.box = source;
        }

        public AndroidBrowserBox getBox()
        {
            return box;
        }
    }
}
