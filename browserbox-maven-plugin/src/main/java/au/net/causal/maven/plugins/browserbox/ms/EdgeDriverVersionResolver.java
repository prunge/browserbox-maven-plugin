package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface EdgeDriverVersionResolver
{
    /**
     * Resolves the version of Edge driver to use for the given Edge browser version.
     *
     * @param edgeVersion the Edge browser version to look up the driver version for.
     * @return the driver version to use, or null if none could be found.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public Version resolve(String edgeVersion)
    throws BrowserBoxException;

    /**
     * Resolves the version of Edge driver to use for the given Edge browser version, falling back to using the 
     * latest driver version if no exact or partial match was found.
     * 
     * @param edgeVersion the Edge browser version to look up the driver version for.
     * @return the driver version to use, or null if none could be found.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public default Version resolveWithFallback(String edgeVersion)
    throws BrowserBoxException
    {
        Version driverVersion = resolve(edgeVersion);
        if (driverVersion != null)
            return driverVersion;
        
        return allVersions().stream()
                .max(Comparator.comparing(Version::getRawVersion)).orElse(null);
    }

    /**
     * Reads all Edge driver versions.
     * 
     * @return a list of all known versions.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public List<? extends Version> allVersions()
    throws BrowserBoxException;
    
    public static class Version
    {
        private static final Pattern versionParser = Pattern.compile("^(\\d+)\\.(\\d+)\\.?.*$");
        
        private final String rawVersion;
        private final String majorVersion;
        private final String edgeVersion;
        private final boolean onDemand;

        public Version(String rawVersion)
        throws BrowserBoxException
        {
            this.rawVersion = rawVersion;

            final String versionToParse;
            if (rawVersion.startsWith("ondemand:"))
            {
                this.onDemand = true;
                versionToParse = rawVersion.substring("ondemand:".length());
            }
            else
            {
                this.onDemand = false;
                versionToParse = rawVersion;
            }

            Matcher m = versionParser.matcher(versionToParse);
            if (!m.matches())
                throw new BrowserBoxException("Failed to parse Edge driver version " + rawVersion);

            majorVersion = m.group(1);
            edgeVersion = m.group(2);
        }

        /**
         * Parses string version numbers of Edge drivers into objects.
         *
         * @param stringVersions the versions to parse.
         *
         * @return a list of parsed versions.
         *
         * @throws BrowserBoxException if an error occurs interpreting the versions.
         */
        public static List<? extends Version> parseVersions(Collection<String> stringVersions)
        throws BrowserBoxException
        {
            List<Version> results = new ArrayList<>(stringVersions.size());
            for (String stringVersion : stringVersions)
            {
                results.add(new Version(stringVersion));
            }
            return results;
        }

        /**
         * @return the full, unparsed version number.
         */
        public String getRawVersion()
        {
            return rawVersion;
        }

        /**
         * @return the major version number.  e.g. in 4.15063 this number will be '4'.
         */
        public String getMajorVersion()
        {
            return majorVersion;
        }

        /**
         * @return the Edge version number.  e.g. in 4.15063.20171120 this number will be '15063'.
         */
        public String getEdgeVersion()
        {
            return edgeVersion;
        }

        /**
         * @return true if the Edge driver is an on-demand install.
         */
        public boolean isOnDemand()
        {
            return onDemand;
        }

        @Override
        public String toString()
        {
            return getRawVersion() + ", major=" + getMajorVersion() + ", edge=" + getEdgeVersion() + ", onDemand=" + isOnDemand();
        }

        @Override
        public boolean equals(Object o) 
        {
            if (this == o) return true;
            if (!(o instanceof Version)) return false;
            Version version = (Version) o;
            return Objects.equals(getRawVersion(), version.getRawVersion());
        }

        @Override
        public int hashCode() 
        {
            return Objects.hash(getRawVersion());
        }
    }
}
