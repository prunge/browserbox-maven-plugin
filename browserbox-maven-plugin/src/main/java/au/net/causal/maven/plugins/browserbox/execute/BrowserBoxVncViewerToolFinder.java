package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import au.net.causal.maven.plugins.browserbox.box.StandardConnectionType;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.ListeningExecutorService;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.apache.maven.shared.invoker.PrintStreamHandler;
import org.codehaus.plexus.util.cli.StreamConsumer;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Properties;

public class BrowserBoxVncViewerToolFinder implements ToolFinder
{
    @Override
    public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                                       ProjectConfiguration projectConfiguration, BoxContext context)
    throws IOException, BrowserBoxException
    {
        return Optional.of(new Runner());
    }

    private static void copyResource(String name, Path targetFile)
    throws IOException
    {
        URL url = BrowserBoxVncViewerToolFinder.class.getResource(name);
        if (url == null)
            throw new Error("Resource " + name + " does not exist.");

        try (OutputStream os = Files.newOutputStream(targetFile))
        {
            Resources.copy(url, os);
        }
    }

    private static class Runner implements ToolRunner
    {
        @Override
        public void prepareRun(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                               ProjectConfiguration projectConfiguration, BoxContext context)
        throws BrowserBoxException
        {
            //Resolve the VNC viewer artifact early, in the blocking thread, to give the user a nicer UI experience
            //Otherwise the output from run(), which is not output to console unless there is an error,
            //might hide a potentiallly long-running download
            Artifact vncViewerArtifact = context.getInternalVncViewerArtifact();
            ArtifactRequest request = new ArtifactRequest(vncViewerArtifact, context.getRemoteRepositories(), null);
            try
            {
                ArtifactResult result = context.getRepositorySystem().resolveArtifact(context.getRepositorySystemSession(), request);
                if (!result.isResolved())
                    throw new BrowserBoxException("Error resolving VNC viewer artifact.");

            }
            catch (ArtifactResolutionException e)
            {
                throw new BrowserBoxException("Error resolving VNC viewer artifact: " + e.getMessage(), e);
            }
        }

        @Override
        public int run(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                       ProjectConfiguration projectConfiguration, BoxContext context, StreamConsumer out,
                       StreamConsumer err, ListeningExecutorService executor)
        throws IOException, BrowserBoxException
        {
            //Extract the VNC viewer invoker POM file that has dependency information and use
            Path vncViewerInvokeProjectDirectory = context.getTempDirectory().resolve("vncviewer-invoke");
            Files.createDirectories(vncViewerInvokeProjectDirectory);
            Path vncViewerInvokePomFile = vncViewerInvokeProjectDirectory.resolve("pom.xml");
            copyResource("vncviewer/pom.xml", vncViewerInvokePomFile);

            ConnectionInfo vncConnectionInfo = box.getConnectionInfo(StandardConnectionType.VNC);
            Properties properties = new Properties();
            properties.setProperty("browserbox.vnc.address", vncConnectionInfo.getUri().getHost() + ":" + vncConnectionInfo.getUri().getPort());
            properties.setProperty("browserbox.vncviewer.version", context.getInternalVncViewerArtifact().getVersion());

            //Properties are configured in the POM itself
            try (ByteArrayOutputStream bufOut = new ByteArrayOutputStream();
                 PrintStream pOut = new PrintStream(bufOut))
            {
                InvocationRequest invocationRequest = new DefaultInvocationRequest()
                                                          .setBaseDirectory(vncViewerInvokeProjectDirectory.toFile())
                                                          .setGoals(ImmutableList.of("exec:exec"))
                                                          .setOutputHandler(new PrintStreamHandler(pOut, true))
                                                          .setErrorHandler(new PrintStreamHandler(pOut, true))
                                                          .setBatchMode(true)
                                                          .setProperties(properties);

                try
                {
                    InvocationResult result = context.getInvoker().execute(invocationRequest);
                    return result.getExitCode();
                }
                catch (MavenInvocationException e)
                {
                    String toolOut = bufOut.toString(); //Intentionally using platform encoding
                    throw new BrowserBoxException("Error executing VNC viewer: " + e.getMessage() + "\n" + toolOut, e);
                }
            }
        }

      @Override
      public String toString()
      {
          return "BrowserBox VNC Viewer";
      }
    }

}
