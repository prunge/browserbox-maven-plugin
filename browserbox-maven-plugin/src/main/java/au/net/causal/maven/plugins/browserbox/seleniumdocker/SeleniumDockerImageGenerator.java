package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import io.fabric8.maven.docker.config.Arguments;
import io.fabric8.maven.docker.config.AssemblyConfiguration;
import io.fabric8.maven.docker.config.BuildImageConfiguration;
import io.fabric8.maven.docker.config.ImageConfiguration;
import io.fabric8.maven.docker.service.BuildService;
import io.fabric8.maven.docker.service.RegistryService;
import io.fabric8.maven.docker.util.MojoParameters;
import org.apache.maven.archiver.MavenArchiveConfiguration;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.assembly.model.Assembly;
import org.apache.maven.plugins.assembly.model.FileSet;
import org.apache.maven.shared.filtering.DefaultMavenFileFilter;
import org.apache.maven.shared.filtering.DefaultMavenReaderFilter;
import org.apache.maven.shared.filtering.MavenFileFilter;
import org.apache.maven.shared.filtering.MavenReaderFilter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Dynamically extend Selenium Docker images and give extended functionality such as SSH support.
 * <p>
 * 
 * This generator is designed to extend the selenium/standalone-[type]-debug images, where <i>type</i> is the browser
 * type ('chrome' or 'firefox').  The following additional features are installed:
 * <ul>
 *     <li>SSH server</li>
 * </ul>
 * 
 * The browser box plugin will then install this image and use this for making Docker containers instead of the original.
 * The image can be kept around on the user's system so generation only needs to happen once per browser version.
 * Since Docker stores file system diffs this shouldn't be too much of a waste of space, and allows the
 * :prepare goal to make a usable system even when offline (image generation requires Internet access for downloading 
 * apt packages).
 */
public class SeleniumDockerImageGenerator 
{
    private final BoxContext boxContext;
    private final ProjectConfiguration projectConfiguration;
    private final BoxConfiguration boxConfiguration;
    private final DockerNaming dockerNaming = new DockerNaming();
    
    public SeleniumDockerImageGenerator(BoxContext boxContext, ProjectConfiguration projectConfiguration, BoxConfiguration boxConfiguration)
    {
        Objects.requireNonNull(boxContext, "boxContext == null");
        Objects.requireNonNull(projectConfiguration, "projectConfiguration == null");
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        this.boxContext = boxContext;
        this.projectConfiguration = projectConfiguration;
        this.boxConfiguration = boxConfiguration;
    }
    
    protected BoxContext getContext()
    {
        return boxContext;
    }
    
    protected ProjectConfiguration getProjectConfiguration()
    {
        return projectConfiguration;
    }
    
    protected BoxConfiguration getBoxConfiguration()
    {
        return boxConfiguration;
    }
    
    public void generate(ImageReference baseImageName, ImageReference targetImageName)
    throws BrowserBoxException, MojoExecutionException, IOException
    {
        //TODO ssh private/public key generation for SSH
        //see KeyPair.genKeyPair()
        
        MavenArchiveConfiguration archiveConfiguration = new MavenArchiveConfiguration();
        MavenFileFilter mavenFileFilter = new DefaultMavenFileFilter();
        MavenReaderFilter mavenReaderFilter = new DefaultMavenReaderFilter();

        Path absoluteTempBase = boxContext.getTempDirectory();
        Path absoluteOutputDirectory = absoluteTempBase.resolve("build");
        Path absoluteSourceDirectory = absoluteTempBase.resolve("src");
        Path relativeSourceDirectory = projectConfiguration.getBaseDirectory().toPath().relativize(absoluteSourceDirectory);
        Path relativeOutputDirectory = projectConfiguration.getBaseDirectory().toPath().relativize(absoluteOutputDirectory);
        Files.createDirectories(absoluteOutputDirectory);
        Files.createDirectories(absoluteSourceDirectory);
        
        //Build sources
        Path entryPointFile = absoluteSourceDirectory.resolve("browserbox_entry_point.sh");
        
        //This file calls back to original entrypoint but starts sshd first
        Files.write(entryPointFile, Collections.singletonList(
                                        "#!/bin/bash\n" +
                                        "sudo /etc/init.d/ssh start\n" +
                                        "export GDK_DPI_SCALE=$SCREEN_SCALE\n" +
                                        "source /opt/bin/entry_point.sh\n"));
        
        MojoParameters mojoParameters = new MojoParameters(boxContext.getSession(), 
                                                projectConfiguration.getProject(), 
                                                archiveConfiguration, 
                                                mavenFileFilter, 
                                                mavenReaderFilter, 
                                                projectConfiguration.getSettings(),
                                                //Both of these need to be relative
                                                relativeSourceDirectory.toString(),
                                                relativeOutputDirectory.toString(),
                                                Collections.emptyList());
        RegistryService.RegistryConfig registryConfig = new RegistryService.RegistryConfig.Builder()
                .authConfigFactory(boxContext.getAuthConfigFactory())
                //.registry(registry)
                .settings(projectConfiguration.getSettings())
                .build();

        BuildService.BuildContext buildContext = new BuildService.BuildContext.Builder()
                                                    .mojoParameters(mojoParameters)
                                                    .registryConfig(registryConfig)
                                                    .build();

        List<String> cmds = Arrays.asList(
                                //Easier setting executable permission like this rather than relying on filesystem which may
                                //not support permissions like on Windows
                                "sudo chmod 0775 /opt/bin/browserbox_entry_point.sh", 
                                "sudo apt-get update",
                                //note: libasound2 and libgtk2.0-0 is only needed for old Firefox versions
                                "sudo apt-get -qqy install openssh-server libasound2 libgtk2.0-0",
                                //for video recording - pretty big
                                //old Ubuntu versions had libav-tools instead of ffmpeg but ffmpeg is available by PPA so use that if needed
                                "if [ `lsb_release -c -s` = 'trusty' ] ; " +
                                    "then (sudo apt-get -qqy install libav-tools && sudo ln -s /usr/bin/avconv /usr/bin/ffmpeg) ; " +
                                    "else (sudo apt-get -qqy install ffmpeg) ; " +
                                "fi",
                                "sudo apt-get -qqy install libnss3-tools", //for adding certificates to Firefox DB
                                "sudo rm -rf /var/lib/apt/lists/* /var/cache/apt/*");

        Assembly assembly = new Assembly();
        FileSet assemblyFileSet = new FileSet();
        assemblyFileSet.setDirectory(relativeSourceDirectory.toString());
        assemblyFileSet.setOutputDirectory(".");
        assemblyFileSet.setFileMode("0775");
        assembly.addFileSet(assemblyFileSet);
        
        AssemblyConfiguration assemblyConfig = new AssemblyConfiguration.Builder()
                                                    .targetDir("/opt/bin")
                                                    .assemblyDef(assembly)
                                                    //Only need this without permission setting in cmds
                                                    //.permissions(PermissionMode.exec.name())
                                                    .build();
        
        BuildImageConfiguration buildConfig = new BuildImageConfiguration.Builder()
                                                    .from(baseImageName.getName())
                                                    //.ports() //TODO add SSH port?
                                                    .runCmds(cmds) 
                                                    .optimise(true)
                                                    .labels(dockerNaming.labels().forBrowserWithVersion(boxConfiguration))
                                                    .entryPoint(new Arguments("/opt/bin/browserbox_entry_point.sh"))
                                                    .assembly(assemblyConfig)
                                                    .build();
        
        ImageConfiguration imageConfig = new ImageConfiguration.Builder()
                                                .name(targetImageName.getName())
                                                .buildConfig(buildConfig)
                                                .build();

        boxContext.getDockerServiceHub().getBuildService().buildImage(imageConfig, getContext().getImagePullCacheManager(), buildContext);
    }
}
