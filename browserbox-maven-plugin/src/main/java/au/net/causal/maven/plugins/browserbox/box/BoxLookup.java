package au.net.causal.maven.plugins.browserbox.box;

import org.apache.maven.plugin.logging.Log;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class for using SPI mechanism to look up box browser box factories.
 */
public class BoxLookup
{
    private final List<BrowserBoxFactory> bundledFactories;
    private final List<BrowserBoxFactory> nonBundledFactories;
    
    /**
     * Constructor for lookup.
     *
     * @param log logger to use for logging warnings.
     * @param loader classloader to use for finding service configuration resources.
     */
    public BoxLookup(Log log, ClassLoader loader)
    {
        this.bundledFactories = new ArrayList<>();
        this.nonBundledFactories = new ArrayList<>();

        for (BrowserBoxFactory factory : ServiceLoader.load(BrowserBoxFactory.class, loader))
        {
            //Special handling for bundled factories
            List<BrowserBoxFactory> factories;
            boolean bundled = factory.getClass().isAnnotationPresent(BrowserBoxBundled.class);
            if (bundled)
                factories = bundledFactories;
            else
                factories = nonBundledFactories;

            factories.add(factory);
        }
    }

    /**
     * Finds a factory by browser type.
     *
     * @param type the browser type.  e.g. 'chrome'
     *
     * @return the factory for this browser type, or <code>null</code> if none exists.
     */
    public BrowserBoxFactory findFactory(String type)
    {
        return Stream.concat(nonBundledFactories.stream(), bundledFactories.stream())
                .filter(f -> f.supportsType(type))
                .findFirst().orElse(null);
    }

    /**
     * @return a list of all registered browser box factories in the system.
     */
    public List<? extends BrowserBoxFactory> getAvailableBoxFactories()
    {
        return Stream.concat(nonBundledFactories.stream(), bundledFactories.stream())
                     .collect(Collectors.toList());
    }

    /**
     * @return a list of the names of all registered browser box factories in the system.
     */
    public List<String> getAvailableBoxFactoryNames()
    {
        return getAvailableBoxFactories().stream()
                .flatMap(f -> f.getKnownTypes().stream())
                .sorted()
                .collect(Collectors.toList());
    }
}
