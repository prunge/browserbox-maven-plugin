package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.URI;

/**
 * Use the Selenium API to control the browser.
 */
public class SeleniumBrowserControl implements BrowserControl
{
    private final RemoteWebDriver driver;
    
    public SeleniumBrowserControl(URI seleniumServerUrl, Capabilities capabilities)
    throws IOException
    {
        driver = new RemoteWebDriver(seleniumServerUrl.toURL(), capabilities);
    }
    
    @Override
    public void launch(URI url) throws IOException, BrowserBoxException 
    {
        driver.manage().window().maximize();
        driver.get(url.toString());
    }

    @Override
    public void quit() throws IOException, BrowserBoxException 
    {
        driver.quit();
    }
}
