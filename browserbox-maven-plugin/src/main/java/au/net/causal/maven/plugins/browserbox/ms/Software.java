package au.net.causal.maven.plugins.browserbox.ms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Software
{
    private String name;
    private final List<SoftwareFile> files = new ArrayList<>();

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<SoftwareFile> getFiles()
    {
        return files;
    }
    
    public void setFiles(List<SoftwareFile> files)
    {
        this.files.clear();
        this.files.addAll(files);
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("Software{");
        sb.append("name='").append(name).append('\'');
        sb.append(", files=").append(files);
        sb.append('}');
        return sb.toString();
    }
}
