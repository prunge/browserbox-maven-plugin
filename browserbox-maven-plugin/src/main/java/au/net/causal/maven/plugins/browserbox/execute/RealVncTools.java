package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class RealVncTools 
{
    public Path generateVncFile(ConnectionInfo connectionInfo, BoxContext context)
    throws IOException
    {
        Path tempDirectory = context.getTempDirectory();
        Path vncFile = tempDirectory.resolve("browserbox.vnc");
        try (BufferedWriter w = Files.newBufferedWriter(vncFile))
        {
            w.write("Host=" + connectionInfo.getUri().getHost() + ":" + connectionInfo.getUri().getPort() + System.lineSeparator());
            w.write("Password=2e2dbf576eb06c9e" + System.lineSeparator()); //Hardcoded for 'secret' password for now
            w.write("WarnUnencrypted=0" + System.lineSeparator());
            w.write("ColorLevel=full" + System.lineSeparator());
        }

        return vncFile;
    }
}
