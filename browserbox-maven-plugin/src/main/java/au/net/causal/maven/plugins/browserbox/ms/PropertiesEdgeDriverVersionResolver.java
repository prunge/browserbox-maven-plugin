package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Properties;

/**
 * Resolves Edge driver versions from a properties file.
 * <p>
 *     
 * Property keys are the simple {@linkplain Version#getEdgeVersion() Edge version number}, e.g. if the full 
 * Edge browser version is 15.15063, a matching key will be '15063'.  Property values are the full
 * Edge driver version.
 */
public abstract class PropertiesEdgeDriverVersionResolver extends BaseEdgeDriverVersionResolver
{
    protected abstract Properties readProperties()
    throws BrowserBoxException;

    /**
     * Saves a list of versions to properties using the Edge version of each driver version as each key.
     * 
     * @param versions a list of Edge driver versions.
     * @param properties save all versions to this properties object, overwriting existing values if necessary.
     */
    public void toProperties(List<? extends Version> versions, Properties properties)
    {
        versions.forEach(v -> properties.setProperty(v.getEdgeVersion(), v.getRawVersion()));
    }
    
    @Override
    public List<? extends Version> allVersions() 
    throws BrowserBoxException 
    {
        Properties props = readProperties();
        return Version.parseVersions(Maps.fromProperties(props).values());
    }

    @Override
    public Version resolve(String edgeVersion)
    throws BrowserBoxException
    {
        Properties props = readProperties();
        
        //Attempt to find exact match for Edge version
        //This allows arbitrary mappings from Edge browsers to drivers in the properties file
        //which could be handy for corner cases later on
        Version edgeBrowserVersion = new Version(edgeVersion);
        String driverRawVersion = props.getProperty(edgeBrowserVersion.getEdgeVersion());
        if (driverRawVersion != null)
            return new Version(driverRawVersion);
        
        //No exact match, fallback logic
        List<? extends Version> edgeDriverVersions = Version.parseVersions(Maps.fromProperties(props).values());
        
        return resolveFromList(edgeDriverVersions, edgeBrowserVersion);
    }
}
