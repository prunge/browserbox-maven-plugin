package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BoxLookup;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import com.google.common.base.Strings;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.List;

@Mojo(name="cleanimages", requiresProject = false)
public class CleanImagesMojo extends AbstractBrowserBoxMojo 
{
    private void deleteAllImagesForBoxType(String boxType, ExceptionalSupplier<DockerService, BrowserBoxException> service)
    throws BrowserBoxException, MojoExecutionException
    {
        browserBoxFactory(service).deleteAllImages(boxType, boxContext(service));
    }
    
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        try 
        {
            BoxLookup lookup = boxLookup();
            String type = box.getBrowserType();
            String version = box.getBrowserVersion();

            if (Strings.isNullOrEmpty(type)) 
            {
                List<String> availableBrowserTypes = lookup.getAvailableBoxFactoryNames();
                throw new MojoExecutionException("No browser type specified - use browser.type property to select" +
                        " a browser type or use '*' to delete all images.  Available browser types: " +
                        availableBrowserTypes);
            }

            if ("*".equals(type))
            {
                for (String curType : lookup.getAvailableBoxFactoryNames())
                {
                    deleteAllImagesForBoxType(curType, service);
                }
            }
            else
            {
                if ("*".equals(version))
                    deleteAllImagesForBoxType(type, service);
                else 
                {
                    //Single version image removal
                    BrowserBox browserBox = browserBox(service);
                    getLog().info("Deleting images for " + type + " " + box.getBrowserVersion()); //Version will now be filled in
                    browserBox.deleteImage();
                }
            }
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }
}
