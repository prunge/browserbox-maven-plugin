package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.io.IOException;
import java.net.URI;

/**
 * An API for controlling the browser itself in the running box.
 */
public interface BrowserControl 
{
    /**
     * Launches the browser and goes to the specified URL.
     * 
     * @param url the URL to go to.
     *            
     * @throws IOException if an I/O error occurs.
     * @throws BrowserBoxException if another error occurs.
     */
    public void launch(URI url) 
    throws IOException, BrowserBoxException;

    /**
     * Quits the running browser that was previously launched.
     * 
     * @throws IOException if an I/O error occurs.
     * @throws BrowserBoxException if another error occurs.
     */
    public void quit()
    throws IOException, BrowserBoxException;
}
