package au.net.causal.maven.plugins.browserbox;

import io.fabric8.maven.docker.util.GavLabel;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;

import java.io.File;
import java.time.Duration;
import java.util.EnumSet;
import java.util.Properties;
import java.util.Set;

/**
 * Project and global configuration that is not specific browser configuration.
 */
public class ProjectConfiguration
{
    private final MavenProject project;
    private final Settings settings;
    private final Set<BuildIntermediates> saveBuildIntermediates;
    private final Duration pollTime;

    /**
     * @param project the Maven project.
     * @param settings Maven global settings.
     */
    public ProjectConfiguration(MavenProject project, Settings settings, Set<BuildIntermediates> saveBuildIntermediates,
                                Duration pollTime)
    {
        this.project = project;
        this.settings = settings;
        this.saveBuildIntermediates = saveBuildIntermediates.isEmpty() ? EnumSet.noneOf(BuildIntermediates.class) : EnumSet.copyOf(saveBuildIntermediates);
        this.pollTime = pollTime;
    }

    /**
     * @return Maven project configuration.  Returns null if there is no project.
     */
    public MavenProject getProject()
    {
        return project;
    }

    /**
     * @return Maven global configuration.
     */
    public Settings getSettings()
    {
        return settings;
    }

    /**
     * @return a set of build intermediate types that should be saved.
     */
    public Set<BuildIntermediates> getSaveBuildIntermediates() 
    {
        return saveBuildIntermediates;
    }

    /**
     * @return Maven project properties.  Returns null if there is no project.
     */
    public Properties getProjectProperties()
    {
        if (getProject() == null)
            return null;

        return getProject().getProperties();
    }
    
    /**
     * @return POM label.
     */
    public GavLabel getPomLabel()
    {
        return new GavLabel(getProject().getGroupId(), getProject().getArtifactId(), getProject().getVersion());
    }

    /**
     * @return the project base directory.
     */
    public File getBaseDirectory()
    {
        File baseDirectory = null;
        if (getProject() != null)
            baseDirectory = getProject().getBasedir();

        if (baseDirectory == null)
            baseDirectory = new File(".");

        return baseDirectory;
    }

    /**
     * @return time to wait between polling for checking container executions, etc.
     */
    public Duration getPollTime()
    {
        return pollTime;
    }
}
