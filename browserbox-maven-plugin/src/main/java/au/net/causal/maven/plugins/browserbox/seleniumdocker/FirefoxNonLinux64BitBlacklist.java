package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.Maps;
import org.codehaus.plexus.util.PropertyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class FirefoxNonLinux64BitBlacklist implements BrowserVersionBlacklist 
{
    protected abstract Properties getKnownVersions()
    throws BrowserBoxException;

    @Override
    public Set<String> getKnownVersionBlacklist() throws BrowserBoxException 
    {
        return Maps.fromProperties(getKnownVersions()).entrySet().stream()
                    .filter(entry -> "false".equalsIgnoreCase(entry.getValue()))
                    .map(Map.Entry::getKey)
                    .sorted()
                    .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getKnownVersionWhitelist() throws BrowserBoxException 
    {
        return Maps.fromProperties(getKnownVersions()).entrySet().stream()
                .filter(entry -> "true".equalsIgnoreCase(entry.getValue()))
                .map(Map.Entry::getKey)
                .sorted()
                .collect(Collectors.toSet());
    }

    protected static class BlacklistResource
    {
        public static Properties fromJarFile(Path versionJar, String browserType)
        throws BrowserBoxException
        {
            try (URLClassLoader versionLoader = URLClassLoader.newInstance(new URL[] {versionJar.toUri().toURL()}, null))
            {
                URL blacklistResource = versionLoader.findResource(browserType + "-known-versions.properties");
                if (blacklistResource == null)
                    throw new BrowserBoxException("Missing blacklist properties for " + browserType + " in " + versionJar);

                Properties blacklist = new Properties();
                try (InputStream blaclistIs = blacklistResource.openStream())
                {
                    blacklist.load(blaclistIs);
                }

                return blacklist;
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("I/O error reading version JAR " + versionJar + ": " + e.getMessage(), e);
            }
        }
    }
}
