package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GeneratedImageFirefoxVersionResolver implements BrowserVersionResolver 
{
    private final Log log;
    private final BrowserVersionBlacklist versionBlacklist;
    private final DockerNaming dockerNaming = new DockerNaming();
    
    public GeneratedImageFirefoxVersionResolver(Log log, BrowserVersionBlacklist versionBlacklist)
    {
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(versionBlacklist, "versionBlacklist == null");
        this.log = log;
        this.versionBlacklist = versionBlacklist;
    }
    
    @Override
    public Tag tagForBrowserVersion(String version) throws BrowserBoxException 
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> availableBrowserVersions() 
    throws BrowserBoxException 
    {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        try 
        {
            FirefoxDesktopDownloadVersionScanner scanner = new FirefoxDesktopDownloadVersionScanner(log, versionBlacklist, executor);
            try 
            {
                return new ArrayList<>(scanner.readAvailableVersions(dockerNaming.firefoxInstallerUrl().toURL()));
            } 
            catch (IOException e) 
            {
                throw new BrowserBoxException("Error reading Firefox versions: " + e.getMessage(), e);
            }
        }
        finally 
        {
            executor.shutdown();
        }
    }

    @Override
    public String defaultVersion() throws BrowserBoxException 
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
