package au.net.causal.maven.plugins.browserbox;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class GenerateVersionsContext implements AutoCloseable
{
    private final Path versionsPath;
    private final Path imagesPath;
    private final Path knownVersionsPath;
    private final Path downloadsPath;
    private final Path driverVersionsPath;
    private final Map<String, Path> platformDriverVersionsPaths;
    private final Path driverCompatibilityPath;

    private final Properties originalVersions = new OrderedProperties();
    private final Properties versions = new OrderedProperties();
    private final Properties originalImages = new OrderedProperties();
    private final Properties images = new OrderedProperties();
    private final Properties originalKnownVersions = new OrderedProperties();
    private final Properties knownVersions = new OrderedProperties();
    private final Properties originalDownloads = new OrderedProperties();
    private final Properties downloads = new OrderedProperties();
    private final Properties originalDriverVersions = new OrderedProperties();
    private final Properties driverVersions = new OrderedProperties();
    private final Properties originalDriverCompatibility = new OrderedProperties();
    private final Properties driverCompatibility = new OrderedProperties();

    private final Map<String, Properties> platformDriverVersionsMap = new HashMap<>();
    private final Map<String, Properties> originalPlatformDriverVersionsMap = new HashMap<>();

    private final ExecutorService downloadExecutor = Executors.newFixedThreadPool(8);

    public GenerateVersionsContext(Path versionsPath, Path imagesPath, Path knownVersionsPath, Path downloadsPath, Path driverVersionsPath,
                                   Map<String, ? extends Path> platformDriverVersionsPaths,
                                   Path driverCompatibilityPath)
    throws IOException, BrowserBoxException      
    {
        this.versionsPath = versionsPath;
        this.imagesPath = imagesPath;
        this.knownVersionsPath = knownVersionsPath;
        this.downloadsPath = downloadsPath;
        this.driverVersionsPath = driverVersionsPath;
        this.platformDriverVersionsPaths = ImmutableMap.copyOf(platformDriverVersionsPaths);
        this.driverCompatibilityPath = driverCompatibilityPath;
        
        loadVersionsFile(getVersions(), versionsPath);
        copyProperties(getVersions(), originalVersions);
        loadVersionsFile(getKnownVersions(), knownVersionsPath);
        copyProperties(getKnownVersions(), originalKnownVersions);
        loadVersionsFile(getImages(), imagesPath);
        copyProperties(getImages(), originalImages);
        loadVersionsFile(getDownloads(), downloadsPath);
        copyProperties(getDownloads(), originalDownloads);
        loadVersionsFile(getDriverVersions(), driverVersionsPath);
        copyProperties(getDriverVersions(), originalDriverVersions);
        loadVersionsFile(getDriverCompatibility(), driverCompatibilityPath);
        copyProperties(getDriverCompatibility(), originalDriverCompatibility);
        
        //Platform specific driver versions
        for (Map.Entry<String, ? extends Path> entry : platformDriverVersionsPaths.entrySet())
        {
            Properties driverVersions = new OrderedProperties();
            loadVersionsFile(driverVersions, entry.getValue());
            platformDriverVersionsMap.put(entry.getKey(), driverVersions);
            Properties curOriginalDriverVersions = new OrderedProperties();
            copyProperties(driverVersions, curOriginalDriverVersions);
            originalPlatformDriverVersionsMap.put(entry.getKey(), curOriginalDriverVersions);
        }
    }

    private static void copyProperties(Properties from, Properties to)
    {
        to.putAll(from);
    }

    public Properties getVersions() 
    {
        return versions;
    }

    public Properties getImages() 
    {
        return images;
    }

    public Properties getKnownVersions() 
    {
        return knownVersions;
    }

    public Properties getDownloads() 
    {
        return downloads;
    }

    public Properties getDriverVersions() 
    {
        return driverVersions;
    }

    public Properties getPlatformDriverVersions(String platform)
    {
        return platformDriverVersionsMap.get(platform);
    }

    public Properties getDriverCompatibility()
    {
        return driverCompatibility;
    }

    public void saveVersions()
    throws IOException
    {
        saveVersionsFile(getVersions(), originalVersions, versionsPath);
    }
    
    public void saveImages()
    throws IOException
    {
        saveVersionsFile(getImages(), originalImages, imagesPath);
    }
    
    public void saveKnownVersions()
    throws IOException
    {
        saveVersionsFile(getKnownVersions(), originalKnownVersions, knownVersionsPath);
    }
    
    public void saveDownloads()
    throws IOException
    {
        saveVersionsFile(getDownloads(), originalDownloads, downloadsPath);
    }
    
    public void saveDriverVersions()
    throws IOException
    {
        saveVersionsFile(getDriverVersions(), originalDriverVersions, driverVersionsPath);
        
        for (Map.Entry<String, Path> entry : platformDriverVersionsPaths.entrySet()) 
        {
            String platform = entry.getKey();
            Properties driverVersions = platformDriverVersionsMap.get(platform);
            Properties curOriginalDriverVersions = originalPlatformDriverVersionsMap.get(platform);
            if (driverVersions != null && !driverVersions.isEmpty())
            {
                saveVersionsFile(driverVersions, curOriginalDriverVersions, entry.getValue());
            }
        }
    }

    public void saveDriverCompatibility()
    throws IOException
    {
        saveVersionsFile(getDriverCompatibility(), originalDriverCompatibility, driverCompatibilityPath);
    }

    private void loadVersionsFile(Properties versions, Path versionsPath)
    throws BrowserBoxException
    {
        if (Files.exists(versionsPath))
        {
            try (InputStream is = Files.newInputStream(versionsPath))
            {
                versions.load(is);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Error reading versions properties file " + versionsPath + ": " + e.getMessage(), e);
            }
        }
    }

    private void saveVersionsFile(Properties versions, Properties originalVersions, Path versionsPath)
    throws IOException
    {
        //If properties has not changed, don't save anything
        if (versions.equals(originalVersions))
            return;

        Files.createDirectories(versionsPath.getParent());
        try (OutputStream os = Files.newOutputStream(versionsPath))
        {
            versions.store(os, "Browser versions file.");
        }
    }

    /**
     * @return an executor for parallel downloading.
     */
    public ExecutorService getDownloadExecutor() 
    {
        return downloadExecutor;
    }

    @Override
    public void close()
    throws BrowserBoxException        
    {
        downloadExecutor.shutdown();
    }

    public Summary getSummary()
    {
        Summary summary = new Summary();

        summary.addValues(originalVersions, versions, versionsPath);
        summary.addValues(originalKnownVersions, knownVersions, knownVersionsPath);
        summary.addValues(originalImages, images, imagesPath);
        summary.addValues(originalDownloads, downloads, downloadsPath);
        summary.addValues(originalDriverVersions, driverVersions, driverVersionsPath);
        summary.addValues(originalDriverCompatibility, driverCompatibility, driverCompatibilityPath);

        for (Map.Entry<String, ? extends Path> entry : platformDriverVersionsPaths.entrySet())
        {
            String platform = entry.getKey();
            summary.addValues(originalPlatformDriverVersionsMap.get(platform), platformDriverVersionsMap.get(platform), entry.getValue());
        }

        return summary;
    }

    public static class Summary
    {
        private final Map<Path, SummaryElement> elements = new HashMap<>();

        private Summary(Map<? extends Path, ? extends SummaryElement> elements)
        {
            this.elements.putAll(elements);
        }

        public Summary()
        {
            this(Collections.emptyMap());
        }

        public int getAdded()
        {
            return elements.values().stream().reduce(0, (c, s) -> c + s.getAdded(), Math::addExact);
        }

        public int getRemoved()
        {
            return elements.values().stream().reduce(0, (c, s) -> c + s.getRemoved(), Math::addExact);
        }

        public int getModified()
        {
            return elements.values().stream().reduce(0, (c, s) -> c + s.getModified(), Math::addExact);
        }

        public void addValues(Properties original, Properties updated, Path file)
        {
            elements.putIfAbsent(file, new SummaryElement());
            elements.get(file).addValues(original, updated);
        }

        public Summary includingOnly(PathMatcher matcher)
        {
            return new Summary(Maps.filterKeys(this.elements, matcher::matches));
        }

        @Override
        public String toString()
        {
            /*
            StringBuilder buf = new StringBuilder();

            buf.append(elements.keySet()).append(" - ");

            if (getAdded() != 0)
                buf.append("Added: ").append(getAdded());
            if (getRemoved() != 0)
            {
                if (buf.length() > 0)
                    buf.append(", ");
                buf.append("Removed: ").append(getRemoved());
            }
            if (getModified() != 0)
            {
                if (buf.length() > 0)
                    buf.append(", ");

                buf.append("Modified: ").append(getModified());
            }

            if (getAdded() == 0 && getRemoved() == 0 && getModified() == 0)
                buf.append("No changes");

            return buf.toString();
            */

            if (getAdded() == 0 && getRemoved() == 0 && getModified() == 0)
                return("No changes");

            //Only display info on entries that actually have changes
            return elements.entrySet().stream()
                                      .filter(entry -> entry.getValue().getAdded() != 0 || entry.getValue().getRemoved() != 0 || entry.getValue().getModified() != 0)
                                      .map(entry -> entry.getKey().getFileName().toString() + ": " + entry.getValue().toString())
                                      .collect(Collectors.joining(", "));
        }
    }

    public static class SummaryElement
    {
        private int added;
        private int removed;
        private int modified;

        public int getAdded()
        {
            return added;
        }

        public int getRemoved()
        {
            return removed;
        }

        public int getModified()
        {
            return modified;
        }

        public void addValues(Properties original, Properties updated)
        {
            MapDifference<?, ?> diff = Maps.difference(original, updated);
            added += diff.entriesOnlyOnRight().size();
            removed += diff.entriesOnlyOnLeft().size();
            modified += diff.entriesDiffering().size();
        }

        @Override
        public String toString()
        {
            StringBuilder buf = new StringBuilder();

            if (getAdded() != 0)
                buf.append("Added: ").append(getAdded());
            if (getRemoved() != 0)
            {
                if (buf.length() > 0)
                    buf.append(", ");
                buf.append("Removed: ").append(getRemoved());
            }
            if (getModified() != 0)
            {
                if (buf.length() > 0)
                    buf.append(", ");

                buf.append("Modified: ").append(getModified());
            }

            if (getAdded() == 0 && getRemoved() == 0 && getModified() == 0)
                buf.append("No changes");

            return buf.toString();
        }
    }
}
