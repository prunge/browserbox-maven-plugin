package au.net.causal.maven.plugins.browserbox.box;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker for bundled box factories that are bundled with the plugin (as opposed to ones in third-party JARs).
 * This qualifier is used to give third-party box factories precendence over the bundled ones in case they are overridden.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BrowserBoxBundled
{
}
