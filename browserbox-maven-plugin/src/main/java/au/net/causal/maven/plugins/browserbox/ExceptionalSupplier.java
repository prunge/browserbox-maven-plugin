package au.net.causal.maven.plugins.browserbox;

public interface ExceptionalSupplier<T, E extends Exception>
{
	public T get()
	throws E;
}
