package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

public class TagDetails extends Tag 
{
    private final String digest;
    private final List<String> aliases;
    
    public TagDetails(String name)
    {
        this(name, null, ImmutableList.of());
    }
    
    public TagDetails(String name, String digest, Collection<String> aliases) 
    {
        super(name);
        this.digest = digest;
        this.aliases = ImmutableList.copyOf(aliases);
    }

    public String getDigest() 
    {
        return digest;
    }

    public List<String> getAliases() 
    {
        return aliases;
    }
}
