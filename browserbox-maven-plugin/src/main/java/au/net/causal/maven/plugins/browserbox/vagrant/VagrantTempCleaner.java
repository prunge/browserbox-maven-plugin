package au.net.causal.maven.plugins.browserbox.vagrant;

import com.google.common.io.MoreFiles;
import com.google.common.io.RecursiveDeleteOption;
import org.apache.maven.plugin.logging.Log;
import org.glassfish.jersey.internal.util.SimpleNamespaceResolver;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Clean up Vagrant's tmp directory after packaging a VM.  Some versions of Vagrant leave these directories
 * lying around consuming disk space.
 */
public class VagrantTempCleaner 
{
    private final XPath xPath;
    
    private final Path tempDirectory;
    private final Log log;
    private final Set<Path> prePackageExistingDirectories = new LinkedHashSet<>();
    
    public VagrantTempCleaner(Log log)
    {
        this(log, Paths.get(System.getProperty("user.home")).resolve(".vagrant.d").resolve("tmp"));
    }
    
    public VagrantTempCleaner(Log log, Path tempDirectory)
    {
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(tempDirectory, "tempDirectory == null");
        this.tempDirectory = tempDirectory;
        xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(new SimpleNamespaceResolver("ovf", "http://schemas.dmtf.org/ovf/envelope/1"));
        this.log = log;
    }
    
    public void prePackage()
    throws IOException
    {
        prePackageExistingDirectories.clear();
        try (Stream<Path> list = Files.list(tempDirectory)) 
        {
            list.filter(Files::isDirectory)
                    .filter(path -> path.getFileName().toString().startsWith("vagrant-package-"))
                    .forEach(prePackageExistingDirectories::add);
        }
    }
    
    public void postPackage(String vmName)
    throws IOException
    {
        Objects.requireNonNull(vmName, "vmName == null");

        try (Stream<Path> list = Files.list(tempDirectory))
        {
            for (Path dir : list.filter(Files::isDirectory)
                    .filter(path -> !prePackageExistingDirectories.contains(path))
                    .filter(path -> path.getFileName().toString().startsWith("vagrant-package-"))
                    .collect(Collectors.toList()))
            {
                //We have a potential directory - look inside and scan OVF
                Path boxOvfFile = dir.resolve("box.ovf");
                if (Files.exists(boxOvfFile)) 
                {
                    try (BufferedReader ovfReader = Files.newBufferedReader(boxOvfFile)) 
                    {
                        String curVmName = xPath.evaluate("//ovf:VirtualSystem/@ovf:id", new InputSource(ovfReader));
                        if (vmName.equals(curVmName))
                            cleanDirectory(dir);
                    } 
                    catch (XPathException e) 
                    {
                        throw new IOException("XPath evaluation error: " + e, e);
                    }
                }
            }

        }
    }
    
    protected void cleanDirectory(Path directory)
    throws IOException
    {
        log.info("Removing Vagrant temp directory " + directory.toAbsolutePath());
        MoreFiles.deleteRecursively(directory, RecursiveDeleteOption.ALLOW_INSECURE);
    }
}
