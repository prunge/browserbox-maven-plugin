package au.net.causal.maven.plugins.browserbox;

/**
 * When should container images be checked for updates from the remote repository?  For Docker, this will control
 * whether the Docker registry is queried for an update for containers.
 */
public enum ImageUpdateMode
{
    /**
     * Check for an update every execution.
     */
    ALWAYS,

    /**
     * Check after the default amount of time for updates.
     */
    ON,

    /**
     * Do not check for updates.  Offline mode.
     */
    OFF
}
