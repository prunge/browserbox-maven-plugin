package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.android.Platform;
import io.fabric8.maven.docker.config.RegistryAuthConfiguration;
import io.fabric8.maven.docker.log.LogOutputSpecFactory;
import io.fabric8.maven.docker.service.ImagePullManager;
import io.fabric8.maven.docker.service.ServiceHub;
import io.fabric8.maven.docker.util.AuthConfigFactory;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.shared.filtering.MavenReaderFilter;
import org.apache.maven.shared.filtering.MavenResourcesFiltering;
import org.apache.maven.shared.invoker.Invoker;
import org.codehaus.plexus.archiver.manager.ArchiverManager;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.repository.RemoteRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public class BoxContext
{
    private final ExceptionalSupplier<DockerService, BrowserBoxException> dockerService;
    private final AuthConfigFactory authConfigFactory;
    private final Path tempDirectory;
    private final Path globalConfigDirectory;
    private final Path androidHome;
    private final ImageUpdateMode imageUpdateMode;
    private final Log log;
    private final LogOutputSpecFactory logSpecFactory;
    private final MavenSession session;
    private final MavenResourcesFiltering resourcesFiltering;
    private final MavenReaderFilter readerFilter;
    private final RepositorySystem repositorySystem;
    private final RepositorySystemSession repositorySystemSession;
    private final List<RemoteRepository> remoteRepositories;
    private final ArchiverManager archiverManager;
    private final ImagePullManager imagePullManager;
    private final Invoker invoker;
    private final Platform platform;
    private final String seleniumVersion;
    private final Artifact pluginArtifact;
    private final Artifact internalVncViewerArtifact;
    private final RegistryAuthConfiguration dockerAuthConfiguration;
    private final boolean dockerSkipExtendedAuth;
    private final Long dockerSharedMemorySize;

    //Toolchain, etc. goes here

    public BoxContext(ExceptionalSupplier<DockerService, BrowserBoxException> dockerService, AuthConfigFactory authConfigFactory,
                      Path tempDirectory, Path globalConfigDirectory, ImageUpdateMode imageUpdateMode, Log log,
                      LogOutputSpecFactory logSpecFactory,
                      MavenSession session, MavenResourcesFiltering resourcesFiltering,
                      MavenReaderFilter readerFilter, RepositorySystem repositorySystem,
                      RepositorySystemSession repositorySystemSession, List<RemoteRepository> remoteRepositories,
                      ArchiverManager archiverManager, ImagePullManager imagePullManager,
                      RegistryAuthConfiguration dockerAuthConfiguration, boolean dockerSkipExtendedAuth,
                      Invoker invoker, 
                      Path androidHome,
                      Platform platform,
                      String seleniumVersion,
                      Artifact pluginArtifact,
                      Artifact internalVncViewerArtifact,
                      Long dockerSharedMemorySize)
    {
        Objects.requireNonNull(dockerService, "dockerService == null");
        Objects.requireNonNull(authConfigFactory, "authConfigFactory == null");
        Objects.requireNonNull(tempDirectory, "tempDirectory == null");
        Objects.requireNonNull(globalConfigDirectory, "globalConfigDirectory == null");
        Objects.requireNonNull(imageUpdateMode, "updateMode == null");
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(logSpecFactory, "logSpecFactory == null");
        Objects.requireNonNull(session, "session == null");
        Objects.requireNonNull(resourcesFiltering, "resourcesFiltering == null");
        Objects.requireNonNull(readerFilter, "readerFilter == null");
        Objects.requireNonNull(repositorySystem, "repositorySystem == null");
        Objects.requireNonNull(repositorySystemSession, "repositorySystemSession == null");
        Objects.requireNonNull(remoteRepositories, "remoteRepositories == null");
        Objects.requireNonNull(archiverManager, "archiverManager == null");
        Objects.requireNonNull(imagePullManager, "imagePullManager == null");
        Objects.requireNonNull(invoker, "invoker == null");
        Objects.requireNonNull(platform, "platform == null");
        Objects.requireNonNull(seleniumVersion, "seleniumVersion == null");
        Objects.requireNonNull(pluginArtifact, "pluginArtifact == null");
        Objects.requireNonNull(internalVncViewerArtifact, "internalVncViewerArtifact == null");

        this.dockerService = dockerService;
        this.authConfigFactory = authConfigFactory;
        this.tempDirectory = tempDirectory;
        this.globalConfigDirectory = globalConfigDirectory;
        this.imageUpdateMode = imageUpdateMode;
        this.log = log;
        this.logSpecFactory = logSpecFactory;
        this.session = session;
        this.resourcesFiltering = resourcesFiltering;
        this.readerFilter = readerFilter;
        this.repositorySystem = repositorySystem;
        this.repositorySystemSession = repositorySystemSession;
        this.remoteRepositories = remoteRepositories;
        this.archiverManager = archiverManager;
        this.imagePullManager = imagePullManager;
        this.invoker = invoker;
        this.androidHome = androidHome;
        this.platform = platform;
        this.seleniumVersion = seleniumVersion;
        this.pluginArtifact = pluginArtifact;
        this.internalVncViewerArtifact = internalVncViewerArtifact;
        this.dockerAuthConfiguration = dockerAuthConfiguration;
        this.dockerSkipExtendedAuth = dockerSkipExtendedAuth;
        this.dockerSharedMemorySize = dockerSharedMemorySize;
    }

    public ServiceHub getDockerServiceHub()
    throws BrowserBoxException
    {
        return dockerService.get().getServiceHub();
    }

    public String getDockerHostAddress()
    throws BrowserBoxException
    {
        return dockerService.get().getDockerHostAddress();
    }

    public AuthConfigFactory getAuthConfigFactory()
    {
        return authConfigFactory;
    }

    public Path getGlobalConfigDirectory()
    throws IOException
    {
        if (!Files.exists(globalConfigDirectory))
            Files.createDirectories(globalConfigDirectory);

        return globalConfigDirectory;
    }

    public Path getTempDirectory()
    throws IOException
    {
        if (!Files.exists(tempDirectory))
            Files.createDirectories(tempDirectory);

        return tempDirectory;
    }

    public Log getLog()
    {
        return log;
    }

    public LogOutputSpecFactory getLogSpecFactory()
    {
        return logSpecFactory;
    }

    public ImageUpdateMode getImageUpdateMode()
    {
        return imageUpdateMode;
    }

    public MavenSession getSession()
    {
        return session;
    }

	public RepositorySystem getRepositorySystem()
	{
		return repositorySystem;
	}

	public RepositorySystemSession getRepositorySystemSession()
	{
		return repositorySystemSession;
	}

	public List<RemoteRepository> getRemoteRepositories()
	{
		return remoteRepositories;
	}

    public ArchiverManager getArchiverManager()
    {
        return archiverManager;
    }

    public ImagePullManager getImagePullCacheManager()
    {
        return imagePullManager;
    }

    public MavenReaderFilter getReaderFilter() 
    {
        return readerFilter;
    }

    public Invoker getInvoker()
    {
        return invoker;
    }
    
    public Path getAndroidHome() 
    {
        return androidHome;
    }

    public Platform getPlatform()
    {
        return platform;
    }

    public String getSeleniumVersion()
    {
        return seleniumVersion;
    }

    public Artifact getPluginArtifact()
    {
        return pluginArtifact;
    }

    public Artifact getInternalVncViewerArtifact()
    {
        return internalVncViewerArtifact;
    }

    public RegistryAuthConfiguration getDockerAuthConfiguration()
    {
        return dockerAuthConfiguration;
    }

    public boolean isDockerSkipExtendedAuth()
    {
        return dockerSkipExtendedAuth;
    }

    public Long getDockerSharedMemorySize()
    {
        return dockerSharedMemorySize;
    }
}
