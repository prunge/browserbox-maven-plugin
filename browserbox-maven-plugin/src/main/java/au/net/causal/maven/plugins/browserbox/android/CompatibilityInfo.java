package au.net.causal.maven.plugins.browserbox.android;

public class CompatibilityInfo
{
    private int minMajorVersion;
    private int maxMajorVersion;

    public CompatibilityInfo(int minMajorVersion, int maxMajorVersion)
    {
        this.minMajorVersion = minMajorVersion;
        this.maxMajorVersion = maxMajorVersion;
    }

    public int getMinMajorVersion()
    {
        return minMajorVersion;
    }

    public int getMaxMajorVersion()
    {
        return maxMajorVersion;
    }

    @Override
    public String toString()
    {
        return getMinMajorVersion() + "-" + getMaxMajorVersion();
    }
}
