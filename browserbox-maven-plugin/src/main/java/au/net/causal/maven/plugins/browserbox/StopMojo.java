package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.TunnelSession;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.IOException;

@Mojo(name="stop", defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST, requiresProject = false)
public class StopMojo extends AbstractBrowserBoxMojo
{
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> dockerService)
    throws DockerAccessException, MojoExecutionException
    {
        try
        {
            BrowserBox browserBox = browserBox(dockerService);

            //Attempt to clean up any tunnel session
            TunnelSession tunnelSession = (TunnelSession)getPluginContext().remove(TUNNEL_SESSION_KEY_PREFIX + browserBox.getName());
            if (tunnelSession != null)
            {
                getLog().info("Shutting down tunnel session");
                tunnelSession.close();
            }

            stopBrowserContainer(browserBox);
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }
}
