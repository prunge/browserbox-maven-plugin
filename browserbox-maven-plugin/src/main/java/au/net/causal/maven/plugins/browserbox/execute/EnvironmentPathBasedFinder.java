package au.net.causal.maven.plugins.browserbox.execute;

import com.google.common.base.Splitter;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class EnvironmentPathBasedFinder extends GenericPathBasedFinder
{
    public EnvironmentPathBasedFinder()
    {
        super(readEnvironmentPath());
    }
    
    private static List<? extends Path> readEnvironmentPath()
    {
        String pathString = System.getenv("PATH");
        if (pathString == null)
            return Collections.emptyList();

        return Splitter.on(File.pathSeparatorChar)
                .splitToList(pathString)
                .stream()
                .map(EnvironmentPathBasedFinder::resolveHome)
                .map(Paths::get)
                .collect(Collectors.toList());
    }
    
    private static String resolveHome(String pathSegment)
    {
        if (pathSegment.startsWith("~"))
            pathSegment = System.getProperty("user.home") + pathSegment.substring(1);
        
        return pathSegment;
    }
}
