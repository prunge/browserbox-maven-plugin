package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

public interface TunnelSession extends AutoCloseable 
{
    @Override
    public void close() throws BrowserBoxException;
}
