package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpWebConnection;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;

import javax.ws.rs.core.HttpHeaders;
import java.io.IOError;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Scans APKMirror website for Chrome APK versions and downloads.
 * <p>
 *
 * To avoid hitting the website too hard, a cached results registry is also used.  Only versions that do not exist on
 * this cached resolver will be read from the website.
 */
public class ChromeApkMirrorVersionRegistry implements VersionRegistry
{
    /**
     * Parses the version of the APK from its item text.
     */
    private static final Pattern VERSION_PATTERN = Pattern.compile("[\\d\\.]+");

    /**
     * Parses version from the Minimum Version column in the download table.
     * e.g. 'Android 7.0+', 'Android 10+'
     */
    private static final Pattern MINIMUM_ANDROID_VERSION_PATTERN = Pattern.compile("android ([\\d][\\d\\.]+)\\+", Pattern.CASE_INSENSITIVE);

    /**
     * Http status code when we're being rate limited.
     */
    private static final int HTTP_TOO_MANY_REQUESTS = 429;

    private final URL searchUrl;
    private final WebClient webClient;

    /**
     * Tracks APK download URLs (and intermediate URIs) that don't actually lead to a valid APK (not the right platform, etc.).
     */
    private final Set<URL> invalidDownloadUrls = new LinkedHashSet<>();

    public ChromeApkMirrorVersionRegistry(URL searchUrl, WebClient webClient)
    {
        Objects.requireNonNull(searchUrl, "searchUrl == null");
        Objects.requireNonNull(webClient, "webClient == null");
        this.searchUrl = searchUrl;
        this.webClient = webClient;
    }

    public ChromeApkMirrorVersionRegistry(URL searchUrl, Log log)
    {
        this(searchUrl, createWebClient(log));
    }

    public ChromeApkMirrorVersionRegistry(Log log)
    {
        this(defaultUrl(), log);
    }

    /**
     * Override to limit the result count.  By default, there is no limit.
     *
     * @return maximum number of results to return when being queried.
     */
    protected int getResultLimit()
    {
        return Integer.MAX_VALUE;
    }
    
    private static WebClient createWebClient(Log log)
    {
        WebClient webClient = new WebClient();

        //Log requests so we can track and make sure we don't overload remote server
        HttpWebConnection webConnection = new HttpWebConnection(webClient)
        {
            @Override
            public WebResponse getResponse(WebRequest request)
            throws IOException
            {
                //TODO turn down logging on this one
                log.info("APK mirror download request for " + request.getUrl());
                return super.getResponse(request);
            }
        };

        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setCssEnabled(false);
        webClient.setWebConnection(webConnection);
        return webClient;
    }

    private static URL defaultUrl()
    {
        try
        {
            return new URL("https://www.apkmirror.com/uploads/?q=chrome");
        }
        catch (MalformedURLException e)
        {
            //Should not happen
            throw new IOError(e);
        }
    }
    
    private String parseVersionFromItemText(String itemText)
    {
        String bestVersion = null;
        Matcher m = VERSION_PATTERN.matcher(itemText);
        while (m.find())
        {
            String matchedVersion = m.group();
            if (bestVersion == null || bestVersion.length() <= matchedVersion.length())
                bestVersion = matchedVersion;
        }
        
        return bestVersion;
    }
    
    private URL readDownloadsFromPage(Query query, URL listPageUrl, Collection<? super Item> downloads)
    throws IOException
    {
        Set<URL> ignoredUrls;
        if (query instanceof ChomeApkMirrorQuery)
            ignoredUrls = ((ChomeApkMirrorQuery)query).getIgnoredUrls();
        else
            ignoredUrls = ImmutableSet.of();

        HtmlPage p1 = webClient.getPage(listPageUrl);
        for (DomNode item : p1.querySelectorAll("#primary .appRowTitle a"))
        {
            //Early exit if we hit the result limit
            if (downloads.size() >= getResultLimit())
                return null;

            if (item instanceof HtmlAnchor)
            {
                HtmlAnchor link = (HtmlAnchor) item;
                String itemText = link.getTextContent();
                String version = parseVersionFromItemText(itemText);

                if (version != null && !query.getIgnoredVersions().contains(version))
                {
                    URL intermediateDownloadUrl = new URL(listPageUrl, link.getHrefAttribute());
                    if (!ignoredUrls.contains(intermediateDownloadUrl))
                    {
                      URL downloadUrlForVersion = readApkLink(intermediateDownloadUrl);
                      if (downloadUrlForVersion != null && !ignoredUrls.contains(downloadUrlForVersion))
                          downloads.add(new Item(version, downloadUrlForVersion));
                    }
                }
            }
        }

        //Find next page if it exists
        HtmlAnchor nextPageLink = p1.querySelector("a.nextpostslink");
        if (nextPageLink != null)
            return new URL(listPageUrl, nextPageLink.getHrefAttribute());
        else
            return null;
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException 
    {
        List<Item> downloads = new ArrayList<>();
        
        try 
        {
            URL nextPageUrl = searchUrl;
            do 
            {
                nextPageUrl = readDownloadsFromPage(query, nextPageUrl, downloads);
            } 
            while (nextPageUrl != null);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
        catch (FailingHttpStatusCodeException e)
        {
            if (e.getStatusCode() == HTTP_TOO_MANY_REQUESTS) 
            {
                //This one we might want to handle with partial results
                String secondsToWait = e.getResponse().getResponseHeaderValue(HttpHeaders.RETRY_AFTER);
                BrowserBoxException ex = new BrowserBoxException("Rate limited attempting to read versions, retry in " + secondsToWait + " seconds: " + e.getMessage(), e);
                return new ItemList(downloads, Collections.singletonList(ex));
            }
            else
                throw new BrowserBoxException(e.getMessage(), e);
        }
        
        return new ItemList(downloads, Collections.emptyList());
    }

    private URL readApkLink(URL pageUrl)
    throws IOException
    {
        HtmlPage page = webClient.getPage(pageUrl);

        NavigableMap<Double, URL> downloadPageUrlsByMinimumAndroidVersion = new TreeMap<>();
        for (DomNode row : page.querySelectorAll(".variants-table .table-row"))
        {
            boolean x86Match = false;
            double minAndroidVersion = Double.POSITIVE_INFINITY; //We look for the min of all these, so positive infinity for unparseables will sort them last
            for (DomNode cell : row.querySelectorAll(".table-cell"))
            {
                String cellText = cell.getTextContent().trim();

                if (cellText.equalsIgnoreCase("x86"))
                    x86Match = true;

                Matcher minAndroidVersionMatcher = MINIMUM_ANDROID_VERSION_PATTERN.matcher(cellText);
                if (minAndroidVersionMatcher.matches())
                {
                    try
                    {
                        minAndroidVersion = Double.parseDouble(minAndroidVersionMatcher.group(1));
                    }
                    catch (NumberFormatException e)
                    {
                        System.out.println("Warning: failed to parse minimum android version: " + minAndroidVersion);
                    }
                }
            }
            if (x86Match)
            {
                HtmlAnchor link = row.querySelector("a");
                downloadPageUrlsByMinimumAndroidVersion.put(minAndroidVersion, new URL(pageUrl, link.getHrefAttribute()));
            }
        }

        //The actual download will be for the lowest matching version
        Map.Entry<Double, URL> lowestVersionEntry = downloadPageUrlsByMinimumAndroidVersion.firstEntry();
        if (lowestVersionEntry == null)
        {
            invalidDownloadUrls.add(pageUrl);
            return null;
        }
        URL downloadPageUrl = lowestVersionEntry.getValue();

        System.out.println("Download page URL: " + downloadPageUrl);

        HtmlPage downloadPage = webClient.getPage(downloadPageUrl);
        HtmlAnchor downloadLink = downloadPage.querySelector("a.downloadButton");
        if (downloadLink == null)
        {
            invalidDownloadUrls.add(pageUrl);
            return null;
        }
        
        URL downloadFinalPageUrl = new URL(downloadPageUrl, downloadLink.getHrefAttribute());
        System.out.println("Download final page APK: " + downloadFinalPageUrl);

        Page downloadFinalPage = webClient.getPage(downloadFinalPageUrl);
        if (!(downloadFinalPage instanceof HtmlPage))
        {
            //TODO this actually downloads the whole APK!  Need to do a HEAD request or something for this
            //Special case, sometimes the actual APK is available from this link
            String contentType = downloadFinalPage.getWebResponse().getContentType();
            if ("application/vnd.android.package-archive".equals(contentType))
                return downloadFinalPageUrl;
            else
                throw new IOException("Failed to read final download page " + downloadFinalPageUrl + ": " + downloadFinalPage.getWebResponse().getStatusMessage() + " (" + downloadFinalPage.getWebResponse().getContentType() + ")"); 
        }
        
        HtmlForm downloadForm = ((HtmlPage)downloadFinalPage).querySelector("#filedownload");
        WebRequest request = downloadForm.getWebRequest(null);
        URL downloadUrl = request.getUrl();
        System.out.println("Download URL: " + downloadUrl);
        
        return downloadUrl;
    }

    public Set<URL> getInvalidDownloadUrls()
    {
      return Collections.unmodifiableSet(invalidDownloadUrls);
    }

    public static class ChomeApkMirrorQuery extends Query
    {
        private final Set<URL> ignoredUrls;

        public ChomeApkMirrorQuery(Set<String> ignoredVersions, Set<URL> ignoredUrls)
        {
            super(ignoredVersions);
            this.ignoredUrls = ImmutableSet.copyOf(ignoredUrls);
        }

        public Set<URL> getIgnoredUrls()
        {
            return ignoredUrls;
        }
    }
}
