package au.net.causal.maven.plugins.browserbox.execute;

import com.github.katjahahn.parser.sections.rsrc.version.VersionInfo;
import com.google.common.collect.ImmutableMap;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class ExeAttributeMatcher implements Predicate<Path> 
{
    private final Map<String, ? extends Predicate<String>> attributeMatcherMap;
    
    public ExeAttributeMatcher(String name, Pattern valueMatcher)
    {
        this(name, valueMatcher.asPredicate());
    }
    
    public ExeAttributeMatcher(String name, Predicate<String> valueMatcher)
    {
        this(ImmutableMap.of(name, valueMatcher));
    }
    
    public ExeAttributeMatcher(Map<String, ? extends Predicate<String>> attributeMatcherMap)
    {
        this.attributeMatcherMap = ImmutableMap.copyOf(attributeMatcherMap);
    }
    
    @Override
    public boolean test(Path file) 
    {
        Set<String> matchedAttributes = new HashSet<>();
        for (VersionInfo vi : VersionInfo.parseFromResources(file.toFile()))
        {
            for (Map.Entry<String, ? extends Predicate<String>> entry : attributeMatcherMap.entrySet()) 
            {
                String value = vi.getVersionStrings().get(entry.getKey());
                if (value != null) 
                {
                    value = value.trim();
                    if (entry.getValue().test(value))
                        matchedAttributes.add(entry.getKey());
                }
            }
        }
        
        return matchedAttributes.equals(attributeMatcherMap.keySet());
    }
}
