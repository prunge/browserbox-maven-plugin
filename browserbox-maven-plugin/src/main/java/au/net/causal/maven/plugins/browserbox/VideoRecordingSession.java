package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.VideoControl;

import java.nio.file.Path;

public class VideoRecordingSession
{
    private final VideoControl.Recording recording;
    private final Path videoFile;

    public VideoRecordingSession(VideoControl.Recording recording, Path videoFile) 
    {
        this.recording = recording;
        this.videoFile = videoFile;
    }

    public VideoControl.Recording getRecording() 
    {
        return recording;
    }
    
    public Path getVideoFile() 
    {
        return videoFile;
    }
}
