package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.gargoylesoftware.htmlunit.HttpWebConnection;
import com.gargoylesoftware.htmlunit.TopLevelWindow;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableDataCell;
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener;
import com.google.common.io.ByteStreams;
import org.apache.http.client.utils.URIBuilder;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Downloads Windows updates from the Microsoft Update Catalog site.
 * <p>
 *
 * This needs to run a whole javascript-enabled client just to get access to the ever-changing
 * download URLs for the update binaries themselves.
 */
public class WindowsUpdateCatalogDownloader implements AutoCloseable
{
    private final WebClient webClient;
    private final URI baseUrl;

    /**
     * Creates a downloader with a default-configured client.
     *
     * @param log logger to use.
     */
    public WindowsUpdateCatalogDownloader(Log log)
    {
        this(createWebClient(log), defaultBaseUrl());
    }

    /**
     * Creates a downloader with a custom web client.
     *
     * @param webClient the web client to use.
     * @param baseUrl the base URL of the update catalog.
     */
    public WindowsUpdateCatalogDownloader(WebClient webClient, URI baseUrl)
    {
        Objects.requireNonNull(webClient, "webClient == null");
        Objects.requireNonNull(baseUrl, "baseUrl == null");
        this.webClient = webClient;
        this.baseUrl = baseUrl;
    }

    /**
     * @return the default Microsoft update catalog site, without any query parameters.
     */
    private static URI defaultBaseUrl()
    {
        return URI.create("https://www.catalog.update.microsoft.com/search.aspx");
    }

    /**
     * Creates the default web client to use to access the catalog site.
     *
     * @param log for logging.
     *
     * @return the created client.
     */
    private static WebClient createWebClient(Log log)
    {
        WebClient webClient = new WebClient();

        //Do nothing for warnings
        webClient.setIncorrectnessListener((message, origin) -> {});
        webClient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());

        //Log requests for resources at debug level - it's sometimes useful to know just how many
        //files are being requested (such as CSS, JS, etc.)
        HttpWebConnection webConnection = new HttpWebConnection(webClient)
        {
            @Override
            public WebResponse getResponse(WebRequest request)
            throws IOException
            {
                log.debug("Download request for " + request.getUrl());
                return super.getResponse(request);
            }
        };

        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.setWebConnection(webConnection);
        return webClient;
    }

    /**
     * Closes the web client.
     */
    public void close()
    {
        webClient.close();
    }

    /**
     * Downloads a Windows update for a KB number.  The binary file is downloaded and saved to an output stream.
     *
     * @param kb the Microsoft KB number and text whose update to look up.
     * @param downloadOut the binary file is downloaded and saved to this stream.
     *
     * @throws IOException if an I/O error occurs.
     * @throws UpdateNotFoundException if an update with the specified KB/text could not be found.
     * @throws BrowserBoxException if an error occurs traversing the catalog site.
     */
    public void download(WindowsUpdateKB kb, OutputStream downloadOut)
    throws IOException, UpdateNotFoundException, BrowserBoxException
    {
        URL downloadUrl = lookUpDownloadUrl(kb);
        try (InputStream downloadIs = downloadUrl.openStream())
        {
            ByteStreams.copy(downloadIs, downloadOut);
        }
    }

    /**
     * Downloads a Windows update for a KB number to file.
     *
     * @param kb the Microsoft KB number and text whose update to look up.
     * @param downloadFile save the download to this file.

     * @throws IOException if an I/O error occurs.
     * @throws UpdateNotFoundException if an update with the specified KB/text could not be found.
     * @throws BrowserBoxException if an error occurs traversing the catalog site.
     */
    public void downloadToFile(WindowsUpdateKB kb, Path downloadFile)
    throws IOException, UpdateNotFoundException, BrowserBoxException
    {
        try (OutputStream out = Files.newOutputStream(downloadFile))
        {
            download(kb, out);
        }
    }

    /**
     * Looks up the download URL for a KB number from the catalog.  The returned URL can be used to download the
     * update binary, but has a limited lifespan.
     *
     * @param kb the Microsoft KB number and text whose update to look up.
     *
     * @return a download URL.
     *
     * @throws IOException if an I/O error occurs.
     * @throws UpdateNotFoundException if an update with the specified KB/text could not be found.
     * @throws BrowserBoxException if an error occurs traversing the catalog site.
     */
    public URL lookUpDownloadUrl(WindowsUpdateKB kb)
    throws IOException, UpdateNotFoundException, BrowserBoxException
    {
        URI catalogUrl;
        try
        {
            catalogUrl = new URIBuilder(baseUrl).addParameter("q", kb.getKb()).build();
        }
        catch (URISyntaxException e)
        {
            throw new BrowserBoxException("Failed to build catalog URL: " + e.getMessage(), e);
        }
        HtmlPage p1 = webClient.getPage(catalogUrl.toURL());

        WebWindow mainWindow = webClient.getCurrentWindow();

        //Find a table row containing text and click on the button in the last cell
        List<HtmlTableDataCell> els = p1.getByXPath("//text()[contains(.,'" + kb.getFilterText() + "')]/ancestor::*[self::tr]/td[last()]");
        HtmlTableDataCell cell = els.get(els.size() - 1);
        HtmlButtonInput downloadButton = cell.getFirstByXPath("input[@type = 'button']");

        if (downloadButton == null)
            throw new UpdateNotFoundException("Download button not fouund for kb:" + kb + ", text=" + kb.getFilterText());

        downloadButton.click();

        //New window appears, pick it up by finding any other window which is not the main one
        webClient.waitForBackgroundJavaScript(10_000L);
        List<TopLevelWindow> otherWindows = webClient.getTopLevelWindows().stream().filter(w -> w != mainWindow).collect(Collectors.toList());
        if (otherWindows.size() != 1)
            throw new BrowserBoxException("Expected only one other window, got " + otherWindows.size());

        TopLevelWindow downloadWindow = otherWindows.get(0);

        //Now find the link in the download window
        HtmlPage downloadPage = (HtmlPage)downloadWindow.getEnclosedPage();
        HtmlAnchor anchor = downloadPage.getFirstByXPath("//a[contains(text(), '.msu')]");

        return new URL(anchor.getHrefAttribute());
    }

    /**
     * Occurs when a download can not be found in the catalog.
     */
    public static class UpdateNotFoundException extends BrowserBoxException
    {
        /**
         * Creates an <code>UpdateNotFoundException</code>.
         */
        public UpdateNotFoundException()
        {
            super();
        }

        /**
         * Creates an <code>UpdateNotFoundException</code> with a detail message.
         *
         * @param message the detail message.
         */
        public UpdateNotFoundException(String message)
        {
            super(message);
        }
    }
}
