package au.net.causal.maven.plugins.browserbox.versionstore;

import java.net.URL;
import java.util.Objects;

/**
 * A single download item that has both a version and a download URL.  The download URL 
 * may be local or remote.
 */
public class Item
{
    private final String version;
    private final URL url;
    
    public Item(String version, URL url)
    {
        Objects.requireNonNull(version, "version == null");
        Objects.requireNonNull(url, "url == null");
        
        this.version = version;
        this.url = url;
    }
    
    /**
     * @return the URL that this item can be downloaded from.  May be local or remote.
     */
    public URL getUrl()
    {
        return url;
    }
    
    /**
     * @return the version of the item.
     */
    public String getVersion()
    {
        return version;
    }

    @Override
    public String toString()
    {
        return getVersion() + ":" + getUrl().toExternalForm();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(version, item.version) &&
                Objects.equals(url, item.url);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(version, url);
    }
}
