package au.net.causal.maven.plugins.browserbox.vagrant;

public class BoxDefinition
{
    private final String name;
    private final String version;
    private final String provider;

    public BoxDefinition(String name, String version, String provider)
    {
        this.name = name;
        this.version = version;
        this.provider = provider;
    }

    public String getName()
    {
        return name;
    }

    public String getVersion()
    {
        return version;
    }

    public String getProvider()
    {
        return provider;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("BoxDefinition{");
        sb.append("name='").append(name).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append(", provider='").append(provider).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
