package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.Collections;
import java.util.Set;

public class EmptyBrowserVersionBlacklist implements BrowserVersionBlacklist 
{
    @Override
    public Set<String> getKnownVersionBlacklist()
    {
        return Collections.emptySet();
    }

    @Override
    public Set<String> getKnownVersionWhitelist() 
    {
        return Collections.emptySet();
    }
}
