package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class VersionPropertiesArchiveResolver implements BrowserVersionResolver
{
    private final BrowserVersionTools versionTools = new BrowserVersionTools();
    
    protected abstract VersionsResources readBrowserVersionResources()
    throws BrowserBoxException;
    
    @Override
    public TagDetails tagForBrowserVersion(String version)
    throws BrowserBoxException
    {
        VersionsResources r = readBrowserVersionResources();
        Set<Map.Entry<String, String>> allVersions = Maps.fromProperties(r.getVersions()).entrySet();
        Tag result = versionTools.findBrowserVersion(version, allVersions, 
                             Map.Entry::getValue,
                             entry -> new TagDetails(entry.getKey(), r.getImages().getProperty(entry.getKey()), Collections.emptyList()));
        return (TagDetails)result;
    }
    
    @Override
    public List<String> availableBrowserVersions()
    throws BrowserBoxException
    {
        VersionsResources r = readBrowserVersionResources();
        
        return r.getVersions().values().stream()
                .map(Object::toString)
                .distinct()
                .sorted(String.CASE_INSENSITIVE_ORDER)
                .collect(Collectors.toList());
    }

    protected static class VersionsResources
    {
        private final Properties versions;
        private final Properties images;
        
        public VersionsResources(Properties versions, Properties images)
        {
            this.versions = versions;
            this.images = images;
        }

        public Properties getVersions()
        {
            return versions;
        }

        public Properties getImages()
        {
            return images;
        }
        
        public static VersionsResources fromJarFile(Path versionJar, String browserType)
        throws BrowserBoxException
        {
            try (URLClassLoader versionLoader = URLClassLoader.newInstance(new URL[] {versionJar.toUri().toURL()}, null))
            {
                URL versionsResource = versionLoader.findResource(browserType + "-versions.properties");
                URL imagesResource = versionLoader.findResource(browserType + "-images.properties");
                if (versionsResource == null || imagesResource == null)
                    throw new BrowserBoxException("Missing versions/images properties for " + browserType + " in " + versionJar);

                Properties versions = new Properties();
                Properties images = new Properties();
                try (InputStream versionsIs = versionsResource.openStream())
                {
                    versions.load(versionsIs);
                }
                try (InputStream imagesIs = imagesResource.openStream())
                {
                    images.load(imagesIs);
                }
                
                return new VersionsResources(versions, images);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("I/O error reading version JAR " + versionJar + ": " + e.getMessage(), e);
            }
        }
    }

    @Override
    public String defaultVersion() throws BrowserBoxException 
    {
        //Pick the last version if it exists
        List<String> allVersions = availableBrowserVersions();
        if (allVersions.isEmpty())
            return null;
        
        return allVersions.get(allVersions.size() - 1);
    }
}
