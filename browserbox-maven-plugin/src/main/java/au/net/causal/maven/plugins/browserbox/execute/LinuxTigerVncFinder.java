package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

public class LinuxTigerVncFinder extends EnvironmentPathBasedFinder
{
    private static final Pattern VNC_PATTERN = Pattern.compile("^vncviewer$", Pattern.CASE_INSENSITIVE);

    private final TigerVncTools tigerVnc = new TigerVncTools();
    
    @Override
    public Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, 
                                      BoxConfiguration boxConfiguration,
                                      ProjectConfiguration projectConfiguration, BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> executable = findInSearchPath(new FileNamePatternMatcher(VNC_PATTERN).and(
                                                    new ExecutableFileMatcher()));
        if (executable.isPresent())
        {
            Path vncPasswordFile = tigerVnc.generatePasswordFile(connectionInfo, context);
            
            Commandline commandline =  new Commandline(executable.get().toAbsolutePath().toString());
            String host = connectionInfo.getUri().getHost();
            int port = connectionInfo.getUri().getPort();
            commandline.addArguments(args(host + ":" + port, "-PasswordFile", vncPasswordFile.toAbsolutePath().toString()));
            
            return Optional.of(commandline);
        }
        
        return Optional.empty();
    }
}
