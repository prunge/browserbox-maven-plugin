package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Multimap;
import org.apache.maven.plugin.logging.Log;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class BrowserVersionScanner 
{
    private final Log log;
    
    private TokenResponse savedToken;
    
    /*
    https://docs.docker.com/registry/spec/api/#listing-repositories
    https://stackoverflow.com/questions/28320134/how-to-list-all-tags-for-a-docker-image-on-a-remote-registry
    https://registry.hub.docker.com/v2/repositories/library/postgres/tags/
    https://docs.docker.com/registry/spec/auth/token/#how-to-authenticate
    https://registry.hub.docker.com/v2/selenium/standalone-firefox-debug/manifests/latest
    */
    
    public BrowserVersionScanner(Log log)
    {
        Objects.requireNonNull(log, "log == null");
        this.log = log;
    }
    
    public List<? extends TaggedBrowserVersion> process(String repositoryName, TagBrowserVersionReader reader)
    throws BrowserBoxException
    {
        List<? extends Tag> tags = readTags(repositoryName);
        return process(tags, reader);
    }
    
    public TaggedBrowserVersion processTag(Tag tag, TagBrowserVersionReader reader)
    throws BrowserBoxException
    {
        String browserVersion = reader.readBrowserVersion(tag);
        if (browserVersion == null)
            return null;
        else
            return new TaggedBrowserVersion(tag, browserVersion);
    }
    
    public List<? extends TaggedBrowserVersion> process(Collection<? extends Tag> tags, TagBrowserVersionReader reader)
    throws BrowserBoxException
    {
        List<TaggedBrowserVersion> results = new ArrayList<>(tags.size());

        //Depending on browser type, map each Docker tag to a browser version
        for (Tag tag : tags)
        {
            String browserVersion = reader.readBrowserVersion(tag);
            if (browserVersion == null)
                log.warn("Browser version not found for tag " + tag.getName());
            else
                results.add(new TaggedBrowserVersion(tag, browserVersion));
        }

        return results;
    }
    
    public List<? extends TagDetails> readTags(String repositoryName)
    {
        Multimap<String, String> digestTags = ArrayListMultimap.create();
        readTagsAndDigests(repositoryName, digestTags);
        
        //We now have digest keys and tag name values
        //Special empty string key maps tags that don't have digests for whatever reason
        
        //Remove 'latest' tag as it's special and never the one we want
        digestTags.values().remove("latest");
        
        //Keep the specials
        Collection<String> keepSpecialTags = digestTags.removeAll("");

        //Keep the longest version for each digest
        BiMap<String, String> keepDigestTags = HashBiMap.create();
        Multimap<String, String> extraTagAliases = ArrayListMultimap.create();
        for (Map.Entry<String, Collection<String>> digestToTagEntry : digestTags.asMap().entrySet())
        {
            String digest = digestToTagEntry.getKey();
            List<String> tags = new ArrayList<>(digestToTagEntry.getValue());
            tags.sort(Comparator.comparingInt(String::length).reversed().thenComparing(String.CASE_INSENSITIVE_ORDER));
            String firstTag = tags.get(0);
            keepDigestTags.put(digest, firstTag);
            extraTagAliases.putAll(firstTag, tags.subList(1, tags.size()));
        }
        
        List<String> sortedTags = new ArrayList<>(keepDigestTags.values());
        sortedTags.sort(String.CASE_INSENSITIVE_ORDER);
        
        List<TagDetails> results = new ArrayList<>(sortedTags.size() + keepSpecialTags.size());
        
        for (String tag : keepSpecialTags)
        {
            results.add(new TagDetails(tag));
        }
        for (String tag : sortedTags)
        {
            String imageId = keepDigestTags.inverse().get(tag);
            Collection<String> aliases = extraTagAliases.get(tag);
            results.add(new TagDetails(tag, imageId, aliases));
        }
        
        return results;
    }

    private void readTagsAndDigests(String repositoryName, Multimap<String, String> digestTags)
    {
        String baseUrl = "https://registry.hub.docker.com/v2/" + repositoryName;

        Client client = ClientBuilder.newClient();
        TagsResponse response = getWithAuth(client, URI.create(baseUrl + "/tags/list"), TagsResponse.class);

        for (String tag : response.getTags())
        {
            log.debug("Reading manifest for tag " + tag);
            Response genericManifestResponse = genericGetWithAuth(client, URI.create(baseUrl + "/manifests/" + tag));

            if (genericManifestResponse.getMediaType().getSubtype().contains("+prettyjws")) 
            {
                log.debug("Wrong response content type, possibly V1 manifest, ignoring...");
                digestTags.put("", tag);
            }
            else
            {
                ManifestResponse manifestResponse = genericManifestResponse.readEntity(ManifestResponse.class);
                if (manifestResponse.getConfig() == null) 
                {
                    log.debug("Missing manifest for " + tag);
                    digestTags.put("", tag);
                }
                else
                    digestTags.put(manifestResponse.getConfig().getDigest(), tag);
            }
        }
    }

    private <T> T getWithAuth(Client client, URI requestUri, Class<T> responseType)
    {
        Response response = genericGetWithAuth(client, requestUri);
        return response.readEntity(responseType);
    }

    private Response genericGetWithAuth(Client client, URI requestUri)
    {
        try
        {
            Invocation.Builder b = client.target(requestUri)
                    .request(MediaType.APPLICATION_JSON,
                            "application/vnd.docker.distribution.manifest.v2+json",
                            "application/vnd.docker.distribution.manifest.v1+json");
            if (savedToken != null)
                b = b.header(HttpHeaders.AUTHORIZATION, "Bearer " + savedToken.getToken());

            Response response = b.get();
            if (response.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode())
                throw new NotAuthorizedException(response);
            else if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL)
                throw new WebApplicationException(response);
            
            return response;
        }
        catch (NotAuthorizedException e)
        {
            Map<String, String> bearerParameters = new LinkedHashMap<>();
            for (Object challenge : e.getChallenges())
            {
                String sChallenge = Objects.toString(challenge, "");
                if (sChallenge.toLowerCase(Locale.ENGLISH).startsWith("bearer "))
                {
                    String bearerContent = sChallenge.substring("bearer ".length(), sChallenge.length());
                    bearerParameters.clear();
                    parseBearerContent(bearerContent, bearerParameters);
                }
            }

            String realm = bearerParameters.remove("realm");
            if (realm == null)
                throw e;
            
            TokenResponse token = authenticate(client, URI.create(realm), bearerParameters);
            savedToken = token;

            return client.target(requestUri)
                    .request(MediaType.APPLICATION_JSON,
                            "application/vnd.docker.distribution.manifest.v2+json",
                            "application/vnd.docker.distribution.manifest.v1+json")
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token.getToken())
                    .get();
        }
    }

    private static void parseBearerContent(String bearerContent, Map<? super String, ? super String> parameterMap)
    {
        Pattern splitter = Pattern.compile(Pattern.quote("="));

        //e.g. realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:selenium/standalone-firefox-debug:pull"
        for (String part : bearerContent.split(Pattern.quote("\",")))
        {
            //realm="https://auth.docker.io/token
            String[] keyValue = splitter.split(part, 2);
            if (keyValue.length >= 2)
            {
                String key = keyValue[0];
                String value = keyValue[1];
                if (value.startsWith("\""))
                    value = value.substring(1);
                if (value.endsWith("\""))
                    value = value.substring(0, value.length() - 1);

                parameterMap.put(key, value);
            }
        }
    }

    private static TokenResponse authenticate(Client client, URI uri, Map<String, String> parameters)
    {
        WebTarget target = client.target(uri);
        for (Map.Entry<String, String> entry : parameters.entrySet())
        {
            target = target.queryParam(entry.getKey(), entry.getValue());
        }
        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .get(TokenResponse.class);
    }

    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TagsResponse
    {
        private String name;
        private final List<String> tags = new ArrayList<>();

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public List<String> getTags()
        {
            return tags;
        }

        public void setTags(List<String> tags)
        {
            this.tags.clear();
            this.tags.addAll(tags);
        }
    }

    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ManifestResponse
    {
        private Config config = new Config();

        public Config getConfig()
        {
            return config;
        }

        public void setConfig(Config config)
        {
            this.config = config;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Config
        {
            private String digest;

            public String getDigest()
            {
                return digest;
            }

            public void setDigest(String digest)
            {
                this.digest = digest;
            }
        }
    }

    @XmlRootElement
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TokenResponse
    {
        private String token;

        @JsonProperty("expires_in")
        private long expiresIn;

        //TODO could also read date in here
        //"issued_at":"2017-09-20T21:16:31.931887312Z"

        public String getToken()
        {
            return token;
        }

        public void setToken(String token)
        {
            this.token = token;
        }

        public long getExpiresIn()
        {
            return expiresIn;
        }

        public void setExpiresIn(long expiresIn)
        {
            this.expiresIn = expiresIn;
        }
    }

    public static class TaggedBrowserVersion
    {
        private final Tag tag;
        private final String browserVersion;

        public TaggedBrowserVersion(Tag tag, String browserVersion)
        {
            this.tag = tag;
            this.browserVersion = browserVersion;
        }

        public Tag getTag()
        {
            return tag;
        }

        public String getBrowserVersion()
        {
            return browserVersion;
        }

        @Override
        public String toString()
        {
            return browserVersion + " -> " + tag;
        }
    }
}
