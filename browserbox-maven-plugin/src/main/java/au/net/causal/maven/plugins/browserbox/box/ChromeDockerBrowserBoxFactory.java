package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;

public class ChromeDockerBrowserBoxFactory extends SeleniumDockerBrowserBoxFactory 
{
    public ChromeDockerBrowserBoxFactory() 
    {
        super("chrome");
    }

    @Override
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
            throws BrowserBoxException
    {
        initializeDefaults(boxConfiguration, context);
        ImageReference baseImageName = dockerBaseImageName(boxConfiguration, projectConfiguration, context);
        return new ChromeDockerBrowserBox(boxConfiguration, projectConfiguration, context, baseImageName);
    }
}
