package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.net.URL;

/**
 * A version registry that also has the capability of saving the contents of items.
 * This store stores the full contents of items, including download data.
 */
public interface VersionedItemStore extends VersionRegistry
{
    /**
     * Saves the contents of an item to the store.
     * 
     * @param item the item to save.
     *             
     * @return the URL to the content of the newly saved item.        
     *             
     * @throws BrowserBoxException if an error occurs.
     */
    public URL saveItemContents(Item item)
    throws BrowserBoxException;
}
