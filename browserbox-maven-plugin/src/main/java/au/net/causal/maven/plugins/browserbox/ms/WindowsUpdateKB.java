package au.net.causal.maven.plugins.browserbox.ms;

import java.util.Objects;

/**
 * Specifies a particular version of a Windows Update based on KB number.  Updates are specified by both
 * a KB number (e.g. KB4056564) and some additional text to get a particular version of that KB
 * (e.g. "2018-05 Security Update for Windows Server 2008 for x86-based Systems (KB4056564)").
 * <p>
 *
 * To work out the filter text required, use the Windows Update catalog site at
 * <a href="https://www.catalog.update.microsoft.com/search.aspx">https://www.catalog.update.microsoft.com/search.aspx</a>,
 * find the update you want and look in the table and use the text in the row for the desired version.
 */
public class WindowsUpdateKB
{
    private final String kb;
    private final String filterText;

    /**
     * Creates an update reference.
     *
     * @param kb the KB number, not including the 'KB' prefix.
     * @param filterText additional text used to pick a specific version of the KB from the Windows Update catalog.
     */
    public WindowsUpdateKB(String kb, String filterText)
    {
        Objects.requireNonNull(kb, "kb == null");
        Objects.requireNonNull(filterText, "filterText == null");

        this.kb = kb;
        this.filterText = filterText;
    }

    public String getKb()
    {
        return kb;
    }

    public String getFilterText()
    {
        return filterText;
    }

    @Override
    public String toString()
    {
        return getKb() + ":" + getFilterText();
    }
}
