package au.net.causal.maven.plugins.browserbox.ms;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Downloads Chocolatey as a nuget package from the Chocolatey site and saves it to a local directory.
 *
 * @see <a href="https://chocolatey.org/docs/installation#completely-offline-install">Chocolatey Software Installation</a>
 */
public class ChocolateyDownloader
{
    private final URL chocolateyUrl;

    /**
     * Creates a chocolatey downloader using the default chocolatey URL.
     */
    public ChocolateyDownloader()
    {
        this(defaultChocolateyUrl());
    }

    /**
     * Creates a chocolatey downloader using the specified chocolatey URL.
     * 
     * @param chocolateyUrl the chocolatey nuget package URL.
     */
    public ChocolateyDownloader(URL chocolateyUrl)
    {
        this.chocolateyUrl = chocolateyUrl;
    }

    /**
     * @return the default Chocolatey package URL.
     */
    private static URL defaultChocolateyUrl()
    {
        try
        {
            //Use 0.10.15 because newer versions aren't compatible with older versions of Windows/powershell
            return new URL("https://chocolatey.org/api/v2/package/chocolatey/0.10.15");
        }
        catch (MalformedURLException e)
        {
            //Should not happen, the URL above is valid
            throw new Error("Bad chocolatey URL.");
        }
    }

    /**
     * Creates a download session for the package but does not actually perform the download yet.
     *
     * @return a URL connection to download the chocolatey package.
     *
     * @throws IOException if an I/O error occurs.
     */
    protected HttpURLConnection createDownloadSession()
    throws IOException
    {
        return (HttpURLConnection)chocolateyUrl.openConnection();
    }

    /**
     * Downloads the Chocolatey nuget package.
     *
     * @param toLocalFile the local nuget file to save the content to.
     *
     * @throws IOException if an I/O error occurs.
     */
    public void downloadChocolatey(Path toLocalFile)
    throws IOException
    {
        HttpURLConnection con = createDownloadSession();

        try
        {
            long size = con.getContentLengthLong();
            try (InputStream is = con.getInputStream())
            {
                long numBytesDownloaded = Files.copy(is, toLocalFile, StandardCopyOption.REPLACE_EXISTING);

                //Validate size is correct in case download was truncated
                if (numBytesDownloaded != size)
                    throw new IOException("Download truncated - expected " + size + " bytes but downloaded only " + numBytesDownloaded);
            }
        }
        finally
        {
            con.disconnect();
        }
    }

    /**
     * Downloads the Chocolatey nuget package and extracts its content.
     *
     * @param downloadDirectory the directory to download and extract Chocolatey to.
     *
     * @throws IOException if an I/O error occurs.
     */
    public void downloadAndExtractChocolatey(Path downloadDirectory)
    throws IOException
    {
        Path nugetFile = downloadDirectory.resolve("chocolatey.nupkg");
        downloadChocolatey(nugetFile);

        //Nuget packages are just zip files
        try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(nugetFile)))
        {
            ZipEntry entry = zis.getNextEntry();
            while (entry  != null)
            {
                Path file = downloadDirectory.resolve(entry.getName());
                if (entry.isDirectory())
                    Files.createDirectories(file);
                else
                {
                    Files.createDirectories(file.getParent());
                    Files.copy(zis, file, StandardCopyOption.REPLACE_EXISTING);
                }

                entry = zis.getNextEntry();
            }
        }
    }
}
