package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.BuildIntermediates;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.SeleniumFirefoxCustomBaseImageGenerator;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.Tag;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.IOException;
import java.nio.file.Path;

public class SeleniumFirefoxCustomVersionBrowserBox extends FirefoxDockerBrowserBox 
{
    private final Tag fromTag;
    private final String browserVersion;
    private final ImageReference seleniumBaseImageName;
    
    public SeleniumFirefoxCustomVersionBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context,
                                                  Tag fromTag, String browserVersion, ImageReference seleniumBaseImageName) 
    {
        super(boxConfiguration, projectConfiguration, context, seleniumBaseImageName);
        this.fromTag = fromTag;
        this.browserVersion = browserVersion;
        this.seleniumBaseImageName = seleniumBaseImageName;
    }

    @Override
    public void prepareImage() 
    throws BrowserBoxException 
    {
        try 
        {
            //If Selenium base image already exists do not generate again
            if (!getContext().getDockerServiceHub().getQueryService().hasImage(seleniumBaseImageName.getName())) 
            {
                //Generate custom Selenium base image
                getContext().getLog().info("Generating from " + fromTag.getName() + " with browser version " + browserVersion);

                try 
                {
                    Path tempDir = getContext().getTempDirectory(); //TODO maybe have a dedicated git dir?
                    SeleniumFirefoxCustomBaseImageGenerator generator = new SeleniumFirefoxCustomBaseImageGenerator(getContext(), getProjectConfiguration(), getBoxConfiguration(), tempDir);
                    SeleniumFirefoxCustomBaseImageGenerator.GeneratedImage gi = generator.generate(fromTag, browserVersion);
                    
                    super.prepareImage();
                    
                    if (!gi.isPreexisting() && !getProjectConfiguration().getSaveBuildIntermediates().contains(BuildIntermediates.BOX_IMAGES)) 
                    {
                        getContext().getLog().info("Removing tag for intermediate image " + gi.getGeneratedImageName().getName());
                        getContext().getDockerServiceHub().getDockerAccess().removeImage(gi.getGeneratedImageName().getName(), false);
                    }
                } 
                catch (IOException | MojoExecutionException e) 
                {
                    throw new BrowserBoxException(e);
                }
            }
            else //generated image already exists, just put our stuff over the top
                super.prepareImage();
        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException(e);
        }
    }
}
