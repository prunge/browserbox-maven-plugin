package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.vagrant.BoxDefinition;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.BoxAddOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.BoxStatus;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.CreatePackageOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.DestroyOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant.StatusOptions;
import au.net.causal.maven.plugins.browserbox.vagrant.VagrantException;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxException;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager.ShowVmInfoOptions;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager.StorageAttachOptions;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenReaderFilterRequest;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WindowsBoxMaker 
{
    private static final List<? extends VmDrive> IDE_CONTROLLER_DRIVES = ImmutableList.of(
                                                                            new VmDrive("IDE Controller", 0, 0),
                                                                            new VmDrive("IDE Controller", 0, 1),
                                                                            new VmDrive("IDE Controller", 1, 0),
                                                                            new VmDrive("IDE Controller", 0, 1));
    private static final List<? extends VmDrive> SATA_CONTROLLER_DRIVES = ImmutableList.of(
                                                                            new VmDrive("SATA Controller", 0, 0),
                                                                            new VmDrive("SATA Controller", 1, 0));

    private final VirtualBoxManager vbox;
    private final Vagrant vagrant;
    private final ProjectConfiguration projectConfig;
    private final BoxContext context;
    private final Clock clock = Clock.systemDefaultZone();
    private final Duration commandTimeout;
    
    public WindowsBoxMaker(VirtualBoxManager vbox, Vagrant vagrant, 
                           ProjectConfiguration projectConfig, BoxContext context, 
                           Duration commandTimeout)
    {
        Objects.requireNonNull(vbox, "vbox == null");
        Objects.requireNonNull(vagrant, "vagrant == null");
        Objects.requireNonNull(projectConfig, "projectConfig == null");
        Objects.requireNonNull(context, "context == null");
        Objects.requireNonNull(commandTimeout, "commandTimeout == null");
        
        this.vbox = vbox;
        this.vagrant = vagrant;
        this.projectConfig = projectConfig;
        this.context = context;
        this.commandTimeout = commandTimeout;
    }
    
    protected Log getLog()
    {
        return context.getLog();
    }
    
    private void generateVagrantBox(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir, String targetBoxName)
    throws BrowserBoxException
    {
        try 
        {
            getLog().info("Creating Vagrant package");
            vagrant.createPackage(new CreatePackageOptions(winIePrototypeDir, prototypeBox.getMachineName()));

            getLog().info("Registering Vagrant box");
            Path boxFile = winIePrototypeDir.resolve("package.box");
            BoxAddOptions addOptions = new BoxAddOptions(new BoxDefinition(targetBoxName, "0", "virtualbox"), boxFile);
            addOptions.setForce(true);
            vagrant.boxAdd(addOptions);
            
            //Destroy the prototype box, we don't need it any more
            getLog().info("Removing prototype box");
            vagrant.destroy(new DestroyOptions(winIePrototypeDir, prototypeBox.getMachineName()));
            
            //Delete the box file as well to save space
            Files.deleteIfExists(boxFile);
            
            getLog().info("Vagrant box generated and is now registered as '" + targetBoxName + "'");
        }
        catch (VagrantException | IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private void removeSetupThings(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir)
    throws BrowserBoxException
    {
        Path ieVagrantFile = winIePrototypeDir.resolve("Vagrantfile");
        
        try 
        {
            //Copy Vagrantfile to this directory (cable off)
            copyFilteredResource("WinIEPrototype-phase3.Vagrantfile", ieVagrantFile, source.getFilterProperties());

            //In this next section Windows can have problems starting up for some reason, so there's some retry
            //logic for that
            int finalStartupRetries = 0;
            int maxFinalStartupRetries = 10;
            boolean setupComplete = false;
            VirtualBoxException lastSetupError = null;
            while (!setupComplete && finalStartupRetries < maxFinalStartupRetries)
            {
                finalStartupRetries++;

                //Bring phase 3 machine up
                startVmUsingVBox(prototypeBox, winIePrototypeDir, "3");

                //Remove admin user and proxy settings
                try
                {
                    VirtualBoxManager.GuestControlRunOptions runOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\scripts\\cleanup.bat");
                    runOptions.setUserName("Administrator");
                    vbox.guestControlRun(runOptions);

                    setupComplete = true;
                    lastSetupError = null;
                }
                catch (VirtualBoxException e)
                {
                    //Might fail because box sometimes fails to start up for some reason
                    //It's safe to retry this logic
                    if (e.getExitCode() == 1)
                    {
                        getLog().warn("Error occurred applying final setup - maybe box crashed? - will attempt retry (" +
                                              finalStartupRetries + " of " + maxFinalStartupRetries + "): " + e);
                        lastSetupError = e;
                    }
                }
            }

            //It never succeeded, abort everything
            if (lastSetupError != null)
                throw lastSetupError;

            //And bring it down again
            stopVmUsingVBox(prototypeBox, winIePrototypeDir, "3");
            
            //Turn networking on after shutdown
            vbox.modifyVm(new VirtualBoxManager.ModifyVmOptions(prototypeBox.getMachineName(), "cableconnected1", "on"));
            
            //Revert to the good Vagrantfile (cable on)
            copyFilteredResource("WinIEPrototype-final.Vagrantfile", ieVagrantFile, source.getFilterProperties());
        }
        catch (VagrantException | VirtualBoxException | IOException | InterruptedException | TimeoutException | MavenFilteringException | JSchException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private void setUpSoftwareWithNetworking(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir)
    throws BrowserBoxException
    {
        //We have a new Vagrantfile for networking support
        Path ieVagrantFile = winIePrototypeDir.resolve("Vagrantfile");
        
        try
        {
            //Copy Vagrantfile to this directory
            copyFilteredResource("WinIEPrototype-phase3.Vagrantfile", ieVagrantFile, source.getFilterProperties());
            
            //Bring up machine with this new Vagrantfile, now with networking
            Vagrant.UpOptions upOptions = new Vagrant.UpOptions(winIePrototypeDir, prototypeBox.getMachineName());
            vagrant.up(upOptions);
            
            //Might not have waited long enough, so verify with vbox
            getLog().info("Waiting for VM to boot (phase: 3)");
            VirtualBoxManager.GuestControlRunOptions runOptions = new VirtualBoxManager.GuestControlRunOptions("WinIEPrototype", "cmd.exe");
            runOptions.setProgramArgs("/c", "echo", "GoodMorning");
            runOptions.setUserName(prototypeBox.getUser());
            runOptions.setPassword(prototypeBox.getPassword());
            runOptions.setLogOutput(false);
            String response = virtualBoxRunUntilUp(vbox, runOptions, commandTimeout);
            if (!response.trim().equals("GoodMorning"))
                throw new BrowserBoxException("Incorrect response while waiting for machine to boot: " + response);

            //Ensure there are permissions to execute powershell files
            //powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted
            VirtualBoxManager.GuestControlRunOptions powershellSetupRun = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe");
            powershellSetupRun.setUserName("Administrator");
            String powershellSetupResult = vbox.guestControlRun(powershellSetupRun);
            context.getLog().info("Powershell setup result: " + powershellSetupResult);
            //TODO verify result

            //Run software installer script
            VirtualBoxManager.GuestControlRunOptions installerRun = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe");
            if (source.getEdgeDriverVersion() == null)
                installerRun.setProgramArgs("-File", "c:\\Scripts\\installer.ps1");
            else if (source.getEdgeDriverVersion().isOnDemand()) //Special case for later Edge versions to use Windows internal installer for webdriver
                installerRun.setProgramArgs("-File", "c:\\Scripts\\installer-od.ps1");
            else
                installerRun.setProgramArgs("-File", "c:\\Scripts\\installer2.ps1", "-edgeDriverVersion", source.getEdgeDriverVersion().getRawVersion());
            
            installerRun.setUserName("Administrator");
            String installerResult = vbox.guestControlRun(installerRun);
            context.getLog().info("Installer result: " + installerResult);
            //TODO verify result
            
            //Run firewall configurations script
            VirtualBoxManager.GuestControlRunOptions firewallRun = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\scripts\\firewallrules.bat");
            firewallRun.setUserName("Administrator");
            String firewallResult = vbox.guestControlRun(firewallRun);
            context.getLog().info("Firewall result: " + firewallResult);
            //TODO verify result
            
            vagrant.halt(new Vagrant.HaltOptions(winIePrototypeDir, prototypeBox.getMachineName()));
        }
        catch (IOException | VagrantException | VirtualBoxException | InterruptedException | TimeoutException | MavenFilteringException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private VmDrive findFirstCdDrive(PrototypeBox prototypeBox)
    throws VirtualBoxException, BrowserBoxException
    {
        //TODO there are cases where the VM doesn't have any CD drives so we might have to add one programatically
        Map<String, String> vmInfo = vbox.showVmInfo(new ShowVmInfoOptions(prototypeBox.getMachineName()));

        Set<String> expectedCdDriveFiles = ImmutableSet.of("emptydrive", "additions");
        
        VmDrive lastUnusedDrive = null;
        for (VmDrive drive : IDE_CONTROLLER_DRIVES)
        {
            String file = vmInfo.get(drive.getVmInfoKey());
            if (expectedCdDriveFiles.contains(file))
                return drive;
            if ("none".equals(file))
                lastUnusedDrive = drive;
        }
        
        //If there are no CD drives at all, just use the last unused drive and storageattach will make one for us
        if (lastUnusedDrive != null)
        {
            context.getLog().info("No CD drive exists on the VM, creating one...");
            vbox.storageAttach(new StorageAttachOptions(prototypeBox.getMachineName(), lastUnusedDrive.getControllerName(), 
                    lastUnusedDrive.getDevice(), lastUnusedDrive.getPort(), "dvddrive", "emptydrive"));
            return lastUnusedDrive;
        }
        
        throw new BrowserBoxException("Could not find CD drive for VM.");
    }

    private void setUpGuestAdditionsAndSoftware(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir)
    throws BrowserBoxException
    {
        //We have a new Vagrantfile for networking support
        Path ieVagrantFile = winIePrototypeDir.resolve("Vagrantfile");

        try
        {
            //Copy Vagrantfile to this directory
            copyFilteredResource("WinIEPrototype-phase2.Vagrantfile", ieVagrantFile, source.getFilterProperties());

            //Before the VM starts, ensure a CD drive exists in case we need to make one
            VmDrive firstCdDrive = findFirstCdDrive(prototypeBox);
            getLog().debug("First CD drive: " + firstCdDrive.getControllerName() + " " + firstCdDrive.getDevice() + " " + firstCdDrive.getPort());
            
            startVmUsingSsh(prototypeBox, winIePrototypeDir, "2");

            //At this point the VM is up

            //Detect the shell type being run - depending on the VM, we could be in a Windows cmd shell or a cygwin bash shell
            int shellTypeCheckResult = sshExecute(prototypeBox, commandTimeout, "export TEST", System.out);
            boolean isUnixShellType = (shellTypeCheckResult == 0);
            getLog().info("UNIX shell type: " + isUnixShellType);

            //Ensure there are permissions to execute powershell files
            //powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted
            String powershellSetupCommand = isUnixShellType ?
                    "powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted < /dev/null" :
                    "powershell -Command Set-ExecutionPolicy -ExecutionPolicy Unrestricted < NUL";
            int result = sshExecute(prototypeBox, commandTimeout, powershellSetupCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up powershell.");

            //Firstly, install any updates
            for (WindowsUpdateKB update : source.getWindowsUpdates())
            {
                String fileName = fileNameForWindowsUpdate(update);
                String absoluteFileName = "c:\\windowsupdates\\" + fileName;
                String sshEscapedFileName = absoluteFileName.replace("\\", "\\\\");

                getLog().info("Installing windows update KB" + update.getKb() + "...");

                String installUpdateCommand = isUnixShellType ?
                        "powershell -File c:\\\\scripts\\\\installwindowsupdate.ps1 -UpdateFile " + sshEscapedFileName + " < /dev/null" :
                        "powershell -File c:\\scripts\\installwindowsupdate.ps1 -UpdateFile " + absoluteFileName + " < NUL";
                int updateResult = sshExecute(prototypeBox, commandTimeout, installUpdateCommand, System.out);
                //3010 means pending restart but that is greater than SSH's capabilities so the code is checked in the script
                //0 exit means OK, 12 exit means something went wrong
                if (updateResult != 0)
                    throw new BrowserBoxException("Error #" + updateResult + " installing update.");

                //Now shutdown/restart and wait for everything to be up again
                context.getLog().info("Rebooting VM to finish installing update...");
                stopVmUsingSsh(prototypeBox, winIePrototypeDir, "2");
                startVmUsingSsh(prototypeBox, winIePrototypeDir, "2");
                context.getLog().info("Update KB" + update.getKb() + " install completed.");
            }

            //Install guest additions by doing SSH stuff
            //VmDrive firstCdDrive = findFirstCdDrive(prototypeBox);
            
            //need to detect/find first removable drive
            //VBoxManage storageattach WinIEPrototype --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium additions
            //vbox.storageAttach(new StorageAttachOptions(prototypeBox.getMachineName(), "IDE Controller", 0, 1, "dvddrive", "additions"));
            vbox.storageAttach(new StorageAttachOptions(prototypeBox.getMachineName(), firstCdDrive.getControllerName(), firstCdDrive.getDevice(), firstCdDrive.getPort(), "dvddrive", "additions"));

            String guestAdditionsCommand = isUnixShellType ?
                                            "/cygdrive/c/scripts/guestadditions.bat" :
                                            "c:\\scripts\\guestadditions.bat";
            result = sshExecute(prototypeBox, commandTimeout, guestAdditionsCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " installing guest additions into VM.");

            getLog().info("Guest additions installed");
            
            vbox.storageAttach(new StorageAttachOptions(prototypeBox.getMachineName(), firstCdDrive.getControllerName(), firstCdDrive.getDevice(), firstCdDrive.getPort(), "dvddrive", "emptydrive"));

            //Set up SSL certificates
            getLog().info("Configuring SSL certificates");
            String sslInstallerCommand = isUnixShellType ?
                                          "/cygdrive/c/scripts/sslinstaller.bat" :
                                          "c:\\scripts\\sslinstaller.bat";
            result = sshExecute(prototypeBox, commandTimeout, sslInstallerCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up SSL certificates.");

            getLog().info("Setting up WinRM");
            String remoteOnCommand = isUnixShellType ?
                                        "powershell -File c:\\\\scripts\\\\remoteon.ps1 < /dev/null" :
                                        "powershell -File c:\\scripts\\remoteon.ps1 < NUL";
            result = sshExecute(prototypeBox, commandTimeout, remoteOnCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up WinRM.");
            
            getLog().info("Installing additional software (Edge driver: " + source.getEdgeDriverVersion() + ")");
            String additionalSoftwareCommand;
            if (source.getEdgeDriverVersion() == null)
            {
                additionalSoftwareCommand = isUnixShellType ?
                        "powershell -File c:\\\\scripts\\\\installer.ps1 < /dev/null" :
                        "powershell -File c:\\scripts\\installer.ps1 < NUL";
            }
            else if (source.getEdgeDriverVersion().isOnDemand())
            {
                additionalSoftwareCommand = isUnixShellType ?
                        "powershell -File c:\\\\scripts\\\\installer-od.ps1 < /dev/null" :
                        "powershell -File c:\\scripts\\installer-od.ps1 < NUL";
            }
            else
            {
                additionalSoftwareCommand = isUnixShellType ?
                        "powershell -File c:\\\\scripts\\\\installer2.ps1 -edgeDriverVersion " + source.getEdgeDriverVersion().getRawVersion() + " < /dev/null" :
                        "powershell -File c:\\scripts\\installer2.ps1 -edgeDriverVersion " + source.getEdgeDriverVersion().getRawVersion() + " < NUL";
            }
            result = sshExecute(prototypeBox, commandTimeout, additionalSoftwareCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up software.");
            
            getLog().info("Configuring firewall rules");
            String firewallRulesCommand = isUnixShellType ?
                                            "/cygdrive/c/scripts/firewallrules.bat" :
                                            "c:\\scripts\\firewallrules.bat";
            result = sshExecute(prototypeBox, commandTimeout, firewallRulesCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up firewall rules.");
            
            getLog().info("Configuring browser");
            String ieUserSetupCommand = isUnixShellType ?
                                            "/cygdrive/c/scripts/ieusersetup.bat" :
                                            "c:\\scripts\\ieusersetup.bat";
            result = sshExecute(prototypeBox, commandTimeout, ieUserSetupCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up browser.");
            
            getLog().info("Configuring VNC");
            String vncSetupCommand = isUnixShellType ?
                                        "/cygdrive/c/scripts/vncsetup.bat" :
                                        "c:\\scripts\\vncsetup.bat";
            result = sshExecute(prototypeBox, commandTimeout, vncSetupCommand, System.out);
            if (result != 0)
                throw new BrowserBoxException("Error #" + result + " setting up browser.");
            
            stopVmUsingSsh(prototypeBox, winIePrototypeDir, "2");
            
            /*
            //Install WinRM
            getLog().info("VM is up, configuring WinRM");
            VirtualBoxManager.GuestControlRunOptions winRmRunOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe");
            winRmRunOptions.setUserName("Administrator");
            winRmRunOptions.setProgramArgs("-File", "c:\\Scripts\\remoteon.ps1");
            String winRmInstallResult = vbox.guestControlRun(winRmRunOptions);

            getLog().info("WinRM installed");

            stopVmUsingSsh(prototypeBox, winIePrototypeDir, "2");
            */
        }
        catch (InterruptedException | TimeoutException | IOException | MavenFilteringException | JSchException | VirtualBoxException | VagrantException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private void setUpWinRm(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir)
    throws BrowserBoxException
    {
        try 
        {
            startVmUsingSsh(prototypeBox, winIePrototypeDir, "2");

            //We're up, install WinRM
            getLog().info("VM is up, configuring WinRM");
            VirtualBoxManager.GuestControlRunOptions winRmRunOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "c:\\windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe");
            winRmRunOptions.setUserName("Administrator");
            winRmRunOptions.setProgramArgs("-File", "c:\\Scripts\\remoteon.ps1");
            String winRmInstallResult = vbox.guestControlRun(winRmRunOptions);
            //TODO verify result
            
            getLog().info("WinRM installed");
            
            stopVmUsingSsh(prototypeBox, winIePrototypeDir, "2");
        }
        catch (VagrantException | VirtualBoxException | InterruptedException | TimeoutException | IOException | JSchException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private Path buildSeleniumDeployProject(BoxSource source)
    throws IOException, MavenInvocationException, BrowserBoxException, MavenFilteringException
    {
        Path seleniumDeployBaseDirectory = context.getTempDirectory().resolve("selenium-deploy");
        Files.createDirectories(seleniumDeployBaseDirectory);
        Path seleniumDeployPomFile = seleniumDeployBaseDirectory.resolve("pom.xml");
        copyFilteredResource("seleniumdeploy/pom.xml", seleniumDeployPomFile,
                             ImmutableMap.of("selenium.version", context.getSeleniumVersion(),
                                             "browserbox.version", context.getPluginArtifact().getVersion()));
        Path srcAssemblyDirectory = seleniumDeployBaseDirectory.resolve("src").resolve("assembly");
        Files.createDirectories(srcAssemblyDirectory);
        Path assemblyDescriptorFile = srcAssemblyDirectory.resolve("selenium-server.xml");
        copyResource("seleniumdeploy/selenium-server.xml", assemblyDescriptorFile);

        Invoker invoker = context.getInvoker();
        InvocationRequest invocationRequest = new DefaultInvocationRequest()
                .setBaseDirectory(seleniumDeployBaseDirectory.toFile())
                .setGoals(Collections.singletonList("package"));

        //If we need to use the Edge driver fix then activate that profile
        //For the on-demand Edge driver, there is a bug in Selenium that requires a patch in the Edge driver on the server
        if (source.getEdgeDriverVersion() != null && source.getEdgeDriverVersion().isOnDemand())
            invocationRequest = invocationRequest.setProfiles(Collections.singletonList("edge-driver-fix"));

        InvocationResult invocationResult = invoker.execute(invocationRequest);
        if (invocationResult.getExecutionException() != null)
            throw new BrowserBoxException("Failed to build Selenium deploy project: " + invocationResult.getExecutionException().toString(), invocationResult.getExecutionException());
        if (invocationResult.getExitCode() != 0)
            throw new BrowserBoxException("Failed to build Selenium deploy project: exit code=" + invocationResult.getExitCode());
        
        return seleniumDeployBaseDirectory.resolve("target");
    }
    
    private void executeConfigurerBox(BoxSource source, PrototypeBox prototypeBox, Path configurerDir, Path windowsDiskFile)
    throws BrowserBoxException
    {
        String configurerMachineName = "BoxConfigurer";
        Path configureVagrantFile = configurerDir.resolve("Vagrantfile");

        try
        {
            if (Files.exists(configureVagrantFile))
            {
                //Need to check status because of bug in some versions of Vagrant: https://github.com/hashicorp/vagrant/issues/9137
                Vagrant.StatusOptions checkStatusOptions = new Vagrant.StatusOptions(configurerDir, configurerMachineName);
                checkStatusOptions.setVagrantFileName(configureVagrantFile.toAbsolutePath().toString());
                Vagrant.BoxStatus checkStatus = vagrant.status(checkStatusOptions);
                if (checkStatus != BoxStatus.NOT_CREATED)
                {
                    getLog().info("Destroying existing configure VM");
                    Vagrant.DestroyOptions destroyOptions = new Vagrant.DestroyOptions(configurerDir, configurerMachineName);
                    destroyOptions.setVagrantFileName(configureVagrantFile.toAbsolutePath().toString());
                    vagrant.destroy(destroyOptions);
                }
                else
                    getLog().info("VM does not exist, no need to destroy");
            }
            else
                getLog().info("Not destroying any existing configure VM");

            //Copy Vagrantfile and resources to this directory
            Path seleniumDeployBuildDirectory = buildSeleniumDeployProject(source);

            //Code signing and SSL certificates
            Path certsDir = configurerDir.resolve("certs");
            Files.createDirectories(certsDir);
            Path codeCertsDir = certsDir.resolve("code");
            Files.createDirectories(codeCertsDir);
            Path sslCertsDir = certsDir.resolve("ssl");
            Files.createDirectories(sslCertsDir);
            prepareCertificates(source, codeCertsDir, sslCertsDir);

            //Chocolatey installer - download this here instead of the in VM because the VM might not support the TLS version required to download
            //The host is much more likely to support this
            Path chocolateyDir = configurerDir.resolve("chocolateyinstaller");
            Files.createDirectories(chocolateyDir);
            downloadChocolatey(chocolateyDir);

            Map<String, String> filterValues = ImmutableMap.of("windows.disk.file", vagrantFileEscape(windowsDiskFile.toAbsolutePath().toString()),
                                                                "selenium.deploy.build.directory", vagrantFileEscape(seleniumDeployBuildDirectory.toAbsolutePath().toString()));
            copyFilteredResource("BoxConfigurer.Vagrantfile", configureVagrantFile, filterValues);
            copyResourcesToDirectory(configurerDir,
                    "remote.reg", "remotedesktop.reg", "cleanup.reg", "iesetup.reg", "iesetup2.reg", "ieusersetup.reg", "vnc.reg",
                    "firewallrules.bat", "selenium-server.bat", "cleanup.bat", "guestadditions.bat", "ieusersetup.bat", "vncsetup.bat",
                    "sslinstaller.bat", "installer.ps1", "installer2.ps1", "installer-od.ps1", "installwindowsupdate.ps1",
                    "remoteon.ps1", "runffmpeg.ps1", "userin.txt", "userin2.txt", "sshservice.reg", "nowindowsupdates.reg");

            Path edgeDir = configurerDir.resolve("edgedriver");
            Files.createDirectories(edgeDir);
            Path edgeToolsDir = edgeDir.resolve("tools");
            Files.createDirectories(edgeToolsDir);
            copyResource("edgedriver/browserboxedge.nuspec", edgeDir.resolve("browserboxedge.nuspec"));
            copyResource("edgedriver/tools/chocolateyinstall.ps1", edgeToolsDir.resolve("chocolateyinstall.ps1"));

            //Download any necessary Windows updates
            Path windowsUpdateDir = configurerDir.resolve("windowsupdates");
            Files.createDirectories(windowsUpdateDir);
            try (WindowsUpdateCatalogDownloader wuDownloader = new WindowsUpdateCatalogDownloader(context.getLog()))
            {
                for (WindowsUpdateKB windowsUpdate : source.getWindowsUpdates())
                {
                    context.getLog().info("Downloading Windows update: KB" + windowsUpdate.getKb());
                    Path updateFile = windowsUpdateDir.resolve(fileNameForWindowsUpdate(windowsUpdate));
                    wuDownloader.downloadToFile(windowsUpdate, updateFile);
                    context.getLog().info("KB" + windowsUpdate.getKb() + " downloaded: " + Files.size(updateFile) + " bytes");
                }
            }

            Vagrant.UpOptions upOptions = new Vagrant.UpOptions(configurerDir, configurerMachineName);
            getLog().info("Starting up configurer VM");
            vagrant.up(upOptions);
            
            //Auto-provision should do all the work, so take it straight down afterwards
            getLog().info("Configurer VM execution complete, now removing");
            vagrant.halt(new Vagrant.HaltOptions(configurerDir, configurerMachineName));
            vagrant.destroy(new Vagrant.DestroyOptions(configurerDir, configurerMachineName));
        }
        catch (IOException | VagrantException | MavenFilteringException | MavenInvocationException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }

    private static String fileNameForWindowsUpdate(WindowsUpdateKB windowsUpdate)
    {
        return "KB" + windowsUpdate.getKb() + ".msu";
    }

    /**
     * @return the disk file for the VM.
     */
    private Path generateInitialWindowsBox(BoxSource source, PrototypeBox prototypeBox, Path winIePrototypeDir)
    throws BrowserBoxException
    {
        Path ieVagrantFile = winIePrototypeDir.resolve("Vagrantfile");
        
        try
        {
            if (Files.exists(ieVagrantFile))
            {
                //Need to check status because of bug in some versions of Vagrant: https://github.com/hashicorp/vagrant/issues/9137
                Vagrant.StatusOptions checkStatusOptions = new Vagrant.StatusOptions(winIePrototypeDir, prototypeBox.getMachineName());
                checkStatusOptions.setVagrantFileName(ieVagrantFile.toAbsolutePath().toString());
                Vagrant.BoxStatus checkStatus = vagrant.status(checkStatusOptions);
                if (checkStatus != BoxStatus.NOT_CREATED)
                {
                    getLog().info("Destroying existing VM");
                    Vagrant.DestroyOptions destroyOptions = new Vagrant.DestroyOptions(winIePrototypeDir, prototypeBox.getMachineName());
                    destroyOptions.setVagrantFileName(ieVagrantFile.toAbsolutePath().toString());
                    vagrant.destroy(destroyOptions);
                }
                else
                    getLog().info("VM does not exist, no need to destroy");
            }
            else
                getLog().info("Not destroying any existing VM");

            //Copy Vagrantfile to this directory
            copyFilteredResource("WinIEPrototype-phase1.Vagrantfile", ieVagrantFile, source.getFilterProperties());

            //Bring phase 1 machine up but not started up, allow for error
            startVmAllowError(prototypeBox, winIePrototypeDir, "1");

            return findVmDiskFile(prototypeBox);
        }
        catch (IOException | VagrantException | VirtualBoxException | InterruptedException | TimeoutException | MavenFilteringException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    private Path findVmDiskFile(PrototypeBox prototypeBox)
    throws VirtualBoxException, BrowserBoxException
    {
        //Get the disk file for the VM
        Map<String, String> vmInfo = vbox.showVmInfo(new VirtualBoxManager.ShowVmInfoOptions(prototypeBox.getMachineName()));

        Path file = null;
        for (VmDrive key : Iterables.concat(IDE_CONTROLLER_DRIVES, SATA_CONTROLLER_DRIVES))
        {
            String diskFileName = vmInfo.get(key.getVmInfoKey());
            if (diskFileName != null)
            {
                file = Paths.get(diskFileName);
                if (Files.exists(file))
                    return file;
            }
        }

        if (file == null)
            throw new BrowserBoxException("Failed to read disk file for the VM.");
        else
            throw new BrowserBoxException("Disk file for VM " + file + " does not exist.");
    }

    @VisibleForTesting
    static String fileNameFromUrl(URL url)
    throws BrowserBoxException
    {
        String path = url.getPath();
        int lastSlashIndex = path.lastIndexOf('/');
        if (lastSlashIndex < 0)
            throw new BrowserBoxException("Could not get file name from URL: " + url.toExternalForm());

        return path.substring(lastSlashIndex + 1);
    }

    /**
     * Downloads code signing and SSL certificates.
     *
     * @param codeCertDir the directory to download code signing certificates to.
     * @param sslCertDir the directory to download SSL certificates to.
     *
     * @throws BrowserBoxException if an error occurs.
     * @throws IOException if a download error occurs.
     */
    private void prepareCertificates(BoxSource source, Path codeCertDir, Path sslCertDir)
    throws BrowserBoxException, IOException
    {
        for (URL codeSigningCertUrl : source.getCodeSigningCertificates())
        {
            String fileName = fileNameFromUrl(codeSigningCertUrl);
            Path codeSigningCertFile = codeCertDir.resolve(fileName);
            try (InputStream is = codeSigningCertUrl.openStream())
            {
                Files.copy(is, codeSigningCertFile, StandardCopyOption.REPLACE_EXISTING);
            }
        }

        //Do we need to install any root certs?
        if (!source.getSslCertificateSites().isEmpty())
        {
            try
            {
                SslCertificateDownloader sslDownloader = new SslCertificateDownloader();

                int counter = 1;
                for (URL siteUrl : source.getSslCertificateSites())
                {
                    Set<? extends X509Certificate> certs = sslDownloader.downloadRootCertificates(siteUrl);
                    for (X509Certificate cert : certs)
                    {
                        Path certFile = sslCertDir.resolve("sslcert" + counter + ".crt");
                        Files.write(certFile, cert.getEncoded());
                        counter++;
                    }
                }
            }
            catch (NoSuchAlgorithmException | KeyStoreException | CertificateEncodingException e)
            {
                throw new BrowserBoxException(e);
            }
        }
    }

    /**
     * Downloads Chocolatey nuget packagte and extracts it to a directory.
     *
     * @param downloadDirectory the directory to download and extract Chocolatey to.
     *
     * @throws IOException if an I/O error occurs.
     */
    private void downloadChocolatey(Path downloadDirectory)
    throws IOException
    {
        //Download nupkg file which is a ZIP and extract it
        ChocolateyDownloader downloader = new ChocolateyDownloader();
        downloader.downloadAndExtractChocolatey(downloadDirectory);
    }
    
    public void run(BoxSource source, PrototypeBox prototypeBox, String targetBoxName)
    throws BrowserBoxException
    {
        try
        {
            Path winIePrototypeDir = context.getTempDirectory().resolve("winieprototype");
            Files.createDirectories(winIePrototypeDir);
            Path configurerDir = context.getTempDirectory().resolve("boxconfigurer");
            Files.createDirectories(configurerDir);

            //Initial create, boot and shutdown of box to get a disk image
            Path diskFile = generateInitialWindowsBox(source, prototypeBox, winIePrototypeDir);

            //Configure the disk from the configurer box
            executeConfigurerBox(source, prototypeBox, configurerDir, diskFile);

            //Back to the Windows box, we now have an unlocked Administrator account without UAC
            setUpGuestAdditionsAndSoftware(source, prototypeBox, winIePrototypeDir);
            
            //One more restart without networking and with guest additions, reset proxy
            removeSetupThings(source, prototypeBox, winIePrototypeDir);

            //Generate a box into Vagrant
            generateVagrantBox(source, prototypeBox, winIePrototypeDir, targetBoxName);

            /*
            //Enable network, install additional software
            setUpSoftwareWithNetworking(source, prototypeBox, winIePrototypeDir);
            
            //Remove Administrator user, proxy setings
            removeSetupThings(source, prototypeBox, winIePrototypeDir);
            
            //Generate a box into Vagrant
            generateVagrantBox(source, prototypeBox, winIePrototypeDir, targetBoxName);
            */
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }

    private Vagrant.BoxStatus waitForVagrantStatusChange(Vagrant vagrant, Vagrant.StatusOptions statusOptions, Vagrant.BoxStatus fromStatus, Vagrant.BoxStatus targetStatus, Duration timeout)
    throws VagrantException, TimeoutException, InterruptedException
    {
        Instant maxTime = Instant.now(clock).plus(timeout);

        Vagrant.BoxStatus status;
        do
        {
            status = vagrant.status(statusOptions);
            Thread.sleep(500L);
        }
        while (status != targetStatus && Instant.now(clock).isBefore(maxTime));

        if (status != targetStatus)
            throw new TimeoutException("Timeout waiting for status change from " + fromStatus + " to " + targetStatus);

        return status;
    }

    private String virtualBoxRunUntilUp(VirtualBoxManager vbox, VirtualBoxManager.GuestControlRunOptions runOptions, Duration startupTimeout)
    throws VirtualBoxException, InterruptedException, TimeoutException
    {
        Instant maxTime = Instant.now(clock).plus(startupTimeout);
        VirtualBoxException lastException;
        do
        {
            try
            {
                return vbox.guestControlRun(runOptions);
            }
            catch (VirtualBoxException e)
            {
                getLog().debug("Waiting for VirtualBox positive response: " + e, e);

                lastException = e;

                if (e.getExitCode() != 1)
                    throw e;

                Thread.sleep(500L);
            }
        }
        while (Instant.now(clock).isBefore(maxTime));

        TimeoutException ex = new TimeoutException("Timed out waiting for successful run.");
        ex.initCause(lastException);
        throw ex;
    }
    
    private void copyResourcesToDirectory(Path targetDirectory, String... resourceNames)
    throws IOException
    {
        for (String resourceName : resourceNames)
        {
            Path targetFile = targetDirectory.resolve(resourceName);
            copyResource(resourceName, targetFile);
        }
    }

    private void copyResource(String name, Path targetFile)
    throws IOException
    {
        URL url = WindowsBoxMaker.class.getResource(name);
        if (url == null)
            throw new Error("Resource " + name + " does not exist.");

        try (InputStream is = url.openStream())
        {
            Files.copy(is, targetFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }
    
    private void copyFilteredResource(String name, Path targetFile, Map<String, String> filterValues)
    throws IOException, MavenFilteringException
    {
        URL url = WindowsBoxMaker.class.getResource(name);
        if (url == null)
            throw new Error("Resource " + name + " does not exist.");
        
        try (Reader fromReader = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)) 
        {
            Properties filterProperties = new Properties();
            filterProperties.putAll(filterValues);
            MavenReaderFilterRequest request = new MavenReaderFilterRequest(fromReader, 
                                                        true, 
                                                        projectConfig.getProject(), 
                                                        Collections.emptyList(), 
                                                        false, 
                                                        context.getSession(), 
                                                        filterProperties);
            try (Reader filteredReader = context.getReaderFilter().filter(request);
                 Writer writer = new OutputStreamWriter(Files.newOutputStream(targetFile), StandardCharsets.UTF_8))
            {
                CharStreams.copy(filteredReader, writer);
            }
        }
    }

    private void stopVmUsingSsh(PrototypeBox prototypeBox, Path winIePrototypeDir, String phaseDescription)
    throws VirtualBoxException, VagrantException, InterruptedException, TimeoutException, BrowserBoxException, JSchException, IOException
    {
        getLog().info("Shutting down VM (phase: " + phaseDescription + ")");
        
        sshExecute(prototypeBox, commandTimeout, "shutdown /s /t 1", System.out);

        //Wait for proper shutdown by polling vagrant status
        Vagrant.StatusOptions statusOptions = new Vagrant.StatusOptions(winIePrototypeDir, prototypeBox.getMachineName());
        Vagrant.BoxStatus status = waitForVagrantStatusChange(vagrant, statusOptions, Vagrant.BoxStatus.RUNNING, Vagrant.BoxStatus.STOPPED, commandTimeout);
        if (status != Vagrant.BoxStatus.STOPPED)
            throw new BrowserBoxException("VM not stopped (state=" + status + ")");

        getLog().info("VM is stopped (phase: " + phaseDescription + ")");
    }

    private void stopVmUsingVBox(PrototypeBox prototypeBox, Path winIePrototypeDir, String phaseDescription)
    throws VirtualBoxException, VagrantException, InterruptedException, TimeoutException, BrowserBoxException, JSchException, IOException
    {
        getLog().info("Shutting down VM (phase: " + phaseDescription + ")");
        
        VirtualBoxManager.GuestControlRunOptions shutDownOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "shutdown.exe");
        shutDownOptions.setProgramArgs("/s", "/t", "1");
        shutDownOptions.setUserName(prototypeBox.getUser());
        shutDownOptions.setPassword(prototypeBox.getPassword());
        vbox.guestControlRun(shutDownOptions);

        //Wait for proper shutdown by polling vagrant status
        Vagrant.StatusOptions statusOptions = new Vagrant.StatusOptions(winIePrototypeDir, prototypeBox.getMachineName());
        Vagrant.BoxStatus status = waitForVagrantStatusChange(vagrant, statusOptions, Vagrant.BoxStatus.RUNNING, Vagrant.BoxStatus.STOPPED, commandTimeout);
        if (status != Vagrant.BoxStatus.STOPPED)
            throw new BrowserBoxException("VM not stopped (state=" + status + ")");

        getLog().info("VM is stopped (phase: " + phaseDescription + ")");
    }

    private void startVmAllowError(PrototypeBox prototypeBox, Path winIePrototypeDir, String phaseDescription)
    throws VirtualBoxException, InterruptedException, TimeoutException, BrowserBoxException
    {
        Vagrant.UpOptions upOptions = new Vagrant.UpOptions(winIePrototypeDir, prototypeBox.getMachineName());
        try
        {
            getLog().info("Creating VM (phase: " + phaseDescription + ")");
            vagrant.up(upOptions);
        }
        catch (VagrantException e)
        {
            //This is OK - it will time out waiting to boot - but we use a different mechanism at this stage to detect
            //if it is up
            getLog().debug("Expected error during startup: " + e, e);
        }

        //Verify vagrant status - machine should not actually be started, just initialized
        try
        {
            BoxStatus status = vagrant.status(new StatusOptions(winIePrototypeDir, prototypeBox.getMachineName()));
            if (status != BoxStatus.STOPPED)
                throw new BrowserBoxException("Unexpected VM state '" + status.name() + "', expected '" + BoxStatus.STOPPED.name() + "'.");
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Failed to read status of created VM: " + e.getMessage(), e);
        }
    }

    private void startVmUsingVBox(PrototypeBox prototypeBox, Path winIePrototypeDir, String phaseDescription)
    throws InterruptedException, TimeoutException, IOException, BrowserBoxException, VirtualBoxException
    {
        Vagrant.UpOptions upOptions = new Vagrant.UpOptions(winIePrototypeDir, prototypeBox.getMachineName());
        try
        {
            getLog().info("Starting up VM (phase: " + phaseDescription + ")");
            vagrant.up(upOptions);
        }
        catch (VagrantException e)
        {
            //This is OK - it will time out waiting to boot - but we use a different mechanism at this stage to detect
            //if it is up
            getLog().debug("Expected error during startup: " + e, e);
        }

        getLog().info("Waiting for VM to boot (phase: " + phaseDescription + ")");
        VirtualBoxManager.GuestControlRunOptions runOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "cmd.exe");
        runOptions.setProgramArgs("/c", "echo", "GoodMorning");
        runOptions.setUserName(prototypeBox.getUser());
        runOptions.setPassword(prototypeBox.getPassword());
        runOptions.setLogOutput(false);
        String response = virtualBoxRunUntilUp(vbox, runOptions, commandTimeout);
        if (!response.trim().equals("GoodMorning"))
            throw new BrowserBoxException("Incorrect response while waiting for machine to boot: " + response);
    }

    private void startVmUsingSsh(PrototypeBox prototypeBox, Path winIePrototypeDir, String phaseDescription)
    throws InterruptedException, TimeoutException, IOException, BrowserBoxException
    {
        Vagrant.UpOptions upOptions = new Vagrant.UpOptions(winIePrototypeDir, prototypeBox.getMachineName());
        try
        {
            getLog().info("Starting up VM (phase: " + phaseDescription + ")");
            vagrant.up(upOptions);
        }
        catch (VagrantException e)
        {
            //This is OK - it will time out waiting to boot - but we use a different mechanism at this stage to detect
            //if it is up
            getLog().debug("Expected error during startup: " + e, e);
        }

        //When machine is up we should be able to SSH in
        waitForSsh(prototypeBox);
        
        /*
        getLog().info("Waiting for VM to boot (phase: " + phaseDescription + ")");
        VirtualBoxManager.GuestControlRunOptions runOptions = new VirtualBoxManager.GuestControlRunOptions(prototypeBox.getMachineName(), "cmd.exe");
        runOptions.setProgramArgs("/c", "echo", "GoodMorning");
        runOptions.setUserName(prototypeBox.getUser());
        runOptions.setPassword(prototypeBox.getPassword());
        runOptions.setLogOutput(false);
        String response = virtualBoxRunUntilUp(vbox, runOptions, commandTimeout);
        if (!response.trim().equals("GoodMorning"))
            throw new BrowserBoxException("Incorrect response while waiting for machine to boot: " + response);
            */
    }
    
    private void waitForSsh(PrototypeBox prototypeBox)
    throws InterruptedException, TimeoutException, IOException, BrowserBoxException
    {
        Instant startTime = Instant.now(clock);
        Instant maxEndTime = startTime.plus(commandTimeout);
        
        while (true)
        {
            try
            {
                checkSsh(prototypeBox, commandTimeout);
                
                //If we get here we successfully connected
                return;
            }
            catch (JSchException e)
            {
                getLog().debug("SSH connect failed: " + e, e);

                if (Instant.now(clock).isAfter(maxEndTime))
                {
                    TimeoutException ex = new TimeoutException("Timeout waiting for SSH connectivity.");
                    ex.initCause(e);
                    throw ex;
                }
            }
            
            Thread.sleep(500L);
        }
    }
    
    private int sshExecute(PrototypeBox prototypeBox, Duration sshTimeout, String command, OutputStream result)
    throws JSchException, IOException
    {
        String host = "localhost";
        int port = 2222;

        JSch jsch = new JSch();
        Session sshSession = jsch.getSession(prototypeBox.getUser(), host, port);
        sshSession.setPassword(prototypeBox.getPassword());

        //Disables known hosts checking - good enough for Selenium testing
        sshSession.setConfig("StrictHostKeyChecking", "no");

        sshSession.connect((int)Math.min(sshTimeout.toMillis(), Integer.MAX_VALUE));
        
        try
        {

            //Execute command
            Channel channel = sshSession.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            InputStream commandOutput = channel.getInputStream();
            InputStream commandErr = ((ChannelExec) channel).getErrStream();

            channel.connect();
            try
            {
                ByteArrayOutputStream err = new ByteArrayOutputStream();
                ByteStreams.copy(commandOutput, result);
                ByteStreams.copy(commandErr, err);
                
                if (err.size() > 0)
                    getLog().error(new String(err.toByteArray(), StandardCharsets.UTF_8)); //TODO dodgey but at least consistent
                
                return channel.getExitStatus();
            }
            finally
            {
                channel.disconnect();
            }
        }
        finally
        {
            sshSession.disconnect();   
        }
    }
    
    private void checkSsh(PrototypeBox prototypeBox, Duration sshTimeout)
    throws JSchException, IOException, BrowserBoxException
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int resultCode = sshExecute(prototypeBox, sshTimeout, "echo Good Morning", out);
        String outString = new String(out.toByteArray(), StandardCharsets.UTF_8);
        if (resultCode != 0)
            throw new BrowserBoxException("SSH execute error #" + resultCode + ": " + outString);
     
        if (!"Good Morning".equals(outString.trim()))
            throw new BrowserBoxException("Unexpected SSH response: " + outString);
    }
    
    private static String vagrantFileEscape(String s)
    {
        return s.replaceAll(Pattern.quote("\\"), Matcher.quoteReplacement("\\\\"));
    }
    
    public static class PrototypeBox
    {
        private final String machineName;
        private final String user;
        private final String password;
        
        public PrototypeBox(String machineName, String user, String password)
        {
            this.machineName = machineName;
            this.user = user;
            this.password = password;
        }

        public String getMachineName() 
        {
            return machineName;
        }

        public String getUser() 
        {
            return user;
        }

        public String getPassword() 
        {
            return password;
        }
    }
    
    public static class BoxSource
    {
        private final String boxName;
        private final String boxVersion;
        private final EdgeDriverVersionResolver.Version edgeDriverVersion;

        /**
         * Windows updates to install as part of the box setup.
         */
        private final List<? extends WindowsUpdateKB> windowsUpdates;

        /**
         * A list of website URLs whose root certificates will be installed into the target VM.
         * This is necessary for some older VMs since the root certs on the OSes are out of date and the sites
         * must be trusted to install stuff from scripts.
         */
        private final List<URL> sslCertificateSites;

        /**
         * URLs to any extra driver code signing certificates that need to be installed on the target VM.
         */
        private final List<URL> codeSigningCertificates;

        public BoxSource(String boxName, String boxVersion, EdgeDriverVersionResolver.Version edgeDriverVersion,
                         List<? extends WindowsUpdateKB> windowsUpdates, List<URL> sslCertificateSites,
                         List<URL> codeSigningCertificates)
        {
            this.boxName = boxName;
            this.boxVersion = boxVersion;
            this.edgeDriverVersion = edgeDriverVersion;
            this.windowsUpdates = ImmutableList.copyOf(windowsUpdates);
            this.sslCertificateSites = ImmutableList.copyOf(sslCertificateSites);
            this.codeSigningCertificates = ImmutableList.copyOf(codeSigningCertificates);
        }

        public String getBoxName()
        {
            return boxName;
        }

        public String getBoxVersion()
        {
            return boxVersion;
        }

        public EdgeDriverVersionResolver.Version getEdgeDriverVersion()
        {
            return edgeDriverVersion;
        }

        public List<? extends WindowsUpdateKB> getWindowsUpdates()
        {
            return windowsUpdates;
        }

        public List<URL> getSslCertificateSites()
        {
            return sslCertificateSites;
        }

        public List<URL> getCodeSigningCertificates()
        {
            return codeSigningCertificates;
        }

        public Map<String, String> getFilterProperties()
        {
            return ImmutableMap.of("source.box.name", getBoxName(), 
                                   "source.box.version", getBoxVersion(), 
                                   "edge.driver.version", getEdgeDriverVersion() == null ? "" : getEdgeDriverVersion().getRawVersion());
        }
    }

    private static class VmDrive
    {
        private final String controllerName;
        private final int device;
        private final int port;

        public VmDrive(String controllerName, int device, int port)
        {
            this.controllerName = controllerName;
            this.device = device;
            this.port = port;
        }

        public String getControllerName()
        {
            return controllerName;
        }

        public int getDevice()
        {
            return device;
        }

        public int getPort()
        {
            return port;
        }
        
        public String getVmInfoKey()
        {
            return getControllerName() + "-" + port + "-" + device;
        }
    }
}
