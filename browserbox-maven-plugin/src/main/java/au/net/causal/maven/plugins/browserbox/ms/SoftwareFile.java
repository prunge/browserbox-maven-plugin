package au.net.causal.maven.plugins.browserbox.ms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.net.URI;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SoftwareFile
{
    private String name;
    private URI url;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public URI getUrl()
    {
        return url;
    }

    public void setUrl(URI url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("SoftwareFile{");
        sb.append("name='").append(name).append('\'');
        sb.append(", url=").append(url);
        sb.append('}');
        return sb.toString();
    }
}
