package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.Tag;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.access.DockerAccess;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.access.ExecException;
import io.fabric8.maven.docker.access.PortMapping;
import io.fabric8.maven.docker.config.ImageConfiguration;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import io.fabric8.maven.docker.config.WaitConfiguration;
import io.fabric8.maven.docker.model.Container;
import io.fabric8.maven.docker.service.QueryService;
import io.fabric8.maven.docker.service.RunService;
import io.fabric8.maven.docker.util.GavLabel;
import io.fabric8.maven.docker.wait.HttpPingChecker;
import io.fabric8.maven.docker.wait.PreconditionFailedException;
import io.fabric8.maven.docker.wait.TcpPortChecker;
import io.fabric8.maven.docker.wait.WaitChecker;
import io.fabric8.maven.docker.wait.WaitUtil;
import org.apache.maven.plugin.MojoExecutionException;

import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

public abstract class DockerBrowserBox implements BrowserBox 
{
    private final BoxContext context;
    private final BoxConfiguration boxConfiguration;
    private final ProjectConfiguration projectConfiguration;

    protected DockerBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        this.boxConfiguration = boxConfiguration;
        this.projectConfiguration = projectConfiguration;
        this.context = context;
    }

    protected BoxConfiguration getBoxConfiguration()
    {
        return boxConfiguration;
    }

    protected ProjectConfiguration getProjectConfiguration()
    {
        return projectConfiguration;
    }

    protected BoxContext getContext()
    {
        return context;
    }

    @Override
    public boolean exists() throws BrowserBoxException
    {
        return findDockerContainer() != null;
    }

    @Override
    public boolean isRunning() throws BrowserBoxException
    {
        Container container = findDockerContainer();
        if (container == null)
            return false;

        return container.isRunning();
    }

    protected Container findDockerContainer()
    throws BrowserBoxException
    {
        QueryService queryService = context.getDockerServiceHub().getQueryService();
        try
        {
            return queryService.getContainer(containerName());
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    @Override
    public void deleteImage() throws BrowserBoxException
    {
        DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();
        try
        {
            boolean force = false;
            dockerAccess.removeImage(dockerImageName().getName(), force);
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    @Override
    public boolean hasImage() throws BrowserBoxException 
    {
        DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();
        try
        {
            return dockerAccess.hasImage(dockerImageName().getName());
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    /**
     * Pulls the container image of the browser box from Docker, possibly updating depending on the pull policy configured
     * on the current Maven project.
     */
    protected void pullRemoteImage()
    throws MojoExecutionException, DockerAccessException, BrowserBoxException
    {
        String imageName = dockerImageName().getName();
        DockerPuller.pullImage(imageName, getProjectConfiguration(), getContext());
    }

    @Override
    public void prepareImage() throws BrowserBoxException 
    {
        try 
        {
            pullRemoteImage();
        }
        catch (MojoExecutionException e)
        {
            throw new BrowserBoxException(e);
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    /**
     * @return the name of the docker container.  Defaults to the name given in the configuration.
     */
    protected String containerName()
    {
        return boxConfiguration.getContainerName();
    }

    /**
     * @return the name of the docker image to build the container with.  Defaults to
     *      <i>browserType</i>:<i>browserVersion</i>
     */
    protected ImageReference dockerImageName()
    {
        return new ImageReference(boxConfiguration.getBrowserType(), new Tag(boxConfiguration.getBrowserVersion()));
    }


    protected static BrowserBoxException createBrowserBoxException(DockerAccessException ex)
    {
        return new BrowserBoxException(ex);
    }

    @Override
    public void createAndStart() throws BrowserBoxException 
    {
        prepareImage();
        
        RunService runService = getContext().getDockerServiceHub().getRunService();
        ImageConfiguration imageConfig = imageConfiguration();
        Properties projProperties = getProjectConfiguration().getProjectProperties();
        GavLabel pomLabel = getProjectConfiguration().getPomLabel();
        PortMapping portMapping = runService.createPortMapping(imageConfig.getRunConfiguration(), projProperties);
        try
        {
            Date buildTimestamp = new Date();
            String containerId = runService.createAndStartContainer(imageConfig, portMapping, pomLabel, projProperties, getProjectConfiguration().getBaseDirectory(), null, buildTimestamp);
            getContext().getLog().info("Created docker container: " + containerId);

        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    /**
     * @return the port the container runs webdriver on.  This is not necessarily the port that is mapped on
     * the host system.
     *
     * @see BoxConfiguration#getWebDriverPort()
     */
    protected abstract int containerWebDriverPort();


    /**
     * Customize the docker run image for the browser box.
     * To augment default settings, call <code>super.configureRunImage()</code> first.
     *
     * @param builder the builder to configure.
     */
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        builder.containerNamePattern("%a")
               .ports(ImmutableList.of(boxConfiguration.getWebDriverPort() + ":" + containerWebDriverPort()));
    }

    /**
     * Customize the docker image configuration for the browser container.
     * To augment default settings, call <code>super.configureImage()</code> first.
     * 
     * @param builder the builder to customize.
     */
    protected void configureImage(ImageConfiguration.Builder builder)
    {
        builder.name(dockerImageName().getName())
                .alias(containerName());
    }

    protected ImageConfiguration imageConfiguration()
    {
        RunImageConfiguration.Builder runBuilder = new RunImageConfiguration.Builder();

        /*
        if (getProjectConfiguration().getKillTimeout() != null)
        {
            WaitConfiguration.Builder waitBuilder = new WaitConfiguration.Builder();
            waitBuilder.kill(Math.toIntExact(getProjectConfiguration().getKillTimeout().toMillis()));
            runBuilder.wait(waitBuilder.build());
        }
        */

        configureRunImage(runBuilder);
        RunImageConfiguration runConfig = runBuilder.build();

        ImageConfiguration.Builder imageBuilder = new ImageConfiguration.Builder();
        imageBuilder.runConfig(runConfig);
        configureImage(imageBuilder);
        return imageBuilder.build();
    }

    @Override
    public void delete() throws BrowserBoxException
    {
        DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();
        try
        {
            boolean removeVolumes = true;
            dockerAccess.removeContainer(containerName(), removeVolumes);
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }

    @Override
    public void start() throws BrowserBoxException
    {
        DockerAccess dockerAccess = context.getDockerServiceHub().getDockerAccess();
        try
        {
            dockerAccess.startContainer(containerName());
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
    }
    
    @Override
    public void stop() throws BrowserBoxException
    {
        ImageConfiguration imageConfig = imageConfiguration();
        try
        {
            //Resolve container name to ID first
            //There's what looks like a bug in RunService.shutdown() that does a substring for a log message and assumes
            //it's an  ID instead of a name and has a particular length
            Container container = context.getDockerServiceHub().getDockerAccess().getContainer(containerName());

            //No need to stop a container that doesn't exist
            if (container == null)
                return;

            String containerId = container.getId();
            context.getDockerServiceHub().getRunService().stopContainer(containerId, imageConfig, true, false);
        }
        catch (DockerAccessException e)
        {
            throw createBrowserBoxException(e);
        }
        catch (ExecException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    @Override
    public String getName() 
    {
        return containerName();
    }

    protected List<? extends BoxConfiguration.TunnelPort> getReverseTunnelPorts()
    throws BrowserBoxException
    {
        return ImmutableList.of();
    }
    
    protected TunnelSession establishSshPortTunnels(String browserBoxSshHost, int browserBoxSshPort, String sshUser, String sshPassword)
    throws BrowserBoxException
    {
        List<BoxConfiguration.TunnelPort> tunnelPorts = getBoxConfiguration().tunnelPortConfiguration();
        List<? extends BoxConfiguration.TunnelPort> reverseTunnelPorts = getReverseTunnelPorts();
        if (tunnelPorts.isEmpty() && reverseTunnelPorts.isEmpty())
            return new NullTunnelSession();
        
        TunnelSession tunnelSession = SshTunnelSession.create(tunnelPorts, reverseTunnelPorts, browserBoxSshHost, browserBoxSshPort, sshUser, sshPassword);
        getContext().getLog().info("SSH tunnel(s) established: " + tunnelPorts);
        return tunnelSession;
    }

    @Override
    public void waitUntilStarted(Duration maxTimeToWait) 
    throws TimeoutException, BrowserBoxException 
    {
        int waitMillis;
        if (maxTimeToWait == null)
            waitMillis = Integer.MAX_VALUE;
        else
            waitMillis = Math.toIntExact(Math.min(maxTimeToWait.toMillis(), Integer.MAX_VALUE));

        int webDriverPort = this.containerWebDriverPort();
        String url = "http://" + getContext().getDockerHostAddress() + ":" + webDriverPort + "/wd/hub";
        //e.g. http://192.168.99.100:4444/wd/hub
        WaitChecker checker = new HttpPingChecker(url, "GET", WaitConfiguration.DEFAULT_STATUS_RANGE);
        try
        {
            WaitUtil.wait(new BrowserBoxRunningPrecondition(this, context), waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BrowserBoxException("Error occurred while waiting for browser box to start up: " + e, e);
        }
        
        //Also wait for VNC and SSH
        int vncPort = getConnectionInfo(StandardConnectionType.VNC).getUri().getPort();
        int sshPort = getConnectionInfo(StandardConnectionType.SSH).getUri().getPort();
        checker = new TcpPortChecker(getContext().getDockerHostAddress(), ImmutableList.of(vncPort, sshPort));
        try 
        {
            WaitUtil.wait(new BrowserBoxRunningPrecondition(this, context), waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BrowserBoxException("Error occurred while waiting for browser box to start up: " + e, e);
        }
    }
}
