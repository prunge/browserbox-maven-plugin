package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class RemoteSslNssDatabaseGenerator
{
    //TODO generate both cert8 and cert9 DBs for maximum compatibility with all versions of Firefox

    private final DockerBrowserBox box;
    private final Random randomGenerator = new Random();

    public RemoteSslNssDatabaseGenerator(DockerBrowserBox box)
    {
        this.box = box;
    }

    public BoxContext getContext()
    {
        return box.getContext();
    }

    public ProjectConfiguration getProjectConfiguration()
    {
        return box.getProjectConfiguration();
    }

    private String getRemoteCertificateDirectory()
    {
        return "/home/seluser/certs";
    }

    protected String certUtilAddCertificateCommand(String certDirectory, String certFileName, String databaseDirectory, DatabaseType type)
    {
        return "certutil -A -d " + type.getPrefix() + databaseDirectory + " -n " + certFileName + " -t C -i " + certDirectory + "/" + certFileName;
    }

    protected String certUtilCreateDatabaseCommand(String databaseDirectory, DatabaseType type)
    {
        return "certutil -N --empty-password -d " + type.getPrefix() + databaseDirectory;
    }

    /**
     * Generates certificate database.
     *
     * @param remoteDbDirectory the directory to save the certificate database to.
     * @param certificates certificates to generate the database from.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void generate(String remoteDbDirectory, DatabaseType type, Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        //Start SSH session
        ConnectionInfo info = box.getConnectionInfo(StandardConnectionType.SSH);
        String sshUser = info.getUsername();
        String sshPassword = info.getPassword();
        String browserBoxSshHost = info.getUri().getHost();
        int browserBoxSshPort = info.getUri().getPort();

        JSch jsch = new JSch();

        try
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            try
            {
                generateRemoteUsingSshSession(sshSession, remoteDbDirectory, type, certificates);
            }
            finally
            {
                sshSession.disconnect();
            }
        }
        catch (JSchException | InterruptedException e)
        {
            throw new BrowserBoxException("Error generating certificate DB: " + e.getMessage(), e);
        }
    }

    private void generateRemoteUsingSshSession(Session sshSession, String remoteDbDirectory,
                                               DatabaseType type,
                                               Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException, JSchException, InterruptedException
    {
        //Create necessary directories
        ChannelExec exec = (ChannelExec)sshSession.openChannel("exec");
        exec.setCommand("mkdir -p " + getRemoteCertificateDirectory() + " " + remoteDbDirectory);
        exec.connect();
        while (exec.getExitStatus() < 0)
        {
            Thread.sleep(getProjectConfiguration().getPollTime().toMillis());
        }
        if (exec.getExitStatus() != 0)
            throw new BrowserBoxException("Error creating directories: " + exec.getExitStatus());

        exec.disconnect();

        //Save certificates to remote machine
        List<String> remoteCertificateFileNames = new ArrayList<>();

        ChannelSftp sftpChannel = (ChannelSftp)sshSession.openChannel("sftp");
        sftpChannel.connect();

        int certificateIndex = 0;
        for (X509Certificate certificate : certificates)
        {
            try (InputStream is = new ByteArrayInputStream(certificate.getEncoded()))
            {
                String remoteCertificateFileName = "cert" +  + certificateIndex + ".cer";
                String remoteCertificateFile = getRemoteCertificateDirectory() + "/" + remoteCertificateFileName;
                remoteCertificateFileNames.add(remoteCertificateFileName);
                sftpChannel.put(is, remoteCertificateFile);
                certificateIndex++;
            }
            catch (CertificateEncodingException | IOException | SftpException e)
            {
                throw new BrowserBoxException("Error saving certificates: " + e.getMessage(), e);
            }
        }
        sftpChannel.disconnect();

        //Create database if needed
        //Later versions of certutil do this automatically but earlier versions fail with SEC_ERROR_IO if not
        //done separately
        boolean dbExists;
        try
        {
            int checkResult = runSshCommand(sshSession, "sh -c \"if [ -e " + type.getFileName() + " ]; then exit 1; fi\"", "check> ");
            switch (checkResult)
            {
                case 0:
                    dbExists = false;
                    break;
                case 1:
                    dbExists = true;
                    break;
                default:
                    throw new BrowserBoxException("Error checking certificate DB: " + checkResult);
            }
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error checking certificate DB: " + e.getMessage(), e);
        }

        if (!dbExists)
        {
            try
            {
                int createResult = runSshCommand(sshSession, certUtilCreateDatabaseCommand(remoteDbDirectory, type), "certutil> ");
                if (createResult != 0)
                    throw new BrowserBoxException("Error creating certificate DB: " + createResult);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Error creating certificate DB: " + e.getMessage(), e);
            }
        }

        //Now import those certificates
        for (String remoteCertificateFileName : remoteCertificateFileNames)
        {
            String cmd = certUtilAddCertificateCommand(getRemoteCertificateDirectory(), remoteCertificateFileName, remoteDbDirectory, type);
            try
            {
                int addResult = runSshCommand(sshSession, cmd, "certutil> ");
                if (addResult != 0)
                    throw new BrowserBoxException("Error importing certificates: " + addResult);
            }
            catch (IOException e)
            {
                throw new BrowserBoxException("Error importing certificates: " + e.getMessage(), e);
            }
        }
    }

    private int runSshCommand(Session sshSession, String command, String logPrefix)
    throws JSchException, InterruptedException, IOException
    {
        ChannelExec exec = (ChannelExec)sshSession.openChannel("exec");
        exec.setCommand(command);
        getContext().getLog().info("SSH command: " + command);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             ByteArrayOutputStream err = new ByteArrayOutputStream())
        {
            exec.setOutputStream(out);
            exec.setErrStream(err);

            exec.connect();
            while (exec.getExitStatus() < 0)
            {
                Thread.sleep(getProjectConfiguration().getPollTime().toMillis());
            }

            if (out.size() > 0)
                getContext().getLog().info(logPrefix + new String(out.toByteArray(), StandardCharsets.UTF_8));
            if (err.size() > 0)
                getContext().getLog().error(logPrefix + new String(err.toByteArray(), StandardCharsets.UTF_8));

            return exec.getExitStatus();
        }
        finally
        {
            exec.disconnect();
        }
    }

    public void generateLocal(Path localDatabaseDirectory, DatabaseType type, Collection<? extends X509Certificate> certificates)
    throws IOException, BrowserBoxException
    {
        //Prepare local target directory
        Files.createDirectories(localDatabaseDirectory);

        //Start SSH session
        ConnectionInfo info = box.getConnectionInfo(StandardConnectionType.SSH);
        String sshUser = info.getUsername();
        String sshPassword = info.getPassword();
        String browserBoxSshHost = info.getUri().getHost();
        int browserBoxSshPort = info.getUri().getPort();

        JSch jsch = new JSch();

        try
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            try
            {
                //Generate remotely to temp directory
                String remoteTempDbDir = "/home/seluser/tempdb" + randomGenerator.nextInt(Integer.MAX_VALUE);
                generateRemoteUsingSshSession(sshSession, remoteTempDbDir, type, certificates);

                //Download each file
                ChannelSftp sftpChannel = (ChannelSftp)sshSession.openChannel("sftp");
                sftpChannel.connect();
                @SuppressWarnings("unchecked") List<? extends LsEntry> files = sftpChannel.ls(remoteTempDbDir);
                for (LsEntry file : files)
                {
                    if (!file.getAttrs().isDir())
                    {
                        String remoteDatabaseFile = remoteTempDbDir + "/" + file.getFilename();
                        Path localDatabaseFile = localDatabaseDirectory.resolve(file.getFilename());
                        getContext().getLog().info("Copying file " + remoteDatabaseFile +" to " + localDatabaseFile.toAbsolutePath().toString());
                        Files.createDirectories(localDatabaseFile.getParent()); //In case there are subdirectories that need creating
                        try (OutputStream localDbFileOut = Files.newOutputStream(localDatabaseFile))
                        {
                            sftpChannel.get(remoteDatabaseFile, localDbFileOut);
                        }
                    }
                }
                sftpChannel.disconnect();

            }
            finally
            {
                sshSession.disconnect();
            }
        }
        catch (JSchException | InterruptedException | SftpException e)
        {
            throw new BrowserBoxException("Error generating certificate DB: " + e.getMessage(), e);
        }
    }

    /**
     * Database types supported by the certutil command.
     */
    public static enum DatabaseType
    {
        /**
         * Legacy Berkeley database.
         */
        DBM("cert8.db"),

        /**
         * Newer SQLite database.
         */
        SQL("cert9.db");

        private final String fileName;

        private DatabaseType(String fileName)
        {
            this.fileName = fileName;
        }

        /**
         * @return the prefix to use for this database type in the certutil command.
         */
        public String getPrefix()
        {
            return name().toLowerCase(Locale.ENGLISH) + ":";
        }

        /**
         * @return the name of a file that is present if the database exists.
         */
        public String getFileName()
        {
            return fileName;
        }
    }
}
