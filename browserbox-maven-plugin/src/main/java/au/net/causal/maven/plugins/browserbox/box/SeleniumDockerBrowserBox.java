package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.BuildIntermediates;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.execute.BrowserControl;
import au.net.causal.maven.plugins.browserbox.execute.SeleniumBrowserControl;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.DockerNaming;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.SeleniumDockerImageGenerator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import org.apache.maven.plugin.MojoExecutionException;
import org.openqa.selenium.Capabilities;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public abstract class SeleniumDockerBrowserBox extends DockerBrowserBox 
{
    private final ImageReference seleniumBaseImageName;
    private final SeleniumDockerImageGenerator imageGenerator;
    private final Capabilities seleniumCapabilities;
    private final DockerNaming dockerNaming = new DockerNaming();

    protected SeleniumDockerBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context,
                                       ImageReference seleniumBaseImageName, Capabilities seleniumCapabilities)
    {
        super(boxConfiguration, projectConfiguration, context);
        
        Objects.requireNonNull(seleniumBaseImageName, "seleniumBaseImageName == null");
        
        this.imageGenerator = new SeleniumDockerImageGenerator(context, projectConfiguration, boxConfiguration);
        this.seleniumBaseImageName = seleniumBaseImageName;
        this.seleniumCapabilities = seleniumCapabilities;
    }

    @Override
    protected int containerWebDriverPort() 
    {
        return 4444;
    }

    @Override
    protected ImageReference dockerImageName() 
    {
        return dockerNaming.browserBoxImageName(seleniumBaseImageName);
    }

    @Override
    protected void configureRunImage(RunImageConfiguration.Builder builder) 
    {
        super.configureRunImage(builder);
        builder.ports(ImmutableList.of(getBoxConfiguration().getWebDriverPort() + ":" + containerWebDriverPort(),
                                        "10022:22", "15900:5900"));

        ImmutableMap.Builder<String, String> envMap = ImmutableMap.builder();
        if (getBoxConfiguration().getResolution() != null && 
            getBoxConfiguration().getResolution().getWidth() > 0 && 
            getBoxConfiguration().getResolution().getHeight() > 0)
        {
            envMap.put("SCREEN_WIDTH", String.valueOf(getBoxConfiguration().getResolution().getWidth()));
            envMap.put("SCREEN_HEIGHT", String.valueOf(getBoxConfiguration().getResolution().getHeight()));
        }
        if (getBoxConfiguration().getScaleFactor() != null && getBoxConfiguration().getScaleFactor() > 0.0)
            envMap.put("SCREEN_SCALE", String.valueOf(getBoxConfiguration().getScaleFactor()));

        builder.env(envMap.build());

        //Default in Docker is 64 MB which is not enough for modern browsers, we change the default in the Mojo to 4GB
        builder.shmSize(getContext().getDockerSharedMemorySize());
    }

    @Override
    public TunnelSession establishTunnels() throws BrowserBoxException 
    {
        ConnectionInfo info = getConnectionInfo(StandardConnectionType.SSH);
        return establishSshPortTunnels(info.getUri().getHost(), info.getUri().getPort(), info.getUsername(), info.getPassword());
    }

    @Override
    public List<? extends ConnectionType> getSupportedConnectionTypes() 
    throws BrowserBoxException 
    {
        return ImmutableList.of(StandardConnectionType.VNC, StandardConnectionType.SSH, StandardConnectionType.SELENIUM);
    }

    @Override
    public ConnectionInfo getConnectionInfo(ConnectionType connectionType) 
    throws BrowserBoxException 
    {
        if (connectionType == StandardConnectionType.SELENIUM)
            return new ConnectionInfo(URI.create("http://" + getContext().getDockerHostAddress() + ":" + containerWebDriverPort() + "/wd/hub"));
        else if (connectionType == StandardConnectionType.SSH)
            return new ConnectionInfo(URI.create("ssh://" + getContext().getDockerHostAddress() + ":" + 10022), "seluser", "secret");
        else if (connectionType == StandardConnectionType.VNC)
            return new ConnectionInfo(URI.create("vnc://" + getContext().getDockerHostAddress() + ":" + 15900), null, "secret");
        else
            throw new BrowserBoxException("Unsupported connection type " + connectionType);
    }

    @Override
    public boolean hasImage() throws BrowserBoxException 
    {
        ImageReference targetImageName = dockerImageName();
        try 
        {
            return getContext().getDockerServiceHub().getQueryService().hasImage(targetImageName.getName());    
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Failed to check Docker image for " + dockerImageName() + ": " + e.getMessage(), e);
        }
    }

    @Override
    public void prepareImage() throws BrowserBoxException 
    {
        boolean baseImageAlreadyExists;
        try 
        {
            baseImageAlreadyExists = getContext().getDockerServiceHub().getDockerAccess().hasImage(seleniumBaseImageName.getName());    
        }
        catch (DockerAccessException e)
        {
            throw new BrowserBoxException("Error checking if image exists: " + e.getMessage(), e);
        }
        
        //First try the default - pull the browserbox image
        //Though it might not exist remotely
        try 
        {
            pullRemoteImage();
            
            //If we get here, pull succeeded so we're done and we don't need to generate image
            return;
        }
        catch (DockerAccessException e)
        {
            //Maybe image was not found, continue
            getContext().getLog().info("Pull image failed - might not exist - will generate");
        }
        catch (MojoExecutionException e)
        {
            throw new BrowserBoxException(e);
        }
        
        //Generate image if needed
        ImageReference targetImageName = dockerImageName();
        try
        {
            imageGenerator.generate(seleniumBaseImageName, targetImageName);
            
            if (!baseImageAlreadyExists && !getProjectConfiguration().getSaveBuildIntermediates().contains(BuildIntermediates.BOX_IMAGES)) 
            {
                getContext().getLog().info("Removing tag for intermediate image " + seleniumBaseImageName);
                getContext().getDockerServiceHub().getDockerAccess().removeImage(seleniumBaseImageName.getName(), false);
            }
        }
        catch (MojoExecutionException | IOException e)
        {
            throw new BrowserBoxException("Failed to generate Docker image for " + dockerImageName() + ": " + e.getMessage(), e);
        }
    }

    /**
     * @return the default Selenium X11 screen resolution if not explicitly configured.
     */
    protected static Resolution defaultSeleniumResolution()
    {
        //See https://github.com/SeleniumHQ/docker-selenium/blob/master/NodeBase/Dockerfile
        return new Resolution(1360, 1020);
    }

    /**
     * @return the major version of the browser.  e.g. for Firefox 58.0.1, the major version will be 58.
     *
     * @throws BrowserBoxException if the major version could not be determined.
     */
    protected int getBrowserMajorVersion()
    throws BrowserBoxException
    {
        String versionString = getBoxConfiguration().getBrowserVersion();
        String majorVersionString = versionString.split(Pattern.quote("."))[0];
        try
        {
            return Integer.parseInt(majorVersionString);
        }
        catch (NumberFormatException e)
        {
            throw new BrowserBoxException("Error parsing major version number '" + majorVersionString + "'.", e);
        }
    }

    @Override
    public BrowserControl browserControl() 
    throws BrowserBoxException 
    {
        try 
        {
            //If there are custom capabilities use those, if not then use defaults
            Capabilities capabilities = getSeleniumDesiredCapabilities();
            if (capabilities == null)
                capabilities = seleniumCapabilities;

            return new SeleniumBrowserControl(getWebDriverUrl(), capabilities);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }

    /**
     * @return true if ffmpeg is available in the container, false if avconv is used instead.  Avconv is used instead of
     *         ffmpeg on older Ubuntu images.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    private boolean detectFfmpeg()
    throws BrowserBoxException
    {
        ConnectionInfo info = getConnectionInfo(StandardConnectionType.SSH);
        String sshUser = info.getUsername();
        String sshPassword = info.getPassword();
        String browserBoxSshHost = info.getUri().getHost();
        int browserBoxSshPort = info.getUri().getPort();

        JSch jsch = new JSch();

        try
        {
            Session sshSession = jsch.getSession(sshUser, browserBoxSshHost, browserBoxSshPort);
            sshSession.setPassword(sshPassword);

            //Disables known hosts checking - good enough for testing
            sshSession.setConfig("StrictHostKeyChecking", "no");

            sshSession.connect();

            try
            {
                ChannelSftp sftpChannel = (ChannelSftp)sshSession.openChannel("sftp");
                sftpChannel.connect();
                try
                {
                    //lstat the file - if it exists this call will work, otherwise it errors out
                    sftpChannel.lstat("/usr/bin/avconv");

                    //If we get here, avconv exists, so ffmpeg does not exist
                    return false;
                }
                catch (SftpException e)
                {
                    //Fails if the file does not exist - avconv does not exist which means ffmpeg must exist
                    return true;
                }
                finally
                {
                    sftpChannel.disconnect();
                }
            }
            finally
            {
                sshSession.disconnect();
            }
        }
        catch (JSchException e)
        {
            throw new BrowserBoxException("Error creating SSH connection: " + e.getMessage(), e);
        }
    }

    @Override
    public VideoControl video() 
    throws BrowserBoxException 
    {
        //Some older boxes don't have FFMPEG but have avconv instead, so detect what is available
        boolean ffmpeg = detectFfmpeg();

        if (ffmpeg)
            return new DockerFfmpegVideoControl(this, getBoxConfiguration());
        else
            return new DockerAvConvVideoControl(this, getBoxConfiguration());
    }

    /**
     * The avconv tool doesn't have the 'press Q to quit' prompt like ffmpeg does, so we need to use the kill command
     * to stop it.  (don't worry, kill actually makes it shut down cleanly)
     */
    private class DockerAvConvVideoControl extends DockerFfmpegVideoControl
    {
        public DockerAvConvVideoControl(BrowserBox box, BoxConfiguration boxConfiguration)
        {
            super(box, boxConfiguration);
        }

        /**
         * Because avconv doesn't have the 'press Q to quit' option, we need to kill it.  And to kill it we need to
         * remember its PID when being spawned.  This file contains the process ID of the avconv process so we can
         * kill it later.
         */
        protected String pidFileName()
        {
            return videoFileName() + ".pid";
        }

        @Override
        protected String ffmpegCommand()
        {
            //Spawn a shell, remember its PID, then replace the process with avconv
            return "sh -c 'echo $$ > " + pidFileName() + "; exec avconv " + ffmpegArguments() + "'";
        }

        @Override
        protected void stopRecording(OutputStream ffmpegInputWriter, Session sshSession)
        throws IOException
        {
            //Instead of just pressing 'Q' like with FFMPEG, run the kill command on the process ID of the
            //avconv process.  This actually shuts it down cleanly.
            try
            {
                ChannelExec exec = (ChannelExec)sshSession.openChannel("exec");
                exec.setCommand("sh -c 'kill `cat " + pidFileName() + "`'");
                getContext().getLog().debug("Killing video recorder...");
                exec.connect();

                //Wait for result
                while (exec.getExitStatus() < 0)
                {
                    Thread.sleep(getProjectConfiguration().getPollTime().toMillis());
                }

                //If kill fails, not much we can do.  Warn the user and hope something happened to terminate the process
                //If not then the user will have to intervene and kill the whole build or something
                if (exec.getExitStatus() != 0)
                    getContext().getLog().warn("Error killing video recorder: " + exec.getExitStatus());
                else
                    getContext().getLog().debug("Killed video recorder");

                exec.disconnect();
            }
            catch (JSchException | InterruptedException e)
            {
                throw new IOException("Error stopping video recorder process: " + e.getMessage(), e);
            }
        }

        @Override
        protected void validateFfmpegExitCode(int ffmpegExitCode)
        throws BrowserBoxException
        {
            //Alas, because we resort to killing avconv and get exit code 255 in this case we have to accept that as valid
            if (ffmpegExitCode != 0 && ffmpegExitCode != 255)
                throw new BrowserBoxException("Error during execution of ffmpeg, result=" + ffmpegExitCode);
        }
    }

    private static class DockerFfmpegVideoControl extends FfmpegVideoControl
    {
        public DockerFfmpegVideoControl(BrowserBox box, BoxConfiguration boxConfiguration) 
        {
            super(box, boxConfiguration);
        }

        @Override
        protected String ffmpegArguments() 
        {
            StringBuilder commandBuf = new StringBuilder();
            
            Resolution boxResolution = getBoxConfiguration().getResolution();
            if (boxResolution == null)
                boxResolution = defaultSeleniumResolution();
            
            if (boxResolution != null && boxResolution.getWidth() > 0 && boxResolution.getHeight() > 0)
                commandBuf.append("-video_size " + boxResolution.getWidth() + "x" + boxResolution.getHeight() + " ");
                        
            commandBuf.append("-y -framerate 25 -f x11grab -i :99.0 -pix_fmt yuv420p ").append(videoFileName());
            
            return commandBuf.toString();
        }

        @Override
        protected String videoFileName() 
        {
            return "/home/seluser/output.mp4";
        }
    }
}
