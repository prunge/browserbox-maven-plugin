package au.net.causal.maven.plugins.browserbox.execute;

import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public abstract class GenericPathBasedFinder extends CommandLineToolFinder
{
    private final List<? extends Path> searchPaths;
    
    public GenericPathBasedFinder(Collection<? extends Path> searchPaths)
    {
        Objects.requireNonNull(searchPaths, "searchPaths == null");
        this.searchPaths = ImmutableList.copyOf(searchPaths);
    }
    
    protected Optional<Path> findInSearchPath(Pattern fileNamePattern)
    throws IOException
    {
        return findInSpecifiedPaths(getSearchPaths(), fileNamePattern);
    }
    
    protected List<? extends Path> getSearchPaths()
    {
        return searchPaths;
    }
    
    protected Optional<Path> findInSearchPath(Predicate<Path> pathMatcher)
    throws IOException
    {
        return findInSpecifiedPaths(getSearchPaths(), pathMatcher);
    }
    
    protected Optional<Path> findInSearchPath(Predicate<Path> pathMatcher, Comparator<Path> sort)
    throws IOException
    {
        return findInSpecifiedPaths(getSearchPaths(), pathMatcher, sort);
    }

    protected Optional<Path> findInSpecifiedPaths(Collection<? extends Path> paths, Pattern fileNamePattern)
    throws IOException
    {
        return findInSpecifiedPaths(paths, new FileNamePatternMatcher(fileNamePattern));
    }
    
    protected Optional<Path> findInSpecifiedPaths(Collection<? extends Path> paths, Predicate<Path> pathMatcher)
    throws IOException
    {
        //Default is reverse order so in case files are ordered by version we get the highest one
        return findInSpecifiedPaths(paths, pathMatcher, Comparator.reverseOrder());
    }
    
    protected Optional<Path> findInSpecifiedPaths(Collection<? extends Path> paths, Predicate<Path> pathMatcher, Comparator<Path> sort)
    throws IOException
    {
        for (Path pathDir : paths)
        {
            if (Files.exists(pathDir))
            {
                try (Stream<Path> pathDirStream = Files.list(pathDir))
                {
                    Optional<Path> result = pathDirStream.filter(pathMatcher).sorted(sort).findFirst();
                    if (result.isPresent())
                        return result;
                }
            }
        }

        return Optional.empty();
    }
    
}
