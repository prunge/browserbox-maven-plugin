package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.execute.BrowserControl;
import au.net.causal.maven.plugins.browserbox.execute.SeleniumBrowserControl;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.sdklib.internal.avd.AvdInfo;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class AndroidFirefoxBrowserBox extends AndroidBrowserBox
{
    private final ScheduledExecutorService profileFixerExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder()
                                                                                                                     .setDaemon(true)
                                                                                                                     .setNameFormat(getName() + "-profilefixer-%d")
                                                                                                                     .build());

    /**
     * Creates an Android browser box for Firefox.
     *
     * @param boxConfiguration configuration for the box itself.
     * @param projectConfiguration project-level configuration.
     * @param context runtime context.
     * @param firefoxApkVersionRegistry version registry for downloading Firefox APKs.
     * @param geckoDriverVersionRegistry version registry for downloading Firefox GeckoDriver artifacts.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public AndroidFirefoxBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context,
                                    VersionedItemStore firefoxApkVersionRegistry, VersionedItemStore geckoDriverVersionRegistry)
    throws BrowserBoxException
    {
        super(boxConfiguration, projectConfiguration, context, firefoxApkVersionRegistry, geckoDriverVersionRegistry);
    }

    @Override
    protected String avdName()
    {
        return "browserbox.causal.net.au_firefoxX";
        //TODO
    }

    @Override
    public BrowserControl browserControl() throws BrowserBoxException
    {
        String deviceId = "emulator-5554";
        DesiredCapabilities capabilities = new DesiredCapabilities(BrowserType.FIREFOX, "", Platform.ANDROID);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "org.mozilla.firefox");
        chromeOptions.put("androidDeviceSerial", deviceId);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        try
        {
            return new SeleniumBrowserControl(getWebDriverUrl(), capabilities);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error creating browser control: " + e.getMessage(), e);
        }
    }

    @Override
    public Capabilities getSeleniumDesiredCapabilities()
    throws BrowserBoxException
    {
        IDevice device = findAvdDevice();
        String deviceId = device.getSerialNumber();

        DesiredCapabilities capabilities = new DesiredCapabilities(BrowserType.FIREFOX, "", Platform.ANDROID);
        Map<String, Object> chromeOptions = new LinkedHashMap<>();
        chromeOptions.put("androidPackage", "org.mozilla.firefox");
        chromeOptions.put("androidDeviceSerial", deviceId);
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        return capabilities;
    }

    @Override
    public String getSeleniumBrowserType()
    {
        return BrowserType.FIREFOX;
    }

    @Override
    protected void afterApkInstall(IDevice emulatorDevice, AvdInfo avd) throws BrowserBoxException
    {
        super.afterApkInstall(emulatorDevice, avd);
        startProfileFixerThread(emulatorDevice);
    }

    /**
     * Geckodriver 0.26.0 (and possibly higher) has a bug on Windows platforms that incorrectly creates profiles on the Android device using Windows
     * path separators.  Browserbox works around this problem by polling for these files, and when detected rename them to the proper name and restart the browser.
     * <p>
     *
     * The incorrect file created: {@code /mnt/sdcard/org.mozilla.firefox-geckodriver-profile\\user.js}
     * <br>
     * The corrected name: {@code /mnt/sdcard/org.mozilla.firefox-geckodriver-profile/user.js}
     *
     * @param emulatorDevice Android emulator device.
     *
     * @see <a href="https://github.com/mozilla/geckodriver/issues/1691">Issue 1691</a>
     */
    private void startProfileFixerThread(IDevice emulatorDevice)
    {
        //Workaround is only needed for Windows
        if (SystemUtils.IS_OS_WINDOWS)
            profileFixerExecutor.scheduleAtFixedRate(() -> profileFixerLoop(emulatorDevice), 0L, 2, TimeUnit.SECONDS);
    }

    private void profileFixerLoop(IDevice device)
    {
        if (!device.isOnline())
        {
            profileFixerExecutor.shutdown();
            return;
        }

        //Monitor filesystem for Windows-mutilated paths in profiles
        // /mnt/sdcard/
        FileListingService files = device.getFileListingService();

        try
        {
            FileListingService.FileEntry mntDir = Stream.of(files.getChildrenSync(files.getRoot())).filter(e -> e.isDirectory() && e.getName().equals("mnt")).findFirst()
                                                        .orElseThrow(() -> new FileNotFoundException("/mnt"));
            FileListingService.FileEntry sdCardDir = Stream.of(files.getChildrenSync(mntDir)).filter(e -> e.isDirectory() && e.getName().equals("sdcard")).findFirst()
                                                        .orElseThrow(() -> new FileNotFoundException("/mnt/sdcard"));

            boolean badProfileFileExists = Stream.of(files.getChildrenSync(sdCardDir)).anyMatch(e -> !e.isDirectory() && e.getName().equals("org.mozilla.firefox-geckodriver-profile\\\\user.js"));

            if (badProfileFileExists)
            {
                getContext().getLog().info("Bad Android/Firefox profile found, fixing...");

                //Fix/rename the file
                ConsoleOutputReceiver out = new ConsoleOutputReceiver();
                device.executeShellCommand("mkdir -p /mnt/sdcard/org.mozilla.firefox-geckodriver-profile", out);
                device.executeShellCommand("mv /mnt/sdcard/org.mozilla.firefox-geckodriver-profile\\\\user.js /mnt/sdcard/org.mozilla.firefox-geckodriver-profile/user.js", out);

                //Kill existing Firefox
                device.executeShellCommand("pm clear org.mozilla.firefox", out);

                //Reconfigure permissions for new instance
                device.executeShellCommand("pm grant org.mozilla.firefox android.permission.READ_EXTERNAL_STORAGE", out);
                device.executeShellCommand("pm grant org.mozilla.firefox android.permission.WRITE_EXTERNAL_STORAGE", out);

                //Restart Firefox
                device.executeShellCommand("am start -W -n org.mozilla.firefox/.App --es args -marionette\\ -profile\\ /mnt/sdcard/org.mozilla.firefox-geckodriver-profile", out);

                getContext().getLog().info("Bad profile fixed, Firefox restarted");
            }
            else
                getContext().getLog().debug("Polling for bad profile - no bad profile file found... yet...");
        }
        catch (AdbCommandRejectedException e)
        {
            //Device offline is fine, don't need to warn about that
            //'closed' message means the user killed the emulator and the connection was closed
            if (!e.isDeviceOffline() && !"closed".equals(e.getMessage()))
                getContext().getLog().warn("Failed to list directory: " + e.getMessage(), e);
        }
        catch (IOException | TimeoutException | ShellCommandUnresponsiveException e)
        {
            getContext().getLog().warn("Failed to list directory: " + e.getMessage(), e);
        }

    }
}
