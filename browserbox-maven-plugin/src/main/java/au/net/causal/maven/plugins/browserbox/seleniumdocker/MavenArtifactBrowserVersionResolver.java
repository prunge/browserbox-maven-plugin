package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.box.BrowserVersionsArtifactReader;

import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

public class MavenArtifactBrowserVersionResolver extends VersionPropertiesArchiveResolver 
{
    private final String browserType;
    private final BoxContext context;
    private final BrowserVersionsArtifactReader browserVersionsArtifactReader = new BrowserVersionsArtifactReader();
    
    public MavenArtifactBrowserVersionResolver(String browserType, BoxContext context)
    {
        Objects.requireNonNull(browserType, "browserType == null");
        Objects.requireNonNull(context, "context == null");
        this.browserType = browserType;
        this.context = context;
    }

    @Override
    protected VersionsResources readBrowserVersionResources() 
    throws BrowserBoxException 
    {
        Path versionJar = browserVersionsArtifactReader.readBrowserVersionArtifact(context);
        if (versionJar == null)
            return new VersionsResources(new Properties(), new Properties());
        
        return VersionsResources.fromJarFile(versionJar, browserType);
    }
}
