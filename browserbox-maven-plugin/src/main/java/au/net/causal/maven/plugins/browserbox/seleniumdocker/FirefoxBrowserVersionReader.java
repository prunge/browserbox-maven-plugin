package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.beijunyi.parallelgit.filesystem.Gfs;
import com.beijunyi.parallelgit.filesystem.GitFileSystem;
import com.google.common.io.MoreFiles;
import com.google.common.io.RecursiveDeleteOption;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.DosFileAttributeView;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Checks out Docker/Selenium using its Git repository and determines available versions from this Git repo.
 */
public class FirefoxBrowserVersionReader implements TagBrowserVersionReader 
{
    private final DockerNaming dockerNaming = new DockerNaming();
    private final Path gitBaseDirectory;

    /**
     * Creates a firefox browser version reader.
     *
     * @param gitBaseDirectory the base directory that will be used to hold the checked out Git repository locally.
     */
    public FirefoxBrowserVersionReader(Path gitBaseDirectory)
    {
        Objects.requireNonNull(gitBaseDirectory, "gitBaseDirectory == null");
        this.gitBaseDirectory = gitBaseDirectory;
    }

    /**
     * Removes the read-only attribute of every file and directory under the specified base directory if it exists on a Windows file system.
     * <p>
     *
     * Sometimes on Windows platforms some Git files are checked out with the read-only attribute which causes problems when
     * switching branches or even just cleaning up files.  Resetting the attribute fixes the problem.
     *
     * @param directory the directory whose files will be marked as non-read-only.
     *
     * @throws IOException if an error occurs.
     */
    private void makeFilesNonReadOnlyOnWindows(Path directory)
    throws IOException
    {
        for (Path file : MoreFiles.fileTraverser().depthFirstPreOrder(directory))
        {
            FileStore fileStore = Files.getFileStore(file);
            if (fileStore.supportsFileAttributeView(DosFileAttributeView.class))
            {
                // Set read-only
                Files.setAttribute(file, "dos:readonly", false);
            }
        }
    }

    /**
     * On Windows platforms, Git sometimes checks out shell scripts with Windows line-endings, which causes problems when using these files to build
     * Docker images.  This method will process any shell script file and rewrite line-endings to be Unix style line endings.
     *
     * @param directory the directory whose files to process recursively.
     *
     * @throws IOException if an I/O error occurs.
     */
    private void fixLineEndingsForShellFiles(Path directory)
    throws IOException
    {
        for (Path file : MoreFiles.fileTraverser().depthFirstPreOrder(directory))
        {
            //Match all *.sh and 'generate_config' file which we know in Docker images are shell scripts
            if (file.getFileName().toString().endsWith(".sh") || file.getFileName().toString().equalsIgnoreCase("generate_config"))
            {
                String content = new String(Files.readAllBytes(file), StandardCharsets.UTF_8);
                content = content.replaceAll("\\r\\n", "\n");
                Files.write(file, content.getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    /**
     * Checks out the specified version of Docker/Selenium.
     *
     * @param tag the Docker image tag to check out.  This also corresponds to the tag name in the Git repo for Docker/Selenium.
     *
     * @return the Git file system with the checked out files.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public GitFileSystem checkoutFirefoxGit(Tag tag)
    throws BrowserBoxException
    {
        try 
        {
            Path gitDirectory = gitBaseDirectory.resolve("firefox-git");

            //Just clean out the directory to be safe
            //Mostly it works to just switch branch using Git, but rarely it fails with a weird error so just clean it to be safe
            if (Files.exists(gitDirectory))
            {
                makeFilesNonReadOnlyOnWindows(gitDirectory);
                MoreFiles.deleteRecursively(gitDirectory, RecursiveDeleteOption.ALLOW_INSECURE);
            }

            Git git = Git.cloneRepository()
                         .setDirectory(gitDirectory.toFile())
                         .setURI(dockerNaming.seleniumDockerGitRepoUrl().toString())
                         .setBranch(tag.getName())
                         .call();

            git.clean().setCleanDirectories(true).call();
            git.reset().setMode(ResetCommand.ResetType.HARD).call();

            git.checkout().setName("refs/tags/" + tag.getName()).call();

            makeFilesNonReadOnlyOnWindows(gitDirectory);
            fixLineEndingsForShellFiles(gitDirectory);

            //Check out project is not already existing
            return Gfs.newFileSystem(tag.getName(), git.getRepository());
        }
        catch (IOException | GitAPIException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
    
    @Override
    public String readBrowserVersion(Tag tag)
    throws BrowserBoxException
    {
        try
        {
            //Check out project is not already existing
            try(GitFileSystem gfs = checkoutFirefoxGit(tag))
            {
                Path dockerFile = gfs.getPath("NodeFirefox/Dockerfile");
                if (Files.notExists(dockerFile))
                    return null;
                
                String dockerFileContent = new String(Files.readAllBytes(dockerFile), StandardCharsets.UTF_8);

                Pattern firefoxVersionPattern = Pattern.compile("^ARG FIREFOX_VERSION=(.+)$", Pattern.MULTILINE);
                Pattern firefoxVersionPattern2 = Pattern.compile("^ENV FIREFOX_VERSION (.+)$", Pattern.MULTILINE);
                Matcher matcher = firefoxVersionPattern.matcher(dockerFileContent);
                if (matcher.find())
                    return matcher.group(1);
                
                matcher = firefoxVersionPattern2.matcher(dockerFileContent);
                if (matcher.find())
                    return matcher.group(1);
            }
            
            return null;
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e.getMessage(), e);
        }
    }
}
