package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.ListeningExecutorService;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenReaderFilterRequest;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;
import org.codehaus.plexus.util.cli.StreamConsumer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Optional;
import java.util.Properties;
import java.util.regex.Pattern;

public class MacMicrosoftRemoteDesktopFinder extends GenericPathBasedFinder
{
    private static final Pattern APP_PATTERN = Pattern.compile("^Microsoft\\sRemote\\sDesktop.app$", Pattern.CASE_INSENSITIVE);
    
    private final WindowsRemoteDesktopTools windowsRemoteDesktopTools = new WindowsRemoteDesktopTools();
    
    //TODO turn this on with a propery or setting or something
    private boolean usePasswordFillHack = true;
    
    public MacMicrosoftRemoteDesktopFinder()
    {
        super(ImmutableList.of(Paths.get("/Applications"), Paths.get(System.getProperty("user.home")).resolve("Applications")));
    }

    @Override
    protected Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, 
                                         BoxConfiguration boxConfiguration, 
                                         ProjectConfiguration projectConfiguration, 
                                         BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> executable = findInSpecifiedPaths(getSearchPaths(), APP_PATTERN);
        if (!executable.isPresent())
            return Optional.empty();

        Commandline commandline =  new Commandline("open");
        Path rdpFile = windowsRemoteDesktopTools.generateRdpFile(connectionInfo, WindowsRemoteDesktopTools.RdpToolMode.MAC, box, boxConfiguration, projectConfiguration, context);
        
        commandline.addArguments(args("-W", rdpFile.toAbsolutePath().toString()));
        
        return Optional.of(commandline);
    }

    @Override
    public Optional<ToolRunner> findTool(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<ToolRunner> original = super.findTool(connectionInfo, box, boxConfiguration, projectConfiguration, context);
        if (!usePasswordFillHack || !original.isPresent())
            return original;
        
        //This runs an Applescript that will attempt to fill in the password into the Mac Remote Desktop client
        //However this only works if there isn't another dialog first such as a cert warning
        //Still might be handy for convenience
        return Optional.of(new ToolRunner()
        {
            @Override
            public void prepareRun(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                                   ProjectConfiguration projectConfiguration, BoxContext context)
            throws BrowserBoxException
            {
                original.get().prepareRun(connectionInfo, box, boxConfiguration, projectConfiguration, context);
            }

            @Override
            public int run(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration,
                           ProjectConfiguration projectConfiguration, BoxContext context,
                           StreamConsumer out, StreamConsumer err, ListeningExecutorService executor)
            throws IOException, BrowserBoxException 
            {
                //Spawn another process after a delay to auto-fill-in the password in the RDP client
                //A bit hacky but make it opt-in
                Runnable r = () -> runFillInScript(connectionInfo, box, boxConfiguration, projectConfiguration, context, out, err);
                new Thread(r).start();
                
                return original.get().run(connectionInfo, box, boxConfiguration, projectConfiguration, context, out, err, executor);
            }
        });
    }
    
    private void runFillInScript(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context, StreamConsumer out, StreamConsumer err)
    {
        try 
        {
            Thread.sleep(500L);
            
            //Generate applescript file
            Path scriptFile = generateFillInApplescriptFile(connectionInfo, box, boxConfiguration, projectConfiguration, context);

            Commandline commandline = new Commandline("osascript");
            commandline.addArguments(args(scriptFile.toAbsolutePath().toString()));

            context.getLog().info("Running RDP password filler script");
            int result = CommandLineUtils.executeCommandLine(commandline, out, err);
            context.getLog().info("Fill-in script result: " + result);
        }
        catch (CommandLineException | IOException | InterruptedException e)
        {
            context.getLog().warn("Error occurred while executing fill-in script: " + e.getMessage(), e);
        }
    }
    
    private Path generateFillInApplescriptFile(ConnectionInfo connectionInfo, BrowserBox box, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws IOException
    {
        Path tempDirectory = context.getTempDirectory();
        Path scriptFile = tempDirectory.resolve("browserbox-rdp-filler.applescript");
        Properties extraProperties = new Properties();
        extraProperties.setProperty("connection.host", connectionInfo.getUri().getHost());
        extraProperties.setProperty("connection.port", String.valueOf(connectionInfo.getUri().getPort()));
        if (connectionInfo.getUsername() != null)
            extraProperties.setProperty("connection.user", connectionInfo.getUsername());
        else
            extraProperties.setProperty("connection.user", "");
        if (connectionInfo.getPassword() != null)
            extraProperties.setProperty("connection.password", connectionInfo.getPassword());
        else
            extraProperties.setProperty("connection.password", "");

        try (Reader r = new InputStreamReader(MacMicrosoftRemoteDesktopFinder.class.getResource("rdpfill.applescript").openStream(), StandardCharsets.UTF_8);
             Writer writer = Files.newBufferedWriter(scriptFile))
        {
            MavenReaderFilterRequest request = new MavenReaderFilterRequest(r, true, projectConfiguration.getProject(),
                    Collections.emptyList(), true, context.getSession(),
                    extraProperties);
            try (Reader fr = context.getReaderFilter().filter(request))
            {
                IOUtil.copy(fr, writer);
            }
            catch (MavenFilteringException e)
            {
                throw new RuntimeException("Error filtering script template: " + e.getMessage(), e);
            }
        }

        return scriptFile;
    }
}
