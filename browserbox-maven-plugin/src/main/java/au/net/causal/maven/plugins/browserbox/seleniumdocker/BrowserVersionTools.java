package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.annotations.VisibleForTesting;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionRange;
import org.eclipse.aether.version.VersionScheme;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Utilities for dealing with browser versions.
 */
public class BrowserVersionTools 
{
    /**
     * Creates/parses versions that abide by the standard Maven versioning rules for version ordering and
     * comparison.
     */
    private final VersionScheme versionScheme = new GenericVersionScheme();

    /**
     * Creates a Maven version range string that matches a particular version that has already been parsed into
     * tokens.
     * <p>
     *
     * For example, the version {@code 1.2.3}, parsed into tokens {@code [1, 2, 3]}, will generate a version range
     * {@code "[1.2.3, 1.2.4)"} so the whole range of {@code 1.2.3} subversions, including {@code 1.2.3} itself will
     * match.  Another example, {@code 1.0} parsed into {@code [1, 0]} will generate {@code "[1.0, 1.1)"}.
     *
     * @param versionTokens already-parsed tokens of a version.  Each token would have been delimited by '.' in the
     *                      original version string.
     *
     * @return a version range string suitable for matching the input version and its sub-versions but not including the
     *         next version at the input version's level.
     *
     * @see VersionScheme#parseVersionRange(String)
     */
    @VisibleForTesting
    static String createVersionRangeString(int... versionTokens) 
    {
        StringBuilder buf = new StringBuilder();

        buf.append("[");

        for (int i = 0; i < versionTokens.length - 1; i++) 
        {
            buf.append(versionTokens[i]);
            buf.append('.');
        }
        buf.append(versionTokens[versionTokens.length - 1]);

        buf.append(',').append(' ');

        for (int i = 0; i < versionTokens.length - 1; i++) 
        {
            buf.append(versionTokens[i]);
            buf.append('.');
        }
        buf.append(versionTokens[versionTokens.length - 1] + 1);

        buf.append(")");

        return buf.toString();
    }

    /**
     * Splits a version string into numeric tokens that are split by '.'.  {@code version} must have numeric-only
     * tokens.
     * <p>
     *
     * For example, {@code 15.2.0} will parse to {@code [15, 2, 0]}.
     *
     * @param version the version string to parse.
     *
     * @return the parsed version tokens, or null if the version was unparseable.
     */
    private static int[] parseVersionTokens(String version)
    {
        String[] tokens = version.split(Pattern.quote("."));
        int[] numericTokens = new int[tokens.length];
        try 
        {
            for (int i = 0; i < tokens.length; i++) 
            {
                numericTokens[i] = Integer.parseInt(tokens[i]);
            }

            return numericTokens;
        } 
        catch (NumberFormatException e) 
        {
            return null;
        }
    }

    /**
     * Searches a collection of versions for a match, returning a converted result.
     *
     * @param version the version string to search for.
     * @param allVersions a collection of typed versions.
     * @param versionExtractor a function that can convert a typed version in {@code allVersions} to a version string.
     * @param resultExtractor a function that converts a value in {@code allVersions} to some kind of result.
     *
     * @param <T> typed version type.
     * @param <R> result type.
     *
     * @return the value matching {@code version} found in {@code allVersions} converted, or {@code null} if no
     *         match was found.
     */
    private <T, R extends Comparable<R>> R findBrowserVersionExactMatch(String version, Collection<T> allVersions,
                                                                        Function<T, String> versionExtractor, 
                                                                        Function<T, R> resultExtractor) 
    {
        for (T curEntry : allVersions) 
        {
            String curVersion = versionExtractor.apply(curEntry);
            if (curVersion.equalsIgnoreCase(version))
                return resultExtractor.apply(curEntry);
        }

        //Not found
        return null;
    }

    /**
     * Finds a matching browser version in a list of versions, possibly not an exact match.
     * Inexact version matching will only work when the input version has numeric-only tokens separated by '.'.
     *
     * @param version the version to find.
     * @param allVersions full list of versions.
     * @param versionExtractor function to convert versions in the versions list to strings that can be matched with the input version.
     * @param resultExtractor function to convert a found matching version in allVersions to a result.
     * @param <T> type of versions in the versions list.
     * @param <R> result type.
     *
     * @return the first matching result, or null if no match was found.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public <T, R extends Comparable<R>> R findBrowserVersion(String version, Collection<T> allVersions,
                                                             Function<T, String> versionExtractor, 
                                                             Function<T, R> resultExtractor)
    throws BrowserBoxException 
    {
        R match = null;
        
        //Try range first - this is so, e.g. 56.0 will match 56.0.3 and not 56.0 which is earlier
        //To match 56.0 you would enter browser version of '56.0.0'
        int[] versionTokens = parseVersionTokens(version);
        if (versionTokens != null)
            match = findHighestMatchingVersion(allVersions, versionTokens, versionExtractor, resultExtractor);
        if (match != null)
            return match;

        //Range failed, try exact string match
        match = findBrowserVersionExactMatch(version, allVersions, versionExtractor, resultExtractor);
        if (match != null)
            return match;

        return null;
    }

    /**
     * Searches a collection of typed versions, finding the highest version matching a target version that has already been split
     * into tokens.  A version will match if a particular version has the target version as a prefix or matches exactly.
     * Maven standard version rules apply when comparing versions.
     * <p>
     *
     * e.g. a target of {@code 1.2} encoded as {@code [1, 2]} will match {@code 1.2.1} and {@code 1.2.2}, but
     * {@code 1.2.2} will be returned because it is the highest.
     *
     * @param entries list of typed versions to search.
     * @param versionTokens the target version already tokenized, split by '.'.
     *                      e.g. version {@code 1.2.3} will be pre-tokenized into {@code [1, 2, 3}.
     * @param versionExtractor function that converts typed versions in {@code entries} to strings for matching with the
     *                         tokenized target version.
     * @param resultExtractor function that converts typed versions to results, which are then compared/ordered to find
     *                        the highest one.
     * @param <T> typed version type.
     * @param <R> result type that allows versions to be compared.
     *
     * @return the highest matching converted version, or null if no versions matched the target version.
     *
     * @throws BrowserBoxException if an error occurs parsing versions.
     */
    @VisibleForTesting
    <T, R extends Comparable<R>> R findHighestMatchingVersion(Collection<T> entries, int[] versionTokens, 
                                                              Function<T, String> versionExtractor, 
                                                              Function<T, R> resultExtractor)
    throws BrowserBoxException 
    {
        String range = createVersionRangeString(versionTokens);
        try 
        {
            VersionRange versionRange = versionScheme.parseVersionRange(range);

            //Want to find the highest matching version, so scan all of the versions, find the ones that match
            //and keep the highest one
            Version bestVersion = null;
            T bestEntry = null;
            List<R> bestList = new ArrayList<>();
            for (T curEntry : entries) 
            {
                String curVersionString = versionExtractor.apply(curEntry);
                Version curVersion = versionScheme.parseVersion(curVersionString);
                if (versionRange.containsVersion(curVersion)) 
                {
                    if (bestVersion == null || bestVersion.compareTo(curVersion) < 0) 
                    {
                        bestEntry = curEntry;
                        bestVersion = curVersion;
                        bestList.clear();
                        bestList.add(resultExtractor.apply(bestEntry));
                    }
                    else if (bestVersion.compareTo(curVersion) == 0)
                        bestList.add(resultExtractor.apply(curEntry));
                }
            }

            if (bestList.isEmpty())
                return null;
            else 
            {
                //Pick the last one of the equal best so we get the latest version
                Collections.sort(bestList);
                return bestList.get(bestList.size() - 1);
            }
        } 
        catch (InvalidVersionSpecificationException e) 
        {
            throw new BrowserBoxException("Error generating version range: " + e.getMessage(), e);
        }
    }

    /**
     * Finds the version in a collection that is the closest one that is less than the specified target version.
     * Versions are parsed and compared to using Maven standard versioning rules.
     * The target version does not have to exist in the list.
     * If there is no version less than the target version, {@code null} is returned.
     * <p>
     *
     * For example, if we have {@code [1.0, 1.1, 2.0, 2.0.2]}, the closest version less than
     * {@code 2.0.1} is {@code 2.0}, the closest version less than {@code 1.8beta} is {@code 1.1} and
     * there is no closes version less than {@code 0.9} so null is returned for that target.
     *
     * @param target the target version to find.
     * @param versions a collection of versions to search.  Does not need to be sorted.
     *
     * @return a version in {@code versions} that is less than but not equal to the target version, or {@code null}
     *         if it doesn't exist.
     *
     * @throws BrowserBoxException if an error occurs parsing version numbers.
     */
    public String findClosestVersionLessThan(String target, Collection<String> versions)
    throws BrowserBoxException
    {
        VersionEntry parsedTarget;
        try
        {
            parsedTarget = new VersionEntry(target, versionScheme.parseVersion(target));
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new BrowserBoxException("Failed to parse target version '" + target + "'.", e);
        }

        //Parse the versions so we get a sensible ordering and comparator
        List<VersionEntry> parsedVersions = new ArrayList<>(versions.size());
        for (String version : versions)
        {
            try
            {
                parsedVersions.add(new VersionEntry(version, versionScheme.parseVersion(version)));
            }
            catch (InvalidVersionSpecificationException e)
            {
                throw new BrowserBoxException("Failed to parse version '" + version + "' in version list.", e);
            }
        }

        Collections.sort(parsedVersions);
        int pos = Collections.binarySearch(parsedVersions, parsedTarget);

        //Exact match, find element before the found one
        if (pos > 0)
            return parsedVersions.get(pos - 1).versionString;

        //Matches first element, no elements before this one
        if (pos == 0)
            return null;

        //Not found
        //pos is -(insertion point) - 1
        int insertionPoint = -pos - 1;
        if (insertionPoint > 0)
            return parsedVersions.get(insertionPoint - 1).versionString;

        //Insertion point is the start of the list
        //No elements before
        return null;
    }

    /**
     * Holds an original version string and its parsed form which is suitable for comparison.
     * Used for sorting version numbers while still maintaining the original string.
     */
    private static class VersionEntry implements Comparable<VersionEntry>
    {
        /**
         * Original version string.
         */
        private final String versionString;

        /**
         * Parsed version that is used for ordering and comparison.
         */
        private final Version parsedVersion;

        public VersionEntry(String versionString, Version parsedVersion)
        {
            this.versionString = versionString;
            this.parsedVersion = parsedVersion;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final VersionEntry that = (VersionEntry) o;
            return parsedVersion.equals(that.parsedVersion);
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(parsedVersion);
        }

        @Override
        public int compareTo(VersionEntry o)
        {
            return this.parsedVersion.compareTo(o.parsedVersion);
        }
    }
}
