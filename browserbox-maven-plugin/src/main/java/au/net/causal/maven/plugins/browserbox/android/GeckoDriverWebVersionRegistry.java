package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

public class GeckoDriverWebVersionRegistry implements VersionRegistry, VersionedItemStore
{
    private final BoxContext context;
    private final Platform defaultPlatform;

    public GeckoDriverWebVersionRegistry(BoxContext context, Platform defaultPlatform)
    {
        this.context = context;
        this.defaultPlatform = defaultPlatform;
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query) throws BrowserBoxException
    {
        //TODO hardcoded for now
        try
        {
            Map<Platform, URL> platformDownloads = ImmutableMap.of(
                    //Platform.WINDOWS, new URL("https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-win64.zip")
                    //Special daily build that fixes:
                    //https://github.com/mozilla/geckodriver/issues/1691
                    //https://github.com/mozilla/geckodriver/issues/1689
                    //Platform.WINDOWS, new URL("https://firefox-ci-tc.services.mozilla.com/api/queue/v1/task/L-5CIG93SR25Ywz5MKEPww/runs/0/artifacts/public/geckodriver.zip"),
                    Platform.WINDOWS, new URL("https://firefox-ci-tc.services.mozilla.com/api/queue/v1/task/CiiMzd-cQqulgKdEhLx2mA/runs/0/artifacts/public/geckodriver.zip"),

                    Platform.LINUX_64, new URL("https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz"),
                    Platform.MAC, new URL("https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-macos.tar.gz")
            );
            WebDriverItem item = new WebDriverItem("0.26.0", defaultPlatform, platformDownloads, new CompatibilityInfo(0, 1000));
            return new ItemList(ImmutableList.of(item), Collections.emptyList());
        }
        catch (MalformedURLException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    @Override
    public URL saveItemContents(Item item) throws BrowserBoxException
    {
        //TODO only temp required

        try
        {
            Path dir = context.getTempDirectory().resolve("geckodriver");
            Files.createDirectories(dir);
            Path file = dir.resolve(FilenameUtils.getName(item.getUrl().getPath()));
            try (InputStream is = item.getUrl().openStream())
            {
                Files.copy(is, file);
            }
            return file.toUri().toURL();
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e);
        }
    }
}
