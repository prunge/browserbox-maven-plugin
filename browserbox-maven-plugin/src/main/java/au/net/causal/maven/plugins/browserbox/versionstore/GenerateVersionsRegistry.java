package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A writable version registry that saves items to the generate versions context.
 * <p>
 *
 * When items are saved, all items (including existing ones) are checked whether they are valid and are removed if they are not.
 */
public class GenerateVersionsRegistry extends PropertiesBasedVersionRegistry implements WritableVersionRegistry
{
    private final GenerateVersionsContext generateContext;
    private final Optional<Log> logger;
    private final boolean checkExistingUrls;
    
    public GenerateVersionsRegistry(GenerateVersionsContext generateContext)
    {
        Objects.requireNonNull(generateContext, "generateContext == null");
        this.generateContext = generateContext;
        this.logger = Optional.empty();
        this.checkExistingUrls = true;
    }

    public GenerateVersionsRegistry(GenerateVersionsContext generateContext, Log logger)
    {
        this(generateContext, logger, true);
    }

    public GenerateVersionsRegistry(GenerateVersionsContext generateContext, Log logger, boolean checkExistingUrls)
    {
        Objects.requireNonNull(generateContext, "generateContext == null");
        this.generateContext = generateContext;
        this.logger = Optional.ofNullable(logger);
        this.checkExistingUrls = checkExistingUrls;
    }

    @Override
    protected Properties readDownloadProperties()
    throws BrowserBoxException
    {
        return generateContext.getDownloads();
    }

    @Override
    public void saveItems(Collection<? extends Item> items) 
    throws BrowserBoxException
    {
        for (Item item : items)
        {
            String existingUrlStr = generateContext.getDownloads().getProperty(item.getVersion());
            String newUrlStr = item.getUrl().toExternalForm();
            if (!newUrlStr.equals(existingUrlStr))
                generateContext.getDownloads().setProperty(item.getVersion(), newUrlStr);
        }

        //Find any versions that are no longer valid if needed
        if (checkExistingUrls)
        {
            FilterNonWorkingUrlsVersionsRegistry filter = new FilterNonWorkingUrlsVersionsRegistry(this, logger.orElse(null));
            Set<String> validVersions = filter.readAllItems(new Query()).stream()
                    .map(Item::getVersion)
                    .collect(Collectors.toSet());
            generateContext.getDownloads().keySet().retainAll(validVersions);
        }

        try
        {
            generateContext.saveDownloads();
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e);
        }
    }

    protected GenerateVersionsContext getGenerateContext()
    {
        return generateContext;
    }

    protected Optional<Log> getLogger()
    {
        return logger;
    }
}
