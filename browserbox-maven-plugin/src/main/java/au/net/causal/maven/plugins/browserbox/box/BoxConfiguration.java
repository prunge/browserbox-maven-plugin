package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.base.Splitter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Configures a browser box.
 */
public class BoxConfiguration
{
    private String browserType;
    private String browserVersion;
    private int webDriverPort;
    private String containerName;
    private Resolution resolution;
    private Double scaleFactor;
    private SslTrustConfiguration sslTrust;
    private final List<String> tunnelPorts = new ArrayList<>();
    private final Map<String, String> configuration = new LinkedHashMap<>();

    /**
     * The name of the browser container.  Could be the name of the Docker container for docker-based boxes or
     * the name of a Vagrant box.
     */
    public String getContainerName()
    {
        return containerName;
    }

    public void setContainerName(String containerName)
    {
        this.containerName = containerName;
    }

    public String getBrowserType() 
    {
        return browserType;
    }

    public void setBrowserType(String browserType) 
    {
        this.browserType = browserType;
    }

    public String getBrowserVersion() 
    {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) 
    {
        this.browserVersion = browserVersion;
    }

    public int getWebDriverPort() 
    {
        return webDriverPort;
    }

    public void setWebDriverPort(int webDriverPort) 
    {
        this.webDriverPort = webDriverPort;
    }

    /**
     * @return additional browser-box-specific configuration.
     */
    public Map<String, String> getConfiguration() 
    {
        return configuration;
    }

    /**
     * @return the screen resolution to run the browser at.  This is a hint, it may not be supported by all
     *         browser types.
     */
    public Resolution getResolution()
    {
        return resolution;
    }

    public void setResolution(Resolution resolution)
    {
        this.resolution = resolution;
    }

    /**
     * @return the graphics/DPI scaling for the browser UI.  This can be used to make things appear larger on high-DPI
     *         displays or simply to test with browsers running in high-DPI mode.  e.g. 2.0 scales the UI by a factor of 2,
     *         making the UI twice as big.
     */
    public Double getScaleFactor()
    {
        return scaleFactor;
    }

    public void setScaleFactor(Double scaleFactor)
    {
        this.scaleFactor = scaleFactor;
    }

    /**
     * @return configuration for SSL certificates that will be installed as trusted into the target browser.
     */
    public SslTrustConfiguration getSslTrust()
    {
        return sslTrust;
    }

    public void setSslTrust(SslTrustConfiguration sslTrust)
    {
        this.sslTrust = sslTrust;
    }

    /**
     * Configures ports to tunnel from the host machine running Maven into the browser box.  These ports will be 
     * accessible from the browser box through localhost.
     * 
     * @param tunnelPortStrings strings configuring tunnel ports.  This can be either a single port number or a string
     *                          in the form of [p]:[q] where p is the port on the host running Maven and q is the port
     *                          as accessed on the browser box.  A single port number translates to [p]:[p] which tunnels
     *                          the same port number from host to browser box.
     */
    public void setTunnelPorts(List<String> tunnelPortStrings)
    {
        this.tunnelPorts.clear();
        this.tunnelPorts.addAll(tunnelPortStrings);
    }
    
    public List<String> getTunnelPorts()
    {
        return tunnelPorts;
    }
    
    public List<TunnelPort> tunnelPortConfiguration()
    throws BrowserBoxException
    {
        List<TunnelPort> tunnelPorts = new ArrayList<>(this.tunnelPorts.size());
        for (String tunnelPortString : this.tunnelPorts)
        {
            if (tunnelPortString != null && !tunnelPortString.trim().isEmpty())
            {
                try
                {
                    tunnelPorts.add(TunnelPort.parse(tunnelPortString));
                }
                catch (ParseException e)
                {
                    throw new BrowserBoxException("Failed to parse tunnel port '" + tunnelPortString + "'.  Use localport:remoteport or just a port number.", e);
                }
            }
        }
        return tunnelPorts;
    }
    
    public void setConfiguration(Map<String, String> configuration)
    {
        this.configuration.clear();
        this.configuration.putAll(configuration);
    }

    /**
     * Configuration of a port tunneled from host to the browser box.  This makes ports on the local machine on 
     * {@linkplain #getLocalPort() local port} ...
     */
    static class TunnelPort
    {
        private int localPort;
        private int browserPort;

        public TunnelPort()
        {
        }

        public TunnelPort(int localPort, int browserPort)
        {
            this.localPort = localPort;
            this.browserPort = browserPort;
        }

        /**
         * @return the port on the local machine to tunnel to the browser.
         */
        public int getLocalPort()
        {
            return localPort;
        }

        /**
         * @return the port as seen from the browser in the box.
         */
        public int getBrowserPort()
        {
            return browserPort;
        }

        public void setLocalPort(int localPort)
        {
            this.localPort = localPort;
        }

        public void setBrowserPort(int browserPort)
        {
            this.browserPort = browserPort;
        }

        public static TunnelPort parse(String s)
        throws ParseException
        {
            List<String> tokens = Splitter.on(':').limit(2).splitToList(s);
            List<Integer> portNumbers = new ArrayList<>(tokens.size());
            for (String token : tokens)
            {
                try
                {
                    portNumbers.add(Integer.parseInt(token));
                }
                catch (NumberFormatException e)
                {
                    ParseException ex = new ParseException(token, 0);
                    ex.initCause(e);
                    throw ex;
                }
            }

            if (portNumbers.size() == 2)
                return new TunnelPort(portNumbers.get(0), portNumbers.get(1));
            else if (portNumbers.size() == 1)
                return new TunnelPort(portNumbers.get(0), portNumbers.get(0));
            else
                throw new ParseException(s, 0);
        }

        @Override
        public String toString()
        {
            return getBrowserPort() + "->" + getLocalPort();
        }
    }
}
