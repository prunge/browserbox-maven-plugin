package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.Collection;

/**
 * A version registry that can save versions and URLS of items.  Stores of this type only
 * saves references to items, not the items themselves (it does not download the item 
 * contents).
 */
public interface WritableVersionRegistry
{
    /**
     * Saves items to the registry.
     * 
     * @param items items to save.
     *              
     * @throws BrowserBoxException if an error occurs.
     */
    public void saveItems(Collection<? extends Item> items)
    throws BrowserBoxException;
}
