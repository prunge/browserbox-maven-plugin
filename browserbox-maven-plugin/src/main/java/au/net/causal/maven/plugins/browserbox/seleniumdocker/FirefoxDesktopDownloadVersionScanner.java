package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import org.apache.maven.plugin.logging.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutorService;

public class FirefoxDesktopDownloadVersionScanner extends FirefoxDownloadVersionScanner
{
    public FirefoxDesktopDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist)
    {
        super(log, versionBlacklist);
    }

    public FirefoxDesktopDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist, ExecutorService executor)
    {
        super(log, versionBlacklist, executor);
    }

    @Override
    protected URL downloadUrlForVersion(URL versionUrl) throws IOException
    {
        return linux64BitVersionDownload(versionUrl);
    }

    private URL linux64BitVersionDownload(URL versionUrl)
    throws IOException
    {
        //This doesn't actually drill right down to the download of the file, but this is not needed for desktop/Docker so it saves some time

        //Next file listing should have something like 'linux-x86_64'
        Document d = Jsoup.parse(versionUrl, Math.toIntExact(pageTimeout.toMillis()));
        for (String href : d.getElementsByTag("a").eachAttr("href"))
        {
            String operatingSystemToken = lastFilePathToken(href);
            if (operatingSystemToken.equalsIgnoreCase("linux-x86_64"))
                return new URL(versionUrl, href);
        }

        return null;
    }
}
