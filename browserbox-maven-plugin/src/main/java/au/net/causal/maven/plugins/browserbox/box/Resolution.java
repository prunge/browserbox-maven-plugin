package au.net.causal.maven.plugins.browserbox.box;

/**
 * Configures the screen resolution the browser box runs in.
 */
public class Resolution
{
    private int width;
    private int height;

    public Resolution()
    {
    }
    
    public Resolution(int width, int height)
    {
        this.width = width;
        this.height = height;
    }
    
    /**
     * @return width in pixels.
     */
    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * @return height in pixels.
     */
    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    @Override
    public String toString()
    {
        return getWidth() + "x" + getHeight();
    }
}
