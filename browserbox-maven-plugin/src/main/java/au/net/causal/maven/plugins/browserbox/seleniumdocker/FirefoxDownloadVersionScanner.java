package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Generates a properties object that maps versions to either a 'true' value to indicate that version is valid or
 * 'false' to indicate that version is invalid and should not be attempted to be downloaded.  A version might be invalid
 * if it does not have a 64-bit binary available for download, for example.
 */
public abstract class FirefoxDownloadVersionScanner
{
    private final Log log;
    protected final Duration pageTimeout = Duration.ofMinutes(5);
    private final ExecutorService executor;
    
    private final BrowserVersionBlacklist versionBlacklist;
    
    protected FirefoxDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist)
    {
        this(log, versionBlacklist, Executors.newSingleThreadExecutor());
    }
    
    protected FirefoxDownloadVersionScanner(Log log, BrowserVersionBlacklist versionBlacklist, ExecutorService executor)
    {
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(versionBlacklist, "versionBlacklist == null");
        Objects.requireNonNull(executor, "executor == null");
        this.log = log;
        this.versionBlacklist = versionBlacklist;
        this.executor = executor;
    }

    public void generateBlacklist(URL downloadListUrl, Properties blacklistStore)
    throws IOException, BrowserBoxException
    {
        AvailableVersionsDetail detail = readAvailableVersionsData(downloadListUrl, ImmutableSet.of(), ImmutableMap.of());
        detail.getAvailableVersions().forEach(v -> blacklistStore.setProperty(v, "true"));
        detail.getBlacklistedVersions().forEach(v -> blacklistStore.setProperty(v, "false"));
    }

    public Set<String> readAvailableVersions(URL downloadListUrl)
    throws IOException, BrowserBoxException
    {
        return readAvailableVersionsData(downloadListUrl, ImmutableSet.of(), ImmutableMap.of()).getAvailableVersions();
    }
    
    private static class CheckedVersion
    {
        private final String version;
        private final URL versionUrl;
        private final URL downloadUrl;
        private final boolean allowed;

        public CheckedVersion(String version, URL versionUrl, URL downloadUrl, boolean allowed)
        {
            this.version = version;
            this.versionUrl = versionUrl;
            this.downloadUrl = downloadUrl;
            this.allowed = allowed;
        }

        public String getVersion() 
        {
            return version;
        }

        public URL getVersionUrl()
        {
            return versionUrl;
        }

        public URL getDownloadUrl()
        {
            return downloadUrl;
        }

        public boolean isAllowed()
        {
            return allowed;
        }
    }
    
    public static class AvailableVersionsDetail
    {
        private final Set<AvailableVersion> availableVersions = new LinkedHashSet<>();
        private final Set<String> blacklistedVersions = new LinkedHashSet<>();
        
        public AvailableVersionsDetail(Set<? extends AvailableVersion> availableVersions, Set<String> blacklistedVersions)
        {
            this.availableVersions.addAll(availableVersions);
            this.blacklistedVersions.addAll(blacklistedVersions);
        }

        public Set<String> getAvailableVersions() 
        {
            return getAvailableVersionDownloads().stream().map(AvailableVersion::getVersion).collect(Collectors.toSet());
        }

        public Set<? extends AvailableVersion> getAvailableVersionDownloads()
        {
            return Collections.unmodifiableSet(availableVersions);
        }

        public Set<String> getBlacklistedVersions() 
        {
            return Collections.unmodifiableSet(blacklistedVersions);
        }
    }

    public class AvailableVersion
    {
        private final String version;
        private final URL versionUrl;
        private final URL downloadUrl;

        public AvailableVersion(String version, URL versionUrl, URL downloadUrl)
        {
            this.version = version;
            this.versionUrl = versionUrl;
            this.downloadUrl = downloadUrl;
        }

        public String getVersion()
        {
            return version;
        }

        public URL getVersionUrl()
        {
            return versionUrl;
        }

        public URL getDownloadUrl()
        throws IOException
        {
            if (downloadUrl != null)
                return downloadUrl;
            else
                return downloadUrlForVersion(versionUrl);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof AvailableVersion)) return false;
            AvailableVersion that = (AvailableVersion) o;
            return getVersion().equals(that.getVersion());
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(getVersion());
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", AvailableVersion.class.getSimpleName() + "[", "]")
                    .add("version='" + version + "'")
                    .add("versionUrl=" + versionUrl)
                    .toString();
        }
    }

    protected abstract URL downloadUrlForVersion(URL versionUrl)
    throws IOException;
    
    public AvailableVersionsDetail readAvailableVersionsData(URL downloadListUrl, Set<String> ignoredVersions, Map<String, URL> knownValidVersionDownloads)
    throws IOException, BrowserBoxException
    {
        Document d = Jsoup.parse(downloadListUrl, Math.toIntExact(pageTimeout.toMillis()));
        Set<AvailableVersion> allowedVersions = new LinkedHashSet<>();
        Set<String> readBlacklist = new LinkedHashSet<>();
        List<Future<CheckedVersion>> tasks = new ArrayList<>();
                
        Set<String> existingKnownVersionBlacklist = versionBlacklist.getKnownVersionBlacklist();
        Set<String> existingKnownVersionWhitelist = versionBlacklist.getKnownVersionWhitelist();
        
        for (String href : d.getElementsByTag("a").eachAttr("href"))
        {
            URL hrefUrl = new URL(downloadListUrl, href);
            Future<CheckedVersion> task = executor.submit(() -> 
            {
                String versionToken = lastFilePathToken(href);
                
                if (existingKnownVersionBlacklist.contains(versionToken) || ignoredVersions.contains(versionToken))
                    return new CheckedVersion(versionToken, hrefUrl, null, false);
                else if (existingKnownVersionWhitelist.contains(versionToken))
                    return new CheckedVersion(versionToken, hrefUrl, null, true);
                else if (knownValidVersionDownloads.containsKey(versionToken))
                    return new CheckedVersion(versionToken, hrefUrl, knownValidVersionDownloads.get(versionToken), true);
                else if (startsWithNumber(versionToken))
                {
                    URL downloadUrl = downloadUrlForVersion(hrefUrl);
                    if (downloadUrl != null)
                        return new CheckedVersion(versionToken, hrefUrl, downloadUrl, true);
                    else 
                    {
                        log.debug("No appropriate version of " + versionToken + " exists");
                        return new CheckedVersion(versionToken, null, null, false);
                    }
                }
                
                return new CheckedVersion(null, null, null, false);
            });
            tasks.add(task);
        }
        
        for (Future<CheckedVersion> task : tasks)
        {
            try 
            {
                CheckedVersion cv = task.get();
                if (cv.getVersion() != null)
                {
                    if (cv.isAllowed())
                        allowedVersions.add(new AvailableVersion(cv.getVersion(), cv.getVersionUrl(), cv.getDownloadUrl()));
                    else
                        readBlacklist.add(cv.getVersion());
                }
            }
            catch (InterruptedException e)
            {
                throw new BrowserBoxException("Interrupted waiting for results", e);
            }
            catch (ExecutionException e)
            {
                Throwables.propagateIfPossible(e.getCause(), IOException.class, BrowserBoxException.class);
                throw new BrowserBoxException(e);
            }
        }
        
        return new AvailableVersionsDetail(allowedVersions, readBlacklist);
    }
    
    private static boolean startsWithNumber(String version)
    {
        return Pattern.compile("\\d.*").matcher(version).matches();
    }
    
    protected static String lastFilePathToken(String path)
    {
        String[] pathElements = path.split(Pattern.quote("/"));
        return pathElements[pathElements.length - 1];
    }
}
