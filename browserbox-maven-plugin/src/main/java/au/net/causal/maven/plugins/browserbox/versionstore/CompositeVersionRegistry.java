package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class CompositeVersionRegistry implements VersionRegistry
{
    private final List<? extends VersionRegistry> registries;

    public CompositeVersionRegistry(List<? extends VersionRegistry> registries)
    {
        this.registries = ImmutableList.copyOf(registries);
    }

    public CompositeVersionRegistry(VersionRegistry... registries)
    {
        this(new ImmutableList.Builder<VersionRegistry>().add(registries).build());
    }

    protected List<? extends VersionRegistry> getRegistries()
    {
        return registries;
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        Map<String, Item> resultMap = new LinkedHashMap<>();
        ImmutableList.Builder<BrowserBoxException> errors = ImmutableList.builder();
        for (VersionRegistry registry : getRegistries())
        {
            ItemList curResults = registry.readAllItemsAllowFailures(query);
            errors.addAll(curResults.getErrors());
            for (Item download : curResults.getItems())
            {
                resultMap.putIfAbsent(download.getVersion(), download);
            }
        }

        return new ItemList(ImmutableList.copyOf(resultMap.values()), errors.build());
    }

    @Override
    public ItemVersions readAllVersionsAllowFailures(Query query)
    throws BrowserBoxException
    {
        Set<String> versions = new LinkedHashSet<>();
        ImmutableList.Builder<BrowserBoxException> errors = ImmutableList.builder();
        for (VersionRegistry registry : getRegistries())
        {
            ItemVersions curResults = registry.readAllVersionsAllowFailures(query);
            errors.addAll(curResults.getErrors());
            versions.addAll(curResults.getVersions());
        }

        return new ItemVersions(ImmutableList.copyOf(versions), errors.build());
    }

    @Override
    public Item itemForVersion(String version) 
    throws BrowserBoxException
    {
        //Overridden to allow failures from one of the children - since other children might return
        //proper results
        
        Objects.requireNonNull(version, "version == null");
        
        ItemList results = readAllItemsAllowFailures(new Query());
        Optional<? extends Item> foundItem = results.getItems().stream()
                                                               .filter(item -> version.equals(item.getVersion()))
                                                               .findAny();
        if (foundItem.isPresent())
            return foundItem.get();
        
        //If no items but there are errors, throw it, otherwise return null indicating nothing found
        if (results.getErrors().isEmpty())
            return null;
        else if (results.getErrors().size() == 1)
            throw results.getErrors().get(0);
        else
        {
            BrowserBoxException mainError = results.getErrors().get(0);
            for (BrowserBoxException otherError : results.getErrors().subList(1, results.getErrors().size()))
            {
                mainError.addSuppressed(otherError);
            }
            throw mainError;
        }
    }
}
