package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration.TunnelPort;
import au.net.causal.maven.plugins.browserbox.box.RemoteSslNssDatabaseGenerator.DatabaseType;
import au.net.causal.maven.plugins.browserbox.execute.SshBrowserControl;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.ImageReference;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

public class FirefoxDockerBrowserBox extends SeleniumDockerBrowserBox 
{
    /**
     * Box configuration property that, if set, enables the Firefox Debugger Server which will run on the specified
     * port.
     */
    public static final String FIREFOX_DEBUGGER_SERVER_PORT = "firefoxDebuggerServerPort";

    private final RemoteSslNssDatabaseGenerator sslNssDatabaseGenerator = new RemoteSslNssDatabaseGenerator(this);

    public FirefoxDockerBrowserBox(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                                      BoxContext context, ImageReference seleniumBaseImageName) 
    {
        super(boxConfiguration, projectConfiguration, context, seleniumBaseImageName, DesiredCapabilities.firefox());
    }

    /* disabled for now since it can't maximize
    @Override
    public BrowserControl browserControl() 
    throws BrowserBoxException
    {
        return new FirefoxSshBrowserControl(getConnectionInfo(StandardConnectionType.SSH), getBoxConfiguration());
    }
    */

    @Override
    public String getSeleniumBrowserType()
    {
        return BrowserType.FIREFOX;
    }

    protected static class FirefoxSshBrowserControl extends SshBrowserControl
    {
        private final BoxConfiguration boxConfiguration;

        public FirefoxSshBrowserControl(ConnectionInfo sshConnectionInfo, BoxConfiguration boxConfiguration)
        {
            super(sshConnectionInfo);
            this.boxConfiguration = boxConfiguration;
        }

        @Override
        protected String launchCommand(URI url)
        {
            String cmd = "export DISPLAY=\":99.0\";firefox \"" + url.toString() + "\"";
            if (boxConfiguration.getScaleFactor() != null && boxConfiguration.getScaleFactor() > 0.0)
                cmd = "export GDK_DPI_SCALE=\"" + boxConfiguration.getScaleFactor() + "\";" + cmd;

            return cmd;
        }

        @Override
        protected String quitCommand()
        {
            return "killall firefox";
        }
    }

    @Override
    public void installCertificates(Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        try
        {
            //Use DBM database type unconditionally
            //Newer Firefox versions will auto-upgrade DBM to SQL when first encountered so DBM is always safe
            sslNssDatabaseGenerator.generateLocal(getCertificateDbDirectory(), DatabaseType.DBM, certificates);
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error generating certificate DB for Firefox: " + e.getMessage(), e);
        }
    }

    private Path getCertificateDbDirectory()
    throws IOException
    {
        return getContext().getTempDirectory().resolve(getBoxConfiguration().getContainerName()).resolve("certdb");
    }

    protected Integer getFirefoxDebuggerServerPort()
    throws BrowserBoxException
    {
        String debuggerServerPortStr = getBoxConfiguration().getConfiguration().get(FIREFOX_DEBUGGER_SERVER_PORT);
        if (Strings.isNullOrEmpty(debuggerServerPortStr))
            return null;

        try
        {
            return Integer.parseInt(debuggerServerPortStr);
        }
        catch (NumberFormatException e)
        {
            throw new BrowserBoxException("Invalid " + FIREFOX_DEBUGGER_SERVER_PORT + " value: " + e, e);
        }
    }

    @Override
    protected List<? extends TunnelPort> getReverseTunnelPorts()
    throws BrowserBoxException
    {
        ImmutableList.Builder<TunnelPort> reverseTunnelPorts = ImmutableList.<TunnelPort>builder()
                                                                .addAll(super.getReverseTunnelPorts());

        Integer firefoxDebuggerServerPort = getFirefoxDebuggerServerPort();
        if (firefoxDebuggerServerPort != null)
            reverseTunnelPorts.add(new TunnelPort(firefoxDebuggerServerPort, firefoxDebuggerServerPort));

        return reverseTunnelPorts.build();
    }

    @Override
    public Capabilities getSeleniumDesiredCapabilities()
    throws BrowserBoxException
    {
        //Assume this has been already generated - if not then bail outPath certDbDirectory
        Path certDbDirectory;
        try
        {
            certDbDirectory = getCertificateDbDirectory();
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error reading certificate directory: " + e.getMessage(), e);
        }

        //Turn off Marionette for early versions
        //For these earlier versions of Firefox, an extension was bundled with the browser which did the Webdriver stuff
        //Later versions had webdriver functionality built into the browser and disabled/omitted this extension
        //but it needed Geckodriver support
        //All Geckodriver versions seem to support legacy communication so this is OK even for modern Selenium installations
        //running older browsers
        boolean requiresMarionetteOverride = (getBrowserMajorVersion() < 48);

        //No cert DB directory, must not have been generated
        boolean certDbDirectoryExists = Files.exists(certDbDirectory);

        //Need custom settings if we want the debugger port opened
        Integer debuggerServerPort = getFirefoxDebuggerServerPort();

        if (!certDbDirectoryExists && !requiresMarionetteOverride && debuggerServerPort == null)
            return null;

        //Don't use this new style for now...
        /*
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile(certDbDirectory.toFile());
        options.setProfile(profile);
         */

        //Use the old style DesiredCapabilities.firefox() for maximum compatibility with older versions
        //This is important when the remote Selenium is older, but works with newest ones as well
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();

        FirefoxProfile profile = null;
        if (certDbDirectoryExists)
            profile = new FirefoxProfile(certDbDirectory.toFile());

        if (requiresMarionetteOverride)
            capabilities.setCapability(FirefoxDriver.MARIONETTE, false);

        if (debuggerServerPort != null)
        {
            capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, ImmutableMap.of("args", ImmutableList.of("--start-debugger-server", debuggerServerPort.toString())));

            if (profile == null)
                profile = new FirefoxProfile();

            //A few profile settings that will allow the debugger to work
            profile.setPreference("remote.enabled", true);
            profile.setPreference("devtools.debugger.remote-enabled", true);
            profile.setPreference("devtools.debugger.prompt-connection", false);
            profile.setPreference("devtools.chrome.enabled", true);
        }

        if (profile != null)
            capabilities.setCapability(FirefoxDriver.PROFILE, profile);

        return capabilities;
    }
}
