package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="prepare", defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST, requiresProject = false)
public class PrepareMojo extends AbstractBrowserBoxMojo 
{
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BrowserBoxException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        try 
        {
            BrowserBox browserBox = browserBox(service);
            if (browserBox.hasImage())
                getLog().info("Image for " + browserBox.getName() + " already exists");
            else
            {
                getLog().info("Preparing image for " + browserBox.getName());
                browserBox.prepareImage();
            }
            
        }
        catch (BrowserBoxException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }
}
