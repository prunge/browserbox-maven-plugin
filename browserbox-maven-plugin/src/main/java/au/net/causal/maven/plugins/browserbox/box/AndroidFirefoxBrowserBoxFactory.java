package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.android.FirefoxApkVersionRegistry;
import au.net.causal.maven.plugins.browserbox.android.GeckoDriverWebVersionRegistry;
import au.net.causal.maven.plugins.browserbox.android.Platform;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionTools;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.EmptyBrowserVersionBlacklist;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.FirefoxAndroidDownloadVersionScanner;
import au.net.causal.maven.plugins.browserbox.versionstore.CompositeVersionedItemStore;
import au.net.causal.maven.plugins.browserbox.versionstore.GenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenBrowserVersionsArtifactRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenRepositoryStore;
import au.net.causal.maven.plugins.browserbox.versionstore.SimpleFileDownloader;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionScheme;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@BrowserBoxBundled
public class AndroidFirefoxBrowserBoxFactory implements BrowserBoxFactory
{
    private static final String ANDROID_FIREFOX = "android-firefox";

    private final BrowserVersionTools versionTools = new BrowserVersionTools();

    private FirefoxApkVersionRegistry webVersionRegistry(BoxContext context)
    throws BrowserBoxException
    {
        BrowserVersionsArtifactReader browserVersionsArtifactReader = new BrowserVersionsArtifactReader();
        Properties downloadsProperties = browserVersionsArtifactReader.readPropertiesFile(ANDROID_FIREFOX + "-downloads.properties", context);
        Properties knownVersionsProperties = browserVersionsArtifactReader.readPropertiesFile(ANDROID_FIREFOX + "-known-versions.properties", context);

        Set<String> ignoredVersions = Maps.filterValues(Maps.fromProperties(knownVersionsProperties), "false"::equals)
                                          .keySet();
        Map<String, URL> knownValidVersions = propertiesToUrlMap(downloadsProperties);

        return new FirefoxApkVersionRegistry(context.getLog(), ignoredVersions, knownValidVersions);
    }

    private static Map<String, URL> propertiesToUrlMap(Properties p)
    throws BrowserBoxException
    {
        ImmutableMap.Builder<String, URL> b = new ImmutableMap.Builder<>();
        for (String key : p.stringPropertyNames())
        {
            String uriString = p.getProperty(key);
            try
            {
                b.put(key, new URL(uriString));
            }
            catch (MalformedURLException e)
            {
                throw new BrowserBoxException("Invalid URL in properties: " + uriString, e);
            }
        }

        return b.build();
    }

    private GeckoDriverWebVersionRegistry geckoDriverWebVersionRegistry(BoxContext context, Platform platform)
    {
        return new GeckoDriverWebVersionRegistry(context, platform);
    }

    private VersionedItemStore geckoDriverVersionRegistry(BoxContext context)
    throws BrowserBoxException
    {
        /*
        Artifact saveTemplate = new DefaultArtifact("org.mozilla.firefox", "geckodriver", "zip", "0.0");
        Platform currentPlatform = context.getPlatform();

        return new CompositeVersionedItemStore(
                new WebDriverMavenRepositoryStore(saveTemplate, context, new SimpleFileDownloader(context::getTempDirectory), currentPlatform, context::getTempDirectory),
                new WebDriverVersionsMavenArtifactRegistry("android-firefox", context, currentPlatform)
        );
         */

        return geckoDriverWebVersionRegistry(context, Platform.current());
    }

    private VersionedItemStore firefoxApkVersionRegistry(BoxContext context)
    throws BrowserBoxException
    {
        Artifact saveTemplate = new DefaultArtifact("org.mozilla.firefox", "firefox-android-x86", "apk", "0.0");

        MavenBrowserVersionsArtifactRegistry mavenBrowserVersionsArtifactRegistry = new MavenBrowserVersionsArtifactRegistry("android-firefox", context);

        return new CompositeVersionedItemStore(
                new MavenRepositoryStore(saveTemplate, context, new SimpleFileDownloader(context::getTempDirectory), context.getLog()),
                mavenBrowserVersionsArtifactRegistry,
                webVersionRegistry(context)
        );
    }

    @Override
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) 
    throws BrowserBoxException 
    {
        initializeDefaults(boxConfiguration, context);
        return new AndroidFirefoxBrowserBox(boxConfiguration, projectConfiguration, context, firefoxApkVersionRegistry(context),
                                            geckoDriverVersionRegistry(context));
    }

    @Override
    public boolean supportsType(String type) 
    {
        return ANDROID_FIREFOX.equals(type);
    }

    @Override
    public Set<String> getKnownTypes() 
    {
        return Collections.singleton(ANDROID_FIREFOX);
    }

    //TODO common
    protected void initializeDefaults(BoxConfiguration boxConfiguration, BoxContext context)
    throws BrowserBoxException
    {
        //Version resolution code
        List<String> allVersions = availableKnownVersions(boxConfiguration.getBrowserType(), context);

        //First, if there is no version specified, default to the latest
        if (boxConfiguration.getBrowserVersion() == null && !allVersions.isEmpty())
        {
            boxConfiguration.setBrowserVersion(allVersions.get(allVersions.size() - 1));
            context.getLog().info("Selected " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion());
        }

        //Attempt to resolve to real version if there's no exact match of version
        if (!allVersions.contains(boxConfiguration.getBrowserVersion()))
        {
            String resolvedVersion = versionTools.findBrowserVersion(boxConfiguration.getBrowserVersion(), allVersions, Function.identity(), Function.identity());
            if (resolvedVersion != null)
            {
                context.getLog().info("Resolved " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() +
                        " to " + resolvedVersion);
                boxConfiguration.setBrowserVersion(resolvedVersion);
            }
            else
                context.getLog().warn("Could not resolve " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() + ".");
        }

        //While it might not be a container based box, the container name is still used for some stuff like naming
        //the video directory
        if (boxConfiguration.getContainerName() == null)
        {
            if (boxConfiguration.getBrowserVersion() == null)
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType());
            else
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType() + "-" + boxConfiguration.getBrowserVersion());
        }
    }

    @Override
    public List<String> availableKnownVersions(String type, BoxContext context) 
    throws BrowserBoxException 
    {
        //Parse into Maven objects purely for the sorting logic
        VersionScheme versionScheme = new GenericVersionScheme();

        Collection<String> versions = firefoxApkVersionRegistry(context).readAllVersions(new Query());

        return versions.stream()
                       .distinct()
                       .map(v -> parseVersion(versionScheme, v))
                       .sorted()
                       .map(Version::toString)
                       .collect(Collectors.toList());
    }

    private Version parseVersion(VersionScheme scheme, String versionString)
    {
        try
        {
            return scheme.parseVersion(versionString);
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAllImages(String type, BoxContext context) throws BrowserBoxException 
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    private FirefoxAndroidDownloadVersionScanner firefoxDownloadVersionScanner(BoxContext context,
                                                                               GenerateVersionsContext generateVersionsContext)
    {
        return new FirefoxAndroidDownloadVersionScanner(context.getLog(), new EmptyBrowserVersionBlacklist(),
                                                        generateVersionsContext.getDownloadExecutor());
    }

    @Override
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, BoxContext context, 
                                      GenerateVersionsContext generateContext) 
    throws BrowserBoxException, IOException 
    {
        FirefoxApkVersionRegistry webResolver = webVersionRegistry(context);
        GenerateVersionsRegistry saveRegistry = new GenerateVersionsRegistry(generateContext, context.getLog(), false);

        //Read any new versions
        Set<String> ignoredVersions = Maps.filterValues(Maps.fromProperties(generateContext.getKnownVersions()), "false"::equals)
                                          .keySet();
        Map<String, URL> knownValidVersions = propertiesToUrlMap(generateContext.getDownloads());
        FirefoxApkVersionRegistry.FirefoxApkItemList itemList =
                webResolver.readAllItemsAllowFailures(new FirefoxApkVersionRegistry.FirefoxApkQuery(ignoredVersions, knownValidVersions));
        saveRegistry.saveItems(itemList.getItems());

        if (itemList.getErrors().size() == 1)
            throw itemList.getErrors().get(0);
        else if (itemList.getErrors().size() > 1)
        {
            BrowserBoxException mainError = itemList.getErrors().get(0);
            for (BrowserBoxException otherError : itemList.getErrors().subList(1, itemList.getErrors().size()))
            {
                mainError.addSuppressed(otherError);
            }
            throw mainError;
        }

        //Save any invalid download URLs as well for next time
        if (!itemList.getInvalidVersions().isEmpty())
        {
            for (String invalidVersion : itemList.getInvalidVersions())
            {
                generateContext.getKnownVersions().setProperty(invalidVersion, "false");
            }
            generateContext.saveKnownVersions();
        }


        /*
        //Construct appropriate resolver tree
        FirefoxApkVersionRegistry webResolver = webVersionRegistry(context);
        GenerateVersionsRegistry saveRegistry = new GenerateVersionsRegistry(generateContext, context.getLog(), false);

        //Read any new versions
        Set<String> ignoredUrlStrings = Maps.filterValues(Maps.fromProperties(generateContext.getKnownVersions()), "false"::equals)
                                            .keySet();
        Set<URL> ignoredUrls = new LinkedHashSet<>();
        for (String ignoredUrlString : ignoredUrlStrings)
        {
            ignoredUrls.add(new URL(ignoredUrlString));
        }
        saveRegistry.saveItems(webResolver.readAllItems(new ChomeApkMirrorQuery(Maps.fromProperties(generateContext.getDownloads()).keySet(), ignoredUrls)));

        //Save any invalid download URLs as well for next time
        if (!webResolver.getInvalidDownloadUrls().isEmpty())
        {
            for (URL invalidDownloadUrl : webResolver.getInvalidDownloadUrls())
            {
                generateContext.getKnownVersions().setProperty(invalidDownloadUrl.toExternalForm(), "false");
            }
            generateContext.saveKnownVersions();
        }

        //Read new versions for GeckoDriver
        VersionRegistry geckoDriverWebResolver = geckoDriverWebVersionRegistry(context, Platform.current()); //Platform doesn't really matter - well it kind of does
        Collection<? extends Item> items = geckoDriverWebResolver.readAllItems(new Query());
        for (Platform platform : Platform.values())
        {
            Collection<? extends WebDriverItem> platformItems = WebDriverItem.translateAllToPlatform(items, platform);
            GeckoDriverGenerateVersionsRegistry geckoDriverSaveRegistry = new GeckoDriverGenerateVersionsRegistry(generateContext, platform, context.getLog());
            geckoDriverSaveRegistry.saveItems(platformItems);
        }


        //TODO added
        //Do blacklist only for Firefox
        context.getLog().info("Reading known download versions for " + type);
        firefoxDownloadVersionScanner(context, generateContext).generateBlacklist(dockerNaming.firefoxInstallerUrl().toURL(), generateContext.getKnownVersions());
        generateContext.saveKnownVersions();
        context.getLog().debug(generateContext.getKnownVersions().size() + " versions recorded");
        */
    }
}
