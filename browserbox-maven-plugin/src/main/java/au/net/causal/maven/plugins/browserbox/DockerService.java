package au.net.causal.maven.plugins.browserbox;

import io.fabric8.maven.docker.service.ServiceHub;

import java.util.Objects;

/**
 * Wrapper for interacting with the Docker service hub.
 */
public class DockerService
{
	private final ServiceHub serviceHub;
	private final String dockerHostAddress;

	public DockerService(ServiceHub serviceHub, String dockerHostAddress)
	{
		Objects.requireNonNull(serviceHub, "serviceHub == null");
		Objects.requireNonNull(dockerHostAddress, "dockerHostAddress == null");

		this.serviceHub = serviceHub;
		this.dockerHostAddress = dockerHostAddress;
	}

	/**
	 * The service hub provides a way for interacting with Docker.
     */
	public ServiceHub getServiceHub()
	{
		return serviceHub;
	}

	/**
	 * Returns the network address of the docker 'machine'.  For native Docker this will be localhost, but for systems
	 * that run Docker under a virtual machine, this may be the address of a VM in the system.  Use this address
	 * for TCP connections to browsers/services that are running in Docker machines.
     */
	public String getDockerHostAddress()
	{
		return dockerHostAddress;
	}
}
