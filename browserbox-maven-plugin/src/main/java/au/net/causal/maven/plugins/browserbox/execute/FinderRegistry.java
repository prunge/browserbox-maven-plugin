package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.box.ConnectionType;
import au.net.causal.maven.plugins.browserbox.box.MicrosoftVagrantBrowserBox.VagrantConnectionType;
import au.net.causal.maven.plugins.browserbox.box.MicrosoftVagrantBrowserBox.VirtualBoxUiFinder;
import au.net.causal.maven.plugins.browserbox.box.StandardConnectionType;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.codehaus.plexus.util.Os;

import java.util.Collection;

public class FinderRegistry 
{
    private final Multimap<ConnectionType, ToolFinder> findersByType = ArrayListMultimap.create();
    
    public FinderRegistry()
    {
        register(StandardConnectionType.VNC, new BrowserBoxVncViewerToolFinder());
        register(VagrantConnectionType.VIRTUALBOX_UI, new VirtualBoxUiFinder());

        if (Os.isFamily(Os.FAMILY_WINDOWS))
        {
            register(StandardConnectionType.VNC, new WindowsTigerVncFinder());
            register(StandardConnectionType.VNC, new WindowsRealVncFinder());
            
            register(StandardConnectionType.RDP, new WindowsRemoteDesktopFinder());
        }
        else if (Os.isFamily(Os.FAMILY_MAC))
        {
            register(StandardConnectionType.VNC, new MacTigerVncFinder());
            register(StandardConnectionType.VNC, new MacRealVncFinder());
            register(StandardConnectionType.VNC, new MacScreenSharingVncFinder());
            
            register(StandardConnectionType.RDP, new MacMicrosoftRemoteDesktopFinder());
            register(StandardConnectionType.RDP, new MacCordRemoteDesktopFinder());
        }
        else if (Os.isFamily(Os.FAMILY_UNIX))
        {
            register(StandardConnectionType.VNC, new LinuxTigerVncFinder());
        }

        //TODO linux, etc.
    }
    
    private void register(ConnectionType type, ToolFinder finder)
    {
        findersByType.put(type, finder);
    }
    
    public Collection<? extends ToolFinder> platformFinders(ConnectionType type)
    {
        return findersByType.get(type);
    }
    
    public ToolFinder platformCombinedFinder(ConnectionType type)
    {
        return new CombinedToolFinder(platformFinders(type));
    }
}
