package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import java.util.Objects;

/**
 * Tag for a Docker image.  Typically contains version information.
 */
public class Tag implements Comparable<Tag>
{
    private final String name;

    public Tag(String name) 
    {
        Objects.requireNonNull(name, "name == null");
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() 
    {
        return name;
    }

    @Override
    public boolean equals(Object o) 
    {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        Tag tag = (Tag) o;
        return Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() 
    {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Tag o) 
    {
        return getName().compareTo(o.getName());
    }
}
