package au.net.causal.maven.plugins.browserbox.ms;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Saves root SSL certificates for visited URLs.
 */
public class SslCertificateDownloader
{
    private final X509TrustManager defaultTrustManager;

    /**
     * Constructs an <code>SslCertificateDownloader</code>.
     *
     * @throws NoSuchAlgorithmException if an error occurs opening the trust store.
     * @throws KeyStoreException if an error occurs opening the trust store.
     */
    public SslCertificateDownloader()
    throws NoSuchAlgorithmException, KeyStoreException
    {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init((KeyStore)null);
        defaultTrustManager = (X509TrustManager)tmf.getTrustManagers()[0];
    }

    /**
     * Given a certificate from a site, lookup the root certificate in the trust store.
     *
     * @param cert the website certificate.
     *
     * @return the root certificate.
     */
    private X509Certificate lookupRootCertificate(X509Certificate cert)
    {
        List<X509Certificate> acceptedIssuers = new ArrayList<>(Arrays.asList(defaultTrustManager.getAcceptedIssuers()));

        //Quick and dirty way of getting at the root cert
        //I tried PKIX cert path builder and its ilk, I really did but I just couldn't get it working
        Map<Principal, X509Certificate> rootCertMap = acceptedIssuers.stream().collect(Collectors.toMap(X509Certificate::getSubjectDN, c -> c));
        X509Certificate best = cert;
        while (cert != null && !cert.getSubjectDN().equals(cert.getIssuerDN()))
        {
            cert = rootCertMap.get(cert.getIssuerDN());
            if (cert != null)
                best = cert;
        }

        return best;

        /*
        CertPathBuilder cpb = CertPathBuilder.getInstance("PKIX");
        X509CertSelector certSelector = new X509CertSelector();
        certSelector.setCertificate(cert);

        Set<TrustAnchor> trustAnchors = Stream.of(defaultTrustManager.getAcceptedIssuers()).map(c -> new TrustAnchor(c, null)).collect(Collectors.toSet());
        PKIXBuilderParameters cpp = new PKIXBuilderParameters(trustAnchors, certSelector);
        Collection<Object> everything = new ArrayList<>();
        everything.addAll(Arrays.asList(defaultTrustManager.getAcceptedIssuers()));
        //everything.addAll(Arrays.asList(certs));

        cpp.addCertStore(CertStore.getInstance("Collection", new CollectionCertStoreParameters(everything)));
        cpp.setRevocationEnabled(false);
        cpp.setMaxPathLength(12);
        cpp.setDate(new Date());
        cpp.setTrustAnchors(trustAnchors);

        try
        {
            CertPathBuilderResult a = cpb.build(cpp);
            CertPath certPath = a.getCertPath();
            System.out.println(certPath);
        }
        catch (CertPathBuilderException e)
        {
            throw e;
        }
        */
    }

    /**
     * Downloads root certificates for the given URL, following redirects.  One root certificate will be obtained
     * per URL visited (through redirects).
     *
     * @param url the URL to load.
     *
     * @return a set of root certificates that were obtained from <code>url</code> and any redirects.
     *
     * @throws IOException if an error occurs loading URLs.
     */
    public Set<? extends X509Certificate> downloadRootCertificates(URL url)
    throws IOException
    {
        Set<X509Certificate> allRoots = new LinkedHashSet<>();

        boolean redirect;

        do
        {
            HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
            con.setInstanceFollowRedirects(false); //We will handle redirects ourselves
            con.connect();
            Certificate[] certs = con.getServerCertificates();

            //Attempt to walk up the chain and lookup root
            X509Certificate cert = (X509Certificate)certs[certs.length - 1];
            X509Certificate root = lookupRootCertificate(cert);
            allRoots.add(root);

            //Now follow redirect if possible
            int status = con.getResponseCode();
            redirect = (status == HttpURLConnection.HTTP_MOVED_PERM ||
                        status == HttpURLConnection.HTTP_MOVED_TEMP ||
                        status == HttpURLConnection.HTTP_SEE_OTHER);

            if (redirect)
            {
                String redirectUrl = con.getHeaderField("Location");
                url = new URL(redirectUrl);
            }
        }
        while (redirect);

        return allRoots;
    }

    /**
     * Download a root certificate for the given URL.  Redirects are not followed.
     *
     * @param url the URL.
     *
     * @return the root certificate for accessing the specified URL.
     *
     * @throws IOException if an error occurs loading URLs.
     */
    public X509Certificate downloadRootCertificate(URL url)
    throws IOException
    {
        HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
        con.connect();
        Certificate[] certs = con.getServerCertificates();

        //Attempt to walk up the chain and lookup root
        X509Certificate cert = (X509Certificate)certs[certs.length - 1];
        X509Certificate root = lookupRootCertificate(cert);

        return root;
    }
}
