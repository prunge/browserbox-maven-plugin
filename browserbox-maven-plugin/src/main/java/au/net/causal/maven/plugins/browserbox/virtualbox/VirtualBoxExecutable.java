package au.net.causal.maven.plugins.browserbox.virtualbox;

import java.time.Duration;
import java.util.Objects;

/**
 * Interface to <code>VirtualBox</code> executable tool.
 */
public interface VirtualBoxExecutable
{
    public void startVm(StartVmOptions options)
    throws VirtualBoxException;
    
    public static abstract class BaseOptions
    {
        private Duration timeout = Duration.ofHours(8L);

        public Duration getTimeout()
        {
            return timeout;
        }

        public void setTimeout(Duration timeout)
        {
            this.timeout = timeout;
        }
    }

    public static class StartVmOptions extends BaseOptions
    {
        private final String vmName;
        private boolean separate;
        private boolean scale;

        public StartVmOptions(String vmName)
        {
            Objects.requireNonNull(vmName, "vmName == null");
            this.vmName = vmName;
        }

        public String getVmName()
        {
            return vmName;
        }

        public boolean isSeparate()
        {
            return separate;
        }

        public void setSeparate(boolean separate)
        {
            this.separate = separate;
        }

        public boolean isScale()
        {
            return scale;
        }

        /**
         * Scale mode scales the VM to the window size.
         */
        public void setScale(boolean scale)
        {
            this.scale = scale;
        }
    }
}