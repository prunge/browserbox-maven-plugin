package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class TigerVncTools
{
    public Path generatePasswordFile(ConnectionInfo connectionInfo, BoxContext context)
    throws IOException
    {
        Path passwordFile = context.getTempDirectory().resolve("browserbox-vnc-passwordfile");
        //TODO could copy the entire key file from the server here instead of hardcoding
        try (InputStream is = TigerVncTools.class.getResourceAsStream("seleniumvncdefaultpass"))
        {
            Files.copy(is, passwordFile, StandardCopyOption.REPLACE_EXISTING);
        }

        return passwordFile;
    }
}
