package au.net.causal.maven.plugins.browserbox.virtualbox;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Interface to <code>VBoxManage</code> tool.
 */
public interface VirtualBoxManager
{
    public String guestControlRun(GuestControlRunOptions options)
    throws VirtualBoxException;

    public Map<String, String> showVmInfo(ShowVmInfoOptions options)
    throws VirtualBoxException;

    public void modifyVm(ModifyVmOptions options)
    throws VirtualBoxException;

    public void storageAttach(StorageAttachOptions options)
    throws VirtualBoxException;

    public void setVideoModeHint(SetVideoModeHintOptions options)
    throws VirtualBoxException;
    
    public static abstract class BaseOptions
    {
        private Duration timeout = Duration.ofHours(8L);

        public Duration getTimeout()
        {
            return timeout;
        }

        public void setTimeout(Duration timeout)
        {
            this.timeout = timeout;
        }
    }

    public static abstract class GuestControlOptions extends BaseOptions
    {
        private final String vmName;
        private String userName;
        private String password;

        public GuestControlOptions(String vmName)
        {
            Objects.requireNonNull(vmName, "vmName == null");
            this.vmName = vmName;
        }

        public String getVmName()
        {
            return vmName;
        }

        public String getUserName()
        {
            return userName;
        }

        public void setUserName(String userName)
        {
            this.userName = userName;
        }

        public String getPassword()
        {
            return password;
        }

        public void setPassword(String password)
        {
            this.password = password;
        }
    }

    public static class GuestControlRunOptions extends GuestControlOptions
    {
        private final String program;
        private final List<String> programArgs = new ArrayList<>();
        private boolean logOutput = true;

        public GuestControlRunOptions(String vmName, String program)
        {
            super(vmName);
            this.program = program;
        }

        public String getProgram()
        {
            return program;
        }

        public List<String> getProgramArgs()
        {
            return Collections.unmodifiableList(programArgs);
        }

        public void setProgramArgs(List<String> programArgs)
        {
            this.programArgs.clear();
            this.programArgs.addAll(programArgs);
        }

        public void setProgramArgs(String... programArgs)
        {
            setProgramArgs(Arrays.asList(programArgs));
        }

        public boolean isLogOutput()
        {
            return logOutput;
        }

        public void setLogOutput(boolean logOutput)
        {
            this.logOutput = logOutput;
        }
    }

    public static class ShowVmInfoOptions extends BaseOptions
    {
        private final String vmName;

        public ShowVmInfoOptions(String vmName)
        {
            this.vmName = vmName;
        }

        public String getVmName()
        {
            return vmName;
        }
    }

    public static class ModifyVmOptions extends BaseOptions
    {
        private final String vmName;
        private final String optionName;
        private final String optionValue;

        public ModifyVmOptions(String vmName, String optionName, String optionValue)
        {
            Objects.requireNonNull(vmName, "vmName == null");
            Objects.requireNonNull(optionName, "optionName == null");
            Objects.requireNonNull(optionValue, "optionValue == null");
            this.vmName = vmName;
            this.optionName = optionName;
            this.optionValue = optionValue;
        }

        public String getVmName()
        {
            return vmName;
        }

        public String getOptionName()
        {
            return optionName;
        }

        public String getOptionValue()
        {
            return optionValue;
        }
    }

    public static class StorageAttachOptions extends BaseOptions
    {
        private final String vmName;
        private final String controllerName;
        private final int device;
        private final int port;
        private final String type;
        private final String medium;

        public StorageAttachOptions(String vmName, String controllerName, int device, int port, String type, String medium)
        {
            this.vmName = vmName;
            this.controllerName = controllerName;
            this.device = device;
            this.port = port;
            this.type = type;
            this.medium = medium;
        }

        public String getVmName()
        {
            return vmName;
        }

        public String getControllerName()
        {
            return controllerName;
        }

        public int getDevice()
        {
            return device;
        }

        public int getPort()
        {
            return port;
        }

        public String getType()
        {
            return type;
        }

        public String getMedium()
        {
            return medium;
        }
    }

    public static class SetVideoModeHintOptions extends BaseOptions
    {
        private final String vmName;
        private final int width;
        private final int height;
        private final int bitsPerPixel;

        public SetVideoModeHintOptions(String vmName, int width, int height, int bitsPerPixel)
        {
            this.vmName = vmName;
            this.width = width;
            this.height = height;
            this.bitsPerPixel = bitsPerPixel;
        }

        public String getVmName() 
        {
            return vmName;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public int getBitsPerPixel()
        {
            return bitsPerPixel;
        }
    }
}