/**
 * JAXB schema package for parsing Microsoft download metadata.
 */
@XmlSchema(namespace = "http://www.w3.org/2005/Atom", elementFormDefault = XmlNsForm.QUALIFIED)
package au.net.causal.maven.plugins.browserbox.ms.feed;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
