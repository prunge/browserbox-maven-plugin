package au.net.causal.maven.plugins.browserbox.virtualbox;

import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;

import java.time.Duration;
import java.util.Objects;

public class LocalVirtualBoxSDL implements VirtualBoxSDL
{
    private final Commandline baseCommandLine;
    private final Log log;

    /**
     * @param baseCommandLine command line that has just the VBoxManage executable, no parameters.
     * @param log log that will receive output from the process.
     */
    public LocalVirtualBoxSDL(Commandline baseCommandLine, Log log)
    {
        Objects.requireNonNull(baseCommandLine, "baseCommandLine == null");
        Objects.requireNonNull(log, "log == null");

        this.baseCommandLine = baseCommandLine;
        this.log = log;
    }

    /**
     * Convenience converter varargs to String array.
     */
    private static String[] args(String... args)
    {
        return args;
    }

    @Override
    public void startVm(StartVmOptions options)
    throws VirtualBoxException
    {
        Commandline command = (Commandline)baseCommandLine.clone();
        if (options.isSeparate())
            command.addArguments(args("--separate"));

        command.addArguments(args("--startvm", options.getVmName()));
        executeCommand(command, options.getTimeout(), false);
    }

    private String executeCommand(Commandline commandLine, Duration timeout, boolean logOutput)
    throws VirtualBoxException
    {
        CommandLineUtils.StringStreamConsumer out;
        CommandLineUtils.StringStreamConsumer err;
        if (logOutput) 
        {
            out = new LoggingConsumer(log, "VBoxSDL> ");
            err = new LoggingConsumer(log, "VBoxSDL> ");
            
        }
        else 
        {
            out = new CommandLineUtils.StringStreamConsumer();
            err = new CommandLineUtils.StringStreamConsumer();
        }
        
        try
        {
            int exitCode = CommandLineUtils.executeCommandLine(commandLine, out, err, Math.toIntExact(timeout.getSeconds()));
            if (exitCode != 0) 
            {
                throw new VirtualBoxException(exitCode, out.getOutput(), err.getOutput(), "VBoxSDL error " + exitCode + ": " +
                        err.getOutput() + "\n" + out.getOutput());
                        //(err.getOutput().isEmpty() ? out.getOutput() : err.getOutput()));
            }
        }
        catch (CommandLineException e)
        {
            throw new VirtualBoxException("Error running VBoxSDL: " + e, e);
        }

        return out.getOutput();
    }

    /**
     * Every line of output from the VBoxManage process is sent to the log at INFO level.
     */
    private static class LoggingConsumer extends CommandLineUtils.StringStreamConsumer 
    {
        private final Log log;
        private final String prefix;

        public LoggingConsumer(Log log, String prefix)
        {
            this.log = log;
            this.prefix = prefix;
        }

        @Override
        public void consumeLine(String line)
        {
            super.consumeLine(line);
            log.info(prefix + line);
        }
    }
}
