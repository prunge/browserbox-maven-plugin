package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import com.google.common.base.Strings;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class BrowserSelector 
{
    private final Type type;
    private final String version;

    public BrowserSelector(Type type, String version) 
    {
        Objects.requireNonNull(type, "type == null");
        Objects.requireNonNull(version, "version == null");
        
        this.type = type;
        this.version = version;
    }
    
    public static BrowserSelector parse(BoxConfiguration box)
    throws ParseException
    {
        //e.g.
        //[name, version]
        //[IE, 8]
        //[IE8, null]
        //[edge, 15]
        //[edge, stable]
        
        String type = box.getBrowserType();
        String version = box.getBrowserVersion();
        
        if (Strings.isNullOrEmpty(version)) 
        {
            //Split by number
            version = type.replaceAll("[^\\d]+", "");
            type = type.replaceAll("[\\d]+", "");
        }
        
        if (version != null)
            version = version.toLowerCase(Locale.ENGLISH);
        
        try 
        {
            Type parsedType = Type.valueOf(type.toUpperCase(Locale.ENGLISH));
            return new BrowserSelector(parsedType, version);
        }
        catch (IllegalArgumentException e)
        {
            ParseException ex = new ParseException("Unrecognized browser type, should be one of " + Arrays.toString(Type.values()), 0);
            ex.initCause(e);
            throw ex;
        }
    }

    public Type getType() 
    {
        return type;
    }

    public String getVersion() 
    {
        return version;
    }

    public static enum Type
    {
        EDGE, IE
    }

    @Override
    public String toString() 
    {
        return getType() + "-" + getVersion();
    }
}
