package au.net.causal.maven.plugins.browserbox.ms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DownloadItem
{
    public static final URI VM_API_URL = URI.create("https://developer.microsoft.com/en-us/microsoft-edge/api/tools/vms/");
    
    private String name;
    private final List<Software> software = new ArrayList<>();

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Software> getSoftware()
    {
        return software;
    }
    
    public void setSoftware(List<Software> software)
    {
        this.software.clear();
        this.software.addAll(software);
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("DownloadItem{");
        sb.append("name='").append(name).append('\'');
        sb.append(", software=").append(software);
        sb.append('}');
        return sb.toString();
    }
}
