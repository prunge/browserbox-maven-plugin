package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.FileDownloader.Download;
import com.google.common.collect.ImmutableList;
import org.apache.maven.plugin.logging.Log;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.installation.InstallRequest;
import org.eclipse.aether.installation.InstallationException;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.version.Version;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class MavenRepositoryStore implements VersionedItemStore
{
    private final Artifact saveArtifactTemplate;
    private final RepositorySystem repositorySystem;
    private final RepositorySystemSession repositorySystemSession;
    private final List<RemoteRepository> remoteRepositories;
    private final FileDownloader downloader;
    private final Log log;
    
    public MavenRepositoryStore(Artifact saveArtifactTemplate,
                                RepositorySystem repositorySystem,
                                RepositorySystemSession repositorySystemSession, 
                                List<? extends RemoteRepository> remoteRepositories,
                                FileDownloader downloader, 
                                Log log)
    {
        Objects.requireNonNull(saveArtifactTemplate, "saveArtifactTemplate == null");
        Objects.requireNonNull(repositorySystem, "repositorySystem == null");
        Objects.requireNonNull(repositorySystemSession, "repositorySystemSession == null");
        Objects.requireNonNull(remoteRepositories, "remoteRepositories == null");
        Objects.requireNonNull(downloader, "downloader == null");
        Objects.requireNonNull(log, "log == null");

        this.saveArtifactTemplate = saveArtifactTemplate;
        this.repositorySystem = repositorySystem;
        this.repositorySystemSession = repositorySystemSession;
        this.remoteRepositories = ImmutableList.copyOf(remoteRepositories);
        this.downloader = downloader;
        this.log = log;
    }

    public MavenRepositoryStore(Artifact saveArtifactTemplate, BoxContext context, FileDownloader downloader, Log log)
    {
        this(saveArtifactTemplate, context.getRepositorySystem(), context.getRepositorySystemSession(), context.getRemoteRepositories(), downloader, log);
    }

    protected Artifact mavenArtifactForItem(String version)
    {
        return saveArtifactTemplate.setVersion(version);
    }

    protected Artifact mavenVersionSearchArtifactForItem()
    {
        return saveArtifactTemplate.setVersion("[0.0,)");
    }

    @Override
    public URL saveItemContents(Item item) 
    throws BrowserBoxException
    {
        Artifact artifact = mavenArtifactForItem(item.getVersion());
        ArtifactRequest resolveRequest = new ArtifactRequest(artifact.setFile(null), Collections.emptyList(), null);
        try
        {
            ArtifactResult resolveResult = repositorySystem.resolveArtifact(repositorySystemSession, resolveRequest);
            
            //If we get here, the artifact already exists in the local repo so no need to save again
            log.info("Artifact " + resolveResult.getArtifact().getFile().toPath() + " already exists in local repository, no need to save.");

            return resolveResult.getArtifact().getFile().toURI().toURL();
            
        }
        catch (MalformedURLException e)
        {
            throw new BrowserBoxException("Failed to install artifact to local repo: " + e.getMessage(), e);
        }
        catch (ArtifactResolutionException e)
        {
            //Means the artifact could not be resolved and doesn't exist in local repo, this is fine
        }
        
        //Try-with-resources will clean up any temp files after processing
        try (Download download = downloader.downloadFile(item.getUrl()))
        {
            Path downloadedFile = download.getFile();
            
            InstallRequest request = new InstallRequest();
            artifact = artifact.setFile(downloadedFile.toFile());
            request.setArtifacts(Collections.singletonList(artifact));
            try
            {
                repositorySystem.install(repositorySystemSession, request);

                //Re-resolve to local repo to get file
                ArtifactRequest reresolveRequest = new ArtifactRequest(artifact.setFile(null), Collections.emptyList(), null);
                ArtifactResult reresolveResult = repositorySystem.resolveArtifact(repositorySystemSession, reresolveRequest);
                return reresolveResult.getArtifact().getFile().toURI().toURL();
            }
            catch (InstallationException | ArtifactResolutionException | MalformedURLException e)
            {
                throw new BrowserBoxException("Failed to install artifact to local repo: " + e.getMessage(), e);
            }   
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Failed to download file: " + item.getUrl() + ": " + e.getMessage(), e);
        }
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        Artifact searchArtifact = mavenVersionSearchArtifactForItem();
        VersionRangeRequest request = new VersionRangeRequest(searchArtifact, remoteRepositories, null);
        try
        {
            VersionRangeResult result = repositorySystem.resolveVersionRange(repositorySystemSession, request);

            //No resolution, this is OK, return no results
            if (result.getHighestVersion() == null)
                return new ItemList(Collections.emptyList(), Collections.emptyList());
            
            ImmutableList.Builder<Item> resultBuilder = new ImmutableList.Builder<>();
            ImmutableList.Builder<BrowserBoxException> errorBuilder = new ImmutableList.Builder<>();
            for (Version version : result.getVersions())
            {
                try
                {
                    if (!query.getIgnoredVersions().contains(version.toString()))
                        resultBuilder.add(versionToItem(version));
                }
                catch (ArtifactResolutionException | IOException | BrowserBoxException e)
                {
                    errorBuilder.add(new BrowserBoxException("Error resolving artifact for version " + version.toString() + ": " + e.getMessage(), e));
                }
            }
            
            return new ItemList(resultBuilder.build(), errorBuilder.build());
        }
        catch (VersionRangeResolutionException e)
        {
            throw new BrowserBoxException("Failed to resolve version range: " + e, e);
        }
    }

    @Override
    public ItemVersions readAllVersionsAllowFailures(Query query)
    throws BrowserBoxException
    {
        Artifact searchArtifact = mavenVersionSearchArtifactForItem();
        VersionRangeRequest request = new VersionRangeRequest(searchArtifact, remoteRepositories, null);
        try
        {
            VersionRangeResult result = repositorySystem.resolveVersionRange(repositorySystemSession, request);

            //No resolution, this is OK, return no results
            if (result.getHighestVersion() == null)
                return new ItemVersions(Collections.emptyList(), Collections.emptyList());

            ImmutableList.Builder<String> resultBuilder = new ImmutableList.Builder<>();
            ImmutableList.Builder<BrowserBoxException> errorBuilder = new ImmutableList.Builder<>();
            for (Version version : result.getVersions())
            {
                if (!query.getIgnoredVersions().contains(version.toString()))
                    resultBuilder.add(version.toString());
            }

            return new ItemVersions(resultBuilder.build(), errorBuilder.build());
        }
        catch (VersionRangeResolutionException e)
        {
            throw new BrowserBoxException("Failed to resolve version range: " + e, e);
        }
    }

    protected Path resolveArtifact(Artifact artifact)
    throws ArtifactResolutionException
    {
        ArtifactRequest artifactRequest = new ArtifactRequest(artifact, getRemoteRepositories(), null);
        ArtifactResult artifactResult = getRepositorySystem().resolveArtifact(getRepositorySystemSession(), artifactRequest);
        if (artifactResult.isMissing() || !artifactResult.isResolved() || artifactResult.getArtifact() == null || artifactResult.getArtifact().getFile() == null)
        {
            ArtifactResolutionException ex = new ArtifactResolutionException(Collections.emptyList(), "Artifact " + artifactResult + " could not be resolved: " + artifactResult.getExceptions());
            for (Exception resultEx : artifactResult.getExceptions())
            {
                ex.addSuppressed(resultEx);
            }
            throw ex;
        }
        return artifactResult.getArtifact().getFile().toPath();
    }

    protected Item versionToItem(Version mavenVersion)
    throws ArtifactResolutionException, IOException, BrowserBoxException
    {
        Artifact artifact = mavenArtifactForItem(mavenVersion.toString());
        Path artifactFile = resolveArtifact(artifact);
        return new Item(mavenVersion.toString(), artifactFile.toUri().toURL());
    }

    protected RepositorySystem getRepositorySystem()
    {
        return repositorySystem;
    }
    
    protected RepositorySystemSession getRepositorySystemSession()
    {
        return repositorySystemSession;
    }

    protected List<RemoteRepository> getRemoteRepositories()
    {
        return remoteRepositories;
    }
}
