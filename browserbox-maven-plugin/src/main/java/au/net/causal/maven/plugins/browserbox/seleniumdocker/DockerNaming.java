package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import com.google.common.collect.ImmutableMap;
import io.fabric8.maven.docker.model.Image;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Container and registry name configuration for Docker boxes.
 */
public class DockerNaming 
{
    private final Labels labels = new Labels();

    public URI seleniumDockerGitRepoUrl()
    {
        return URI.create("https://github.com/SeleniumHQ/docker-selenium.git");
    }
    
    public URI firefoxInstallerUrl()
    {
        return URI.create("https://download-installer.cdn.mozilla.net/pub/firefox/releases/");
    }

    public URI firefoxMobileInstallerUrl()
    {
        return URI.create("https://download-installer.cdn.mozilla.net/pub/mobile/releases/");
    }
    
    public String customVersionRepositoryName(String seleniumImageNameFragment)
    {
        return "selenium-custom/" + seleniumImageNameFragment;
    }
    
    public String browserBoxSeleniumRepositoryPrefix()
    {
        return "au-net-causal-browserbox-selenium/";
    }
    
    public ImageReference browserBoxImageName(ImageReference base)
    {
        String repo = base.getRepository();
        
        repo = repo.replaceAll("^selenium/", browserBoxSeleniumRepositoryPrefix());
        repo = repo.replaceAll("^selenium-custom/", browserBoxSeleniumRepositoryPrefix());
        
        return new ImageReference(repo, base.getTag());
    }
    
    public Tag firefoxCustomVersionTag(Tag fromTag, String customVersion)
    {
        return new Tag(fromTag.getName() + "-ff-" + customVersion);
    }

    public boolean isCustomVersionFirefoxImage(Image image, String browserVersion)
    {
        return labels().matchesAnyImageForBrowserVersion(image, "firefox", browserVersion);
    }
    
    public Labels labels()
    {
        return labels;
    }
    
    public static class Labels
    {
        public Map<String, String> browserBox()
        {
            return ImmutableMap.of("au.net.causal.browserbox", "true");
        }
        
        public Map<String, String> browserType(String type)
        {
            return ImmutableMap.of("au.net.causal.browserbox.browser.type", type.toLowerCase(Locale.ENGLISH));
        }
        
        public Map<String, String> browserVersion(String type, String version)
        {
            return ImmutableMap.of("au.net.causal.browserbox.browser.version", version);
        }
        
        public Map<String, String> intermediateImage()
        {
            return ImmutableMap.of("au.net.causal.browserbox.intermediate", "true");
        }
        
        public Map<String, String> forBrowser(String browserType)
        {
            return ImmutableMap.<String, String>builder().putAll(browserBox())
                    .putAll(browserType(browserType))
                    .build();
        }
        
        public Map<String, String> forBrowserWithVersion(BoxConfiguration box)
        {
            return forBrowserWithVersion(box.getBrowserType(), box.getBrowserVersion());
        }
        
        public Map<String, String> forBrowserWithVersion(String type, String version)
        {
            return ImmutableMap.<String, String>builder().putAll(forBrowser(type)) 
                                    .putAll(browserVersion(type, version))
                                    .build();
        }
        
        public Map<String, String> forBrowserIntermediate(String type, String version)
        {
            return ImmutableMap.<String, String>builder().putAll(forBrowserWithVersion(type, version)).putAll(intermediateImage()).build();
        }
        
        public Map<String, String> forBrowserIntermediate(BoxConfiguration box)
        {
            return forBrowserIntermediate(box.getBrowserType(), box.getBrowserVersion());
        }

        public boolean matchesAnyImageForBrowserType(Image image, String browserType)
        {
            Map<String, String> labelMap = new HashMap<>(safelyGetLabels(image));
            Map<String, String> expected = forBrowser(browserType);
            labelMap.keySet().retainAll(expected.keySet());
            return expected.equals(labelMap);
        }

        /**
         * Safely read labels from an image.  There is a known bug with the Docker plugin and its image implementation
         * where it throws ClassCastException when reading an image with no tags where a null is stored as a value
         * in the JSON representation.
         *
         * @param image the image to read labels from.
         *
         * @return the label map if it exists, empty map if reading labels fails.
         *
         * @see Image#getLabels()
         */
        //TODO remove this once Docker Maven Plugin bug is fixed
        private Map<String, String> safelyGetLabels(Image image)
        {
            try
            {
                return image.getLabels();
            }
            catch (ClassCastException e)
            {
                //Known bug in Docker plugin - happens when there are no labels
                //java.lang.ClassCastException: class com.google.gson.JsonNull cannot be cast to class com.google.gson.JsonObject
                //but this only happens when there are no labels so it is safe to return false when we get this
                System.err.println("Error reading labels for image: " + image.getId());
                return Collections.emptyMap();
            }
        }

        public boolean matchesAnyImageForBrowserVersion(Image image, String browserType, String browserVersion)
        {
            Map<String, String> labelMap = new HashMap<>(safelyGetLabels(image));
            Map<String, String> expected = forBrowserWithVersion(browserType, browserVersion);
            labelMap.keySet().retainAll(expected.keySet());
            return expected.equals(labelMap);
        }
    }
}
