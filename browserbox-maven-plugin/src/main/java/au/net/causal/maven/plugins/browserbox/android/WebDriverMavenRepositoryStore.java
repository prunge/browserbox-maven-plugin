package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ExceptionalSupplier;
import au.net.causal.maven.plugins.browserbox.versionstore.FileDownloader;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenRepositoryStore;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.logging.Log;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.ArtifactType;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.installation.InstallRequest;
import org.eclipse.aether.installation.InstallationException;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.version.Version;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;

public class WebDriverMavenRepositoryStore extends MavenRepositoryStore
{
    private final Platform platform;
    private final ExceptionalSupplier<Path, IOException> tempDirectorySupplier;

    public WebDriverMavenRepositoryStore(Artifact saveArtifactTemplate, RepositorySystem repositorySystem,
                                         RepositorySystemSession repositorySystemSession,
                                         List<? extends RemoteRepository> remoteRepositories,
                                         FileDownloader downloader,
                                         Log log,
                                         ExceptionalSupplier<Path, IOException> tempDirectorySupplier,
                                         Platform platform)
    {
        super(saveArtifactTemplate, repositorySystem, repositorySystemSession, remoteRepositories, downloader, log);
        Objects.requireNonNull(platform, "platform == null");
        Objects.requireNonNull(tempDirectorySupplier, "tempDirectorySupplier == null");
        this.platform = platform;
        this.tempDirectorySupplier = tempDirectorySupplier;
    }

    public WebDriverMavenRepositoryStore(Artifact saveArtifactTemplate, BoxContext context,
                                         FileDownloader downloader, Platform platform,
                                         ExceptionalSupplier<Path, IOException> tempDirectorySupplier)
    {
        super(saveArtifactTemplate, context, downloader, context.getLog());
        Objects.requireNonNull(platform, "platform == null");
        Objects.requireNonNull(tempDirectorySupplier, "tempDirectorySupplier == null");
        this.platform = platform;
        this.tempDirectorySupplier = tempDirectorySupplier;
    }
    
    @Override
    protected Item versionToItem(Version mavenVersion)
    throws ArtifactResolutionException, IOException, BrowserBoxException
    {
        //Main binary artifact
        Artifact artifact = mavenArtifactForItem(mavenVersion.toString());
        Path artifactFile = resolveArtifact(artifact);
        URL driverFileUrl = artifactFile.toUri().toURL();

        //Also resolve metadata file
        Artifact propertiesArtifact = mavenArtifactForCompatibilityProperties(mavenVersion.toString());
        Path compatibilityPropertiesFile = resolveArtifact(propertiesArtifact);
        
        WebDriverItem item = new WebDriverItem(mavenVersion.toString(), platform, ImmutableMap.of(platform, driverFileUrl));
        item.setCompatibilityInfo(readCompatibilityMetadataFile(compatibilityPropertiesFile));

        return item;
    }

    @Override
    public URL saveItemContents(Item baseItem) 
    throws BrowserBoxException
    {
        if (!(baseItem instanceof WebDriverItem))
            return super.saveItemContents(baseItem);
        
        return saveWebDriverItemContents((WebDriverItem)baseItem);
    }

    @Override
    protected Artifact mavenArtifactForItem(String version)
    {
        return artifactWithClassifier(super.mavenArtifactForItem(version), platform.name().toLowerCase(Locale.ENGLISH));
    }

    @Override
    protected Artifact mavenVersionSearchArtifactForItem()
    {
        return artifactWithClassifier(super.mavenVersionSearchArtifactForItem(), platform.name().toLowerCase(Locale.ENGLISH));
    }

    private static Artifact artifactWithClassifier(Artifact artifact, String classifier)
    {
        return new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(), classifier, artifact.getExtension(),
                                   artifact.getVersion(), artifact.getProperties(), (ArtifactType)null);
    }

    private static Artifact artifactWithType(Artifact artifact, String type)
    {
        return new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(), artifact.getClassifier(), type,
                                   artifact.getVersion(), artifact.getProperties(), (ArtifactType)null);
    }

    protected Artifact mavenArtifactForCompatibilityProperties(String version)
    {
        Artifact artifact = mavenArtifactForItem(version);
        artifact = artifactWithClassifier(artifact, null);
        artifact = artifactWithType(artifact, "compatibility.properties");

        return artifact;
    }

    protected URL saveWebDriverItemContents(WebDriverItem item)
    throws BrowserBoxException
    {
        Path metadataFile = generateCompatibilityMetadataFile(item.getCompatibilityInfo());
        
        InstallRequest request = new InstallRequest();
        Artifact artifact = mavenArtifactForCompatibilityProperties(item.getVersion());
        artifact = artifact.setFile(metadataFile.toFile());
        request.setArtifacts(Collections.singletonList(artifact));
        try
        {
            getRepositorySystem().install(getRepositorySystemSession(), request);

        }
        catch (InstallationException e)
        {
            throw new BrowserBoxException("Error saving capability metadata to Maven repository: " + e.getMessage(), e);
        }
        
        return super.saveItemContents(item);
    }

    protected Path generateCompatibilityMetadataFile(CompatibilityInfo compatibilityInfo)
    throws BrowserBoxException
    {
        Properties props = new Properties();
        props.setProperty("minVersion", String.valueOf(compatibilityInfo.getMinMajorVersion()));
        props.setProperty("maxVersion", String.valueOf(compatibilityInfo.getMaxMajorVersion()));

        try
        {
            Path tempDirectory = tempDirectorySupplier.get();
            Path tempFile = Files.createTempFile(tempDirectory, "compatibility",  ".properties");
            try (OutputStream os = Files.newOutputStream(tempFile))
            {
                props.store(os, null);
            }

            return tempFile;
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("Error writing properties file: " + e.getMessage(), e);
        }
    }

    protected CompatibilityInfo readCompatibilityMetadataFile(Path file)
    throws BrowserBoxException, IOException
    {
        Properties props = new Properties();
        try (InputStream is = Files.newInputStream(file))
        {
            props.load(is);
        }

        String minMajorVersionStr = props.getProperty("minVersion");
        String maxMajorVersionStr = props.getProperty("maxVersion");
        if (minMajorVersionStr == null)
            throw new BrowserBoxException("Missing minVersion property in compatibility properties");
        if (maxMajorVersionStr == null)
            throw new BrowserBoxException("Missing maxVersion property in compatibility properties");
        try
        {
            int minMajorVersion = Integer.parseInt(minMajorVersionStr);
            int maxMajorVersion = Integer.parseInt(maxMajorVersionStr);
            return new CompatibilityInfo(minMajorVersion, maxMajorVersion);
        }
        catch (NumberFormatException e)
        {
            throw new BrowserBoxException("Failed to parse compatibility properties: " + e, e);
        }
    }
}
