package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class FilterNonWorkingUrlsVersionsRegistry implements VersionRegistry
{
    private final VersionRegistry delegate;
    private final Optional<Log> logger;

    public FilterNonWorkingUrlsVersionsRegistry(VersionRegistry delegate)
    {
        Objects.requireNonNull(delegate, "delegate == null");
        this.delegate = delegate;
        this.logger = Optional.empty();
    }
    
    public FilterNonWorkingUrlsVersionsRegistry(VersionRegistry delegate, Log logger)
    {
        Objects.requireNonNull(delegate, "delegate == null");
        this.delegate = delegate;
        this.logger = Optional.ofNullable(logger);
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        ImmutableList.Builder<Item> workingDownloadList = new ImmutableList.Builder<>();
        List<BrowserBoxException> errors = new ArrayList<>();

        ItemList unfilteredList = delegate.readAllItemsAllowFailures(query);
        for (Item item : unfilteredList.getItems())
        {
            URL url = item.getUrl();
            if (checkUrlIsWorking(url, errors))
                workingDownloadList.add(item);
        }

        return new ItemList(workingDownloadList.build(), errors);
    }

    //Errors are ignored when filtering and getting items directly
    @Override
    public Collection<? extends Item> readAllItems(Query query)
    throws BrowserBoxException
    {
        ImmutableList.Builder<Item> workingDownloadList = new ImmutableList.Builder<>();

        ItemList unfilteredList = delegate.readAllItemsAllowFailures(query);
        for (Item item : unfilteredList.getItems())
        {
            URL url = item.getUrl();
            List<BrowserBoxException> errors = new ArrayList<>();
            if (checkUrlIsWorking(url, errors))
                workingDownloadList.add(item);
            else
                logger.ifPresent(log -> log.warn("Non-working URL " + url.toExternalForm() + ": " + errors));
        }

        return workingDownloadList.build();
    }

    @Override
    public ItemVersions readAllVersionsAllowFailures(Query query)
    throws BrowserBoxException
    {
        //Don't care about URL errors when just reading versions
        return delegate.readAllVersionsAllowFailures(query);
    }

    protected boolean checkUrlIsWorking(URL zipUrl, Collection<? super BrowserBoxException> errors)
    {
        try
        {
            URLConnection con = zipUrl.openConnection();
            long fileSize = con.getContentLengthLong();

            con.getInputStream().close();

            //Fails with exception for HTTP 404, but also validate file size
            //If it's too small then something went wrong
            if (fileSize < 1_000_000L)
            {
                errors.add(new BrowserBoxException("Browser box download " + zipUrl.toExternalForm() + " is too small (" + fileSize + " bytes)"));
                return false;
            }
            else
                return true;
        }
        catch (IOException e)
        {
            errors.add(new BrowserBoxException("Error when checking connection to " + zipUrl.toExternalForm() + ": " + e, e));
            return false;
        }
    }
}
