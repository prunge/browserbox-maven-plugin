package au.net.causal.maven.plugins.browserbox.box;

/**
 * Dependency for executing a Java class.
 */
public class RunnerDependency
{
	private final String groupId;
	private final String artifactId;
	private final String version;
	private final String type;
	private final String classifier;

	public RunnerDependency(String groupId, String artifactId, String version, String type, String classifier)
	{
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
		this.type = type;
		this.classifier = classifier;
	}

	public RunnerDependency(String groupId, String artifactId, String version)
	{
		this(groupId, artifactId, version, "jar", null);
	}

	public String getGroupId()
	{
		return groupId;
	}

	public String getArtifactId()
	{
		return artifactId;
	}

	public String getVersion()
	{
		return version;
	}

	public String getType()
	{
		return type;
	}

	public String getClassifier()
	{
		return classifier;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder("RunnerDependency{");
		sb.append("groupId='").append(groupId).append('\'');
		sb.append(", artifactId='").append(artifactId).append('\'');
		sb.append(", version='").append(version).append('\'');
		sb.append(", type='").append(type).append('\'');
		sb.append(", classifier='").append(classifier).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
