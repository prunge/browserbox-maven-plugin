package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.DockerNaming;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.EmptyBrowserVersionBlacklist;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.FirefoxAndroidDownloadVersionScanner;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.FirefoxDownloadVersionScanner;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Scans Firefox website for APK versions and downloads.
 */
public class FirefoxApkVersionRegistry implements VersionRegistry
{
    private final URL downloadListUrl;
    private final FirefoxAndroidDownloadVersionScanner versionScanner;
    private final Set<String> invalidVersions;
    private final Map<String, URL> knownValidVersions;

    public FirefoxApkVersionRegistry(Log log, Set<String> invalidVersions, Map<String, URL> knownValidVersions)
    {
        this.versionScanner = new FirefoxAndroidDownloadVersionScanner(log, new EmptyBrowserVersionBlacklist());
        this.invalidVersions = ImmutableSet.copyOf(invalidVersions);
        this.knownValidVersions = ImmutableMap.copyOf(knownValidVersions);
        try
        {
            this.downloadListUrl = new DockerNaming().firefoxMobileInstallerUrl().toURL();
        }
        catch (MalformedURLException e)
        {
            throw new Error(e);
        }
    }

    @Override
    public FirefoxApkItemList readAllItemsAllowFailures(Query query) throws BrowserBoxException
    {
        FirefoxApkQuery fQuery;
        if (query instanceof FirefoxApkQuery)
            fQuery = (FirefoxApkQuery)query;
        else
            fQuery = new FirefoxApkQuery(ImmutableSet.of(), ImmutableMap.of());

        Set<String> allInvalidVersions = ImmutableSet.<String>builder().addAll(fQuery.getIgnoredVersions()).addAll(invalidVersions).build();
        Map<String, URL> allKnownValidVersions = new LinkedHashMap<>();
        allKnownValidVersions.putAll(fQuery.getKnownValidVersions());
        allKnownValidVersions.putAll(knownValidVersions);

        try
        {
            FirefoxDownloadVersionScanner.AvailableVersionsDetail results = versionScanner.readAvailableVersionsData(downloadListUrl, allInvalidVersions, allKnownValidVersions);

            List<Item> items = results.getAvailableVersionDownloads().stream().map(AvailableVersionItem::new).collect(Collectors.toList());

            return new FirefoxApkItemList(items, Collections.emptyList(), results.getBlacklistedVersions());
        }
        catch (IOException e)
        {
            //Treat all errors as fatal since it's reading from one site
            throw new BrowserBoxException(e);
        }
    }

    public static class FirefoxApkItemList extends ItemList
    {
        private final Set<String> invalidVersions;

        public FirefoxApkItemList(Collection<? extends Item> items, Collection<? extends BrowserBoxException> errors, Set<String> invalidVersions)
        {
            super(items, errors);
            this.invalidVersions = ImmutableSet.copyOf(invalidVersions);
        }

        public Set<String> getInvalidVersions()
        {
            return invalidVersions;
        }
    }

    /**
     * Lazily evaluate the item version because it is costly and hardly ever needed for all items.
     */
    private static class AvailableVersionItem extends Item
    {
        private final FirefoxDownloadVersionScanner.AvailableVersion availableVersion;

        public AvailableVersionItem(FirefoxDownloadVersionScanner.AvailableVersion availableVersion)
        {
            super(availableVersion.getVersion(), availableVersion.getVersionUrl());
            this.availableVersion = availableVersion;
        }

        @Override
        public URL getUrl()
        {
            try
            {
                return availableVersion.getDownloadUrl();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public static class FirefoxApkQuery extends Query
    {
        private final Map<String, URL> knownValidVersions;

        public FirefoxApkQuery(Set<String> ignoredVersions, Map<String, URL> knownValidVersions)
        {
            super(ignoredVersions);
            this.knownValidVersions = ImmutableMap.copyOf(knownValidVersions);
        }

        /**
         * @return a map mapping versions to download URLs.
         */
        public Map<String, URL> getKnownValidVersions()
        {
            return knownValidVersions;
        }
    }
}
