package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ms.feed.FeedType;
import com.google.common.net.HttpHeaders;
import org.codehaus.plexus.util.IOUtil;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Uses the Chocolatey REST API to read available versions of Edge drivers. 
 */
public class ChocolateyEdgeDriverVersionResolver extends BaseEdgeDriverVersionResolver
{
    private final URL findPackagesUrl;

    /**
     * Constructs using the specified URL for finding packages.
     * 
     * @param findPackagesUrl find packages URL.
     */
    public ChocolateyEdgeDriverVersionResolver(URL findPackagesUrl)
    {
        Objects.requireNonNull(findPackagesUrl, "findPackagesUrl == null");
        this.findPackagesUrl = findPackagesUrl;
    }

    /**
     * Constructs using the default lookup URL.
     * 
     * @throws IOException if an error occurs.
     */
    public ChocolateyEdgeDriverVersionResolver()
    throws IOException
    {
        this(new URL("https://chocolatey.org/api/v2/FindPackagesById()?id=%27selenium-edge-driver%27"));
    }

    /**
     * Reads all available Edge driver versions from the remote endpoint.
     * 
     * @return a list of Edge driver version numbers.
     * 
     * @throws BrowserBoxException if an error occurs reading versions from the remote endpoint.
     */
    protected List<String> readVersions()
    throws BrowserBoxException        
    {
        //JAXB doesn't do well passing the URL directly for some reason, so let's make the stream ourselves
        try
        {
            //Accept header needed or the server freaks out
            URLConnection con = findPackagesUrl.openConnection();
            con.setRequestProperty(HttpHeaders.ACCEPT, "*/*");
            
            try (InputStream findPackagesResultData = con.getInputStream())
            {
                FeedType feed = JAXB.unmarshal(findPackagesResultData, FeedType.class);
                return feed.getEntry().stream()
                           .map(FeedType.Entry::getProperties)
                           .map(FeedType.Properties::getVersion)
                           .collect(Collectors.toList());
            }
            catch (IOException e)
            {
                //Debugging the error by getting response if possible
                if (con instanceof HttpURLConnection)
                {
                    HttpURLConnection hcon = (HttpURLConnection)con;
                    InputStream errorStream = hcon.getErrorStream();
                    if (errorStream != null)
                        throw new IOException(e.getMessage() + " - " + IOUtil.toString(errorStream, StandardCharsets.UTF_8.name()));
                }
                
                throw e;
            }
            
        }
        catch (DataBindingException | IOException e)
        {
            throw new BrowserBoxException("Error reading Edge driver versions from " + findPackagesUrl.toExternalForm() + ": " + e.getMessage(), e);
        }
    }

    @Override
    public List<? extends Version> allVersions() 
    throws BrowserBoxException 
    {
        return Version.parseVersions(readVersions());
    }

    @Override
    public Version resolve(String edgeVersion)
    throws BrowserBoxException
    {
        List<? extends Version> edgeDriverVersions = allVersions();
        
        //Parse the Edge version, it will be in the form <major>.<full>
        //e.g. 4.15063
        Version parsedEdgeVersion = new Version(edgeVersion);

        return resolveFromList(edgeDriverVersions, parsedEdgeVersion);
    }
}
