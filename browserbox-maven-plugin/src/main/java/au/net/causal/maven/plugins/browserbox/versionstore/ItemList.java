package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.Collection;

/**
 * Collection of items with both version and URL for items with potential resolution or lookup errors.
 * Check if {@link #getErrors()} has any errors when using results of this type.
 */
public class ItemList extends HasErrorList
{
    private final Collection<Item> items;
    
    public ItemList(Collection<? extends Item> items, Collection<? extends BrowserBoxException> errors)
    {
        super(errors);
        this.items = ImmutableList.copyOf(items);
    }
    
    public Collection<? extends Item> getItems()
    {
        return items;
    }
}
