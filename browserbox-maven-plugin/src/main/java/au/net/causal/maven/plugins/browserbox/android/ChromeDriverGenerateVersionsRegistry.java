package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.versionstore.GenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

public class ChromeDriverGenerateVersionsRegistry extends GenerateVersionsRegistry
{
    private final Platform platform;

    public ChromeDriverGenerateVersionsRegistry(GenerateVersionsContext generateContext, Platform platform)
    {
        super(generateContext);
        this.platform = platform;
    }

    public ChromeDriverGenerateVersionsRegistry(GenerateVersionsContext generateContext, Platform platform, Log logger)
    {
        super(generateContext, logger);
        this.platform = platform;
    }

    @Override
    protected Properties readDownloadProperties()
    throws BrowserBoxException
    {
        return getGenerateContext().getDriverVersions();
    }

    @Override
    public void saveItems(Collection<? extends Item> items)
    throws BrowserBoxException
    {
        for (Item item : items)
        {
            String existingUrlStr = getGenerateContext().getDownloads().getProperty(item.getVersion());
            String newUrlStr = item.getUrl().toExternalForm();
            if (!newUrlStr.equals(existingUrlStr))
                getGenerateContext().getPlatformDriverVersions(platform.name()).setProperty(item.getVersion(), newUrlStr);

            if (item instanceof WebDriverItem)
            {
                //Also save compatibility information
                WebDriverItem webDriverItem = (WebDriverItem)item;

                if (webDriverItem.getCompatibilityInfo() != null)
                {
                    int minVersion = webDriverItem.getCompatibilityInfo().getMinMajorVersion();
                    int maxVersion = webDriverItem.getCompatibilityInfo().getMaxMajorVersion();

                    //Keep this format in sync with ChromeDriverVersionsMavenArtifactRegistry
                    getGenerateContext().getDriverCompatibility().setProperty(item.getVersion() + ".minVersion", String.valueOf(minVersion));
                    getGenerateContext().getDriverCompatibility().setProperty(item.getVersion() + ".maxVersion", String.valueOf(maxVersion));
                }
            }
        }

        /*
        //Find any versions that are no longer valid
        FilterNonWorkingUrlsVersionsRegistry filter = new FilterNonWorkingUrlsVersionsRegistry(this, getLogger().orElse(null));
        Set<String> validVersions = filter.readAllItems(new Query()).stream()
                                          .map(Item::getVersion)
                                          .collect(Collectors.toSet());
        getGenerateContext().getKnownVersions().keySet().retainAll(validVersions);
        */
        //TODO would be nice to clean up known versions as well

        try
        {
            getGenerateContext().saveDriverCompatibility();
            getGenerateContext().saveDriverVersions();
        }
        catch (IOException e)
        {
            throw new BrowserBoxException(e);
        }
    }
}
