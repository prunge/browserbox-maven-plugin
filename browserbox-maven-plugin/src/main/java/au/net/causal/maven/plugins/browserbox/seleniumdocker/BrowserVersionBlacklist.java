package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.Set;

public interface BrowserVersionBlacklist 
{
    public Set<String> getKnownVersionBlacklist()
    throws BrowserBoxException;
    
    public Set<String> getKnownVersionWhitelist()
    throws BrowserBoxException;
}
