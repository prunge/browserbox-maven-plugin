package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A collection of items that have a version and a download URL.  Download URLs may be local (such as the local filesystem) or remote (such as HTTP).
 * There is the potential to retrieve partial lists of results and handle errors gracefully using the <code>...AllowFailures()</code> family
 * of methods.
 */
public interface VersionRegistry
{
    /**
     * Retrieves both the versions and URLs of all items in the store, allowing partial results in cases of failure.  Callers should check
     * and handle {@link ItemList#getErrors()} for failures that occur.
     *
     * @param query query options.
     *
     * @return a list of items and failures.
     * 
     * @throws BrowserBoxException in the case of critical failure only.
     */
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException;

    /**
     * Retrieves all versions of items in the store, allowing partial results in cases of failure.  Callers should check and handle
     * {@link ItemList#getErrors()} for failures that occur.
     *
     * @param query query options.
     *
     * @return a list of versions and failures.
     * 
     * @throws BrowserBoxException in the case of critical failure only.
     */
    public default ItemVersions readAllVersionsAllowFailures(Query query)
    throws BrowserBoxException
    {
        ItemList fullItemList = readAllItemsAllowFailures(query);
        List<String> versions = fullItemList.getItems().stream()
                                    .map(Item::getVersion)
                                    .collect(Collectors.toList());
        return new ItemVersions(versions, fullItemList.getErrors());
    }

    /**
     * Reads versions and URLS for all items.
     *
     * @param query query options.
     *
     * @return all items in the store.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public default Collection<? extends Item> readAllItems(Query query)
    throws BrowserBoxException
    {
        ItemList results = readAllItemsAllowFailures(query);
        if (results.getErrors().isEmpty())
            return results.getItems();
        else if (results.getErrors().size() == 1)
            throw results.getErrors().get(0);
        else
        {
            BrowserBoxException mainError = results.getErrors().get(0);
            for (BrowserBoxException otherError : results.getErrors().subList(1, results.getErrors().size()))
            {
                mainError.addSuppressed(otherError);
            }
            throw mainError;
        }
    }

    /**
     * Reads the versions of all items in the store.
     *
     * @param query query options.
     *
     * @return a collection of versions for all items.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public default Collection<String> readAllVersions(Query query)
    throws BrowserBoxException
    {
        ItemVersions results = readAllVersionsAllowFailures(query);
        if (results.getErrors().isEmpty())
            return results.getVersions();
        else if (results.getErrors().size() == 1)
            throw results.getErrors().get(0);
        else
        {
            BrowserBoxException mainError = results.getErrors().get(0);
            for (BrowserBoxException otherError : results.getErrors().subList(1, results.getErrors().size()))
            {
                mainError.addSuppressed(otherError);
            }
            throw mainError;
        }
    }

    /**
     * Looks up the download URL for a version.
     * 
     * @param version the version to look up.
     *                
     * @return the item with a download URL, or null if no item is stored under the specified version.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public default Item itemForVersion(String version)
    throws BrowserBoxException
    {
        Objects.requireNonNull(version, "version == null");

        return readAllItems(new Query()).stream()
                      .filter(item -> version.equals(item.getVersion()))
                      .findAny().orElse(null);
    }

    public static class Query
    {
        private final Set<String> ignoredVersions;

        public Query(Set<String> ignoredVersions)
        {
            this.ignoredVersions = ImmutableSet.copyOf(ignoredVersions);
        }

        public Query()
        {
            this(ImmutableSet.of());
        }

        public Set<String> getIgnoredVersions()
        {
            return ignoredVersions;
        }
    }
}
