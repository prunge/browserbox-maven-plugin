package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Controls for recording video from the browser.  Allows video of the browser to be recorded and saved to file.
 */
public interface VideoControl 
{
    /**
     * Start recording video from the browser.
     * 
     * @return the recording object which can be used to stop and save the video.
     * 
     * @throws IOException if an I/O error occurs.
     * @throws BrowserBoxException if another error occurs.
     */
    public Recording startRecording() 
    throws IOException, BrowserBoxException;
    
    public static interface Recording 
    {
        /**
         * Stops recording video and saves the video file.
         * 
         * @param videoFile the video file to save to.
         *                  
         * @throws IOException if an I/O error occurs.
         * @throws BrowserBoxException if another error occurs.
         */
        public void stopAndSave(Path videoFile)
        throws IOException, BrowserBoxException;

        /**
         * Stops recording video and discards the video file.
         * 
         * @throws IOException if an I/O error occurs.
         * @throws BrowserBoxException if another error occurs.
         */
        public void stopAndCancel()
        throws IOException, BrowserBoxException;
    }
}
