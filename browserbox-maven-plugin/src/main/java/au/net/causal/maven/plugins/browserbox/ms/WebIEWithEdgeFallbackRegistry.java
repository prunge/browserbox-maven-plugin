package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.client.ClientBuilder;
import java.util.Collections;
import java.util.Comparator;

/**
 * Attempts to read IE versions from an IE registry, but if none exist falls back to using the latest Edge VM from the Edge registry since we know that this VM contains IE 11
 * as well.
 * <p>
 *
 * Since late 2019/early 2020, Microsoft removed all the old IE VMs and only supplies a single Edge VM.  So now the only way to run IE is to use the Edge VM.
 */
public class WebIEWithEdgeFallbackRegistry implements VersionRegistry
{
    private final WebIEVersionRegistry ieRegistry;
    private final WebEdgeVersionRegistry edgeRegistry;

    public WebIEWithEdgeFallbackRegistry()
    {
        this(ClientBuilder.newBuilder());
    }

    public WebIEWithEdgeFallbackRegistry(ClientBuilder clientBuilder)
    {
        this.ieRegistry = new WebIEVersionRegistry(clientBuilder);
        this.edgeRegistry = new WebEdgeVersionRegistry(clientBuilder);
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query)
    throws BrowserBoxException
    {
        ItemList results = ieRegistry.readAllItemsAllowFailures(query);
        if (!results.getErrors().isEmpty() || !results.getItems().isEmpty())
            return results;

        //Fallback to using latest Edge VM if there are no IEs
        results = edgeRegistry.readAllItemsAllowFailures(query);
        if (!results.getErrors().isEmpty() || results.getItems().isEmpty())
            return results;

        Item highestEdgeVersion = results.getItems().stream().max(Comparator.comparing(Item::getVersion)).get();
        Item ieItem = new Item("11", highestEdgeVersion.getUrl()); //We know latest Edge comes with IE11
        return new ItemList(ImmutableList.of(ieItem), Collections.emptyList());
    }
}
