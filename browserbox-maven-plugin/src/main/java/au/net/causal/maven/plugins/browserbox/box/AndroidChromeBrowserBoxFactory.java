package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.android.ChromeApkMirrorVersionRegistry;
import au.net.causal.maven.plugins.browserbox.android.ChromeApkMirrorVersionRegistry.ChomeApkMirrorQuery;
import au.net.causal.maven.plugins.browserbox.android.ChromeDriverGenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.android.WebDriverItem;
import au.net.causal.maven.plugins.browserbox.android.WebDriverMavenRepositoryStore;
import au.net.causal.maven.plugins.browserbox.android.WebDriverVersionsMavenArtifactRegistry;
import au.net.causal.maven.plugins.browserbox.android.ChromeDriverWebVersionRegistry;
import au.net.causal.maven.plugins.browserbox.android.Platform;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionTools;
import au.net.causal.maven.plugins.browserbox.versionstore.CompositeVersionedItemStore;
import au.net.causal.maven.plugins.browserbox.versionstore.GenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenBrowserVersionsArtifactRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenRepositoryStore;
import au.net.causal.maven.plugins.browserbox.versionstore.SimpleFileDownloader;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import com.google.common.collect.Maps;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionScheme;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@BrowserBoxBundled
public class AndroidChromeBrowserBoxFactory implements BrowserBoxFactory
{
    private static final String ANDROID_CHROME = "android-chrome";

    private final BrowserVersionTools versionTools = new BrowserVersionTools();

    private ChromeApkMirrorVersionRegistry webVersionRegistry(BoxContext context)
    {
        return new ChromeApkMirrorVersionRegistry(context.getLog());
    }

    private VersionRegistry chromeDriverWebVersionRegistry(BoxContext context, Platform platform)
    {
        return new ChromeDriverWebVersionRegistry(platform, context.getLog());
    }

    private VersionedItemStore chromeApkVersionRegistry(BoxContext context)
    {
        Artifact saveTemplate = new DefaultArtifact("com.google.chrome", "chrome-android-x86", "apk", "0.0");

        return new CompositeVersionedItemStore(
                new MavenRepositoryStore(saveTemplate, context, new SimpleFileDownloader(context::getTempDirectory), context.getLog()),
                //This one smashes APKMirror and eventually hits a HTTP 429 Too Many Requests, so let's just trust versions registry
                //new FilterNonWorkingUrlsVersionsRegistry(new MavenBrowserVersionsArtifactRegistry("android-chrome", context), context.getLog())
                new MavenBrowserVersionsArtifactRegistry("android-chrome", context)
                //TODO maybe allow this enabled conditionally
                //webVersionRegistry(context)
        );
    }

    private VersionedItemStore chromeWebDriverVersionRegistry(BoxContext context)
    throws BrowserBoxException
    {
        Artifact saveTemplate = new DefaultArtifact("com.google.chrome", "chrome-webdriver", "zip", "0.0");
        Platform currentPlatform = context.getPlatform();

        return new CompositeVersionedItemStore(
            new WebDriverMavenRepositoryStore(saveTemplate, context, new SimpleFileDownloader(context::getTempDirectory), currentPlatform, context::getTempDirectory),
            //new FilterNonWorkingUrlsVersionsRegistry(new ChromeDriverVersionsMavenArtifactRegistry("android-chrome", context, currentPlatform))
            new WebDriverVersionsMavenArtifactRegistry("android-chrome", context, currentPlatform)
        );
    }
    
    @Override
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) 
    throws BrowserBoxException 
    {
        initializeDefaults(boxConfiguration, context);
        return new AndroidChromeBrowserBox(boxConfiguration, projectConfiguration, context, chromeApkVersionRegistry(context),
                                           chromeWebDriverVersionRegistry(context));
    }

    @Override
    public boolean supportsType(String type) 
    {
        return ANDROID_CHROME.equals(type);
    }

    @Override
    public Set<String> getKnownTypes() 
    {
        return Collections.singleton(ANDROID_CHROME);
    }
    
    protected void initializeDefaults(BoxConfiguration boxConfiguration, BoxContext context)
    throws BrowserBoxException
    {
        //Version resolution code
        List<String> allVersions = availableKnownVersions(boxConfiguration.getBrowserType(), context);

        //First, if there is no version specified, default to the latest
        if (boxConfiguration.getBrowserVersion() == null && !allVersions.isEmpty())
        {
            boxConfiguration.setBrowserVersion(allVersions.get(allVersions.size() - 1));
            context.getLog().info("Selected " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion());
        }

        //Attempt to resolve to real version if there's no exact match of version
        if (!allVersions.contains(boxConfiguration.getBrowserVersion()))
        {
            String resolvedVersion = versionTools.findBrowserVersion(boxConfiguration.getBrowserVersion(), allVersions, Function.identity(), Function.identity());
            if (resolvedVersion != null)
            {
                context.getLog().info("Resolved " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() +
                        " to " + resolvedVersion);
                boxConfiguration.setBrowserVersion(resolvedVersion);
            }
            else
                context.getLog().warn("Could not resolve " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() + ".");
        }

        //While it might not be a container based box, the container name is still used for some stuff like naming
        //the video directory
        if (boxConfiguration.getContainerName() == null)
        {
            if (boxConfiguration.getBrowserVersion() == null)
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType());
            else
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType() + "-" + boxConfiguration.getBrowserVersion());
        }
    }

    @Override
    public List<String> availableKnownVersions(String type, BoxContext context) 
    throws BrowserBoxException 
    {
        //Parse into Maven objects purely for the sorting logic
        VersionScheme versionScheme = new GenericVersionScheme();
        
        Collection<String> versions = chromeApkVersionRegistry(context).readAllVersions(new Query());

        return versions.stream()
                      .distinct()
                      .map(v -> parseVersion(versionScheme, v))
                      .sorted()
                      .map(Version::toString)
                      .collect(Collectors.toList());
    }

    private Version parseVersion(VersionScheme scheme, String versionString)
    {
        try
        {
            return scheme.parseVersion(versionString);
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAllImages(String type, BoxContext context) throws BrowserBoxException 
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, BoxContext context, 
                                      GenerateVersionsContext generateContext) 
    throws BrowserBoxException, IOException 
    {
        //Construct appropriate resolver tree
        ChromeApkMirrorVersionRegistry webResolver = webVersionRegistry(context);
        GenerateVersionsRegistry saveRegistry = new GenerateVersionsRegistry(generateContext, context.getLog(), false);

        //Read any new versions
        Set<String> ignoredUrlStrings = Maps.filterValues(Maps.fromProperties(generateContext.getKnownVersions()), "false"::equals)
                                            .keySet();
        Set<URL> ignoredUrls = new LinkedHashSet<>();
        for (String ignoredUrlString : ignoredUrlStrings)
        {
            ignoredUrls.add(new URL(ignoredUrlString));
        }
        saveRegistry.saveItems(webResolver.readAllItems(new ChomeApkMirrorQuery(Maps.fromProperties(generateContext.getDownloads()).keySet(), ignoredUrls)));

        //Save any invalid download URLs as well for next time
        if (!webResolver.getInvalidDownloadUrls().isEmpty())
        {
            for (URL invalidDownloadUrl : webResolver.getInvalidDownloadUrls())
            {
                generateContext.getKnownVersions().setProperty(invalidDownloadUrl.toExternalForm(), "false");
            }
            generateContext.saveKnownVersions();
        }

        //Read new versions for ChromeDriver
        VersionRegistry chromeDriverWebResolver = chromeDriverWebVersionRegistry(context, Platform.current()); //Platform doesn't really matter - well it kind of does
        Collection<? extends Item> items = chromeDriverWebResolver.readAllItems(new Query());
        for (Platform platform : Platform.values())
        {
            Collection<? extends WebDriverItem> platformItems = WebDriverItem.translateAllToPlatform(items, platform);
            ChromeDriverGenerateVersionsRegistry chromeDriverSaveRegistry = new ChromeDriverGenerateVersionsRegistry(generateContext, platform, context.getLog());
            chromeDriverSaveRegistry.saveItems(platformItems);
        }
    }
}
