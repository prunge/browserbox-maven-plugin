package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.BuildIntermediates;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import com.beijunyi.parallelgit.filesystem.GitFileSystem;
import com.google.common.collect.ImmutableMap;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.config.AssemblyConfiguration;
import io.fabric8.maven.docker.config.AssemblyConfiguration.PermissionMode;
import io.fabric8.maven.docker.config.AssemblyMode;
import io.fabric8.maven.docker.config.BuildImageConfiguration;
import io.fabric8.maven.docker.config.ImageConfiguration;
import io.fabric8.maven.docker.service.BuildService;
import io.fabric8.maven.docker.service.QueryService;
import io.fabric8.maven.docker.service.RegistryService;
import io.fabric8.maven.docker.util.AnsiLogger;
import io.fabric8.maven.docker.util.MojoParameters;
import org.apache.maven.archiver.MavenArchiveConfiguration;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.filtering.DefaultMavenFileFilter;
import org.apache.maven.shared.filtering.DefaultMavenReaderFilter;
import org.apache.maven.shared.filtering.MavenFileFilter;
import org.apache.maven.shared.filtering.MavenReaderFilter;
import org.codehaus.plexus.util.FileUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SeleniumFirefoxCustomBaseImageGenerator 
{
    private final BoxContext boxContext;
    private final ProjectConfiguration projectConfiguration;
    private final BoxConfiguration boxConfiguration;
    private final FirefoxBrowserVersionReader firefoxBrowserVersionReader;
    private final DockerNaming dockerNaming = new DockerNaming();
    
    public SeleniumFirefoxCustomBaseImageGenerator(BoxContext boxContext, ProjectConfiguration projectConfiguration, 
                                                   BoxConfiguration boxConfiguration, Path gitBaseDirectory)
    {
        Objects.requireNonNull(boxContext, "boxContext == null");
        Objects.requireNonNull(projectConfiguration, "projectConfiguration == null");
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(gitBaseDirectory, "gitBaseDirectory == null");
        this.boxContext = boxContext;
        this.projectConfiguration = projectConfiguration;
        this.boxConfiguration = boxConfiguration;
        this.firefoxBrowserVersionReader = new FirefoxBrowserVersionReader(gitBaseDirectory);
    }
    
    protected BoxContext getContext()
    {
        return boxContext;
    }
    
    protected ProjectConfiguration getProjectConfiguration()
    {
        return projectConfiguration;
    }
    
    public GeneratedImage generate(Tag fromTag, String customVersion)
    throws BrowserBoxException, MojoExecutionException, IOException
    {
        Tag generatedTag = dockerNaming.firefoxCustomVersionTag(fromTag, customVersion);
        ImageReference nodeTargetImageName = new ImageReference(dockerNaming.customVersionRepositoryName("node-firefox"), generatedTag);
        ImageReference nodeDebugTargetImageName = new ImageReference(dockerNaming.customVersionRepositoryName("node-firefox-debug"), generatedTag);
        ImageReference standaloneDebugTargetImageName = new ImageReference(dockerNaming.customVersionRepositoryName("standalone-firefox-debug"), generatedTag);

        QueryService dockerQueryService = getContext().getDockerServiceHub().getQueryService();

        //Early exit if the end target image already exists
        boolean standaloneDebugAlreadyExists = dockerQueryService.hasImage(standaloneDebugTargetImageName.getName());
        if (standaloneDebugAlreadyExists)
            return new GeneratedImage(standaloneDebugTargetImageName, true);
        
        //node bast tag is pulled as base image for first generated image
        ImageReference nodeBaseTag = new ImageReference("selenium/node-base", fromTag);
        boolean nodeBaseAlreadyExists = dockerQueryService.hasImage(nodeBaseTag.getName());
        boolean nodeAlreadyExists = dockerQueryService.hasImage(nodeTargetImageName.getName());
        boolean nodeDebugAlreadyExists = dockerQueryService.hasImage(nodeDebugTargetImageName.getName());
        

        //Read the Dockerfile for firefox
        try (GitFileSystem git = firefoxBrowserVersionReader.checkoutFirefoxGit(fromTag))
        {
            Path gitBasePath = git.getRepository().getWorkTree().toPath();
            
            //Build node image
            if (!nodeAlreadyExists) 
            {
                Path nodeFirefoxPath = gitBasePath.resolve("NodeFirefox");
                generateFromDockerFile(nodeFirefoxPath, nodeTargetImageName,
                                        null,
                                        ImmutableMap.of("FIREFOX_VERSION", customVersion),
                                        Collections.emptyList());
                cleanUpImageTag(nodeBaseTag, nodeBaseAlreadyExists); //We know this is the base
            }
            
            //Build node debug image
            if (!nodeDebugAlreadyExists) 
            {
                //Copy entrypoint sh file from NodeDebug directory
                //Normally makefile does this but we don't have capability to execute it here from Maven
                Path nodeDebugPath = gitBasePath.resolve("NodeDebug");

                //Copy all *.sh and *.conf files
                //These files might differ between different Selenium versions but this pattern seems to work everywhere
                List<Path> extraFiles = Files.list(nodeDebugPath).filter(file -> file.getFileName().toString().endsWith(".sh") ||
                                                                                 file.getFileName().toString().endsWith(".conf"))
                                                                 .collect(Collectors.toList());
                getContext().getLog().debug("Extra files: " + extraFiles);

                Path nodeFirefoxDebugPath = gitBasePath.resolve("NodeFirefoxDebug");
                generateFromDockerFile(nodeFirefoxDebugPath, nodeDebugTargetImageName,
                                       nodeTargetImageName,
                                       Collections.emptyMap(),
                                       extraFiles);
                cleanUpImageTag(nodeTargetImageName, nodeAlreadyExists);
            }

            if (!standaloneDebugAlreadyExists) 
            {
                //Copy entrypoint sh file from StandaloneDebug directory
                //Normally makefile does this but we don't have capability to execute it here from Maven
                Path standaloneDebugPath = gitBasePath.resolve("StandaloneDebug");

                //Copy all *.sh and *.conf files
                //These files might differ between different Selenium versions but this pattern seems to work everywhere
                List<Path> extraFiles = Files.list(standaloneDebugPath).filter(file -> file.getFileName().toString().endsWith(".sh") ||
                                                                               file.getFileName().toString().endsWith(".conf"))
                                             .collect(Collectors.toList());
                getContext().getLog().debug("Extra files: " + extraFiles);

                //Build standalone debug image
                Path standaloneFirefoxDebugPath = gitBasePath.resolve("StandaloneFirefoxDebug");
                generateFromDockerFile(standaloneFirefoxDebugPath, standaloneDebugTargetImageName,
                                       nodeDebugTargetImageName,
                                       Collections.emptyMap(),
                                       extraFiles);
                cleanUpImageTag(nodeDebugTargetImageName, nodeDebugAlreadyExists);
            }
            
            return new GeneratedImage(standaloneDebugTargetImageName, standaloneDebugAlreadyExists);
        }
    }
    
    private void cleanUpImageTag(ImageReference tag, boolean existedBeforeGeneration)
    throws BrowserBoxException, DockerAccessException
    {
        if (existedBeforeGeneration) 
        {
            getContext().getLog().debug("Not removing node image " + tag.getName() + " since it already existed before");
            return;
        }
        
        boolean removeIntermediateImages = !projectConfiguration.getSaveBuildIntermediates().contains(BuildIntermediates.BOX_IMAGES);
        if (removeIntermediateImages)
        {
            boolean removed = boxContext.getDockerServiceHub().getDockerAccess().removeImage(tag.getName());
            if (removed)
                getContext().getLog().info("Removing tag for intermediate image " + tag.getName());
            else
                getContext().getLog().warn("Could not remove tag for node image " + tag.getName());
        }
    }
    
    protected ImageReference generateFromDockerFile(Path dockerFileDirectory, ImageReference targetImageName,
                                               ImageReference fromImageOverride,
                                               Map<String, String> dockerBuildArgs,
                                               Collection<? extends Path> extraFilesToCopyToDockerFileDirectory)
    throws BrowserBoxException, MojoExecutionException, IOException
    {
        MavenArchiveConfiguration archiveConfiguration = new MavenArchiveConfiguration();
        MavenFileFilter mavenFileFilter = new DefaultMavenFileFilter();
        MavenReaderFilter mavenReaderFilter = new DefaultMavenReaderFilter();

        Path tempBase = projectConfiguration.getBaseDirectory().toPath().relativize(boxContext.getTempDirectory());
        
        //Use a directory inside the temp dir named the same as the docker file directory
        tempBase = tempBase.resolve(dockerFileDirectory.getFileName());

        Path outputDirectory = tempBase.resolve("build");
        Path sourceDirectory = tempBase.resolve("src");
        Path dockerFileGeneratedDirectory = tempBase.resolve("docker");
        Files.createDirectories(outputDirectory);
        Files.createDirectories(sourceDirectory);
        Files.createDirectories(dockerFileGeneratedDirectory);
        FileUtils.cleanDirectory(outputDirectory.toFile());
        FileUtils.cleanDirectory(sourceDirectory.toFile());
        FileUtils.cleanDirectory(dockerFileGeneratedDirectory.toFile());

        //Copy files from dockerFileDirectory to dockerFileGeneratedDirectory
        //Don't want to modify anything in Git but we might need to transform the Dockerfile
        FileUtils.copyDirectory(dockerFileDirectory.toFile(), dockerFileGeneratedDirectory.toFile());
        
        //Copy any extra files
        for (Path extraFile : extraFilesToCopyToDockerFileDirectory)
        {
            FileUtils.copyFileToDirectory(extraFile.toFile(), dockerFileGeneratedDirectory.toFile());
        }
        
        //Transform Dockerfile in generated directory, replacing FROM if needed
        if (fromImageOverride != null)
            overrideDockerFrom(dockerFileGeneratedDirectory.resolve("Dockerfile"), fromImageOverride.getName());
        
        MojoParameters mojoParameters = new MojoParameters(boxContext.getSession(),
                projectConfiguration.getProject(),
                archiveConfiguration,
                mavenFileFilter,
                mavenReaderFilter,
                projectConfiguration.getSettings(),
                sourceDirectory.toString(),
                outputDirectory.toString(), 
                Collections.emptyList());
        RegistryService.RegistryConfig registryConfig = new RegistryService.RegistryConfig.Builder()
                .authConfigFactory(boxContext.getAuthConfigFactory())
                //.registry(registry)
                .authConfig(boxContext.getDockerAuthConfiguration() != null ? boxContext.getDockerAuthConfiguration().toMap() : null)
                .skipExtendedAuth(boxContext.isDockerSkipExtendedAuth())
                .settings(projectConfiguration.getSettings())
                .build();

        BuildService.BuildContext buildContext = new BuildService.BuildContext.Builder()
                .mojoParameters(mojoParameters)
                .registryConfig(registryConfig)
                .build();
        
        AssemblyConfiguration assemblyConfig = new AssemblyConfiguration.Builder()
                .permissions(PermissionMode.exec.name()) //Needed or else scripts will be built non-executable (was problem on Windows + Mac)
                .mode(AssemblyMode.dir.name())
                .build();

        BuildImageConfiguration buildConfig = new BuildImageConfiguration.Builder()
                .dockerFileDir(dockerFileGeneratedDirectory.toAbsolutePath().toString())
                .labels(dockerNaming.labels().forBrowserIntermediate(boxConfiguration))
                //See DockerFileUtil.extractDelimiters() - using 'false' or 'none' skips filtering
                //Since Selenium's dockerfile uses ${} for substitution env variables itself we don't want these pre-processed away
                .filter("false")
                .args(dockerBuildArgs)
                .assembly(assemblyConfig)
                .build();

        ImageConfiguration imageConfig = new ImageConfiguration.Builder()
                .name(targetImageName.getName())
                .buildConfig(buildConfig)
                .build();

        //TODO might want to reconfigure logger better
        String verbose = "false";
        imageConfig.getBuildConfiguration().initAndValidate(new AnsiLogger(getContext().getLog(), true, verbose));

        boxContext.getDockerServiceHub().getBuildService().buildImage(imageConfig, boxContext.getImagePullCacheManager(), buildContext);
        
        return targetImageName;
    }
    
    private void overrideDockerFrom(Path dockerFile, String from)
    throws IOException, BrowserBoxException
    {
        String dockerFileContent = new String(Files.readAllBytes(dockerFile), StandardCharsets.UTF_8);

        Pattern p = Pattern.compile("^FROM .*$", Pattern.MULTILINE);
        String newDockerFileContent = p.matcher(dockerFileContent).replaceFirst("FROM " + from);
        
        if (dockerFileContent.equals(newDockerFileContent))
            throw new BrowserBoxException("FROM replacement failed in Dockerfile");
        
        Files.write(dockerFile, newDockerFileContent.getBytes(StandardCharsets.UTF_8));
    }

    public static class GeneratedImage
    {
        private final ImageReference generatedImageName;
        private final boolean preexisting;

        public GeneratedImage(ImageReference generatedImageName, boolean preexisting)
        {
            this.generatedImageName = generatedImageName;
            this.preexisting = preexisting;
        }

        public ImageReference getGeneratedImageName()
        {
            return generatedImageName;
        }

        /**
         * @return true if the image already existed and generation wasn't needed, false if the image was generated.
         */
        public boolean isPreexisting()
        {
            return preexisting;
        }
    }
}
