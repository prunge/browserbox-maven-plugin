package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MultiEdgeDriverVersionResolver extends BaseEdgeDriverVersionResolver
{
    private final List<? extends EdgeDriverVersionResolver> resolvers;
    
    public MultiEdgeDriverVersionResolver(List<? extends EdgeDriverVersionResolver> resolvers)
    {
        this.resolvers = ImmutableList.copyOf(resolvers);
    }
    
    public MultiEdgeDriverVersionResolver(EdgeDriverVersionResolver... resolvers)
    {
        this(ImmutableList.copyOf(resolvers));
    }
    
    @Override
    public Version resolve(String edgeVersion) 
    throws BrowserBoxException 
    {
        for (EdgeDriverVersionResolver resolver : resolvers)
        {
            Version result = resolver.resolve(edgeVersion);
            if (result != null)
                return result;
        }
        
        return null;
    }

    @Override
    public Version resolveWithFallback(String edgeVersion) 
    throws BrowserBoxException 
    {
        List<? extends Version> edgeDriverVersions = allVersions();
        Version edgeBrowserVersion = new Version(edgeVersion);
        return resolveFromList(edgeDriverVersions, edgeBrowserVersion);
    }

    @Override
    public List<? extends Version> allVersions() 
    throws BrowserBoxException 
    {
        Set<Version> combinedAllVersions = new TreeSet<>(Comparator.comparing(Version::getRawVersion));
        
        for (EdgeDriverVersionResolver resolver : resolvers)
        {
            combinedAllVersions.addAll(resolver.allVersions());
        }

        return new ArrayList<>(combinedAllVersions);
    }
}
