package au.net.causal.maven.plugins.browserbox.box;

public interface ConnectionType
{
    /**
     * @return the name of the connection type, which might be specified by the user in option lists.
     */
    public String name();
}
