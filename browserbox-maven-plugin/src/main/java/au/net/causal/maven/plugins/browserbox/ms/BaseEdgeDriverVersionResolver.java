package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class BaseEdgeDriverVersionResolver implements EdgeDriverVersionResolver 
{
    protected Version resolveFromList(List<? extends Version> edgeDriverVersions, Version parsedEdgeVersion)
    throws BrowserBoxException
    {
        //We match the driver full version against the Edge full version

        String edgeBrowserMajor = parsedEdgeVersion.getMajorVersion();
        String edgeBrowserFull = parsedEdgeVersion.getEdgeVersion();

        //For each Edge driver version, parse and match, picking the highest version found if there are multiples
        //Try full match first
        Optional<? extends Version> foundMatchingEdgeDriverVersion = edgeDriverVersions.stream()
                .filter(edv -> edv.getEdgeVersion().equals(edgeBrowserFull))
                .max(Comparator.comparing(Version::getRawVersion, String.CASE_INSENSITIVE_ORDER));
        if (foundMatchingEdgeDriverVersion.isPresent())
            return foundMatchingEdgeDriverVersion.get();

        //No exact match, now try matching by Edge major version
        //Highest first
        foundMatchingEdgeDriverVersion = edgeDriverVersions.stream()
                .filter(edv -> edv.getEdgeVersion().startsWith(edgeBrowserMajor))
                .max(Comparator.comparing(Version::getRawVersion, String.CASE_INSENSITIVE_ORDER));
        if (foundMatchingEdgeDriverVersion.isPresent())
            return foundMatchingEdgeDriverVersion.get();

        //No match, find highest Edge driver version less than or equal to what we're searching for
        //Or if that fails, return null
        foundMatchingEdgeDriverVersion = edgeDriverVersions.stream()
               .filter(edv -> Long.parseLong(edgeBrowserFull) >= Long.parseLong(edv.getEdgeVersion()))
               .max(Comparator.comparing(Version::getRawVersion, String.CASE_INSENSITIVE_ORDER));
        return foundMatchingEdgeDriverVersion.orElse(null);
    }
}
