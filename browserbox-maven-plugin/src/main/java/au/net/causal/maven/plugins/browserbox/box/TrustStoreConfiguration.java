package au.net.causal.maven.plugins.browserbox.box;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A keystore holding server and root certificates to register as trusted in browsers.
 * Stores can be in any format supported by the Java crypto system.
 *
 * @see java.security.KeyStore
 */
public class TrustStoreConfiguration
{
    private File file;
    private String type = "PKCS12";
    private String password;
    private final List<String> aliases = new ArrayList<>();

    /**
     * @return the keystore file holding certificates.
     */
    public File getFile()
    {
        return file;
    }

    public void setFile(File file)
    {
        this.file = file;
    }

    /**
     * @return the keystore type.  Defaults to PKCS12.
     */
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * @return the store password.  May be null / omitted for an unprotected store.
     */
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the aliases of certificates in the store to use.  If none are specified, all aliases in the store are
     *         used.
     */
    public List<String> getAliases()
    {
        return aliases;
    }

    public void setAliases(List<String> aliases)
    {
        this.aliases.clear();
        this.aliases.addAll(aliases);
    }
}
