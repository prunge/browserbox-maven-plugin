package au.net.causal.maven.plugins.browserbox.versionstore;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.box.BrowserVersionsArtifactReader;

import java.util.Objects;
import java.util.Properties;

/**
 * Resolves download URLs from a properties file in the browser versions artifact in Maven.
 */
public class MavenBrowserVersionsArtifactRegistry extends PropertiesBasedVersionRegistry
{
    private final String browserType;
    private final BoxContext context;
    private final BrowserVersionsArtifactReader browserVersionsArtifactReader = new BrowserVersionsArtifactReader();

    /**
     * Creates the registry.
     *
     * @param browserType the browser type.  This forms part of the name of the versions properties resource read from
     *                    the Maven artifact.  The artifact name is called
     *                    <code>&lt;browserType&gt;-versions.properties</code>.
     * @param context box context.
     */
    public MavenBrowserVersionsArtifactRegistry(String browserType, BoxContext context)
    {
        Objects.requireNonNull(browserType, "browserType == null");
        Objects.requireNonNull(context, "context == null");
        this.browserType = browserType;
        this.context = context;
    }

    @Override
    protected Properties readDownloadProperties() 
    throws BrowserBoxException
    {
        return getBrowserVersionsArtifactReader().readPropertiesFile(browserType + "-downloads.properties", context);
    }

    protected BrowserVersionsArtifactReader getBrowserVersionsArtifactReader()
    {
        return browserVersionsArtifactReader;
    }

    protected String getBrowserType()
    {
        return browserType;
    }

    protected BoxContext getContext()
    {
        return context;
    }
}
