package au.net.causal.maven.plugins.browserbox.ms.feed;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(namespace = "http://www.w3.org/2005/Atom", name = "feed")
@XmlType
public class FeedType 
{
    private List<Entry> entry = new ArrayList<>();

    @XmlElement(namespace = "http://www.w3.org/2005/Atom", name = "entry")
    public List<Entry> getEntry() 
    {
        return entry;
    }

    public void setEntry(List<Entry> entry) 
    {
        this.entry = entry;
    }

    @XmlType
    public static class Entry
    {
        private String title;
        private Properties properties = new Properties();

        public String getTitle() 
        {
            return title;
        }

        public void setTitle(String title) 
        {
            this.title = title;
        }

        @XmlElement(namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata")
        public Properties getProperties() 
        {
            return properties;
        }

        public void setProperties(Properties properties) 
        {
            this.properties = properties;
        }
    }
    
    @XmlType
    public static class Properties
    {
        private String version;

        @XmlElement(name = "Version", namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")
        public String getVersion() 
        {
            return version;
        }

        public void setVersion(String version) 
        {
            this.version = version;
        }
    }
}
