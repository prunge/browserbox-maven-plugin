package au.net.causal.maven.plugins.browserbox.android;

import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.net.URL;
import java.util.Collection;
import java.util.Map;

public class WebDriverItem extends Item
{
    /**
     * Maps platform name to download URL for each platform.
     */
    private final Map<Platform, URL> platformSpecificDownloads;

    private final Platform defaultPlatform;

    private CompatibilityInfo compatibilityInfo;

    public WebDriverItem(String version, Platform defaultPlatform, Map<Platform, URL> platformSpecificDownloads,
                         CompatibilityInfo compatibilityInfo)
    {
        super(version, platformSpecificDownloads.get(defaultPlatform));
        this.defaultPlatform = defaultPlatform;
        this.platformSpecificDownloads = ImmutableMap.copyOf(platformSpecificDownloads);
        this.compatibilityInfo = compatibilityInfo;
    }

    public WebDriverItem(String version, Platform defaultPlatform, Map<Platform, URL> platformSpecificDownloads)
    {
        this(version, defaultPlatform, platformSpecificDownloads, null);
    }

    public Platform getDefaultPlatform()
    {
        return defaultPlatform;
    }

    public Map<Platform, URL> getPlatformSpecificDownloads()
    {
        return platformSpecificDownloads;
    }

    public CompatibilityInfo getCompatibilityInfo()
    {
        return compatibilityInfo;
    }

    public void setCompatibilityInfo(CompatibilityInfo compatibilityInfo)
    {
        this.compatibilityInfo = compatibilityInfo;
    }

    public WebDriverItem translateToPlatform(Platform platform)
    {
        return new WebDriverItem(getVersion(), platform, platformSpecificDownloads, compatibilityInfo);
    }

    public static Collection<? extends WebDriverItem> translateAllToPlatform(Collection<? extends Item> items, Platform platform)
    {
        ImmutableList.Builder<WebDriverItem> builder = new ImmutableList.Builder<>();
        for (Item item : items)
        {
            if (item instanceof WebDriverItem)
              builder.add(((WebDriverItem)item).translateToPlatform(platform));
        }

        return builder.build();
    }
}
