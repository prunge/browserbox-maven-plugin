package au.net.causal.maven.plugins.browserbox;

import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.VideoControl;
import com.google.common.base.Splitter;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

/**
 * A simple Java HTTP server that allows test extensions to communicate with the plugin to control browser testing
 * tools, such as the video recorder.
 */
public class ToolServer
{
    private final int port;
    private final BrowserBox box;
    private final Path videoDirectory;
    
    private volatile VideoControl.Recording currentVideo;

    public ToolServer(int port, BrowserBox box, Path videoDirectory)
    {
        this.port = port;
        this.box = box;
        this.videoDirectory = videoDirectory;
    }

    private String readHttpParameter(URI requestUri, String parameterName)
    {
        String queryString = requestUri.getQuery();
        Map<String, String> parameters = Splitter.on("&").withKeyValueSeparator("=").split(queryString);
        return parameters.get(parameterName);
    }
    
    public void start()
    throws IOException
    {
        HttpServer httpServer = HttpServer.create();
        httpServer.setExecutor(null);

        //Start recording video
        httpServer.createContext("/browserbox/start", h -> 
        {
            String name = readHttpParameter(h.getRequestURI(), "name");
            System.out.println("ToolServer: video start " + name);
            startVideoRecording();
            byte[] content = "success".getBytes(StandardCharsets.UTF_8);
            h.sendResponseHeaders(200, content.length);
            h.getResponseBody().write(content);
            h.close();
        });

        //Cancel recording video - stops recording and throws away the file
        httpServer.createContext("/browserbox/cancel", h ->
        {
            String name = readHttpParameter(h.getRequestURI(), "name");
            System.out.println("ToolServer: video cancel " + name);
            cancelVideoRecording();
            byte[] content = "success".getBytes(StandardCharsets.UTF_8);
            h.sendResponseHeaders(200, content.length);
            h.getResponseBody().write(content);
            h.close();
        });

        //Stops recording video and saves the video to the local filesystem
        httpServer.createContext("/browserbox/save", h ->
        {
            String name = readHttpParameter(h.getRequestURI(), "name");
            Path videoFile = videoDirectory.resolve(name + ".mp4");
            System.out.println("ToolServer: video save " + videoFile.toAbsolutePath());
            stopAndSaveVideoRecording(videoFile);
            byte[] content = "success".getBytes(StandardCharsets.UTF_8);
            h.sendResponseHeaders(200, content.length);
            h.getResponseBody().write(content);
            h.close();
        });

        httpServer.bind(new InetSocketAddress(port), 0);
        httpServer.start();
    }
    
    private void startVideoRecording()
    {
        if (currentVideo != null)
            cancelVideoRecording();
        
        try
        {
            currentVideo = box.video().startRecording();
        }
        catch (BrowserBoxException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    private void cancelVideoRecording()
    {
        if (currentVideo == null)
            return;
        
        try
        {
            currentVideo.stopAndCancel();
        }
        catch (BrowserBoxException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        
        currentVideo = null;
    }
    
    private void stopAndSaveVideoRecording(Path videoFile)
    {
        if (currentVideo == null)
            return;
        
        try
        {
            Files.createDirectories(videoFile.getParent());
            currentVideo.stopAndSave(videoFile);
        }
        catch (BrowserBoxException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        
        currentVideo = null;
    }
}
