package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.execute.BrowserControl;
import org.openqa.selenium.Capabilities;

import java.net.URI;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface BrowserBox 
{
    /**
     * @return true if the container exists, false if not.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public boolean exists()
    throws BrowserBoxException;

    /**
     * @return true if the container exists and is currently running.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public boolean isRunning()
    throws BrowserBoxException;

    /**
     * Generates or downloads any images required for browser box operation.  Does nothing if the image already exists 
     * locally.
     * <p>
     *     
     * This might simply involve downloading an image from the Internet or might involve a combination of downloading 
     * and other operations to generate a container image.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public void prepareImage()
    throws BrowserBoxException;

    /**
     * Returns whether an image exists for this box.
     * 
     * @return true if the image exists for this box, false if not.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public boolean hasImage()
    throws BrowserBoxException;
    
    /**
     * Starts an already-existing container.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void start()
    throws BrowserBoxException;

    /**
     * Stops a running container.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void stop()
    throws BrowserBoxException;

    /**
     * Creates a container and starts it.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void createAndStart()
    throws BrowserBoxException;

    /**
     * Creates and configures TCP port tunneling.
     * 
     * @return tunnel session, a resource that must be closed when tunneling is finished.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public TunnelSession establishTunnels()
    throws BrowserBoxException;

    /**
     * Deletes the container.  Container must be stopped.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void delete()
    throws BrowserBoxException;

    /**
     * Deletes any images for the box.  This clears out image caches and cleans up as much as possible.
     * No containers must exist and can be running for this browser box.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public void deleteImage()
    throws BrowserBoxException;

    /**
     * @return the name of the container running the browser.
     */
    public String getName();

    /**
     * Blocks until the browser box has completely started up.
     *
     * @param maxTimeToWait the maximum time to wait.  If null, potentially wait forever.
     *
     * @throws TimeoutException if the maximum time to wait is exceeded while waiting for startup.
     * @throws BrowserBoxException if another error occurs.
     */
    public void waitUntilStarted(Duration maxTimeToWait)
    throws TimeoutException, BrowserBoxException;

    /**
     * @return the URI of the webdriver server.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public default URI getWebDriverUrl()
    throws BrowserBoxException
    {
        return getConnectionInfo(StandardConnectionType.SELENIUM).getUri();
    }
    
    /**
     * Returns a list of supported connection types for this browser box, in order of preference.
     * For example, if there are multiple connection types to support remote desktop display, such as VNC and RDP, 
     * different boxes might return these types in a different order depending on which offers the best experience for 
     * that particular box.
     * 
     * @return a list of connection types supported by the browser box.
     * 
     * @throws BrowserBoxException if an error occurs.
     * 
     * @see #getConnectionInfo(ConnectionType) 
     * @see StandardConnectionType
     */
    public List<? extends ConnectionType> getSupportedConnectionTypes()
    throws BrowserBoxException;

    /**
     * Returns connection details for a connection type.
     * 
     * @param connectionType the connection type.  Must be supported by this box.
     *                       
     * @return connection details.
     * 
     * @throws BrowserBoxException if an error occurs, or if the connection type is unsupported.
     * 
     * @see #getSupportedConnectionTypes() 
     * @see StandardConnectionType
     */
    public ConnectionInfo getConnectionInfo(ConnectionType connectionType)
    throws BrowserBoxException;

    /**
     * @return an interface for launching and quitting the web browser.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public BrowserControl browserControl()
    throws BrowserBoxException;

    /**
     * @return a control for recording video from the browser.
     * 
     * @throws BrowserBoxException if an error occurs.
     */
    public VideoControl video()
    throws BrowserBoxException;

    /**
     * @return the Selenium browser type as defined in {@link org.openqa.selenium.remote.BrowserType}
     */
    public String getSeleniumBrowserType();

    /**
     * @return the Selenium desired capabilities configuration required to connect to this box with Selenium, or
     *          <code>null</code> if no special capabilities are required.
     *
     * @throws BrowserBoxException if an error occurs.
     */
    public default Capabilities getSeleniumDesiredCapabilities()
    throws BrowserBoxException
    {
        return null;
    }

    //TODO undefault?

    /**
     * Installs trusted SSL certificates into the target browser.  This is used to trust additional CAs than the
     * default, and typically for self-signed certificates of a development server.
     *
     * @param certificates certificates to install.
     *
     * @throws BrowserBoxException if an error occurs installing the certificates.
     *
     * @since 1.1
     */
    public default void installCertificates(Collection<? extends X509Certificate> certificates)
    throws BrowserBoxException
    {
        throw new UnsupportedOperationException();
    }
}
