package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.box.BrowserVersionsArtifactReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Properties;

public class MavenArtifactPropertiesEdgeDriverVersionResolver extends PropertiesEdgeDriverVersionResolver 
{
    private final BrowserVersionsArtifactReader browserVersionsArtifactReader = new BrowserVersionsArtifactReader();
    private final BoxContext context;
    
    public MavenArtifactPropertiesEdgeDriverVersionResolver(BoxContext context)
    {
        this.context = context;
    }
    
    @Override
    protected Properties readProperties() throws BrowserBoxException 
    {
        Path versionJar = browserVersionsArtifactReader.readBrowserVersionArtifact(context);
        if (versionJar == null)
            return new Properties();

        try (URLClassLoader versionLoader = URLClassLoader.newInstance(new URL[] {versionJar.toUri().toURL()}, null))
        {
            URL downloadsResource = versionLoader.findResource("edge-driver-downloads.properties");
            if (downloadsResource == null)
                throw new BrowserBoxException("Missing driver versions properties for edge in " + versionJar);

            Properties downloads = new Properties();
            try (InputStream versionsIs = downloadsResource.openStream())
            {
                downloads.load(versionsIs);
            }

            return downloads;
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("I/O error reading version JAR " + versionJar + ": " + e.getMessage(), e);
        }
    }
}
