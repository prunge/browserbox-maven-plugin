package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.net.URI;

public abstract class SshBrowserControl implements BrowserControl
{
    private final ConnectionInfo sshConnectionInfo;
    
    protected SshBrowserControl(ConnectionInfo sshConnectionInfo)
    {
        this.sshConnectionInfo = sshConnectionInfo;
    }
    
    private int executeSshCommand(String command)
    throws BrowserBoxException
    {
        JSch jsch = new JSch();
        
        try
        {
            Session sshSession = jsch.getSession(sshConnectionInfo.getUsername(), sshConnectionInfo.getUri().getHost(), sshConnectionInfo.getUri().getPort());
            try 
            {
                sshSession.setPassword(sshConnectionInfo.getPassword());

                //Disables known hosts checking - good enough for testing
                sshSession.setConfig("StrictHostKeyChecking", "no");

                sshSession.connect();

                ChannelExec exec = (ChannelExec) sshSession.openChannel("exec");
                exec.setCommand(command);
                
                //TODO we should make this go to logs instead
                exec.setOutputStream(System.out, true);
                exec.setErrStream(System.err, true);
                
                exec.connect();
                int result = exec.getExitStatus();
                exec.disconnect();
                return result;
            }
            finally
            {
                sshSession.disconnect();
            }
        }
        catch (JSchException e)
        {
            throw new BrowserBoxException("Error running SSH command: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void launch(URI url) 
    throws IOException, BrowserBoxException 
    {
        executeSshCommand(launchCommand(url));
    }

    @Override
    public void quit() 
    throws IOException, BrowserBoxException 
    {
        executeSshCommand(quitCommand());
    }
    
    protected abstract String launchCommand(URI url);
    protected abstract String quitCommand();
}
