package au.net.causal.maven.plugins.browserbox;

public class BrowserBoxException extends Exception 
{
    public BrowserBoxException() 
    {
    }

    public BrowserBoxException(String message) 
    {
        super(message);
    }

    public BrowserBoxException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public BrowserBoxException(Throwable cause) 
    {
        super(cause);
    }

    public BrowserBoxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
