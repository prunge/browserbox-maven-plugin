package au.net.causal.maven.plugins.browserbox.seleniumdocker;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;

import java.util.List;

public interface BrowserVersionResolver 
{
    public Tag tagForBrowserVersion(String version)
    throws BrowserBoxException;
    
    public List<String> availableBrowserVersions()
    throws BrowserBoxException;
    
    public String defaultVersion()
    throws BrowserBoxException;
}
