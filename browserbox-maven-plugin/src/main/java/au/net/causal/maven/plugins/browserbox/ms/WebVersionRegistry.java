package au.net.causal.maven.plugins.browserbox.ms;

import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Resolves available download versions of Microsoft browser boxes from the Microsoft site.
 */
public abstract class WebVersionRegistry implements VersionRegistry
{
    private final ClientBuilder clientBuilder;

    /**
     * List of RegExp patterns that are used in an attempt to find a match on download item names and whose groups are used to parse out the version number.
     */
    private final Iterable<? extends Pattern> itemNamePatterns;

    /**
     * Creates a web version registry with a custom HTTP client builder and multiple patterns.
     *
     * @param clientBuilder client builder used to build HTTP clients.
     * @param itemNamePatterns list of RegExp patterns that are used in an attempt to find a match on download item names and whose groups are used to parse out the version number.
     *                         The first capturing group of each pattern should extract the version.
     */
    protected WebVersionRegistry(ClientBuilder clientBuilder, Iterable<? extends Pattern> itemNamePatterns)
    {
        Objects.requireNonNull(clientBuilder, "clientBuilder == null");
        Objects.requireNonNull(itemNamePatterns, "itemNamePatterns == null");
        this.clientBuilder = clientBuilder;
        this.itemNamePatterns = ImmutableList.copyOf(itemNamePatterns);
    }

    /**
     * Creates a web version registry using the default HTTP client builder and multiple patterns.
     *
     * @param itemNamePatterns list of RegExp patterns that are used in an attempt to find a match on download item names and whose groups are used to parse out the version number.
 *                             The first capturing group of each pattern should extract the version.
     */
    protected WebVersionRegistry(Iterable<? extends Pattern> itemNamePatterns)
    {
        this(ClientBuilder.newBuilder(), itemNamePatterns);
    }

    /**
     * Creates a web version registry with a custom HTTP client builder and a single pattern.
     *
     * @param clientBuilder client builder used to build HTTP clients.
     * @param itemNamePattern a RegExp pattern that is used in an attempt to find a match on download item names and whose groups are used to parse out the version number.
     *                        The first capturing group of this pattern should extract the version.
     */
    protected WebVersionRegistry(ClientBuilder clientBuilder, Pattern itemNamePattern)
    {
        this(clientBuilder, ImmutableList.of(itemNamePattern));
    }

    /**
     * Creates a web version registry using the default HTTP client builder and a single pattern.
     *
     * @param itemNamePattern a RegExp pattern that is used in an attempt to find a match on download item names and whose groups are used to parse out the version number.
     *                        The first capturing group of this pattern should extract the version.
     */
    protected WebVersionRegistry(Pattern itemNamePattern)
    {
        this(ImmutableList.of(itemNamePattern));
    }

    /**
     * Determines the version of a download item that is pre-known without needing to parse the text.
     * This method always returns null, but subclasses may override this to hardcode certain known versions.
     *
     * @param item the download item.
     *
     * @return the known version for this item, or null if the version should be determined by parsing the item's name.
     */
    protected String getPreKnownVersionForItem(DownloadItem item)
    {
        return null;
    }

    @Override
    public ItemList readAllItemsAllowFailures(Query query) throws BrowserBoxException
    {
        List<BrowserBoxException> downloadErrors = new ArrayList<>();
        List<Item> downloadList = new ArrayList<>();

        //Get box list from Microsoft site
        Client client = clientBuilder.build();

        try
        {
            List<DownloadItem> items = client.target(DownloadItem.VM_API_URL)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(new GenericType<List<DownloadItem>>() {});

            //Keep track of versions already processed
            //Use the first listed download for each version - tends to be stable (vs preview) or Win7 vs Win8.1
            Set<String> processedVersions = new HashSet<>();

            for (DownloadItem item : items)
            {
                Matcher itemNameMatcher = findMatchingItemNamePattern(item.getName());

                String versionString;
                if (itemNameMatcher != null)
                    versionString = itemNameMatcher.group(1);
                else
                    versionString = getPreKnownVersionForItem(item);

                if (versionString != null && !query.getIgnoredVersions().contains(versionString) && processedVersions.add(versionString))
                {
                    List<Software> vagrantSoftware = item.getSoftware().stream()
                            .filter(software -> Strings.nullToEmpty(software.getName()).equalsIgnoreCase("Vagrant"))
                            .collect(Collectors.toList());
                    if (vagrantSoftware.isEmpty())
                    {
                        throw new BrowserBoxException("No Vagrant software available for " + item.getName() + ": " +
                                item.getSoftware().stream().map(Software::getName).collect(Collectors.joining(", ")));
                    }

                    Software selectedSoftware = vagrantSoftware.get(0);

                    //If there are multiple files, pick the ZIP file
                    List<SoftwareFile> files = selectedSoftware.getFiles().stream()
                            .filter(file -> Strings.nullToEmpty(file.getName()).toLowerCase(Locale.ENGLISH).endsWith(".zip"))
                            .collect(Collectors.toList());
                    if (files.isEmpty())
                        throw new BrowserBoxException("No files available to download for " + selectedSoftware.getName() + ": " + selectedSoftware.getFiles());

                    downloadList.add(new Item(versionString, files.get(0).getUrl().toURL()));
                }
            }
        }
        catch (WebApplicationException | ProcessingException | MalformedURLException e)
        {
            downloadErrors.add(new BrowserBoxException("Failed to retrieve VM list from Microsoft site: " + e, e));
        }

        return new ItemList(downloadList, downloadErrors);
    }

    /**
     * Attempts to use each itemNamePattern to find a match so that the browser version can be parsed out.
     *
     * @param itemName the item name.
     *
     * @return a RegExp matcher that successfully matched whose groups may be used to read out the version number, or null if no match was found.
     */
    private Matcher findMatchingItemNamePattern(String itemName)
    {
        for (Pattern itemNamePattern : itemNamePatterns)
        {
            Matcher itemNameMatcher = itemNamePattern.matcher(itemName);
            if (itemNameMatcher.matches())
                return itemNameMatcher;
        }

        return null;
    }
}
