package au.net.causal.maven.plugins.browserbox.box;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.GenerateVersionsContext;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.ms.*;
import au.net.causal.maven.plugins.browserbox.seleniumdocker.BrowserVersionTools;
import au.net.causal.maven.plugins.browserbox.vagrant.BoxDefinition;
import au.net.causal.maven.plugins.browserbox.vagrant.LocalVagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.Vagrant;
import au.net.causal.maven.plugins.browserbox.vagrant.VagrantException;
import au.net.causal.maven.plugins.browserbox.versionstore.CompositeVersionedItemStore;
import au.net.causal.maven.plugins.browserbox.versionstore.FilterNonWorkingUrlsVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.GenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.Item;
import au.net.causal.maven.plugins.browserbox.versionstore.ItemList;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenBrowserVersionsArtifactRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.MavenRepositoryStore;
import au.net.causal.maven.plugins.browserbox.versionstore.ResumableFileDownloader;
import au.net.causal.maven.plugins.browserbox.versionstore.UrlKeyPreservingGenerateVersionsRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionRegistry.Query;
import au.net.causal.maven.plugins.browserbox.versionstore.VersionedItemStore;
import au.net.causal.maven.plugins.browserbox.virtualbox.LocalVirtualBoxExecutable;
import au.net.causal.maven.plugins.browserbox.virtualbox.LocalVirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.LocalVirtualBoxSDL;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxExecutable;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxManager;
import au.net.causal.maven.plugins.browserbox.virtualbox.VirtualBoxSDL;
import com.google.common.collect.ImmutableSet;
import org.codehaus.plexus.util.cli.Commandline;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionScheme;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MicrosoftVagrantBrowserBoxFactory implements BrowserBoxFactory 
{
    private static final Set<String> SUPPORTED_TYPES = ImmutableSet.of("edge", "ie", "ie8", "ie9", "ie10", "ie11");

    private final BrowserVersionTools versionTools = new BrowserVersionTools();
    
    @Override
    public BrowserBox create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) 
    throws BrowserBoxException 
    {
        initializeDefaults(boxConfiguration, context);

        Vagrant vagrant = createVagrantExecutor(context);
        VirtualBoxManager virtualBoxManager = createVirtualBoxManagerExecutor(boxConfiguration, projectConfiguration, context);
        VirtualBoxExecutable virtualBox = createVirtualBoxExecutableExecutor(boxConfiguration, projectConfiguration, context);
        VirtualBoxSDL vboxSdl = createVirtualBoxSdlExecutor(boxConfiguration, projectConfiguration, context);

        try 
        {
            Path containerDirectory = context.getGlobalConfigDirectory().resolve(boxConfiguration.getContainerName());
            Files.createDirectories(containerDirectory);
            return new MicrosoftVagrantBrowserBox(boxConfiguration, projectConfiguration, context,
                                                  virtualBoxManager, virtualBox, vboxSdl, vagrant, containerDirectory,
                                                  versionRegistry(boxConfiguration.getBrowserType(), context));
        }
        catch (IOException e)
        {
            throw new BrowserBoxException("I/O error creating Vagrant box: " + e.getMessage(), e);
        }
    }
    
    private VirtualBoxManager createVirtualBoxManagerExecutor(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        //TODO toolchain lookup, etc.
        Commandline commandLine = new Commandline("VBoxManage");

        VirtualBoxManager vbox = new LocalVirtualBoxManager(commandLine, context.getLog());
        return vbox;
    }

    private VirtualBoxSDL createVirtualBoxSdlExecutor(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        //TODO toolchain lookup, etc.
        Commandline commandLine = new Commandline("VBoxSDL");

        VirtualBoxSDL vboxSdl = new LocalVirtualBoxSDL(commandLine, context.getLog());
        return vboxSdl;
    }

  private VirtualBoxExecutable createVirtualBoxExecutableExecutor(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
  {
    //TODO toolchain lookup, etc.
    Commandline commandLine = new Commandline("VirtualBox");

    VirtualBoxExecutable virtualBox = new LocalVirtualBoxExecutable(commandLine, context.getLog());
    return virtualBox;
  }

    private Vagrant createVagrantExecutor(BoxContext context)
    {
        //TODO toolchain lookup, etc.
        Commandline commandLine = new Commandline("vagrant");

        Vagrant vagrant = new LocalVagrant(commandLine, context.getLog());
        return vagrant;
    }

    @Override
    public boolean supportsType(String type) 
    {
        return SUPPORTED_TYPES.contains(type);
    }

    @Override
    public Set<String> getKnownTypes() 
    {
        //Don't include shortcut aliases
        return ImmutableSet.of("edge", "ie");
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, BoxContext context)
    throws BrowserBoxException
    {
        if (boxConfiguration.getContainerName() == null)
        {
            if (boxConfiguration.getBrowserVersion() == null)
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType());
            else
                boxConfiguration.setContainerName("browserbox-" + boxConfiguration.getBrowserType() + "-" + boxConfiguration.getBrowserVersion());
        }
        if (boxConfiguration.getWebDriverPort() <= 0)
            boxConfiguration.setWebDriverPort(24444);
        
        //Version resolution code
        List<String> allVersions = availableKnownVersions(boxConfiguration.getBrowserType(), context);
        
        //First, if there is no version specified, default to the latest
        if (boxConfiguration.getBrowserVersion() == null && !allVersions.isEmpty()) 
        {
            boxConfiguration.setBrowserVersion(allVersions.get(allVersions.size() - 1));
            context.getLog().info("Selected " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion());
        }

        //Attempt to resolve to real version if there's no exact match of version
        if (!allVersions.contains(boxConfiguration.getBrowserVersion()))
        {
            String resolvedVersion = versionTools.findBrowserVersion(boxConfiguration.getBrowserVersion(), allVersions, Function.identity(), Function.identity());
            if (resolvedVersion != null) 
            {
                context.getLog().info("Resolved " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() +
                                        " to " + resolvedVersion);
                boxConfiguration.setBrowserVersion(resolvedVersion);
            }
            else
                context.getLog().warn("Could not resolve " + boxConfiguration.getBrowserType() + " version " + boxConfiguration.getBrowserVersion() + ".");
        }
    }

    /**
     * The version download resolver for downloading version metadata from a known Microsoft website.
     * This site lists the latest versions of each browser, typically IE 8-11 and 1-2 latest versions of Edge.
     * 
     * @param browserType the browser type string, either ie or edge.
     *                    
     * @return a download resolver.
     */
    protected VersionRegistry webVersionRegistry(String browserType)
    {
        if ("edge".equalsIgnoreCase(browserType))
            return new WebEdgeVersionRegistry();
        else
            return new WebIEWithEdgeFallbackRegistry();
    }

    /**
     * Returns a composite version download resolver with all known sources of browser versions.
     * 
     * @param browserType the browser type, either ie or edge.
     * @param context box context.
     *                
     * @return a download resolver.
     */
    protected VersionedItemStore versionRegistry(String browserType, BoxContext context)
    {
        BrowserSelector.Type selectorType = BrowserSelector.Type.valueOf(browserType.toUpperCase(Locale.ENGLISH));

        Artifact saveTemplate = new DefaultArtifact("com.microsoft.browser.vm", selectorType.name().toLowerCase(Locale.ENGLISH), "zip", "0.0");

        return new CompositeVersionedItemStore(
                //Use resumable downloader because Microsoft's downloads are frequently truncated
                new MavenRepositoryStore(saveTemplate, context, new ResumableFileDownloader(context::getTempDirectory, context.getLog()), context.getLog()),
                new FilterNonWorkingUrlsVersionsRegistry(new MavenBrowserVersionsArtifactRegistry(browserType, context), context.getLog()),
                webVersionRegistry(browserType)
        );
    }

    @Override
    public List<String> availableKnownVersions(String type, BoxContext context) 
    throws BrowserBoxException 
    {
        //Parse into Maven objects purely for the sorting logic
        VersionScheme versionScheme = new GenericVersionScheme();
        
        ItemList results = versionRegistry(type, context).readAllItemsAllowFailures(new Query());
        for (BrowserBoxException ex : results.getErrors())
        {
            context.getLog().warn(ex.getMessage());
            context.getLog().debug(ex);
        }
        if (!results.getErrors().isEmpty())
            context.getLog().warn("Problems occurred while building version list - it may be incomplete.  Are you offline?");
        
        return results.getItems().stream()
                .map(Item::getVersion)
                .distinct()
                .map(v -> parseVersion(versionScheme, v))
                .sorted()
                .map(Version::toString)
                .collect(Collectors.toList());
    }
    
    private Version parseVersion(VersionScheme scheme, String versionString)
    {
        try 
        {
            return scheme.parseVersion(versionString);
        }
        catch (InvalidVersionSpecificationException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAllImages(String type, BoxContext context) 
    throws BrowserBoxException 
    {
        MicrosoftVagrantBoxManager microsoftVagrantBoxManager = new MicrosoftVagrantBoxManager();
        
        Vagrant.BoxListOptions options = new Vagrant.BoxListOptions();
        
        Vagrant vagrant = createVagrantExecutor(context);
        
        try 
        {
            for (BoxDefinition vagrantDefinition : vagrant.boxList(options))
            {
                if (microsoftVagrantBoxManager.isKnownVagrantBoxImageName(vagrantDefinition.getName())) 
                {
                    Vagrant.BoxRemoveOptions removeOptions = new Vagrant.BoxRemoveOptions(vagrantDefinition);
                    removeOptions.setForce(true);
                    vagrant.boxRemove(removeOptions);
                }
            }
        }
        catch (VagrantException e)
        {
            throw new BrowserBoxException("Error running Vagrant command: " + e.getMessage(), e);
        }
    }

    @Override
    public void generateVersionsFiles(String type, ProjectConfiguration projectConfiguration, 
                                      BoxContext context, GenerateVersionsContext generateContext) 
    throws BrowserBoxException, IOException 
    {
        //Construct appropriate resolver tree
        //The subclass of the registry will preserve any existing mapped versions
        //This is important for Edge especially, since the version number of later browsers cannot be automatically
        //calculated for now
        VersionRegistry webResolver = webVersionRegistry(type);
        GenerateVersionsRegistry saveRegistry = new UrlKeyPreservingGenerateVersionsRegistry(generateContext);

        //Read any new versions
        saveRegistry.saveItems(webResolver.readAllItems(new Query()));

        //For Edge only, also generate the Edge driver versions file
        if ("edge".equals(type))
        {
            MavenArtifactPropertiesEdgeDriverVersionResolver targetEdgeDriverResolver = new MavenArtifactPropertiesEdgeDriverVersionResolver(context);
            EdgeDriverVersionResolver sourceEdgeDriverResolver = new ChocolateyEdgeDriverVersionResolver();
            sourceEdgeDriverResolver = new RetryEdgeDriverVersionResolver(sourceEdgeDriverResolver, 20, Duration.ofSeconds(2), context.getLog());
            
            List<? extends EdgeDriverVersionResolver.Version> edgeDriverVersions = sourceEdgeDriverResolver.allVersions();
            targetEdgeDriverResolver.toProperties(edgeDriverVersions, generateContext.getDriverVersions());
            generateContext.saveDriverVersions();
        }
    }
}
