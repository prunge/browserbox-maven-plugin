package au.net.causal.maven.plugins.browserbox.execute;

import au.net.causal.maven.plugins.browserbox.BoxContext;
import au.net.causal.maven.plugins.browserbox.BrowserBoxException;
import au.net.causal.maven.plugins.browserbox.ProjectConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BoxConfiguration;
import au.net.causal.maven.plugins.browserbox.box.BrowserBox;
import au.net.causal.maven.plugins.browserbox.box.ConnectionInfo;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.regex.Pattern;

public class WindowsRealVncFinder extends EnvironmentPathBasedFinder
{
    private static final Pattern VNC_PATTERN = Pattern.compile("^vnc-viewer.*\\.exe$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PRODUCT_NAME_PATTERN = Pattern.compile("^VNC®$");
    
    private final RealVncTools realVnc = new RealVncTools();
    
    @Override
    public Optional<Commandline> find(ConnectionInfo connectionInfo, BrowserBox box, 
                                      BoxConfiguration boxConfiguration, 
                                      ProjectConfiguration projectConfiguration, 
                                      BoxContext context) 
    throws IOException, BrowserBoxException 
    {
        Optional<Path> executable = findInSearchPath(new FileNamePatternMatcher(VNC_PATTERN).and(
                                                    new ExeAttributeMatcher("ProductName", PRODUCT_NAME_PATTERN)));
        if (executable.isPresent())
        {
            Commandline commandline =  new Commandline(executable.get().toAbsolutePath().toString());
            
            Path vncFile = realVnc.generateVncFile(connectionInfo, context);
            commandline.addArguments(args(vncFile.toAbsolutePath().toString()));
            
            return Optional.of(commandline);
        }
        
        return Optional.empty();
    }
}
