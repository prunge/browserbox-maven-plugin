package com.android.ddmlib;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class BrowserBoxAdbHelper 
{
    public static void removeReverse(InetSocketAddress adbSockAddr, IDevice device,
                                     String localPortSpec, String remotePortSpec)
    throws TimeoutException, AdbCommandRejectedException, IOException
    {
        SocketChannel adbChan = null;
        try
        {
            adbChan = SocketChannel.open(adbSockAddr);
            adbChan.configureBlocking(false);

            AdbHelper.setDevice(adbChan, device);

            byte[] request = AdbHelper.formAdbRequest(String.format(
                    "reverse:killforward:%1$s", //$NON-NLS-1$
                    localPortSpec));

            AdbHelper.write(adbChan, request);

            AdbHelper.AdbResponse resp = AdbHelper.readAdbResponse(adbChan, false /* readDiagString */);
            if (!resp.okay)
            {
                Log.w("create-reverse", "Error removing reverse: " + resp.message);
                throw new AdbCommandRejectedException(resp.message);
            }
        }
        finally
        {
            if (adbChan != null)
                adbChan.close();
        }
    }
    
    public static void createReverse(InetSocketAddress adbSockAddr, IDevice device,
                                     String localPortSpec, String remotePortSpec)
    throws TimeoutException, AdbCommandRejectedException, IOException 
    {
        SocketChannel adbChan = null;
        try 
        {
            adbChan = SocketChannel.open(adbSockAddr);
            adbChan.configureBlocking(false);

            AdbHelper.setDevice(adbChan, device);

            byte[] request = AdbHelper.formAdbRequest(String.format(
                    "reverse:forward:%1$s;%2$s", //$NON-NLS-1$
                    localPortSpec, remotePortSpec));
            
            AdbHelper.write(adbChan, request);

            AdbHelper.AdbResponse resp = AdbHelper.readAdbResponse(adbChan, false /* readDiagString */);
            if (!resp.okay) 
            {
                Log.w("create-reverse", "Error creating reverse: " + resp.message);
                throw new AdbCommandRejectedException(resp.message);
            }
        } 
        finally 
        {
            if (adbChan != null) 
                adbChan.close();
        }
    }
}
