param (
    [string]$UpdateFile
)

$ErrorActionPreference = "Stop"

$proc = Start-Process -FilePath "wusa.exe" -ArgumentList ($UpdateFile, "/quiet", "/norestart") -Wait -PassThru

#3010 means pending restart
if (($proc.ExitCode -ne 0) -and ($proc.ExitCode -ne 3010))
{
    exit 12;
}
