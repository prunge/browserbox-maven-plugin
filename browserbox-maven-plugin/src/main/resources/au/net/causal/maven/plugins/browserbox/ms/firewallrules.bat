call refreshenv

netsh advfirewall firewall add rule name="Java" program="%JAVA_HOME%\bin\java.exe" protocol=tcp dir=in enable=yes action=allow profile=any
if %errorlevel% neq 0 exit /b %errorlevel%

netsh advfirewall firewall add rule name="Selenium IE" program="c:\tools\Selenium\IEDriverServer.exe" protocol=tcp dir=in enable=yes action=block profile=public,private
if %errorlevel% neq 0 exit /b %errorlevel%

netsh advfirewall firewall add rule name="Windows Remote Management (HTTP-In) Public" protocol=tcp dir=in enable=yes action=allow profile=public localport=5985
if %errorlevel% neq 0 exit /b %errorlevel%

netsh advfirewall firewall add rule name="TigerVNC" protocol=tcp dir=in enable=yes action=allow profile=any localport=5900
if %errorlevel% neq 0 exit /b %errorlevel%

netsh advfirewall firewall set rule group="Remote Desktop" new enable=yes
if %errorlevel% neq 0 exit /b %errorlevel%
