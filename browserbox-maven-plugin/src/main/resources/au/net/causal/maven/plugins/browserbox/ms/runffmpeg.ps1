$ErrorActionPreference = "Stop"

$psi = New-Object System.Diagnostics.ProcessStartInfo;
$psi.FileName = "ffmpeg.exe"
$psi.Arguments = $args;
$psi.UseShellExecute = $false;
$psi.RedirectStandardInput = $true;

$p = [System.Diagnostics.Process]::Start($psi);

$n = 1

#Wait until the finish file exists
while (!(Test-Path "C:\vagrant\stopffmpeg")) 
{ 
    Start-Sleep -Milliseconds 50 
    if ($p.HasExited)
    {
        #Rename the old output file
        if (Test-Path "c:\vagrant\output.mp4")
        {
            Write-Host "Saving output to output-$n.mp4"
            Rename-Item c:\vagrant\output.mp4 c:\vagrant\output-$n.mp4
            $n++
        }
        
        #Restart process
        Write-Host "FFMpeg process died, restarting... " + $p.ExitCode
        $p = [System.Diagnostics.Process]::Start($psi);
    }
    else
    {
        #Write-Host "FFMpeg is still alive..."
    }
}

#Send quit command to ffmpeg
$p.StandardInput.WriteLine("q");
$p.StandardInput.Close();

#Wait for process to end
$p.WaitForExit();

#Rename last good file if actual one does not exist
while (!(Test-Path "c:\vagrant\output.mp4") -and $n -gt 0)
{
    $n--
    if (Test-Path "c:\vagrant\output-$n.mp4")
    {
        Rename-Item "c:\vagrant\output-$n.mp4" "c:\vagrant\output.mp4"
    }
}

return $p.ExitCode;
