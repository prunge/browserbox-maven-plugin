<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- Copy everything -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <!-- Copy and augment root map element -->
    <xsl:template match="/map">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />

            <!-- Add our elements to the end -->
            <int name="com.android.chrome.SEARCH_ENGINE_PROMO_SHOWN" value="0" />
            <boolean name="skip_welcome_page" value="true" />
            <boolean name="first_run_tos_accepted" value="true" />
            <boolean name="promos_skipped_on_first_start" value="true" />
            <boolean name="first_run_signin_complete" value="true" />
            <boolean name="first_run_signin_setup" value="false" />
            <boolean name="first_run_flow" value="true" />
            <boolean name="BANDWIDTH_REDUCTION_PROXY_ENABLED" value="false" />
        </xsl:copy>
    </xsl:template>

    <!-- Do not copy the following elements, we are replacing them -->
    <xsl:template match="int[@name = 'com.android.chrome.SEARCH_ENGINE_PROMO_SHOWN']" />
    <xsl:template match="boolean[@name = 'skip_welcome_page']" />
    <xsl:template match="boolean[@name = 'first_run_tos_accepted']" />
    <xsl:template match="boolean[@name = 'promos_skipped_on_first_start']" />
    <xsl:template match="boolean[@name = 'first_run_signin_complete']" />
    <xsl:template match="boolean[@name = 'first_run_signin_setup']" />
    <xsl:template match="boolean[@name = 'first_run_flow']" />
    <xsl:template match="boolean[@name = 'BANDWIDTH_REDUCTION_PROXY_ENABLED']" />
</xsl:stylesheet>