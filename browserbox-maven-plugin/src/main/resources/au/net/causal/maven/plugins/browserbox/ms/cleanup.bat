netsh.exe winhttp reset proxy
if %errorlevel% neq 0 exit /b %errorlevel%

regedit.exe /S c:\scripts\cleanup.reg
if %errorlevel% neq 0 exit /b %errorlevel%

net user Administrator /active:no
if %errorlevel% neq 0 exit /b %errorlevel%
