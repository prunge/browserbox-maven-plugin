param (
    [string]$edgeDriverVersion = "4.15063"
)

$ErrorActionPreference = "Stop"
try
{
    & c:\chocolateyinstaller\tools\chocolateyInstall.ps1
}
catch
{
    exit 12
}

choco install jdk8 -y --execution-timeout 7200
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}
choco install selenium-ie-driver -y --x86 --execution-timeout 7200
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}
choco install selenium-edge-driver --version $edgeDriverVersion -y --execution-timeout 7200
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}
choco install pstools tightvnc -y --execution-timeout 7200
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}

#Install FFMPEG if we are > Vista, otherwise use an old version of ImageMagick (which bundles an older compatible ffmpeg)
#Newest versions of FFMPEG are not compatible with Vista and old versions of ffmpeg choco package are undownloadable
#But ImageMagick has what looks to be a more solid dowwnload site (versions don't seem to disappear)
if ([Environment]::OsVersion.Version -ge (new-object 'Version' 6,1))
{
    #Force version 4.3.1 since newer versions don't have 32-bit version
    #Also ignore dependencies otherwise it also upgrades chocolatey to a version that might not be
    #compatible with the running version of Windows/powershell
    choco install ffmpeg --version=4.3.1 --ignore-dependencies -y --execution-timeout 7200
}
else
{
    choco install imagemagick.tool -y --version 7.0.7.6 --execution-timeout 7200
}
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}
