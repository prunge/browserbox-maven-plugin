# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "${boxDefinition.name}"
  config.vm.communicator = "winrm"
  config.winrm.username = "IEUser"
  config.winrm.password = "Passw0rd!"
  config.winrm.retry_limit = 300
  config.winrm.timeout = 1800
  
  config.vm.define "${box.name}" do |t|
  end

  # Only use this when Vagrant won't detect boot - use other mechanisms such as with VBoxManage to detect if machine is up
  #config.vm.boot_timeout = 30

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 4444, host: 24444, host_ip: "127.0.0.1", id: "selenium"
  config.vm.network "forwarded_port", guest: 5900, host: 15900, host_ip: "127.0.0.1", id: "vnc"
  config.vm.network "forwarded_port", guest: 3389, host: 13389, host_ip: "127.0.0.1", id: "rdp"

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
    config.vm.provider "virtualbox" do |vb|
      vb.name = "${box.name}"
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"

      # Later versions of Vagrant (2.2.1+) need this or else they default to virtio which is not what we want
      # See https://github.com/hashicorp/vagrant/issues/10416
      vb.default_nic_type = nil

      # For Virtualbox 6.1 and later, use vboxsvga graphics (otherwise there may be crashes/bluescreens in guest)
      vbox = VagrantPlugins::ProviderVirtualBox::Driver::Meta.new
      if Gem::Version.new(vbox.version) >= Gem::Version.new('6.1')
        vb.customize ["modifyvm", :id, "--graphicscontroller", "vboxsvga"]
      end

      # Use this only for first-time run, it disables Internet and NAT
      #vb.customize ['modifyvm', :id, '--cableconnected1', 'off']
      #vb.customize ['modifyvm', :id, '--cableconnected1', 'on']
      #vb.customize ['modifyvm', :id, '--vrde', 'on', '--vrdeport', '13389', '--vrdeaddress', '127.0.0.1']
    end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Prevent auto-update of guest additions on Windows VMs
  # Audo-update requires a modern version of Powershell which not all these VMs have
  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  config.vm.provision "initbox", type: "shell", inline: <<-SHELL 
    taskkill /f /im slui.exe    
    dir c:/vagrant
  SHELL
end
