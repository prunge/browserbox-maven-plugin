tell application "Microsoft Remote Desktop"
	activate
	tell application "System Events"
		tell process "Microsoft Remote Desktop"
			set checkText to (get value of attribute "AXDescription" of static text 1 of sheet 1 of window "${connection.host}:${connection.port} " of application process "Microsoft Remote Desktop" of application "System Events")
			if checkText contains "Invalid login credentials." then
				keystroke "${connection.password}"
				key code 36
			end if
		end tell
	end tell
end tell

