# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/focal64"

  config.vm.define "BoxConfigurer" do |t|
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "${selenium.deploy.build.directory}", "/boxbrowser"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
 config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"

    vb.name = "BoxConfigurer"
    vb.customize ['storageattach', :id, '--storagectl', 'SCSI', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', '${windows.disk.file}']

    # Later versions of Vagrant (2.2.1+) need this or else they default to virtio which is not what we want
    # See https://github.com/hashicorp/vagrant/issues/10416
    vb.default_nic_type = nil

    # Serial port was removed from base Vagrant box for focal but is still configured in the kernel
    # Add this back so the whole thing does not hang
    # https://bugs.launchpad.net/cloud-images/+bug/1874453
    vb.customize [ "modifyvm", :id, "--uartmode1", "file", File::NULL ]
 end
  
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    set -e
    apt-get update
    apt-get install -y ntfs-3g chntpw
    sudo mkdir /media/win
    if [ -e /dev/sdc3 ]; then export WINDISK=/dev/sdc3; else export WINDISK=/dev/sdc1; fi;
    sudo mount -o rw,mount,rw -t ntfs $WINDISK /media/win
    cd /media/win/Windows/System32/config/
    set +e
    reged -C -I SYSTEM HKEY_LOCAL_MACHINE\\\\SYSTEM /vagrant/remote.reg
    reged -C -I SYSTEM HKEY_LOCAL_MACHINE\\\\SYSTEM /vagrant/remotedesktop.reg
    reged -C -I SOFTWARE HKEY_LOCAL_MACHINE\\\\SOFTWARE /vagrant/iesetup.reg
    reged -C -I SYSTEM HKEY_LOCAL_MACHINE\\\\SYSTEM /vagrant/sshservice.reg
    reged -C -I SOFTWARE HKEY_LOCAL_MACHINE\\\\SOFTWARE /vagrant/nowindowsupdates.reg
    chntpw -u Administrator SAM < /vagrant/userin.txt
    chntpw -u IEUser SAM < /vagrant/userin2.txt
    set -e
    mkdir /media/win/Scripts
    cp /vagrant/*.ps1 /media/win/Scripts/
    cp /vagrant/ieusersetup.bat /media/win/Scripts/
    cp /vagrant/ieusersetup.reg /media/win/Scripts/
    cp /vagrant/firewallrules.bat /media/win/Scripts/
    cp /vagrant/sslinstaller.bat /media/win/Scripts/
    cp /vagrant/guestadditions.bat /media/win/Scripts/ 
    cp /vagrant/selenium-server.bat /media/win/Scripts/    
    cp /vagrant/cleanup.bat /media/win/Scripts/
    cp /vagrant/cleanup.reg /media/win/Scripts/
    cp /vagrant/iesetup2.reg /media/win/Scripts/
    cp /vagrant/vncsetup.bat /media/win/Scripts/
    cp /vagrant/vnc.reg /media/win/Scripts/
    cp /vagrant/runffmpeg.ps1 /media/win/Scripts/
    cp /vagrant/selenium-server.bat /media/win/Users/IEUser/AppData/Roaming/Microsoft/Windows/Start\\ Menu/Programs/Startup/
    mkdir /media/win/Scripts/edgedriver
    mkdir /media/win/Scripts/edgedriver/tools
    cp /vagrant/edgedriver/browserboxedge.nuspec /media/win/Scripts/edgedriver/
    cp /vagrant/edgedriver/tools/chocolateyinstall.ps1 /media/win/Scripts/edgedriver/tools/
    mkdir /media/win/chocolateyinstaller
    cp -r /vagrant/chocolateyinstaller /media/win
    mkdir /media/win/certs
    cp -r /vagrant/certs /media/win
    mkdir /media/win/windowsupdates
    cp -r /vagrant/windowsupdates /media/win
    mkdir /media/win/selenium-server
    cd /media/win/selenium-server
    tar xf /boxbrowser/*-selenium-server.tar.gz
    rm -rf /media/win/Program\\ Files/Oracle
    rm /media/win/Windows/System32/VBox*
  SHELL

end
