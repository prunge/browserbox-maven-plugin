﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$toolsLocation = Get-ToolsLocation
$seleniumDir = "$toolsLocation\selenium"
$driverPath = "$seleniumDir\MicrosoftWebDriver.exe"

$parameters = Get-PackageParameters

$packageArgs = @{
  packageName  = 'selenium-edge-driver'
  url          = 'https://download.microsoft.com/download/C/0/7/C07EBF21-5305-4EC8-83B1-A6FCC8F93F45/MicrosoftWebDriver.exe'
  checksum     = 'a02b9daed04ca254a103071069f4380acdecab50632b7f59e9af5a6372a94a952605dd489e991d20caffce3089ad9c5591edd48a364c69ac11f0d026ad31b72f'
  checksumType = 'sha512'
  fileFullPath = $driverPath
}
Get-ChocolateyWebFile @packageArgs

Uninstall-BinFile -Name 'MicrosoftWebDriver'
If ($parameters['SkipShim'] -ne 'true') {
  Install-BinFile -Name 'MicrosoftWebDriver' -Path $driverPath
}

$menuPrograms = [environment]::GetFolderPath([environment+specialfolder]::Programs)
$shortcutArgs = @{
  shortcutFilePath = "$menuPrograms\Selenium\Selenium Edge Driver.lnk"
  targetPath       = $driverPath
  iconLocation     = "$toolsDir\icon.ico"
}
Install-ChocolateyShortcut @shortcutArgs