$ErrorActionPreference = "Stop"

if (Get-Command "Set-NetConnectionProfile" -errorAction SilentlyContinue)
{
    Write-Output "Using modern network configuration"
    Set-NetConnectionProfile -NetworkCategory Private
    Enable-PSRemoting -Force -SkipNetworkProfileCheck
}
else
{
    Write-Output "Using fallback network configuration"
    $NLMType = [Type]::GetTypeFromCLSID('DCB00C01-570F-4A9B-8D69-199FDBA5723B')
    $NetworkListManager = [Activator]::CreateInstance($NLMType)
    $Networks = $NetworkListManager.GetNetworks(1)
    foreach ($Network in $Networks)
    {
        $Network.SetCategory(1); #1=private
    }
    Enable-PSRemoting -Force 
}

sc.exe config WinRM start= auto
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}

netsh.exe winhttp set proxy galah.galah.galah:8888
$native_call_success = $?
if (-not $native_call_success)
{
    exit 12
}
