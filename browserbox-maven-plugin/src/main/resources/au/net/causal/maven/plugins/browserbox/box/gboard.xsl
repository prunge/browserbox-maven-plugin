<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- Copy everything -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <!-- Copy and augment root map element -->
    <xsl:template match="/map">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />

            <!-- Add our elements to the end -->
            <boolean name="pref_key_contacts_suggestion_notice_posted" value="true" />
            <int name="show_privacy_notice" value="1" />
            <boolean name="import_user_contacts" value="false" />
            <boolean name="enable_share_snippets" value="false" />
        </xsl:copy>
    </xsl:template>

    <!-- Do not copy the following elements, we are replacing them -->
    <xsl:template match="boolean[@name = 'pref_key_contacts_suggestion_notice_posted']" />
    <xsl:template match="int[@name = 'show_privacy_notice']" />
    <xsl:template match="boolean[@name = 'import_user_contacts']" />
    <xsl:template match="boolean[@name = 'enable_share_snippets']" />
</xsl:stylesheet>